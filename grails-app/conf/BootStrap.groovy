import com.hrm.Role
import com.hrm.UserInfor
import com.hrm.Users
import com.hrm.UsersRole
import com.hrm.sys.Menu

import java.text.SimpleDateFormat

class BootStrap {

  def init = { servletContext ->
    if (Menu.count() == 0) {
      println("===================== CREATE FIRST MENU =======================")
      def menu = new Menu();
      menu.setName('Admin')
      menu.setDescription('Admin')
      menu.setIcon('fa-briefcase')
      menu.save(flush: true)
      def menuMenu = new Menu();
      menuMenu.setName('Menu')
      menuMenu.setDescription('Menu')
      menuMenu.setParent('1')
      menuMenu.setUrl('menu/index')
      menuMenu.setPosition(0)
      menuMenu.setIcon('fa-map-o')
      menuMenu.save(flush: true);
      println("=================================================================")
      println("===================== CREATE FIRST ACCOUNT=======================")
      def adminRole = new Role('ROLE_ADMIN').save()
      def userRole = new Role('ROLE_USER').save()
      def testUser = new Users('admin', 'admin')
      testUser.setUserInfor(UserInfor.findById(4))
      testUser.save(flush: true)
      println("Create ==============> login User ")
      UsersRole.create testUser, adminRole, true
      println("Create Menu ==============> url Menu ")
      println("=================================================================")
    }

  }
  def destroy = {
  }
}
