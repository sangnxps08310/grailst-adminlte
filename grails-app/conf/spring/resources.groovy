import springsecurity.UsersDetailsService

// Place your Spring DSL code here

beans = {
    userDetailsService(UsersDetailsService);
}
