package com.hrm

import grails.plugin.springsecurity.annotation.Secured

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
@Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
class StaffInforController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        println params
        def listUserInstance = StaffInfor.list(params);
        if (params.search != null) {
            listUserInstance = StaffInfor.findAllByNameIlikeOrMailIlike('%'+params.name+'%', '%'+params.mail+'%')
        }
        println("======> "+ listUserInstance)
        respond listUserInstance, model: [userInforInstanceCount: UserInfor.count(), 'params': params[]]
    }

    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def show(StaffInfor staffInforInstance) {
        respond staffInforInstance
    }

    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def create() {
        respond new StaffInfor(params)
    }


    @Transactional
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def save(StaffInfor staffInforInstance) {
        if (staffInforInstance == null) {
            notFound()
            return
        }

        if (staffInforInstance.hasErrors()) {
            respond staffInforInstance.errors, view: 'create'
            return
        }

        staffInforInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'staffInfor.label', default: 'StaffInfor'), staffInforInstance.id])
                redirect staffInforInstance
            }
            '*' { respond staffInforInstance, [status: CREATED] }
        }
    }

    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def edit(StaffInfor staffInforInstance) {
        respond staffInforInstance
    }

    @Transactional
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def update(StaffInfor staffInforInstance) {
        if (staffInforInstance == null) {
            notFound()
            return
        }

        if (staffInforInstance.hasErrors()) {
            respond staffInforInstance.errors, view: 'edit'
            return
        }

        staffInforInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'StaffInfor.label', default: 'StaffInfor'), staffInforInstance.id])
                redirect staffInforInstance
            }
            '*' { respond staffInforInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def delete(StaffInfor staffInforInstance) {

        if (staffInforInstance == null) {
            notFound()
            return
        }

        staffInforInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'StaffInfor.label', default: 'StaffInfor'), staffInforInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }


    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'staffInfor.label', default: 'StaffInfor'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
