package com.hrm.sys

import com.google.gson.Gson
import com.hrm.Depart
import com.hrm.DepartUser
import com.hrm.Requests
import com.hrm.UserInfor
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
class IndexController {
  def springSecurityService
  def rollPayHistoryService

  @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
  def index() {
    def mapModel = [:]
    ArrayList<String> currentRole = new ArrayList<String>(springSecurityService.authentication.authorities)
    println "Role Name ========> " + currentRole
    if (currentRole.toString().contains('ROLE_ADMIN') || currentRole.toString().contains('ROLE_MANAGER')) {
      UserInfor loginUser = springSecurityService.getCurrentUser().userInfor
      def currentDepart = DepartUser.findByUserInfo(loginUser);
      if (currentDepart) {
        def departInstance = Depart.findById(currentDepart.depart.id)
        def listWaitingRequest = Requests.findAllByDepartAndStatus(departInstance, 'WAITING');
        mapModel['listWaitingRequest'] = listWaitingRequest
        mapModel['roomMaster'] = DepartUser.findByDepartAndIs_room_master(currentDepart.depart, true).userInfo;
      }
    }
    render view: 'general', model: mapModel
  }

  @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
  def test() {
    response.setContentType("application/json; charset=utf-8");
    def listSalary = rollPayHistoryService.calcRolPay(4,2020);
    render(new Gson().toJson(listSalary));
  }
  @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
  def export() {
  rollPayHistoryService.exportExcelFile();
  }
}
