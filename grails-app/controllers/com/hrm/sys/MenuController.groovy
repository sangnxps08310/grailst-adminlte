package com.hrm.sys

import grails.plugin.springsecurity.annotation.Secured
import com.hrm.MenuService

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
@Secured('ROLE_ADMIN')
class MenuController {

  static allowedMethods = [save: "POST", update: "PUT"]
  MenuService menuService

  @Secured('ROLE_ADMIN')
  def index(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    System.out.println("params ===========> "+ params)
    def menuInstance =Menu.list(params);
    if(params.search!=null){
      menuInstance =Menu.findAllByNameIlike('%'+params.name+'%',params);
    }
    respond menuInstance, model: [menuInstanceCount: Menu.count()]
  }

  @Secured('ROLE_ADMIN')
  def show(Menu menuInstance) {
    respond menuInstance
  }

  @Secured('ROLE_ADMIN')
  def create() {
    respond new Menu(params), model: [FASData: menuService.getFASData()]
  }

  @Secured('ROLE_ADMIN')
  @Transactional
  def save(Menu menuInstance) {
    if (menuInstance == null) {
      notFound()
      return
    }

    if (menuInstance.hasErrors()) {
      respond menuInstance.errors, view: 'create'
      return
    }
    if (Menu.findByName(menuInstance.name)) {
      flash.message = message(code: 'default.avaiable.message', args: [menuInstance.name])
      response menuInstance, view: 'create'
    }
    String icon = "fa-" + menuInstance.icon;
    menuInstance.setIcon(icon)

    menuInstance.save flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.created.message', args: [message(code: 'menu.label', default: 'Menu'), menuInstance.name])
        redirect menuInstance
      }
      '*' { respond menuInstance, [status: CREATED] }
    }
  }

  @Secured('ROLE_ADMIN')
  def edit(Menu menuInstance) {
    respond menuInstance, model: [FASData: menuService.getFASData()]
  }

  @Secured('ROLE_ADMIN')
  @Transactional
  def update(Menu menuInstance) {
    if (menuInstance == null) {
      notFound()
      return
    }

    if (menuInstance.hasErrors()) {
      respond menuInstance.errors, view: 'edit'
      return
    }
    String icon = "fa-" + menuInstance.icon;
    menuInstance.setIcon(icon)

    menuInstance.save flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.updated.message', args: [message(code: 'Menu.label', default: 'Menu'), menuInstance.name])
        redirect menuInstance
      }
      '*' { respond menuInstance, [status: OK] }
    }
  }

  @Secured('ROLE_ADMIN')
  @Transactional
  def delete(Menu menuInstance) {

    if (menuInstance == null) {
      notFound()
      return
    }
    menuInstance.delete flush: true
    flash.message = message(code: 'default.deleted.message', args: [message(code: 'Menu.label', default: 'Menu'), menuInstance.name])
    flash.type = 'success'
    redirect action: "index"
  }

  protected void notFound() {
    request.withFormat {
      flash.message = message(code: 'default.not.found.message', args: [message(code: 'menu.label', default: 'Menu'), params.id])
      flash.type = 'danger'
      redirect action: "index"
    }
  }
}
