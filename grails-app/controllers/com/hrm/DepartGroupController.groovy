package com.hrm


import grails.plugin.springsecurity.annotation.Secured

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Secured(['ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
@Transactional(readOnly = true)
class DepartGroupController {
  /* ***************************************************
     *              READ COMMAND FIRST                 *
     ***************************************************/

  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

  @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
  def index(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    def listDepartGroupInstance
    if (params.search) {
      listDepartGroupInstance = DepartGroup.findAllByIdLike("%" + params.id + "%")
    } else {
      listDepartGroupInstance = DepartGroup.list(params)
    }
    respond listDepartGroupInstance, model: [departGroupInstanceCount: DepartGroup.count()]
  }

  @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
  def show(DepartGroup departGroupInstance) {
    def leaderInfo = DepartUser.findByGroupAndIs_leader(departGroupInstance, true)
    def listUserInfor = DepartUser.findAllByGroupAndIs_leaderAndIs_room_master(departGroupInstance, false, false)
    respond departGroupInstance, model: [leaderInfo: leaderInfo,listUserInfor:listUserInfor]
  }

  @Secured(['ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
  def create() {
    def departGroupInstance = new DepartGroup(params)
    departGroupInstance.setDepart(Depart.findById(Long.parseLong(params.id + '')))
    respond departGroupInstance
  }

  @Secured(['ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
  @Transactional
  def save(DepartGroup departGroupInstance) {
    if (departGroupInstance == null) {
      notFound()
      return
    }
    def tempDepartGroupInstance = new DepartGroup();
    tempDepartGroupInstance.setDescription(departGroupInstance.description)
    tempDepartGroupInstance.setName(departGroupInstance.name)

    tempDepartGroupInstance.setDepart(Depart.findById(Long.parseLong(params.departId)))
    println "params =======> " + params

    println "departUser =======> " + departGroupInstance
    /* ***************************************************
       *              READ COMMAND FIRST                 *
       ***************************************************/
    // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
    if (tempDepartGroupInstance.hasErrors()) {
      respond tempDepartGroupInstance.errors, view: 'create'
      return
    }

    tempDepartGroupInstance.save flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.created.message', args: [message(code: 'departGroup.label', default: 'DepartGroup'), tempDepartGroupInstance.name])
        redirect controller: 'depart', action: 'show', resource: tempDepartGroupInstance.depart
      }
      '*' { redirect controller: 'depart', action: 'show', resource: tempDepartGroupInstance.depart, [status: CREATED] }
    }
  }

  @Secured(['ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
  def edit(DepartGroup departGroupInstance) {
    respond departGroupInstance
  }

  @Secured(['ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
  @Transactional
  def update(DepartGroup departGroupInstance) {
    if (departGroupInstance == null) {
      notFound()
      return
    }

    /* ***************************************************
       *              READ COMMAND FIRST                 *
       ***************************************************/
    // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
    if (departGroupInstance.hasErrors()) {
      respond departGroupInstance.errors, view: 'edit'
      return
    }

    departGroupInstance.save flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.updated.message', args: [message(code: 'DepartGroup.label', default: 'DepartGroup'), departGroupInstance.id])
        redirect departGroupInstance
      }
      '*' { respond departGroupInstance, [status: OK] }
    }
  }

  @Secured('ROLE_ADMIN,ROLE_MANAGER,ROLE_SUPERVISOR')
  @Transactional
  def delete(DepartGroup departGroupInstance) {

    if (departGroupInstance == null) {
      notFound()
      return
    }

    departGroupInstance.delete flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.deleted.message', args: [message(code: 'DepartGroup.label', default: 'DepartGroup'), departGroupInstance.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NO_CONTENT }
    }
  }

  protected void notFound() {
    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'departGroup.label', default: 'DepartGroup'), params.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NOT_FOUND }
    }
  }
}
