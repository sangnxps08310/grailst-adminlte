package com.hrm




import grails.plugin.springsecurity.annotation.Secured

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
@Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
@Transactional(readOnly = true)
class ProvinceController {
    /* ***************************************************
       *              READ COMMAND FIRST                 *
       ***************************************************/

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def listProvinceInstance
        if(params.search){
            listProvinceInstance = Province.findAllByIdLike("%"+params.id+"%")
        }else {
            listProvinceInstance = Province.list(params)
        }
        respond listProvinceInstance, model:[provinceInstanceCount: Province.count()]
    }
    @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
    def show(Province provinceInstance) {
        respond provinceInstance
    }
    @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
    def create() {
        respond new Province(params)
    }
    @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
    @Transactional
    def save(Province provinceInstance) {
        if (provinceInstance == null) {
            notFound()
            return
        }
    /* ***************************************************
       *              READ COMMAND FIRST                 *
       ***************************************************/
    // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
        if (provinceInstance.hasErrors()) {
            respond provinceInstance.errors, view:'create'
            return
        }

        provinceInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'province.label', default: 'Province'), provinceInstance.id])
                redirect provinceInstance
            }
            '*' { respond provinceInstance, [status: CREATED] }
        }
    }
    @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
    def edit(Province provinceInstance) {
        respond provinceInstance
    }
    @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
    @Transactional
    def update(Province provinceInstance) {
        if (provinceInstance == null) {
            notFound()
            return
        }

            /* ***************************************************
               *              READ COMMAND FIRST                 *
               ***************************************************/
            // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
        if (provinceInstance.hasErrors()) {
            respond provinceInstance.errors, view:'edit'
            return
        }

        provinceInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Province.label', default: 'Province'), provinceInstance.id])
                redirect provinceInstance
            }
            '*'{ respond provinceInstance, [status: OK] }
        }
    }
    @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
    @Transactional
    def delete(Province provinceInstance) {

        if (provinceInstance == null) {
            notFound()
            return
        }

        provinceInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Province.label', default: 'Province'), provinceInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'province.label', default: 'Province'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
