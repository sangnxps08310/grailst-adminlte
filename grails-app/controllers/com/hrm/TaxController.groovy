package com.hrm




import grails.plugin.springsecurity.annotation.Secured

import java.text.SimpleDateFormat

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
@Secured(['ROLE_ADMIN','ROLE_MANAGER'])
@Transactional(readOnly = true)
class TaxController {
    /* ***************************************************
       *              READ COMMAND FIRST // Ngoc Anh
       ***************************************************/

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    TaxService taxService
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        params.offset = params.offset ? params.offset : 0
        params.firstResult = params.offset * params.max
        def listTaxInstance = Tax.list(params)
        if(params.search != null){
            listTaxInstance = taxService.find(params)
        }
        respond listTaxInstance, model:[taxInstanceCount: Tax.count()]
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def show(Tax taxInstance) {
        respond taxInstance
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def create() {
        respond new Tax(params)
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    @Transactional
    def save(Tax taxInstance) {
        if (taxInstance == null) {
            notFound()
            return
        }
        SimpleDateFormat format = new SimpleDateFormat('yyyy-MM-dd')
        taxInstance.setCreateDate(new Date())
        def tempTaxInstance = new Tax()
        tempTaxInstance.setEffectiveDate(format.parse(params.effectiveDate))
        tempTaxInstance.setNameTax(taxInstance.nameTax)
        String tempPercent = (Double.parseDouble(taxInstance.percent)/100)+""
        tempTaxInstance.setPercent(tempPercent)

        if (tempTaxInstance.hasErrors()) {
            respond tempTaxInstance.errors, view:'create'
            return
        }

        tempTaxInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'tax.label', default: 'Tax'), tempTaxInstance.id])
                redirect tempTaxInstance
            }
            '*' { respond tempTaxInstance, [status: CREATED] }
        }
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def edit(Tax taxInstance) {

        respond taxInstance
    }
        @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    @Transactional
    def update(Tax taxInstance) {
        if (taxInstance == null) {
            notFound()
            return
        }

            /* ***************************************************
               *              READ COMMAND FIRST                 *
               ***************************************************/
            // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
        if (taxInstance.hasErrors()) {
            respond taxInstance.errors, view:'edit'
            return
        }

        taxInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Tax.label', default: 'Tax'), taxInstance.id])
                redirect taxInstance
            }
            '*'{ respond taxInstance, [status: OK] }
        }
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    @Transactional
    def delete(Tax taxInstance) {

        if (taxInstance == null) {
            notFound()
            return
        }

        taxInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Tax.label', default: 'Tax'), taxInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'tax.label', default: 'Tax'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }



}
