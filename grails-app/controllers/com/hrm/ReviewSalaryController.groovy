package com.hrm

import com.google.gson.Gson
import grails.plugin.springsecurity.annotation.Secured

import java.text.SimpleDateFormat

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
@Secured(['ROLE_ADMIN','ROLE_MANAGER'])
@Transactional(readOnly = true)
class ReviewSalaryController {
    /* ***************************************************
       *              READ COMMAND FIRST // Ngoc Anh     *
       ***************************************************/

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    ReviewSalaryService reviewSalaryService
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        params.offset = params.offset ? params.offset : 0
        params.firstResult = params.offset * params.max

        def listReviewSalaryInstance = ReviewSalary.list(params)
        if (params.search != null) {
            listReviewSalaryInstance = reviewSalaryService.find(params)
        }

        respond listReviewSalaryInstance, model:[reviewSalaryInstanceCount: ReviewSalary.count(), 'params': params]
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def show(ReviewSalary reviewSalaryInstance) {
        respond reviewSalaryInstance
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def create() {
        respond new ReviewSalary(params)
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    @Transactional
    def save(ReviewSalary reviewSalaryInstance) {
        if (reviewSalaryInstance == null) {
            notFound()
            return
        }

        SimpleDateFormat format = new SimpleDateFormat('yyyy-MM-dd')
        reviewSalaryInstance.setCreateDate(new Date())
        def tempReview = new ReviewSalary()
        tempReview.setEffectiveDate(format.parse(params.effectiveDate))
        tempReview.setUserId(reviewSalaryInstance.userId)
        tempReview.setRankSalary(reviewSalaryInstance.rankSalary)
        tempReview.setEntranceSalary(reviewSalaryInstance.entranceSalary)
        tempReview.setNewSalary(reviewSalaryInstance.newSalary)
        if (tempReview.hasErrors()) {
            respond tempReview.errors, view:'create'
            return
        }

       // tempReview.save flush:true
        if (tempReview.save(flush:true)){}
        else {
            flash.message = 'Create failed'
            respond tempReview, view: 'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'reviewSalary.label', default: 'ReviewSalary'), tempReview.id])
                redirect tempReview
            }
            '*' { respond tempReview, [status: CREATED] }
        }
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def edit(ReviewSalary reviewSalaryInstance) {
        respond reviewSalaryInstance
    }
        @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    @Transactional
    def update(ReviewSalary reviewSalaryInstance) {
        if (reviewSalaryInstance == null) {
            notFound()
            return
        }
            /* ***************************************************
               *              READ COMMAND FIRST                 *
               ***************************************************/
            // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
        if (reviewSalaryInstance.hasErrors()) {
            respond reviewSalaryInstance.errors, view:'edit'
            return
        }

        reviewSalaryInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'ReviewSalary.label', default: 'ReviewSalary'), reviewSalaryInstance.id])
                redirect reviewSalaryInstance
            }
            '*'{ respond reviewSalaryInstance, [status: OK] }
        }
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    @Transactional
    def delete(ReviewSalary reviewSalaryInstance) {

        if (reviewSalaryInstance == null) {
            notFound()
            return
        }

        reviewSalaryInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'ReviewSalary.label', default: 'ReviewSalary'), reviewSalaryInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'reviewSalary.label', default: 'ReviewSalary'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    @Secured('permitAll')
    def fillEntranceByUserInfor() {
        response.setContentType("application/json;charset=utf-8")
        def userInfo = UserInfor.findById(Long.parseLong(params.userId + ""))
        render(userInfo.entranceSalary)
    }
}
