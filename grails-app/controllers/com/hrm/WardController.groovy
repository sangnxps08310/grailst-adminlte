package com.hrm

import com.google.gson.Gson
import grails.plugin.springsecurity.annotation.Secured

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Secured('permitAll')
@Transactional(readOnly = true)
class WardController {
  /* ***************************************************
     *              READ COMMAND FIRST                 *
     ***************************************************/
  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

  @Secured('permitAll')
  def index(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    def listWardInstance
    if (params.search) {
      listWardInstance = Ward.findAllByIdLike("%" + params.id + "%")
    } else {
      listWardInstance = Ward.list(params)
    }
    respond listWardInstance, model: [wardInstanceCount: Ward.count()]
  }

  @Secured('permitAll')
  def show(Ward wardInstance) {
    respond wardInstance
  }

  @Secured('permitAll')
  def create() {
    respond new Ward(params)
  }

  @Secured('permitAll')
  @Transactional
  def save(Ward wardInstance) {
    if (wardInstance == null) {
      notFound()
      return
    }
    /* ***************************************************
       *              READ COMMAND FIRST                 *
       ***************************************************/
    // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
    if (wardInstance.hasErrors()) {
      respond wardInstance.errors, view: 'create'
      return
    }

    wardInstance.save flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.created.message', args: [message(code: 'ward.label', default: 'Ward'), wardInstance.id])
        redirect wardInstance
      }
      '*' { respond wardInstance, [status: CREATED] }
    }
  }

  @Secured('permitAll')
  def edit(Ward wardInstance) {
    respond wardInstance
  }

  @Secured('permitAll')
  @Transactional
  def update(Ward wardInstance) {
    if (wardInstance == null) {
      notFound()
      return
    }

    /* ***************************************************
       *              READ COMMAND FIRST                 *
       ***************************************************/
    // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
    if (wardInstance.hasErrors()) {
      respond wardInstance.errors, view: 'edit'
      return
    }

    wardInstance.save flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.updated.message', args: [message(code: 'Ward.label', default: 'Ward'), wardInstance.id])
        redirect wardInstance
      }
      '*' { respond wardInstance, [status: OK] }
    }
  }

  @Secured('permitAll')
  @Transactional
  def delete(Ward wardInstance) {

    if (wardInstance == null) {
      notFound()
      return
    }

    wardInstance.delete flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.deleted.message', args: [message(code: 'Ward.label', default: 'Ward'), wardInstance.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NO_CONTENT }
    }
  }

  protected void notFound() {
    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'ward.label', default: 'Ward'), params.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NOT_FOUND }
    }
  }

  @Secured('permitAll')
  def getWard() {
    response.setContentType("application/json;charset=utf-8")
    def requestParams = params;
    def resonseParams = []
    println "parmas" + params
    int provinceCode = requestParams['provinceCode'] ? Integer.parseInt(requestParams['provinceCode']) : 0
    int districtCode = requestParams['districtCode'] ? Integer.parseInt(requestParams['districtCode']) : 0
    def wardInstance = Ward.findAllByDistrict_codeAndProvince_codeAndNameIlike(districtCode, provinceCode, '%' + (params.name ? params.name : '') + '%')
    wardInstance.each { ward ->
      def mapWard = [:]
      mapWard['id'] = ward.ward_code
      mapWard['text'] = ward.name
      resonseParams << mapWard
    }
    println "Result" + new Gson().toJson(resonseParams)
    render(new Gson().toJson(resonseParams))
  }
}
