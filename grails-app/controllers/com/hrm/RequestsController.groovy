package com.hrm


import grails.plugin.springsecurity.annotation.Secured
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Secured('permitAll')
@Transactional(readOnly = true)
class RequestsController {
  /* ***************************************************
     *              READ COMMAND FIRST                 *
     ***************************************************/
  def requestsService
  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

  @Secured('permitAll')
  def index(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    def listRequestsInstance
    if (params.search) {
      listRequestsInstance = requestsService.search(params)
    } else {
      listRequestsInstance = Requests.list(params)
    }
    respond listRequestsInstance, model: [requestsInstanceCount: Requests.count(), params: params]
  }

  @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
  def show(Requests requestsInstance) {
    respond requestsInstance
  }

  @Secured('permitAll')
  def create() {
    def requestInstance = new Requests(params)
    if (params.userId) {
      requestInstance.setDepart(Depart.findById(Long.parseLong(params.departId)))
      requestInstance.setStaff(UserInfor.findById(Long.parseLong(params.userId)))
    }
    respond requestInstance
  }

  @Secured('permitAll')
  @Transactional
  def save(Requests requestsInstance) {
    if (requestsInstance == null) {
      notFound()
      return
    }
    Requests tempRequestInstance = requestsService.loadRequestData(params, requestsInstance)
    tempRequestInstance.setStatus('WAITING')
    /* ***************************************************
       *              READ COMMAND FIRST                 *
       ***************************************************/
    // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity

    if (tempRequestInstance.hasErrors()) {
      respond tempRequestInstance.errors, view: 'create'
      return
    }

    tempRequestInstance.save flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.created.message', args: [message(code: 'requests.label', default: 'Requests'), tempRequestInstance.id])
        redirect tempRequestInstance
      }
      '*' { respond tempRequestInstance, [status: CREATED] }
    }
  }

  @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
  def edit(Requests requestsInstance) {
    respond requestsInstance
  }

  @Secured('permitAll')
  @Transactional
  def update(Requests requestsInstance) {
    if (requestsInstance == null) {
      notFound()
      return
    }
    Requests tempRequestInstance = requestsService.loadRequestData(params, requestsInstance)
    tempRequestInstance.setStatus('WAITING')
    tempRequestInstance.setId(requestsInstance.id)
    /* ***************************************************
       *              READ COMMAND FIRST                 *
       ***************************************************/
    // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
    if (requestsInstance.hasErrors()) {
      respond requestsInstance.errors, view: 'edit'
      return
    }

    requestsInstance.save flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.updated.message', args: [message(code: 'Requests.label', default: 'Requests'), requestsInstance.id])
        redirect requestsInstance
      }
      '*' { respond requestsInstance, [status: OK] }
    }
  }

  @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
  @Transactional
  def delete(Requests requestsInstance) {

    if (requestsInstance == null) {
      notFound()
      return
    }

    requestsInstance.delete flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.deleted.message', args: [message(code: 'Requests.label', default: 'Requests'), requestsInstance.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NO_CONTENT }
    }
  }

  protected void notFound() {
    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'requests.label', default: 'Requests'), params.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NOT_FOUND }
    }
  }

  @Secured(['ROLE_MANAGER', 'ROLE_ADMIN'])
  @Transactional
  def accept() {
    long id = Long.parseLong(params.id);
    Requests requestInstance = Requests.findById(id)
    requestInstance.setStatus('ACCEPTED')
    requestInstance.setAcceptedAt(new Date())
    requestInstance.save(flush: true)
    redirect controller: 'index'
  }

  @Secured(['ROLE_MANAGER', 'ROLE_ADMIN'])
  @Transactional
  def denied() {
    long id = Long.parseLong(params.id);
    def requestInstance = Requests.findById(id)
    requestInstance.setStatus('DENIED')
    requestInstance.save(flush: true)
    redirect controller: 'index'
  }
}
