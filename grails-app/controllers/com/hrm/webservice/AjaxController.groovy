package com.hrm.webservice

import com.hrm.Ward
import grails.rest.RestfulController
import org.springframework.security.access.annotation.Secured;


@Secured('permitAll')
class AjaxController extends RestfulController {

  static responseFormats = ['json', 'xml']

  @Secured('permitAll')
  def index() {
    response.setContentType("application/json;charset=utf-8")
    return [
      'code'  : 200,
      'status': 'OK'
    ];
  }

  @Secured('permitAll')
  def getWard() {
    response.setContentType("application/json;charset=utf-8")
    def requestParams = request.getParameterMap();
    int districtCode = Integer.parseInt(requestParams['districtCode']);
    int provinceCode = Integer.parseInt(requestParams['provinceCode'])
    def wardInstance = Ward.findAllByDistrict_codeAndProvince_code(districtCode, provinceCode)
    response wardInstance
  }

  @Secured('permitAll')
  def getDistrict() {
    response.setContentType("application/json;charset=utf-8")
  }
}
