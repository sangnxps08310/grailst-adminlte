package com.hrm.webservice

import com.google.gson.Gson
import com.hrm.UserInfor
import com.hrm.Users
import com.hrm.common.HttpUtils
import com.hrm.sys.ChangePassLog
import grails.plugin.mail.MailService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper

import javax.mail.internet.MimeMessage
import java.sql.Date


class WebserviceController {
  static responseFormats = ['json', 'xml']
  def springSecurityService

  def usersService

  def getUserInfo() {
    def response = [:]
    response['code'] = HttpUtils.HTTP_OK;
    render new Gson().toJson(response);
  }

  def testExportExcel() {

  }

  def testMail() {
//    MimeMessage mail = mailer.createMimeMessage();
//    MimeMessageHelper mailHelper = new MimeMessageHelper(mail);
//    mailHelper.setFrom('sangnx6297@gmai.com', 'HRM SYSTEM')
//    mailHelper.setTo('sangnxps08310@fpt.edu.vn');
//    mailHelper.setSubject('TEST MAIL HRM')
//    mailHelper.setText('Hello World !')
//    mailer.send(mail)
    sendMail {
      to "sangnxps08310@fpt.edu.vn"
      subject "This is a test mail"
      body "Hello, This is a test mail, how are you?"
    }
  }

  def checkAutoLogin() {
    response.setContentType('application/json;charset=utf-8')
    def responseData = [:]
    try {
      long id = Long.parseLong(params.id);
      def changePassLogInstance = ChangePassLog.findById(id);
      if (changePassLogInstance) {
        println "loading ....."
        if (changePassLogInstance.status == 'DONE') {
          responseData['status'] = HttpUtils.HTTP_OK
          responseData['auth_key'] = changePassLogInstance.auth_key
        } else if (changePassLogInstance.status == 'FAILED') {
          responseData['status'] = 0
        } else {
          responseData['status'] = 1
        }
      } else {
        responseData['status'] = HttpUtils.HTTP_INTERNAL_ERROR
      }
    } catch (Exception e) {
      e.printStackTrace();
      responseData['status'] = HttpUtils.HTTP_INTERNAL_ERROR
    }

    render(new Gson().toJson(responseData))
  }

  def sendForgotMail() {
    response.setContentType('application/json;charset=utf-8')
    def usersInstance = Users.findByUsername(params.usn)
    def responseData = [:]
    try {
      println "params .." + params
      def changePassLogInstances = ChangePassLog.findAllByCreatedByAndCreatedAt(usersInstance.id, new Date(new java.util.Date().getTime()))
      if (changePassLogInstances.size() <= 3) {
        if (usersInstance) {
          println "check mail...."
          String email = usersInstance.userInfor.email;
          println "check mailing to $usersInstance.id...."
          println "check mailing to $usersInstance.userInfor.name...."
          String plaintext = usersInstance.userInfor.name + new java.util.Date().getTime()
          String token = usersService.hasPlaintext(plaintext);
          def changePassLogInstance = new ChangePassLog();
          changePassLogInstance.setStatus('NEW');
          changePassLogInstance.setCreatedBy(usersInstance.id)
          changePassLogInstance.setSubmitCounter(1);
          changePassLogInstance.setToken(token)
          changePassLogInstance.save(flush: true)
          sendMail {
            to email
            subject "HRM: FORGOT PASSWORD"
            html g.render(template: '/mail/mail-forgot-password', model: [name: usersInstance.userInfor.name, token: token])
          }
          responseData['status'] = HttpUtils.HTTP_OK;
          responseData['access_id'] = changePassLogInstance.id;
        } else {
          println "Mailing failed ...."
          responseData['status'] = HttpUtils.HTTP_INTERNAL_ERROR;
        }
      } else {
        responseData['status'] = 500;
      }
    } catch (Exception e) {
      e.printStackTrace()
      responseData['status'] = HttpUtils.HTTP_INTERNAL_ERROR;
    }

    render(new Gson().toJson(responseData))
  }

  def checkChangePass() {
    String token = params.token
    def changePassLogInstance = ChangePassLog.findByToken(token)
    if (changePassLogInstance) {
      if (changePassLogInstance.submitCounter == 1) {
        String plaintext = Users.findById(changePassLogInstance.createdBy).userInfor.name + new java.util.Date().getTime()
        String newToken = usersService.hasPlaintext(plaintext);
        changePassLogInstance.setStatus('WAITING')
        changePassLogInstance.setToken(newToken)
        changePassLogInstance.save(flush: true)
        render(view: 'change-pass', model: [token: newToken])
        return;
      }
      changePassLogInstance.setStatus('FAILED')
      changePassLogInstance.save(flush: true)
    }
    render('session expired !')
    return
  }

  def changePass() {
    String password = params.password
    String confirm_password = params.confirm_password
    String token = params.token
    def changePassLogInstance = ChangePassLog.findByToken(token)
    try {
      if (changePassLogInstance) {
        if (password.matches(confirm_password)) {
          String plaintext = Users.findById(changePassLogInstance.createdBy).userInfor.name + new java.util.Date().getTime()
          String newToken = usersService.hasPlaintext(plaintext);
          def userInstance = Users.findById(changePassLogInstance.createdBy);
          userInstance.setPassword(password)
          userInstance.save(flush: true)
          changePassLogInstance.setStatus('DONE');
          changePassLogInstance.setToken(newToken)
          changePassLogInstance.setAuth_key(password);
          changePassLogInstance.save(flush: true)
          render('<h3>Your password is updated !</h3>')
          return;
        } else {
          flash.message = "Your password is not matched with confirm password"
        }
      } else {
        changePassLogInstance.setStatus('FAILED');
        changePassLogInstance.save(flush: true)
        render("This session is expired!")
        return;
      }
    } catch (Exception e) {
      flash.message = 'Something went wrong!'
      e.printStackTrace();
    }
    changePassLogInstance.setStatus('FAILED')
    changePassLogInstance.save(flush: true)
    render(view: '_change-pass-form', model: [params: params, token: token])
    return
  }
}
