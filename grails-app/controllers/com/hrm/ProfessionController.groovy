package com.hrm




import grails.plugin.springsecurity.annotation.Secured

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
@Secured(['ROLE_ADMIN','ROLE_MANAGER'])
@Transactional(readOnly = true)
class ProfessionController {
    /* ***************************************************
       *              READ COMMAND FIRST                 *
       ***************************************************/

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    ProfessionService  professionService

    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        params.offset = params.offset ? params.offset : 0
        params.firstResult = params.offset * params.max

        def listProfessionInstance = Profession.list(params)
        if (params.search != null) {
            listProfessionInstance = professionService.find(params)
        }

        respond listProfessionInstance, model:[professionInstanceCount: Profession.count(), 'params': params]
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def show(Profession professionInstance) {
        respond professionInstance
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def create() {
        respond new Profession(params)
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    @Transactional
    def save(Profession professionInstance) {
        if (professionInstance == null) {
            notFound()
            return
        }
        /* ***************************************************
           *              READ COMMAND FIRST //  Ngoc Anh    *
           ***************************************************/
        // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
        if (professionInstance.hasErrors()) {
            respond professionInstance.errors, view:'create'
            return
        }

        professionInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'profession.label', default: 'Position'), professionInstance.id])
                redirect professionInstance
            }
            '*' { respond professionInstance, [status: CREATED] }
        }
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def edit(Profession professionInstance) {
        respond professionInstance
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    @Transactional
    def update(Profession professionInstance) {
        if (professionInstance == null) {
            notFound()
            return
        }
        if (professionInstance.hasErrors()) {
            respond professionInstance.errors, view:'edit'
            return
        }
        professionInstance.save flush:true
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Profession.label', default: 'Profession'), professionInstance.id])
                redirect professionInstance
            }
            '*'{ respond professionInstance, [status: OK] }
        }
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    @Transactional
    def delete(Profession professionInstance) {

        if (professionInstance == null) {
            notFound()
            return
        }

        def rankSalaryInstance = RankSalary.findAllByProfession(professionInstance)
        rankSalaryInstance.each { rankSalary->
            rankSalary.delete()
        }
        def rankSalaryInstance2 = LaborContract.findAllByProfession(professionInstance)
        rankSalaryInstance2.each { LaborContract->
            LaborContract.delete()
        }

        professionInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Profession.label', default: 'Profession'), professionInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'profession.label', default: 'Profession'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
