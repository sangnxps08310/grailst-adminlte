package com.hrm


import grails.plugin.springsecurity.annotation.Secured

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Secured(['ROLE_ADMIN','ROLE_MANAGER'])
@Transactional(readOnly = true)
class DepartUserController {
  /* ***************************************************
     *              READ COMMAND FIRST                 *
     ***************************************************/

  static allowedMethods = [save: "POST", update: "PUT",]

  @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
  def index(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    def listDepartUserInstance
    if (params.search) {
      listDepartUserInstance = DepartUser.findAllByIdLike("%" + params.id + "%")
    } else {
      listDepartUserInstance = DepartUser.list(params)
    }
    respond listDepartUserInstance, model: [departUserInstanceCount: DepartUser.count()]
  }

  @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
  def show(DepartUser departUserInstance) {
    respond departUserInstance
  }

  @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
  def create() {
    def departUserInstance = new DepartUser(params)
    departUserInstance.setDepart(Depart.findById(Long.parseLong(params.id + '')))
    departUserInstance.setGroup(DepartGroup.findById(Long.parseLong(params.group + "")))
    def listDepartUser = DepartUser.list()
    def listNoDepartUser = []
    println("Users ===========> " + listDepartUser.size())

    if (listDepartUser.size() > 0) {
      println("Users ===========> " + listDepartUser.userInfo.id)
      listNoDepartUser = UserInfor.findAllByIdNotInList(listDepartUser.userInfo.id);
    } else {
      listNoDepartUser = UserInfor.findAll()
    }
    respond departUserInstance, model: [listNoDepartUser: listNoDepartUser]
  }

  @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
  @Transactional
  def save(DepartUser departUserInstance) {
    println('============ CREATE DEPART USRER =================')
    println('params ============> ' + params)
    if (departUserInstance == null) {
      notFound()
      return
    }
    def departInstance = Depart.findById(Long.parseLong(params.departId + ""));
    def tempDepartUserInstance = new DepartUser()
    tempDepartUserInstance.setDescription(departUserInstance.description)
    tempDepartUserInstance.setDepart(departInstance)
    tempDepartUserInstance.setUserInfo(departUserInstance.userInfo)
    tempDepartUserInstance.setGroup(DepartGroup.findById(Long.parseLong(params.group + "")))
    tempDepartUserInstance.setIs_room_master(departUserInstance.is_room_master)
    tempDepartUserInstance.setIs_leader(departUserInstance.is_leader)

    /* ***************************************************
       *              READ COMMAND FIRST                 *
       ***************************************************/
    // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
    if (tempDepartUserInstance.hasErrors()) {
      respond departUserInstance.errors, view: 'create'
      return
    }
    if (departUserInstance.is_room_master) {
      def roomMaster = DepartUser.findByDepartAndIs_room_master(departUserInstance.depart, true)
      if (roomMaster) {
        roomMaster.setIs_room_master(false)
        roomMaster.save()
      }
    }
    tempDepartUserInstance.save flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.created.message', args: [message(code: 'departUser.label', default: 'DepartUser'), tempDepartUserInstance.userInfo.name])
        redirect controller: 'depart', action: 'show', resource: tempDepartUserInstance.depart
      }
      '*' { redirect controller: 'depart', action: 'show', resource: tempDepartUserInstance.depart, [status: CREATED] }
    }
    println('============ END CREATE DEPART USRER =================')

  }

  @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
  def edit(DepartUser departUserInstance) {
    def listNoDepartUser = UserInfor.findAll()
    respond departUserInstance, model: [listNoDepartUser: listNoDepartUser]
  }

  @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
  @Transactional
  def update(DepartUser departUserInstance) {
    if (departUserInstance == null) {
      notFound()
      return
    }

    /* ***************************************************
       *              READ COMMAND FIRST                 *
       ***************************************************/
    // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
    if (departUserInstance.hasErrors()) {
      respond departUserInstance.errors, view: 'edit'
      return
    }
    if (departUserInstance.is_room_master) {
      def roomMaster = DepartUser.findByDepartAndIs_room_master(departUserInstance.depart, true)
      if (roomMaster) {
        roomMaster.setIs_room_master(false)
        roomMaster.save(flush: true)
      }
    }
    if (departUserInstance.is_leader) {
      def roomMaster = DepartUser.findByGroupAndIs_leader(departUserInstance.group, true)
      if (roomMaster) {
        roomMaster.setIs_leader(false)
        roomMaster.save(flush: true)
      }
    }
    departUserInstance.save flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.updated.message', args: [message(code: 'DepartUser.label', default: 'DepartUser'), departUserInstance.userInfo.name])
        redirect controller: 'depart', action: 'show', resource: departUserInstance.depart

      }
      '*' { respondredirect controller: 'depart', action: 'show', resource: departUserInstance.depart, [status: OK] }
    }
  }

  @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
  @Transactional
  def delete(DepartUser departUserInstance) {

    if (departUserInstance == null) {
      notFound()
      return
    }



    departUserInstance.delete flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.deleted.message', args: [message(code: 'DepartUser.label', default: 'DepartUser'), departUserInstance.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NO_CONTENT }
    }
  }

  protected void notFound() {
    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'departUser.label', default: 'DepartUser'), params.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NOT_FOUND }
    }
  }
}
