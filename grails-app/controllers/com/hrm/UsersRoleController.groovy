package com.hrm

import grails.plugin.springsecurity.annotation.Secured

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class UsersRoleController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond UsersRole.list(params), model:[usersRoleInstanceCount: UsersRole.count()]
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def show(UsersRole usersRoleInstance) {
        respond usersRoleInstance
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def create() {
        respond new UsersRole(params)
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    @Transactional
    def save(UsersRole usersRoleInstance) {
        if (usersRoleInstance == null) {
            notFound()
            return
        }

        if (usersRoleInstance.hasErrors()) {
            respond usersRoleInstance.errors, view:'create'
            return
        }

        usersRoleInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'usersRole.label', default: 'UsersRole'), usersRoleInstance.id])
                redirect usersRoleInstance
            }
            '*' { respond usersRoleInstance, [status: CREATED] }
        }
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def edit(UsersRole usersRoleInstance) {
        respond usersRoleInstance
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    @Transactional
    def update(UsersRole usersRoleInstance) {
        if (usersRoleInstance == null) {
            notFound()
            return
        }

        if (usersRoleInstance.hasErrors()) {
            respond usersRoleInstance.errors, view:'edit'
            return
        }

        usersRoleInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'UsersRole.label', default: 'UsersRole'), usersRoleInstance.id])
                redirect usersRoleInstance
            }
            '*'{ respond usersRoleInstance, [status: OK] }
        }
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    @Transactional
    def delete(UsersRole usersRoleInstance) {

        if (usersRoleInstance == null) {
            notFound()
            return
        }

        usersRoleInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'UsersRole.label', default: 'UsersRole'), usersRoleInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'usersRole.label', default: 'UsersRole'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
