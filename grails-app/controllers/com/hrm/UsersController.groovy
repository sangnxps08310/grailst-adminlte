package com.hrm

import grails.plugin.springsecurity.annotation.Secured
import org.springframework.security.crypto.password.PasswordEncoder

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
@Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
class UsersController {

  static allowedMethods = [save: "POST", update: "PUT"]
  def springSecurityService

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  def index(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    def listUserInstance = Users.list()
    if (params.search) {
      listUserInstance = Users.findAllByUsernameIlike('%' + params.username + "%");
    }
    respond listUserInstance, model: [usersInstanceCount: listUserInstance.size(), 'params': params]
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  def show(Users usersInstance) {
    def userRole = UsersRole.findAllByUsers(usersInstance).roleId
    def role = Role.findAllByIdInList(userRole).authority


    respond usersInstance, model: ['viewRole': role]
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  def create() {
    respond new Users(params)
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  @Transactional
  def save(Users usersInstance) {
    System.out.println("params ==============> " + params)
    if (usersInstance == null) {
      notFound()
      return
    }
    if (!params.userInfor) {
      flash.message = message(code: 'userInfo.not.found.message', args: [usersInstance.userInfor])
      respond usersInstance, view: 'create'
      return
    }
    if (usersInstance.hasErrors()) {
      respond usersInstance.errors, view: 'create'
      return
    }
    if (validate(params, usersInstance)) {
      respond usersInstance, view: 'create'
      return
    }
    usersInstance.save flush: true
    params.role.each {
      UsersRole.create usersInstance, Role.findById(Long.parseLong(it.toString())), true
    }
    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.created.message', args: [message(code: 'users.label', default: 'Users'), usersInstance.id])
        redirect usersInstance
      }
      '*' { respond usersInstance, [status: CREATED] }
    }
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  def edit(Users usersInstance) {
    println "userInstance ======> " + usersInstance.username
    respond usersInstance
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  @Transactional
  def update(Users usersInstance) {
    if (usersInstance == null) {
      notFound()
      return
    }
    if (params.userInfor) {
      flash.message = message(code: 'userInfo.not.found.message', args: [usersInstance.userInfor])
      flash.messageType = 'alert-danger'
      respond usersInstance, view: 'create'
      return
    }

    if (usersInstance.hasErrors()) {
      respond usersInstance.errors, view: 'edit'
      return
    }
    if (validate(params, usersInstance)) {
      respond usersInstance, view: 'edit'
      return
    }
    def userRoleInstance = UsersRole.findAllByUsers(usersInstance);
    userRoleInstance.each { userRole ->
      userRole.delete();
    }
    params.role.each {
      UsersRole.create usersInstance, Role.findById(Long.parseLong(it.toString())), true
    }
    usersInstance.save(true)

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.updated.message', args: [message(code: 'Users.label', default: 'Users'), usersInstance.id])
        redirect usersInstance
      }
      '*' { respond usersInstance, [status: OK] }
    }
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  @Transactional
  def delete(Users usersInstance) {
    if (usersInstance == null) {
      notFound()
      return
    }
    def userRoleInstance = UsersRole.findAllByUsers(usersInstance)
    usersInstance.each { userRole ->
      userRole.delete(flush: true)
    }
    usersInstance.delete flush: true
    flash.message = message(code: 'default.deleted.message', args: [message(code: 'Users.label', default: 'Users'), usersInstance.username])
    flash.type = 'success'
    redirect action: "index"
  }


  protected void notFound() {
    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'users.label', default: 'Users'), params.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NOT_FOUND }
    }
  }

  protected boolean validate(params, Users usersInstance) {
    def confirmPassword = params.confirmPassword
    return !confirmPassword.equals(usersInstance.password)

  }
}
