package com.hrm

import com.google.gson.Gson
import com.hrm.common.HttpUtils
import grails.plugin.springsecurity.annotation.Secured

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Secured('ROLE_ADMIN,ROLE_MANAGER')
@Transactional(readOnly = true)
class DistrictController {
  /* ***************************************************
     *              READ COMMAND FIRST                 *
     ***************************************************/

  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]



  def index(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    def listDistrictInstance
    if (params.search) {
      listDistrictInstance = District.findAllByIdLike("%" + params.id + "%")
    } else {
      listDistrictInstance = District.list(params)
    }
    respond listDistrictInstance, model: [districtInstanceCount: District.count()]
  }

  @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
  def show(District districtInstance) {
    respond districtInstance
  }

  @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
  def create() {
    respond new District(params)
  }

  @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
  @Transactional
  def save(District districtInstance) {
    if (districtInstance == null) {
      notFound()
      return
    }
    /* ***************************************************
       *              READ COMMAND FIRST                 *
       ***************************************************/
    // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
    if (districtInstance.hasErrors()) {
      respond districtInstance.errors, view: 'create'
      return
    }

    districtInstance.save flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.created.message', args: [message(code: 'district.label', default: 'District'), districtInstance.id])
        redirect districtInstance
      }
      '*' { respond districtInstance, [status: CREATED] }
    }
  }

  @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
  def edit(District districtInstance) {
    respond districtInstance
  }

  @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
  @Transactional
  def update(District districtInstance) {
    if (districtInstance == null) {
      notFound()
      return
    }

    /* ***************************************************
       *              READ COMMAND FIRST                 *
       ***************************************************/
    // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
    if (districtInstance.hasErrors()) {
      respond districtInstance.errors, view: 'edit'
      return
    }

    districtInstance.save flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.updated.message', args: [message(code: 'District.label', default: 'District'), districtInstance.id])
        redirect districtInstance
      }
      '*' { respond districtInstance, [status: OK] }
    }
  }

  @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
  @Transactional
  def delete(District districtInstance) {

    if (districtInstance == null) {
      notFound()
      return
    }

    districtInstance.delete flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.deleted.message', args: [message(code: 'District.label', default: 'District'), districtInstance.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NO_CONTENT }
    }
  }

  protected void notFound() {
    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'district.label', default: 'District'), params.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NOT_FOUND }
    }
  }

  @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
  def getDistrict() {
    response.setContentType("application/json;charset=utf-8")
    def requestParams = params;
    def resonseParams = []
    int provinceCode = requestParams.provinceCode ? Integer.parseInt(requestParams.provinceCode) : 0
    println "params ===> " + provinceCode
    def districtInstance = District.findAllByNameIlikeAndProvince_code('%' + (params.name ? params.name : '') + '%', provinceCode)
    districtInstance.each { district ->
      def mapDistrict = [:]
      mapDistrict['id'] = district.district_code
      mapDistrict['text'] = district.name
      resonseParams << mapDistrict
    }
    println "Result" + new Gson().toJson(resonseParams)
    render(new Gson().toJson(resonseParams))
  }

}
