package com.hrm

import com.google.gson.Gson
import com.hrm.common.HttpUtils
import grails.plugin.springsecurity.annotation.Secured


import java.text.SimpleDateFormat

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Secured(['ROLE_MANAGER', 'ROLE_ADMIN', 'ROLE_SUPERVISOR'])
@Transactional(readOnly = true)
class AttendanceController {
  /* ***************************************************
     *              READ COMMAND FIRST                 *
     ***************************************************/
  def springSecurityService
  def userInforService
  def dataSource

  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

  @Secured(['ROLE_SUPERVISOR', 'ROLE_ADMIN'])
  def index() {
    def listUserInfoIds = DepartUser.findAllByDepart(DepartUser.findByUserInfo(springSecurityService.getCurrentUser().userInfor).depart);
    def listUserId = [];
    for (def listUserInfoId : listUserInfoIds) {
      listUserId.add(listUserInfoId.userInfoId);
    }
    def date = new Date()
    def listAttendanceInstance
    listAttendanceInstance = UserInfor.findAllByIdInList(listUserId)
    Calendar cal = Calendar.getInstance();
    cal.setTime(date)
    Date dateStart = userInforService.setDays(0, 0, 0, cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH), cal.get(Calendar.YEAR));
    Date dateEnd = userInforService.setDays(59, 59, 23, cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH), cal.get(Calendar.YEAR));
    if (checkTodayAttendance(dateStart, dateEnd)) {
      redirect action: "show"
    }
    respond listAttendanceInstance, model: [listAttendanceInstance: listAttendanceInstance]
  }

  @Secured(['ROLE_MANAGER', 'ROLE_ADMIN', 'ROLE_SUPERVISOR'])
  def show() {
    def depart = DepartUser.findByUserInfo(springSecurityService.getCurrentUser().userInfor).depart;
    def date = new Date();
    SimpleDateFormat format = new SimpleDateFormat('yyyy-MM-dd hh:mm:ss');
    println "params =========> " + params;
    if (params.date) {
      println "date =========> " + new Date(Long.parseLong(params.date));

      date = new Date(Long.parseLong(params.date))
    }

    Calendar cal = Calendar.getInstance();
    cal.setTime(date)
    Date dateStart = userInforService.setDays(0, 0, 0, cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH), cal.get(Calendar.YEAR));
    Date dateEnd = userInforService.setDays(59, 59, 23, cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH), cal.get(Calendar.YEAR));
    println('startDate ================> ' + dateStart);
    println('endDate ================> ' + dateEnd);
    def attendanceInstance = Attendance.findByCreatedDateBetweenAndDepart(dateStart, dateEnd, depart, [limit: 1]);
    println('attendanceInstance ================> ' + attendanceInstance);
    def listAttendanceInstance = AttendanceDetails.findAllByAttendance(attendanceInstance);
    println('listAttendanceInstance ================> ' + listAttendanceInstance);

    render view: 'show', model: [date: date, listAttendanceInstance: listAttendanceInstance]
  }

  @Secured(['ROLE_SUPERVISOR', 'ROLE_ADMIN'])
  @Transactional
  def save() {
    def depart = DepartUser.findByUserInfo(springSecurityService.getCurrentUser().userInfor).depart;
    Calendar cal = Calendar.getInstance();
    cal.setTime(new Date())
    Date dateStart = userInforService.setDays(0, 0, 0, cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH), cal.get(Calendar.YEAR));
    Date dateEnd = userInforService.setDays(59, 59, 23, cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH), cal.get(Calendar.YEAR));
    Attendance attendanceInstance = Attendance.findByCreatedDateBetweenAndDepart(dateStart, dateEnd, depart, [limit: 1]);
    if (!attendanceInstance) {
      attendanceInstance = new Attendance();
      attendanceInstance.createdDate = new Date();
      attendanceInstance.depart = depart
      attendanceInstance.save flush: true
    }
    def listUserInfos = DepartUser.findAllByDepart(depart);
    List<String> idPresented = new ArrayList<String>();
    println "usrerPresent =========> " + params.userPresent
    if (params.userPresent) {
      try {
        idPresented = params.userPresent
      } catch (Exception ex) {
        idPresented.add(params.userPresent)
      }
    }
    int count = 0;
    for (def userInfo : listUserInfos) {
      AttendanceDetails attd = AttendanceDetails.findByUserInfoAndAttendance(UserInfor.findById(userInfo.userInfoId), attendanceInstance);
//      attd.userInfo = UserInfor.findById(userInfo.userInfoId);
//      attd.attendance = attendanceInstance;
      if (idPresented.contains(String.valueOf(userInfo.userInfoId))) {
        attd.present = true;
      } else {
        attd.present = false;
      }
      attd.save flush: true
      count++
    }
    if (count == AttendanceDetails.findAllByAttendance(attendanceInstance).size()) {
      redirect action: "show", method: "GET"
      attendanceInstance.setStatus('DONE')
    }
    respond action: "index", method: "GET"
  }

  def checkTodayAttendance(startTime, endTime) {
    def depart = DepartUser.findByUserInfo(springSecurityService.getCurrentUser().userInfor).depart;
    Attendance att = Attendance.findByDepartAndCreatedDateBetweenAndStatus(depart, startTime, endTime, "DONE")
    println('startDate ================> ' + startTime);
    println('endDate ================> ' + endTime);
    println('attendance ================> ' + att)
    def attdt = AttendanceDetails.findAllByAttendance(att);
    println('attendance size ================> ' + attdt.size())
    if (attdt.size() > 0) {
      return true;
    } else {
      return false;
    }
  }

  @Secured(['permitAll'])
  @Transactional
  def collectListAttendanceDetails() {
    def responseData = [:];
    Gson gson = new Gson()
    try {
      response.setContentType('application/json;charset=utf-8')
      def listDepartInstance = Depart.list();
      def sql = new groovy.sql.Sql(dataSource);
      SimpleDateFormat format = new SimpleDateFormat('yyyy-MM-dd')
      int count = 0;
      listDepartInstance.each { depart ->
        String query = "SELECT * FROM attendance WHERE depart_id = $depart.id AND";
        if (params.date) {
          query += " created_date like '${params.date}%'"
        } else {
          query += " created_date like '${format.format(new Date())}%'"
        }
//        responseData['query'] = query;
        def attendanceInstance = sql.firstRow(query)
//        def attendanceInstance = Attendance.findByCreatedDateAndDepart(new Date(),depart)
        println "attendanceInstance ========> " + attendanceInstance
        long attendanceId = 0
        if (!attendanceInstance) {
          def attendance = new Attendance()
          attendance.setDepart(depart);
          attendance.save flush: true
          attendanceId = attendance.id
        } else {
          attendanceId = attendanceInstance.id
        }
        def listDepartUserInstance = DepartUser.findAllByDepart(depart)
        listDepartUserInstance.each { departUser ->
          def attendanceDetailsInstance = AttendanceDetails.findByUserInfoAndAttendance(departUser.userInfo, Attendance.findById(attendanceId));
          if (!attendanceDetailsInstance) {
            def attendanceDetails = new AttendanceDetails()
            attendanceDetails.setUserInfo(departUser.userInfo)
            attendanceDetails.setAttendance(Attendance.findById(attendanceId))
            attendanceDetails.setPresent(false)
            attendanceDetails.save(flush: true)
            if (attendanceDetails.id) {
              count++;
            }
          }
        }
      }
      responseData['status'] = HttpUtils.HTTP_OK
      responseData['imported'] = count
    } catch (Exception e) {
      e.printStackTrace();
      responseData['status'] = HttpUtils.HTTP_INTERNAL_ERROR;
      responseData['exception'] = e.toString()
    }
    render(gson.toJson(responseData))
  }

  protected void notFound() {
    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'attendance.label', default: 'Attendance'), params.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NOT_FOUND }
    }
  }
}
