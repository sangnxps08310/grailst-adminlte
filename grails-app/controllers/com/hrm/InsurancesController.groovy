package com.hrm




import grails.plugin.springsecurity.annotation.Secured

import java.sql.Date
import java.text.SimpleDateFormat

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
@Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
@Transactional(readOnly = true)
class InsurancesController {
    /* ***************************************************
       *              READ COMMAND FIRST                 *
       ***************************************************/

    static allowedMethods = [save: "POST", update: "PUT"]
    @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def listInsurancesInstance
        if(params.search){
            listInsurancesInstance = Insurances.findAllByIdLike("%"+params.id+"%")
        }else {
            listInsurancesInstance = Insurances.list(params)
        }
        respond listInsurancesInstance, model:[insurancesInstanceCount: Insurances.count()]
    }
    @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
    def show(Insurances insurancesInstance) {
        respond insurancesInstance
    }
    @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
    def create() {
        respond new Insurances(params)
    }
    @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
    @Transactional
    def save(Insurances insurancesInstance) {
        if (insurancesInstance == null) {
            notFound()
            return
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type

        insurancesInstance.setCreateDate(new Date(new java.util.Date().getTime()))
        insurancesInstance.setEffectiveDate(new Date(dateFormat.parse(params.effectiveDate).getTime()))
    /* ***************************************************ss
       *              READ COMMAND FIRST                 *
       ***************************************************/
    // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
        if (insurancesInstance.hasErrors()) {
            respond insurancesInstance.errors, view:'create'
            return
        }

        insurancesInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'insurances.label', default: 'Insurances'), insurancesInstance.id])
                redirect insurancesInstance
            }
            '*' { respond insurancesInstance, [status: CREATED] }
        }
    }
    @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
    def edit(Insurances insurancesInstance) {
        respond insurancesInstance
    }
    @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
    @Transactional
    def update(Insurances insurancesInstance) {
        if (insurancesInstance == null) {
            notFound()
            return
        }

            /* ***************************************************
               *              READ COMMAND FIRST                 *
               ***************************************************/
            // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
        if (insurancesInstance.hasErrors()) {
            respond insurancesInstance.errors, view:'edit'
            return
        }

        insurancesInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Insurances.label', default: 'Insurances'), insurancesInstance.id])
                redirect insurancesInstance
            }
            '*'{ respond insurancesInstance, [status: OK] }
        }
    }
    @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
    @Transactional
    def delete(Insurances insurancesInstance) {

        if (insurancesInstance == null) {
            notFound()
            return
        }

        insurancesInstance.delete flush:true
        redirect action: 'index'
//
//        request.withFormat {
//            form multipartForm {
//                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Insurances.label', default: 'Insurances'), insurancesInstance.id])
//                redirect action:"index", method:"GET"
//            }
//            '*'{ render status: NO_CONTENT }
//        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'insurances.label', default: 'Insurances'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
