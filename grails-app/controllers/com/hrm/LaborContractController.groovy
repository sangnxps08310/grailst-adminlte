package com.hrm

import com.hrm.common.CommonUtils
import grails.plugin.springsecurity.annotation.Secured
import org.springframework.core.io.ByteArrayResource

import java.sql.Date
import java.sql.Timestamp
import java.text.SimpleDateFormat

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
@Transactional(readOnly = true)
class LaborContractController {
  /* ***************************************************
     *              READ COMMAND FIRST   // Ngoc Anh   *
     ***************************************************/

  static allowedMethods = [save: "POST", edit: 'GET', delete: "DELETE"]
  LaborContractService laborContractService
  def assetResourceLocator

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  def index(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    params.offset = params.offset ? params.offset : 0
    params.firstResult = params.offset * params.max
    def listLaborContractInstance = LaborContract.list(params)
    if (params.search != null) {
      listLaborContractInstance = laborContractService.find(params)
    }
    respond listLaborContractInstance, model: [laborContractInstanceCount: LaborContract.count(), 'params': params]
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  def show(LaborContract laborContractInstance) {
    respond laborContractInstance
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  def create() {
    respond new LaborContract(params)
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  @Transactional
  def save(LaborContract laborContractInstance) {
    if (laborContractInstance == null) {
      notFound()
      return
    }
    SimpleDateFormat format = new SimpleDateFormat('yyyy-MM-dd')
    laborContractInstance.setCreatedDate(new Date(new java.util.Date().getTime()))
    def tempLaborInstance = new LaborContract()

    tempLaborInstance.setEffectiveDate(new Date(format.parse(params.effectiveDate).getTime()))
    tempLaborInstance.setExpiredDate(new Date(format.parse(params.expiredDate).getTime()))
    tempLaborInstance.setSignDate(new Date(format.parse(params.signDate).getTime()))
    tempLaborInstance.setContractNumber(laborContractInstance.contractNumber)
    tempLaborInstance.setContent(laborContractInstance.content)

    tempLaborInstance.setEmployeeName(laborContractInstance.employeeName)
    tempLaborInstance.setEmployeeDateOfBirth(new Date(format.parse(params.employeeDateOfBirth).getTime()))
    tempLaborInstance.setEmployeePhoneNumber(laborContractInstance.employeePhoneNumber)

    tempLaborInstance.setEmployerName(laborContractInstance.employerName)
    tempLaborInstance.setEmployerDateOfBirth(new Date(format.parse(params.employerDateOfBirth).getTime()))
    tempLaborInstance.setEmployerPhoneNumber(laborContractInstance.employerPhoneNumber)

    tempLaborInstance.setProfession(laborContractInstance.profession)
    tempLaborInstance.setType(laborContractInstance.type)

    tempLaborInstance.setPhoto(params.photo)
    tempLaborInstance.setFileFolder(params.fileFolder)

    if (tempLaborInstance.hasErrors()) {
      respond tempLaborInstance.errors, view: 'create'
      return
    }
    laborContractService.saveContractImage(params.image_photo, '');
    laborContractService.saveContractFile(params.attachFile, '');
    if (laborContractInstance.save(flush: true)) {
    } else {
      flash.message = 'Create failed'
      respond laborContractInstance, view: 'create'
      return
    }
    // tempLaborInstance.save flush:true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.created.message', args: [message(code: 'laborContract.label', default: 'LaborContract'), tempLaborInstance.id])
        redirect tempLaborInstance
      }
      '*' { respond tempLaborInstance, [status: CREATED] }
    }
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  def edit(LaborContract laborContractInstance) {
    respond laborContractInstance
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  @Transactional
  def update(LaborContract laborContractInstance) {
    if (laborContractInstance == null) {
      notFound()
      return
    }
    SimpleDateFormat format = new SimpleDateFormat('yyyy-MM-dd hh:mm:ss')
    println "class 1: " + laborContractInstance.effectiveDate.class

    def tempLaborInstance = laborContractInstance
    println "class 2: " + format.parse(params.effectiveDate + " 00:00:00.0").class
    println "class 3: " + new Date(format.parse(params.effectiveDate + " 00:00:00.0").getTime()).class + "\n"
    println "date 1: " + laborContractInstance.effectiveDate
    println "date 2: " + format.parse(params.effectiveDate + " 00:00:00")
    println "date 3: " + new Timestamp(format.parse(params.effectiveDate + " 00:00:00.0").getTime()) + "\n"
    tempLaborInstance.setEffectiveDate(new Date(format.parse(params.effectiveDate + " 00:00:00.0").getTime()))
    tempLaborInstance.setExpiredDate(new Date(format.parse(params.expiredDate + " 00:00:00").getTime()))
    tempLaborInstance.setSignDate(new Date(format.parse(params.signDate + " 00:00:00").getTime()))
    tempLaborInstance.setContractNumber(laborContractInstance.contractNumber)
    tempLaborInstance.setContent(laborContractInstance.content)
    tempLaborInstance.setEmployeeName(laborContractInstance.employeeName)
    tempLaborInstance.setEmployeeDateOfBirth(new Date(format.parse(params.employeeDateOfBirth + " 00:00:00").getTime()))
    tempLaborInstance.setEmployeePhoneNumber(laborContractInstance.employeePhoneNumber)
    tempLaborInstance.setEmployerName(laborContractInstance.employerName)
    tempLaborInstance.setEmployerDateOfBirth(new Date(format.parse(params.employerDateOfBirth + " 00:00:00").getTime()))
    tempLaborInstance.setEmployerPhoneNumber(laborContractInstance.employerPhoneNumber)
    tempLaborInstance.setProfession(laborContractInstance.profession)
    tempLaborInstance.setType(laborContractInstance.type)
    tempLaborInstance.setPhoto(params.photo)
    tempLaborInstance.setFileFolder(params.fileFolder)

    /* ***************************************************
       *              READ COMMAND FIRST                 *
       ***************************************************/
    // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
    if (tempLaborInstance.hasErrors()) {
      respond tempLaborInstance.errors, view: 'edit'
      return
    }
    if (LaborContract.findByContractNumberAndIdNotEqual(laborContractInstance.contractNumber, laborContractInstance.id)) {
      flash.message = 'Contract Number is available !'
      flash.message_type = 'error'
      respond laborContractInstance, view: 'edit'
      return
    }
    tempLaborInstance.save flush: true
    laborContractService.saveContractImage(params.image_photo, '');
    laborContractService.saveContractFile(params.attachFile, '');

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.updated.message', args: [message(code: 'LaborContract.label', default: 'LaborContract'), laborContractInstance.id])
        redirect laborContractInstance
      }
      '*' { respond laborContractInstance, [status: OK] }
    }
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  @Transactional
  def delete(LaborContract laborContractInstance) {

    if (laborContractInstance == null) {
      notFound()
      return
    }

    def userInformationInstance = UserInfor.findAllByLaborContract(laborContractInstance)
    userInformationInstance.each { UserInfor ->
      UserInfor.delete()
    }

    laborContractInstance.delete flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.deleted.message', args: [message(code: 'LaborContract.label', default: 'LaborContract'), laborContractInstance.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NO_CONTENT }
    }
  }

  protected void notFound() {
    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'laborContract.label', default: 'LaborContract'), params.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NOT_FOUND }
    }
  }

  @Secured('permitAll')
  def loadImage() {
    File tempFile = null;
    println("============== Streamming file ==============")
    println('params ========> ' + params)
    response.setContentType("image/**")
    ByteArrayResource resource = assetResourceLocator.findAssetForURI('default.jpg')
    if (params.path) {
      println resource.byteArray
      String path = CommonUtils.IMAGES_PATH + '/' + params.path
      tempFile = new File(path)
      if (tempFile.exists()) {
        response.setHeader("Content-disposition", "filename=${CommonUtils.IMAGES_PATH + '/' + path}")
        response.outputStream << tempFile.newInputStream()
      } else {
        response.outputStream << resource.inputStream
      }
    } else {
      response.outputStream << resource.inputStream
    }

    println("============== End function ==============")
  }

  @Secured('permitAll')
  def uploadImage() {
    response.setContentType("application/json;charset=utf-8")
    println("============== Upload file ==============")
    if (params.path) {
      userInforService.saveUserImage(params.path);
      render createLink(controller: 'userInfor', action: 'loadImage', params: ['image': false, 'path': params.path.originalFilename]);
      println("==============End Upload file ==============")
    }
  }

  @Secured('permitAll')
  def loadFile() {
    File tempFile = null;
    println("============== Streamming file ==============")
    println('params ========> ' + params)
    response.setContentType("application/**")
    ByteArrayResource resource = assetResourceLocator.findAssetForURI('default.jpg')
    if (params.path) {
      println resource.byteArray
      String path = CommonUtils.IMAGES_PATH + '/' + params.path
      tempFile = new File(path)
      if (tempFile.exists()) {
        response.setHeader("Content-disposition", "filename=${CommonUtils.IMAGES_PATH + '/' + path}")
        response.outputStream << tempFile.newInputStream()
      } else {
        response.outputStream << resource.inputStream
      }
    } else {
      response.outputStream << resource.inputStream
    }

    println("============== End function ==============")
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  def download() {
    def laborContractInstance = LaborContract.findById(Long.parseLong(params.id))
    println('params ========> ' + params)
    response.setContentType("application/**")
    String path = CommonUtils.FILES_PATH + '/' + laborContractInstance.fileFolder
    println('path ========> ' + path)
    def tempFile = new File(path)
    if (tempFile.exists()) {
      response.setHeader("Content-disposition", "filename=${laborContractInstance.fileFolder}")
      response.outputStream << tempFile.newInputStream()
    } else {
      response.setHeader("404/file not found", HttpUtils.HTTP_NOT_FOUND)
    }

    println("============== End function ==============")
  }

  def checkExportStatus() {

  }
}
