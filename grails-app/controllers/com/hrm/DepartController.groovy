package com.hrm


import grails.plugin.springsecurity.annotation.Secured
import org.h2.engine.User

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
@Transactional(readOnly = true)
class DepartController {
  /* ***************************************************
     *              READ COMMAND FIRST                 *
     ***************************************************/

  static allowedMethods = [save: "POST", update: "PUT"]

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  def index(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    def listDepartInstance
    if (params.search) {
      listDepartInstance = Depart.findAllByNameIlike("%" + params.name + "%")
    } else {
      listDepartInstance = Depart.list(params)
    }
    respond listDepartInstance, model: [departInstanceCount: Depart.count(), 'params': params]
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  def show(Depart departInstance) {
    def currentDepartGroups = DepartGroup.findAllByDepart(departInstance)
    def departGroupUser = [:]
    currentDepartGroups.each { departGroup ->
      departGroupUser[departGroup.id] = DepartUser.findAllByDepartAndGroupAndIs_room_master(departInstance, departGroup, false, [sort: 'is_leader', order: 'desc'])
    }
    def roomMasterInfo = DepartUser.findByDepartAndIs_room_master(departInstance, true)
    respond departInstance, model: [departGroups: currentDepartGroups, departUsers: departGroupUser, roomMasterInfo: roomMasterInfo]
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  def create() {
    def roomMaster = new UserInfor();
    respond new Depart(params), model: [roomMaster: roomMaster]
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  @Transactional
  def save(Depart departInstance) {
    if (departInstance == null) {
      notFound()
      return
    }
    /* ***************************************************
       *              READ COMMAND FIRST                 *
       ***************************************************/
    // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
    if (departInstance.hasErrors()) {
      respond departInstance.errors, view: 'create'
      return
    }
    departInstance.save flush: true
    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.created.message', args: [message(code: 'depart.label', default: 'Depart'), departInstance.name])
        redirect departInstance
      }
      '*' { respond departInstance, [status: CREATED] }
    }
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  def edit(Depart departInstance) {
    def roomMaster = DepartUser.findByDepartAndIs_room_master(departInstance, true)?.userInfo
    if (!roomMaster) {
      roomMaster = new UserInfor()
    }
    respond departInstance, model: [roomMaster: roomMaster]
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  @Transactional
  def update(Depart departInstance) {
    if (departInstance == null) {
      notFound()
      return
    }

    /* ***************************************************
       *              READ COMMAND FIRST                 *
       ***************************************************/
    // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
    if (departInstance.hasErrors()) {
      respond departInstance.errors, view: 'edit'
      return
    }

    departInstance.save flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.updated.message', args: [message(code: 'Depart.label', default: 'Depart'), departInstance.name])
        redirect departInstance
      }
      '*' { respond departInstance, [status: OK] }
    }
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  @Transactional
  def delete(Depart departInstance) {

    if (departInstance == null) {
      notFound()
      return
    }

    def departUserInstance = DepartUser.findAllByDepart(departInstance)
    departUserInstance.each { DepartUser ->
      DepartUser.delete()
    }
    def departGroupInstance = DepartGroup.findAllByDepart(departInstance)
    departGroupInstance.each { DepartGroup ->
      DepartGroup.delete()
    }
    def attendanceInstance = Attendance.findAllByDepart(departInstance)
    attendanceInstance.each { Attendance ->
      Attendance.delete()
    }
    departInstance.delete flush: true

    redirect view: 'index'
  }

  protected void notFound() {
    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'depart.label', default: 'Depart'), params.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NOT_FOUND }
    }
  }
}
