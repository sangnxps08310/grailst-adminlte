package com.hrm




import grails.plugin.springsecurity.annotation.Secured

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
@Secured(['ROLE_ADMIN','ROLE_MANAGER'])
@Transactional(readOnly = true)
class TypesOfContractController {
    /* ***************************************************
       *              READ COMMAND FIRST    // Ngoc Anh  *
       ***************************************************/

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def listTypesOfContractInstance
        if(params.search){
            listTypesOfContractInstance = TypesOfContract.findAllByNameLike("%"+params.name+"%")
        }else {
            listTypesOfContractInstance = TypesOfContract.list(params)
        }
        respond listTypesOfContractInstance, model:[typesOfContractInstanceCount: TypesOfContract.count()]
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def show(TypesOfContract typesOfContractInstance) {
        respond typesOfContractInstance
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def create() {
        respond new TypesOfContract(params)
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    @Transactional
    def save(TypesOfContract typesOfContractInstance) {
        if (typesOfContractInstance == null) {
            notFound()
            return
        }
    /* ***************************************************
       *              READ COMMAND FIRST                 *
       ***************************************************/
    // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
        if (typesOfContractInstance.hasErrors()) {
            respond typesOfContractInstance.errors, view:'create'
            return
        }

        typesOfContractInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'typesOfContract.label', default: 'TypesOfContract'), typesOfContractInstance.id])
                redirect typesOfContractInstance
            }
            '*' { respond typesOfContractInstance, [status: CREATED] }
        }
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def edit(TypesOfContract typesOfContractInstance) {
        respond typesOfContractInstance
    }
        @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    @Transactional
    def update(TypesOfContract typesOfContractInstance) {
        if (typesOfContractInstance == null) {
            notFound()
            return
        }

            /* ***************************************************
               *              READ COMMAND FIRST                 *
               ***************************************************/
            // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
        if (typesOfContractInstance.hasErrors()) {
            respond typesOfContractInstance.errors, view:'edit'
            return
        }

        typesOfContractInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'TypesOfContract.label', default: 'TypesOfContract'), typesOfContractInstance.id])
                redirect typesOfContractInstance
            }
            '*'{ respond typesOfContractInstance, [status: OK] }
        }
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    @Transactional
    def delete(TypesOfContract typesOfContractInstance) {

        if (typesOfContractInstance == null) {
            notFound()
            return
        }

        typesOfContractInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'TypesOfContract.label', default: 'TypesOfContract'), typesOfContractInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'typesOfContract.label', default: 'TypesOfContract'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
