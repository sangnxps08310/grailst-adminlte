package com.hrm

import com.hrm.common.CommonUtils
import grails.plugin.springsecurity.annotation.Secured
import grails.plugin.springsecurity.authentication.encoding.BCryptPasswordEncoder
import org.springframework.core.io.ByteArrayResource
import org.springframework.security.crypto.password.PasswordEncoder

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
@Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
class UserInforController {

  static allowedMethods = [save: "POST", update: "POST", delete: "DELETE"]
  UserInforService userInforService
  def assetResourceLocator
  def springSecurityService

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])

  def index(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    params.offset = params.offset ? params.offset as Integer : 0;
    def listUserInstance = UserInfor.list(params);
    if (params.search != null) {
      listUserInstance = userInforService.find(params)
    }
    def listDepartInstance = [:]
    DepartUser.findAllByUserInfoInList(listUserInstance)?.each { departUser ->
      listDepartInstance[departUser.userInfo.id] = departUser.depart;
    }
    respond listUserInstance, model: [userInforInstanceCount: UserInfor.count(), 'params': params, mapDepartInstance: listDepartInstance]
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  def show(UserInfor userInforInstance) {
    respond userInforInstance
  }

  @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
  def profile(UserInfor userInforInstance) {
    def model = [:]
    def listRequestInstance = Requests.findAllByStaffAndStatus(userInforInstance, 'ACCEPTED')
    def departInstance = DepartUser.findByUserInfo(userInforInstance);
    model['listRequestInstance'] = listRequestInstance;
    if (departInstance) {
      def roomMaster = DepartUser.findByDepartAndIs_room_master(departInstance.depart, true).userInfo
      model['departInstance'] = departInstance.depart;
      model['roomMaster'] = roomMaster;
    }
    int restRemainDays = userInforService.getRestDaysRemain(userInforInstance);
    model['remainDays'] = 12 - restRemainDays;
    respond userInforInstance, model: model
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  def create() {
    respond new UserInfor(params), model: ['userDepartInstance': new Depart()]
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  @Transactional
  def save() {
    def tempUserInforInstance = new UserInfor();
    tempUserInforInstance = userInforService.loadUserInforInstance(params, tempUserInforInstance)
    if (tempUserInforInstance == null) {
      notFound()
      return
    }

    if (tempUserInforInstance.hasErrors()) {
      respond tempUserInforInstance.errors, view: 'create'
      return
    }
    userInforService.saveUserImage(params.image_photo, '');
    userInforService.saveUserImage(params.image_certificate, '');
    userInforService.saveUserImage(params.image_personal_id, '');

    if (tempUserInforInstance.save(flush: true)) {
      if (params.depart != "0" && params.depart != null) {
        def tempDepartUserInstance = new DepartUser();
        tempDepartUserInstance.setIs_leader(false);
        tempDepartUserInstance.setIs_room_master(false);
        tempDepartUserInstance.setDepart(Depart.findById(Long.parseLong(params.depart + "")))
        tempDepartUserInstance.setUserInfo(tempUserInforInstance)
        tempDepartUserInstance.save(flush: true);
      }
    } else {
      flash.message = 'Create failed'
      respond tempUserInforInstance, view: 'create'
      return
    }
//
//    def userInforInstance = UserInfor.findByNameAndEmailAndGenderAndDateOfBirthAndCertificate(
//      tempUserInforInstance.name, tempUserInforInstance.email, tempUserInforInstance.gender, tempUserInforInstance.dateOfBirth, tempUserInforInstance.certificate)

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.created.message', args: [message(code: 'userInfor.label', default: 'UserInfor'), tempUserInforInstance.name])
        redirect tempUserInforInstance
      }
      '*' { respond tempUserInforInstance, [status: CREATED] }
    }
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  def edit(UserInfor userInforInstance) {
    def userDepartInstance
    def currentDepartUser = DepartUser.findByUserInfo(userInforInstance)
    if (currentDepartUser) {
      userDepartInstance = Depart.findById(currentDepartUser.depart.id)
    } else {
      userDepartInstance = new Depart()
    }
    respond userInforInstance, model: ['userDepartInstance': userDepartInstance]
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  @Transactional
  def update() {
    println "params ==============> " + params
    def userInforInstance = UserInfor.findById(Long.parseLong(params.id));
    if (userInforInstance == null) {
      notFound()
      return
    }
    userInforInstance = userInforService.loadUserInforInstance(params, userInforInstance)
    if (userInforInstance.hasErrors()) {
      respond userInforInstance.errors, view: 'create'
      return
    }

    userInforInstance.save(flush: true)
    def departUserInstance = DepartUser.findAllByUserInfo(userInforInstance)
    departUserInstance?.each { depart ->
      depart.delete()
    }
    if (params.image_photo) {
      userInforService.saveUserImage(params.image_photo, '');
    }
    if (params.image_certificate) {
      userInforService.saveUserImage(params.image_certificate, '');
    }
    if (params.image_personal_id) {
      userInforService.saveUserImage(params.image_personal_id, '');
    }

    if (params.depart != "0" && params.depart != null) {
      println "======================== SAVE DEPART ================= "


      def tempDepartUserInstance = new DepartUser();
      tempDepartUserInstance.setVersion(0)
      tempDepartUserInstance.setIs_leader(false);
      tempDepartUserInstance.setIs_room_master(false);
      tempDepartUserInstance.setDepart(Depart.findById(Long.parseLong(params.depart + "")))
      tempDepartUserInstance.setUserInfo(userInforInstance)
      tempDepartUserInstance.save(flush: true)

    }
    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.updated.message', args: [message(code: 'UserInfor.label', default: 'UserInfor'), userInforInstance.id])
        redirect userInforInstance
      }
      '*' { respond userInforInstance, [status: OK] }
    }
  }

  @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
  @Transactional
  def changePass() {
    String currentPassword = params.current_password
    String newPassword = params.new_password
    String confirmPassword = params.confirm_password
    String id = params.id
    def currentUser = springSecurityService.getCurrentUser();
    if (currentUser) {
      PasswordEncoder encoder = new org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder();
      if (encoder.matches(currentPassword, currentUser.getPassword())) {
        if (newPassword.toString().matches(confirmPassword.toString())) {
          def user = Users.findById(currentUser.id);
          user.setPassword(newPassword)
          user.save(flush: true)
          flash.message = 'Password has been changed'
          flash.message_type = 'success'
        } else {
          flash.message = 'Confirm password is not matches'
          flash.message_type = 'warning'
        }
      } else {
        flash.message = 'Current password is not matches.'
        flash.message_type = 'warning'
      }
    } else {
      flash.message = 'Something was wrong.'
      flash.message_type = 'warning'
    }

    redirect action: 'profile', id: id
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  @Transactional
  def delete(UserInfor userInforInstance) {

    if (userInforInstance == null) {
      notFound()
      return
    }
    def departInstance = DepartUser.findAllByUserInfo(userInforInstance)
    departInstance.each { departUser ->
      departUser.delete()
    }
    userInforInstance.delete flush: true
    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.deleted.message', args: [message(code: 'UserInfor.label', default: 'UserInfor'), userInforInstance.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NO_CONTENT }
    }
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  def generateUserInfo() {
    def laborContractInstance = LaborContract.findById(Long.parseLong(params.id))
    def userInforInstance = new UserInfor()
    userInforInstance.name = laborContractInstance.employeeName
    userInforInstance.dateOfBirth = laborContractInstance.employeeDateOfBirth
    userInforInstance.phoneNumber = laborContractInstance.employeePhoneNumber
    userInforInstance.laborContract = laborContractInstance
    respond userInforInstance, view: 'create', model: ['userDepartInstance': new Depart()]
  }

  @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
  def loadImage() {
    File tempFile = null;
    println("============== Streamming file ==============")
    println('params ========> ' + params)
    response.setContentType("image/**")
    if (params.path) {
      String path = CommonUtils.IMAGES_PATH + '/' + params.path
      tempFile = new File(path)
      if (tempFile.exists()) {
        response.setHeader("Content-disposition", "filename=${CommonUtils.IMAGES_PATH + '/' + path}")
        response.outputStream << tempFile.newInputStream()
      } else {
        ByteArrayResource resource = assetResourceLocator.findAssetForURI('default.jpg')
        response.outputStream << resource.inputStream
      }
    } else {
      ByteArrayResource resource = assetResourceLocator.findAssetForURI('default.jpg')
      response.outputStream << resource.inputStream
    }

    println("============== End function ==============")
  }

  @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
  def uploadImage() {
    response.setContentType("application/json")
    println("============== Upload file ==============")
    if (params.path) {
      userInforService.saveUserImage(params.path);
      render createLink(controller: 'userInfor', action: 'loadImage', params: ['image': false, 'path': params.path.originalFilename]);
      println("==============End Upload file ==============")
    }
  }

  @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_MANAGER','ROLE_SUPERVISOR'])
  def delTempFile() {
    response.setContentType("application/json")
    println("============== Delete Temp file ==============")
    File tempFolder = new File(CommonUtils.IMAGES_TEMP_PATH)
    render tempFolder.deleteDir();
  }

  protected void notFound() {
    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'userInfor.label', default: 'UserInfor'), params.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NOT_FOUND }
    }
  }
}
