package com.hrm

import grails.plugin.springsecurity.annotation.Secured

import java.text.SimpleDateFormat

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
@Secured(['ROLE_ADMIN','ROLE_MANAGER'])
@Transactional(readOnly = true)
class InsurancesController_bk {
    /* ***************************************************
       *              READ COMMAND FIRST Ngoc Anh        *
       ***************************************************/

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    InsurancesService insurancesService

    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        params.offset = params.offset ? params.offset : 0
        params.firstResult = params.offset * params.max

        def listInsurancesInstance  = Insurances.list(params)
        if(params.search != null){
            listInsurancesInstance = insurancesService.find(params)
        }
        respond listInsurancesInstance, model:[insurancesInstanceCount: Insurances.count(), 'params': params]
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def show(Insurances insurancesInstance) {
        respond insurancesInstance
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def create() {
        respond new Insurances(params)
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    @Transactional
    def save(Insurances insurancesInstance) {
        if (insurancesInstance == null) {
            notFound()
            return
        }
        SimpleDateFormat format = new SimpleDateFormat('yyyy-MM-dd')
        insurancesInstance.setCreateDate(new Date())
        def tempInsurancesInstance = new Insurances()
        tempInsurancesInstance.setEffectiveDate(format.parse(params.effectiveDate))
        tempInsurancesInstance.setName(insurancesInstance.name)
        String tempFactor = (Double.parseDouble(insurancesInstance.factor)/100)+""
        tempInsurancesInstance.setFactor(tempFactor)
        println("created date ===============> " + insurancesInstance.createDate)
        println("effective date ==============> " + tempInsurancesInstance.getEffectiveDate())
        if (tempInsurancesInstance.hasErrors()) {
            respond tempInsurancesInstance.errors, view:'create'
            return
        }
        tempInsurancesInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'insurances.label', default: 'Insurances'), tempInsurancesInstance.id])
                redirect tempInsurancesInstance
            }
            '*' { respond tempInsurancesInstance, [status: CREATED] }
        }
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def edit(Insurances insurancesInstance) {
        respond insurancesInstance
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    @Transactional
    def update(Insurances insurancesInstance) {
        if (insurancesInstance == null) {
            notFound()
            return
        }

        /* ***************************************************
           *              READ COMMAND FIRST                 *
           ***************************************************/
        // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
        if (insurancesInstance.hasErrors()) {
            respond insurancesInstance.errors, view:'edit'
            return
        }

        insurancesInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Insurances.label', default: 'Insurances'), insurancesInstance.id])
                redirect insurancesInstance
            }
            '*'{ respond insurancesInstance, [status: OK] }
        }
    }
    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    @Transactional
    def delete(Insurances insurancesInstance) {

        if (insurancesInstance == null) {
            notFound()
            return
        }

        insurancesInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Insurances.label', default: 'Insurances'), insurancesInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'insurances.label', default: 'Insurances'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
