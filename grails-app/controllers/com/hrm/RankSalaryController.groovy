package com.hrm

import com.google.gson.Gson
import grails.plugin.springsecurity.annotation.Secured

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Secured(['ROLE_ADMIN','ROLE_MANAGER'])
@Transactional(readOnly = true)
class RankSalaryController {
    /* ***************************************************
       *              READ COMMAND FIRST  //  Ngoc Anh    *
       ***************************************************/

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    RankSalaryService rankSalaryService

    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        params.offset = params.offset ? params.offset : 0
        params.firstResult = params.offset * params.max
        def listRankSalaryInstance = RankSalary.list(params)


        if (params.search != null) {
            listRankSalaryInstance = rankSalaryService.find(params)
        }

        respond listRankSalaryInstance, model: [rankSalaryInstanceCount: RankSalary.count(), 'params': params]
    }

    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def show(RankSalary rankSalaryInstance) {
        respond rankSalaryInstance
    }

    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def create() {
        respond new RankSalary(params)
    }

    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    @Transactional
    def save(RankSalary rankSalaryInstance) {
        if (rankSalaryInstance == null) {
            notFound()
            return
        }
        /* ***************************************************
           *              READ COMMAND FIRST                 *
           ***************************************************/
        // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
        if (rankSalaryInstance.hasErrors()) {
            respond rankSalaryInstance.errors, view: 'create'
            return
        }

        rankSalaryInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'rankSalary.label', default: 'RankSalary'), rankSalaryInstance.id])
                redirect rankSalaryInstance
            }
            '*' { respond rankSalaryInstance, [status: CREATED] }
        }
    }

    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def edit(RankSalary rankSalaryInstance) {
        respond rankSalaryInstance
    }

    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    @Transactional
    def update(RankSalary rankSalaryInstance) {
        if (rankSalaryInstance == null) {
            notFound()
            return
        }

        /* ***************************************************
           *              READ COMMAND FIRST                 *
           ***************************************************/
        // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
        if (rankSalaryInstance.hasErrors()) {
            respond rankSalaryInstance.errors, view: 'edit'
            return
        }

        rankSalaryInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'RankSalary.label', default: 'RankSalary'), rankSalaryInstance.id])
                redirect rankSalaryInstance
            }
            '*' { respond rankSalaryInstance, [status: OK] }
        }
    }

    @Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    @Transactional
    def delete(RankSalary rankSalaryInstance) {

        if (rankSalaryInstance == null) {
            notFound()
            return
        }

        rankSalaryInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'RankSalary.label', default: 'RankSalary'), rankSalaryInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    def ajaxGetRankSalary() {
        response.setContentType("application/json;charset=utf-8")
        def requestParams = params;
        def resonseParams = []
        String rankName = ''
        if (requestParams.rankSalary) {
            rankName = requestParams.rankSalary
        }
        long userId = Long.parseLong(requestParams.userId + "");
        def containUser = UserInfor.findById(userId);
        println "TEST ===========> " + containUser?.laborContract
        long professionId = userId == 0 ? 0 : containUser?.laborContract?.profession?.id
        def profession = Profession.findById(professionId)
        println "params ===> " + params
        if (profession) {
            def rankSalaryInstance = RankSalary.findAllByNameRankIlikeAndProfession('%' + rankName + '%', profession)
            rankSalaryInstance.each { rankSalary ->
                def mapRankSalary = [:]
                mapRankSalary['id'] = rankSalary.id
                mapRankSalary['text'] = rankSalary.nameRank + " ("+rankSalary.rankSalary+")"
                resonseParams << mapRankSalary
            }
        } else {
            resonseParams << ['id': 0, 'text': '']
        }
        println "Result" + new Gson().toJson(resonseParams)
        render(new Gson().toJson(resonseParams))
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'rankSalary.label', default: 'RankSalary'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }

}
