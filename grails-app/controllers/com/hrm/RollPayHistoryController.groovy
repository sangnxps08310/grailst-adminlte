package com.hrm

import com.google.gson.Gson
import com.hrm.common.CommonUtils
import com.hrm.common.HttpUtils
import grails.plugin.springsecurity.annotation.Secured
import org.apache.http.protocol.HTTP
import org.springframework.core.io.ByteArrayResource

import java.text.SimpleDateFormat

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Secured('permitAll')
@Transactional(readOnly = true)
class RollPayHistoryController {
  /* ***************************************************
     *              READ COMMAND FIRST                 *
     ***************************************************/

  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
  def springSecurityService
  def rollPayHistoryService

  @Secured('permitAll')
  def index(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    def listRollPayHistoryInstance
    if (params.search) {
      listRollPayHistoryInstance = RollPayHistory.findAllByMonth(params.month)
    } else {
      listRollPayHistoryInstance = RollPayHistory.list(params)
    }
    respond listRollPayHistoryInstance, model: [rollPayHistoryInstanceCount: RollPayHistory.count(), 'params': params]
  }

  @Secured('permitAll')
  def show(RollPayHistory rollPayHistoryInstance) {
    Calendar cal = Calendar.getInstance();
    def rollPayInstance = RollPay.findAllByRollPayHistory(rollPayHistoryInstance);
    if (rollPayInstance.size() > 0) {
      redirect action: 'confirmRollPay', params: params
    }
    def mapUserRollPay = rollPayHistoryService.calcRolPay(rollPayHistoryInstance.month, cal.get(Calendar.YEAR));

    respond rollPayHistoryInstance, model: ['mapUserRollPay': mapUserRollPay, params: params]
  }

  @Secured('permitAll')
  def create() {
    respond new RollPayHistory(params)
  }

  @Secured('permitAll')
  @Transactional
  def save(RollPayHistory rollPayHistoryInstance) {
    if (rollPayHistoryInstance == null) {
      notFound()
      return
    }
    SimpleDateFormat format = new SimpleDateFormat("yyyy_MM_dd")
    rollPayHistoryInstance.setCreateDate(new Date())
    def tempRollPayHistoryInstance = new RollPayHistory()
//    tempRollPayHistoryInstance.setFileName(rollPayHistoryService.EXPORT_FILE_NAME + format.format(new Date()) + CommonUtils.EXCEL_SUFFIX)
    tempRollPayHistoryInstance.setCreater(springSecurityService.getCurrentUser().userInfor)
    tempRollPayHistoryInstance.setMonth(rollPayHistoryInstance.month)
    tempRollPayHistoryInstance.setStatus("WAITING")
    if (tempRollPayHistoryInstance.hasErrors()) {
      respond tempRollPayHistoryInstance.errors, view: 'create'
      return
    }

    tempRollPayHistoryInstance.save flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.created.message', args: [message(code: 'rollPayHistory.label', default: 'RollPayHistory'), tempRollPayHistoryInstance.id])
        redirect tempRollPayHistoryInstance
      }
      '*' { respond tempRollPayHistoryInstance, [status: CREATED] }
    }
  }


  @Secured('permitAll')
  def edit(RollPayHistory rollPayHistoryInstance) {
    respond rollPayHistoryInstance
  }

  @Secured('permitAll')
  @Transactional
  def update(RollPayHistory rollPayHistoryInstance) {
    if (rollPayHistoryInstance == null) {
      notFound()
      return
    }

    /* ***************************************************
       *              READ COMMAND FIRST                 *
       ***************************************************/
    // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
    if (rollPayHistoryInstance.hasErrors()) {
      respond rollPayHistoryInstance.errors, view: 'edit'
      return
    }

    rollPayHistoryInstance.save flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.updated.message', args: [message(code: 'RollPayHistory.label', default: 'RollPayHistory'), rollPayHistoryInstance.id])
        redirect rollPayHistoryInstance
      }
      '*' { respond rollPayHistoryInstance, [status: OK] }
    }
  }

  @Secured('permitAll')
  @Transactional
  def delete(RollPayHistory rollPayHistoryInstance) {
    if (rollPayHistoryInstance == null) {
      notFound()
      return
    }
    rollPayHistoryInstance.delete flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.deleted.message', args: [message(code: 'RollPayHistory.label', default: 'RollPayHistory'), rollPayHistoryInstance.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NO_CONTENT }
    }
  }

  protected void notFound() {
    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'rollPayHistory.label', default: 'RollPayHistory'), params.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NOT_FOUND }
    }
  }

  @Secured('permitAll')
  @Transactional
  def confirmRollPay() {
    def rollPayHistoryInstance = RollPayHistory.findById(Long.parseLong(params.id))
    Calendar cal = Calendar.getInstance();
    def rollPayInstance = RollPay.findAllByRollPayHistory(rollPayHistoryInstance);
    if (rollPayInstance.size() > 0) {
      rollPayInstance.each {
        params[it.userInfor.id + "_bonus"] = it.bonus
        params[it.userInfor.id + "_allowance"] = it.allowance
      }
    }
    def mapUserRollPay = rollPayHistoryService.calcRolPay(rollPayHistoryInstance.month, cal.get(Calendar.YEAR), params);
    if (rollPayInstance.size() == 0) {
      rollPayHistoryService.importRollPayDataToDb(mapUserRollPay, rollPayHistoryInstance.id);
    }
    respond rollPayHistoryInstance, view: 'show', model: ['mapUserRollPay': mapUserRollPay, params: params]
  }

  @Secured(['ROLE_ADMIN', 'ROLE_MANAGER'])
  def download() {

    def rollPayHistoryInstance = RollPayHistory.findById(Long.parseLong(params.id))
    println('params ========> ' + params)
    response.setContentType("application/xlsx")

    String path = CommonUtils.EXPORT_PATH + '/' + rollPayHistoryInstance.id + "/" + rollPayHistoryInstance.fileName
    println('path ========> ' + path)

    def tempFile = new File(path)
    if (tempFile.exists()) {
      response.setHeader("Content-disposition", "filename=${rollPayHistoryInstance.fileName}")
      response.outputStream << tempFile.newInputStream()
    } else {
      response.setHeader("404/file not found", HttpUtils.HTTP_NOT_FOUND)
    }

    println("============== End function ==============")
  }

  def checkExportStatus() {

  }

  @Secured('permitAll')
  @Transactional
  def exportExcelFile() {
    try {
      def rollPayHistoryInstance = RollPayHistory.findById(Long.parseLong(params.id))
      Calendar cal = Calendar.getInstance();
      def rollPayInstance = RollPay.findAllByRollPayHistory(rollPayHistoryInstance);
      if (rollPayInstance.size() > 0) {
        rollPayInstance.each {
          params[it.userInfor.id + "_bonus"] = it.bonus
          params[it.userInfor.id + "_allowance"] = it.allowance
        }
      }
      def mapUserRollPay = rollPayHistoryService.calcRolPay(rollPayHistoryInstance.month, cal.get(Calendar.YEAR), params);
      String fileName = rollPayHistoryService.exportRollPayToExcelFile(mapUserRollPay, rollPayHistoryInstance.id);
      rollPayHistoryInstance.setFileName(fileName);
      rollPayHistoryInstance.setStatus('DONE');
      rollPayHistoryInstance.save(flush: true);
      respond rollPayHistoryInstance, view: 'show', model: ['mapUserRollPay': mapUserRollPay, params: params];
    } catch (Exception e) {
      response.setContentType("application/json; charset=utf-8");
      e.printStackTrace()
      render(HttpUtils.HTTP_INTERNAL_ERROR)
    }
  }
}
