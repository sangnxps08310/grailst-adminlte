package com.hrm




import grails.plugin.springsecurity.annotation.Secured

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
@Secured('permitAll')
@Transactional(readOnly = true)
class RollPayController {
    /* ***************************************************
       *              READ COMMAND FIRST                 *
       ***************************************************/

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    @Secured('permitAll')
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def listRollPayInstance
        if(params.search){
            listRollPayInstance = RollPay.findAllByIdLike("%"+params.id+"%")
        }else {
            listRollPayInstance = RollPay.list(params)
        }
        respond listRollPayInstance, model:[rollPayInstanceCount: RollPay.count()]
    }
    @Secured('permitAll')
    def show(RollPay rollPayInstance) {
        respond rollPayInstance
    }
    @Secured('permitAll')
    def create() {
        respond new RollPay(params)
    }
    @Secured('permitAll')
    @Transactional
    def save(RollPay rollPayInstance) {
        if (rollPayInstance == null) {
            notFound()
            return
        }
    /* ***************************************************
       *              READ COMMAND FIRST                 *
       ***************************************************/
    // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
        if (rollPayInstance.hasErrors()) {
            respond rollPayInstance.errors, view:'create'
            return
        }

        rollPayInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'rollPay.label', default: 'RollPay'), rollPayInstance.id])
                redirect rollPayInstance
            }
            '*' { respond rollPayInstance, [status: CREATED] }
        }
    }
    @Secured('permitAll')
    def edit(RollPay rollPayInstance) {
        respond rollPayInstance
    }
        @Secured('permitAll')
    @Transactional
    def update(RollPay rollPayInstance) {
        if (rollPayInstance == null) {
            notFound()
            return
        }

            /* ***************************************************
               *              READ COMMAND FIRST                 *
               ***************************************************/
            // ** If the current class has a field as a date attribute, uncommant and reset the code to fix the bug.

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") //set date type
//        userInforInstance.set<date>(dateFormat.format(params.dateOfBirth)); //reset date propertie for this entity
        if (rollPayInstance.hasErrors()) {
            respond rollPayInstance.errors, view:'edit'
            return
        }

        rollPayInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'RollPay.label', default: 'RollPay'), rollPayInstance.id])
                redirect rollPayInstance
            }
            '*'{ respond rollPayInstance, [status: OK] }
        }
    }
    @Secured('permitAll')
    @Transactional
    def delete(RollPay rollPayInstance) {

        if (rollPayInstance == null) {
            notFound()
            return
        }

        rollPayInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'RollPay.label', default: 'RollPay'), rollPayInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'rollPay.label', default: 'RollPay'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def fillInsurance(){
        List<Insurances> listOfInsurances = Insurances.findAll()
        return [insuranceName: listOfInsurances]
    }
}
