package com.hrm

import grails.transaction.Transactional

import java.text.SimpleDateFormat

@Transactional
class RequestsService {

  def serviceMethod() {

  }

  def search(params) {
    println("params ========================> " + params)
    def mapDateCreated = compareDateFromDateTo(params.createdFrom, params.createdTo)
    def mapDateAccepted = compareDateFromDateTo(params.acceptedFrom, params.acceptedTo)
    def mapDateEffected = compareDateFromDateTo(params.effectedFrom, params.effectedTo)
    def mapDateExpired = compareDateFromDateTo(params.expiredFrom, params.expiredTo)
    def listRequestInstance = Requests.createCriteria().list(params) {
      if (mapDateAccepted) {
        between('acceptedAt', mapDateAccepted['dateFrom'], mapDateAccepted['dateTo'])
      }
      if (mapDateCreated) {
        between('createdDate', mapDateCreated['dateFrom'], mapDateCreated['dateTo'])
      }
      if (mapDateEffected) {
        between('effectedAt', mapDateEffected['dateFrom'], mapDateEffected['dateTo'])
      }
      if (mapDateExpired) {
        between('expiredAt', mapDateExpired['dateFrom'], mapDateExpired['dateTo'])
      }
      if (params?.depart?.id != '0') {
        eq('depart.id', Long.parseLong(params.depart.id))
      }
      if (params?.type?.id != '0') {
        eq('type.id', Long.parseLong(params.type.id))
      }
      if (params.staff) {
        inList('staff.id', UserInfor.findAllByNameIlike("%" + params.staff + "%").id)
      }
    }
    return listRequestInstance;
  }

  def compareDateFromDateTo(dateF, dateT) {
    println("date ===========> " + dateT + " class " + dateT.class)
    String dateFrom = dateF.toString()
    String dateTo = dateT.toString()
    SimpleDateFormat format = new SimpleDateFormat('yyyy-MM-dd')
    Date tempDateFrom
    Date tempDateTo
    def mapDate = [:]
    if (dateFrom != null && dateFrom != "" && dateTo != null && dateTo != "") {
      tempDateFrom = format.parse(dateFrom)
      tempDateTo = format.parse(dateTo)
      if (tempDateFrom.getTime() <= tempDateTo.getTime()) {
        mapDate['dateFrom'] = tempDateFrom;
        mapDate['dateTo'] = tempDateTo;
      }
      if (tempDateFrom.getTime() > tempDateTo.getTime()) {
        mapDate['dateFrom'] = tempDateTo;
        mapDate['dateTo'] = tempDateFrom;
      }
      return mapDate;
    }
    if ((dateFrom == null || dateFrom == "") && (dateTo != null && dateTo != "")) {
      tempDateFrom = format.format(dateTo)
      tempDateTo = format.format(dateTo)
      mapDate['dateFrom'] = tempDateTo;
      mapDate['dateTo'] = tempDateFrom;
      return mapDate;
    }
    if ((dateFrom != null && dateFrom != "") && (dateTo == null || dateTo == "")) {
      tempDateFrom = format.format(dateFrom)
      tempDateTo = format.format(dateFrom)
      mapDate['dateFrom'] = tempDateTo;
      mapDate['dateTo'] = tempDateFrom;
      return mapDate;
    }
    return null;
  }

  def loadRequestData(params, Requests requests) {
    SimpleDateFormat format = new SimpleDateFormat('yyyy-MM-dd')
    Requests requestInstance = new Requests()
    requestInstance.setDepart(requests.depart)
    requestInstance.setStaff(requests.staff)
    requestInstance.setType(requests.type)
    requestInstance.setExpiredAt(format.parse(params.expiredAt))
    requestInstance.setEffectedAt(format.parse(params.effectedAt))
    return requestInstance;
  }
}
