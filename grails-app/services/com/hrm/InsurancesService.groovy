package com.hrm


import grails.transaction.Transactional

import java.text.SimpleDateFormat

@Transactional
class InsurancesService {

  def serviceMethod() {
  }

  def find(params) {
    def mapEfftiveDate = compareDate(params.effectiveDateFrom + "", params.effectiveDateTo + "")
    println "params ===============> " + mapEfftiveDate

    def listInsurancesInstance = Insurances.createCriteria().list {
      or {
        if (params.name) {
          'ilike'('name', '%' + params.name + '%')
        }
        if (params.factor) {
          ilike('factor', '%' + params.factor + '%')
        }
        between('effectiveDate', mapEfftiveDate['dateFrom'], mapEfftiveDate['dateTo'])
      }


      maxResults(10)
      firstResult(params.firstResult)
      order('effectiveDate', 'desc')
    }
    return listInsurancesInstance
  }

  def collectInsuaranceFactor(dateFrom, dateTo, type) {
    def mapEfftiveDate = compareDate(params.effectiveDateFrom + "", params.effectiveDateTo + "")
    if (dateFrom && dateTo) {
      return Insurances.findByNameAndEffectiveDateBetween(type, mapEfftiveDate['dateFrom'], mapEfftiveDate['dateTo'])
    }
    return Insurances.findByNameAndEffectiveDateLessThanEquals(type, new Date(), [order: 'desc', sort: 'effectiveDate'])
  }

  def compareDate(dateFrom, dateTo) {
    SimpleDateFormat format = new SimpleDateFormat('yyyy-MM-dd')
    long tempDateFrom = 0
    long tempDateTo = 0
    if (dateFrom) {
      tempDateFrom = format.parse(dateFrom).getTime();
    }
    if (dateTo) {
      tempDateTo = format.parse(dateTo).getTime();
    }
    def mapTime = [:]
    if (tempDateFrom <= tempDateTo) {
      mapTime['dateFrom'] = new Date(tempDateFrom)
      mapTime['dateTo'] = new Date(tempDateTo)
    }
    if (tempDateFrom > tempDateTo) {
      mapTime['dateFrom'] = new Date(tempDateTo)
      mapTime['dateTo'] = new Date(tempDateFrom)
    }
    if (!dateFrom) {
      mapTime['dateFrom'] = new Date(tempDateTo)
      mapTime['dateTo'] = new Date(tempDateTo)
    }
    if (!dateTo) {
      mapTime['dateFrom'] = new Date(tempDateFrom)
      mapTime['dateTo'] = new Date(tempDateFrom)
    }
    if (!dateTo && !dateFrom) {
      mapTime['dateFrom'] = new Date()
      mapTime['dateTo'] = new Date()
    }
    return mapTime;
  }
}
