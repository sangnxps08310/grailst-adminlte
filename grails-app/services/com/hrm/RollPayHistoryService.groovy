package com.hrm

import com.hrm.common.CommonUtils
import grails.transaction.Transactional
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import java.sql.Date
import java.text.SimpleDateFormat
import java.time.YearMonth
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

@Transactional
class RollPayHistoryService {

  def EXPORT_FILE_NAME = "ROLL_PAY_"
  def dataSource
  def TAX_PAYER = 9000000;
  def TAX_DEPENDANT = 3600000;

  def exportExcelFile() {
    try {
      XSSFWorkbook workbook = new XSSFWorkbook();
      XSSFSheet sheet = workbook.createSheet("Java Books");

      Object[][] bookData = [
        [
          "Head First Java", "Kathy Serria", 79
        ],
        [
          "Effective Java", "Joshua Bloch", 36
        ],
        [
          "Clean Code", "Robert martin", 42
        ],
        [
          "Thinking in Java", "Bruce Eckel", 35
        ],
      ];

      int rowCount = 0;

      for (Object[] aBook : bookData) {
        Row row = sheet.createRow(++rowCount);

        int columnCount = 0;

        for (Object field : aBook) {
          Cell cell = row.createCell(++columnCount);
          if (field instanceof String) {
            cell.setCellValue((String) field);
          } else if (field instanceof Integer) {
            cell.setCellValue((Integer) field);
          }
        }
      }
      FileOutputStream outputStream = new FileOutputStream("/home/JavaBooks.xlsx")
      workbook.write(outputStream);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  def calcRolPay(int month, int year, mapBonusMoney = [:]) {
    def userWorkDays = collectWorkingday(month, year);
    def insuaranceInstance = collectNewInsurance();
    def usersSalary = collectUserSalary();
    println 'insuaranceInstance  => ' + insuaranceInstance;
    def listUserSalary = [];
    SimpleDateFormat format = new SimpleDateFormat('yyyy-MM-dd');
    if (userWorkDays.size() == 0) {
      return [];
    }
    YearMonth yearMonthObject = YearMonth.of(year, month);
    int daysInMonth = yearMonthObject.lengthOfMonth(); //28
    UserInfor.findAll().each { userInfor ->
      def userId = userInfor.id;
      long bonus = 0
      long allowance = 0
      if (mapBonusMoney.size() != 0) {
        if (mapBonusMoney[userId + '_bonus']) {
          bonus = Double.parseDouble(mapBonusMoney[userId + '_bonus'])
        }
        if (mapBonusMoney[userId + '_allowance']) {
          allowance = Double.parseDouble(mapBonusMoney[userId + '_allowance'])
        }
      }
      def userWork = userWorkDays.find { it -> it.user_info_id == userId }
      if (userWork) {
        def mapUserSalary = [:]
        def userSalary = usersSalary.find { usersalary -> usersalary.user_info_id == userId }
        double salary = 0;
        if (userSalary.salary) {
          println "salary =====> " + userSalary.salary
          salary = calcRankSalary(userSalary.salary, Double.parseDouble(userSalary.rank_salary));
        }
        def restSalary = calcCeaseDaySalary(salary, daysInMonth, userWork.work_days);
        def userInsurance = calcInsurance(salary, insuaranceInstance);
        def userTax = calcTax(salary, userInsurance, userInfor.dependant);
        def total = salary + allowance + bonus - userTax - userInsurance - restSalary;
        mapUserSalary['user_name'] = userInfor.name;
        mapUserSalary['user_id'] = userInfor.id;
        mapUserSalary['work_days'] = userWork.work_days;
        mapUserSalary['allowance'] = Math.round(allowance);
        mapUserSalary['bonus'] = Math.round(bonus);
        mapUserSalary['dependant'] = userInfor.dependant;
        mapUserSalary['rank_salary_name'] = userSalary.name_rank + "";
        mapUserSalary['rank_salary'] = userSalary.rank_salary + "";
        mapUserSalary['rank_salary_id'] = userSalary.rank_id + "";
        mapUserSalary['profession_name'] = userSalary.profession_name + "";
        mapUserSalary['profession_id'] = userSalary.profession_id + "";
        mapUserSalary['temp_salary'] = Math.round(salary) + '';
        mapUserSalary['rest_salary'] = restSalary + '';
        mapUserSalary['unauthorized_absence'] = (daysInMonth - userWork.work_days);
        mapUserSalary['insurance'] = Math.round(userInsurance);
        mapUserSalary['tax'] = Math.round(userTax) + '';
        mapUserSalary['total'] = Math.round(total) + '';
        mapUserSalary['effective_date'] = format.format(new Date(new java.util.Date().getTime()));
        println 'User salary => ' + total;
        listUserSalary << mapUserSalary;
      }
    }
    return listUserSalary;
  }

  def calcInsurance(salary, insurance) {
    println "insuarance ========> ${insurance.healthInsurance} - ${insurance.socialInsurance} - ${insurance.unEmploymentInsurance}"
    double bhld = insurance.unEmploymentInsurance / 100
    double bhtn = insurance.healthInsurance / 100
    double bhxh = insurance.socialInsurance / 100
    double temp_salary = (salary * bhld) + (salary * bhtn) + (salary * bhxh);
    return temp_salary;
  }

  def calcTax(salary, insurance, dependant = 0) {
    if (salary < TAX_PAYER) {
      return 0;
    }
    def taxableIncome = salary - (TAX_PAYER + insurance + (dependant * TAX_DEPENDANT));
    if (taxableIncome <= 0) {
      return 0
    }

    if (taxableIncome > 0 && taxableIncome <= 5000000) {
      return (taxableIncome) * (5 / 100);
    }
    if (taxableIncome > 5000000 && taxableIncome <= 10000000) {
      return (taxableIncome) * (10 / 100);
    }
    if (taxableIncome > 10000000 && taxableIncome <= 18000000) {
      return (taxableIncome) * (15 / 100);
    }
    if (taxableIncome > 18000000 && taxableIncome <= 32000000) {
      return (taxableIncome) * (20 / 100);
    }
    if (taxableIncome > 32000000 && taxableIncome <= 52000000) {
      return (taxableIncome) * (25 / 100);
    }
    if (taxableIncome > 52000000 && taxableIncome <= 80000000) {
      return (taxableIncome) * (30 / 100)
    }
    if (taxableIncome > 80000000) {
      return (taxableIncome) * (35 / 100)
    }
    return 0;
  }

  def calcRankSalary(salary, rank) {
    return salary + salary * rank;
  }

  def calcCeaseDaySalary(salary, totalDays, workDays) {
    double salaryPerday = salary / totalDays;
    int restDays = totalDays - workDays;
    return restDays * salaryPerday;
  }

  def collectUserSalary() {
    println "============ User Salary ================"
    def sql = new groovy.sql.Sql(dataSource);
    String query = 'SELECT\n' +
      '\tui.id as user_info_id,\n' +
      '\trv.entrance_salary as salary,\n' +
      '\trank.name_rank,\n' +
      '\trank.rank_salary,\n' +
      '\trank.id AS rank_id,\n' +
      '\tpf.id AS profession_id,\n' +
      '\tpf.profession_name \n' +
      'FROM\n' +
      '\treview_salary rv\n' +
      '\tRIGHT JOIN user_info ui ON rv.user_id_id = ui.id\n' +
      '\tLEFT JOIN rank_salary rank ON rank.id = rv.rank_salary_id\n' +
      '\tLEFT JOIN profession pf ON pf.id = rank.profession_id\n' +
      'WHERE\n' +
      '\trv.id IN ( SELECT max( id ) AS id FROM review_salary GROUP BY user_id_id );';
    println "Query =====> " + query;

    println "=============================================="
    return sql.rows(query)
  }

  def collectWorkingday(int month, int year) {
    println "============ Collect work days ================"
    def listDepartId = Depart.findAll();
    def sql = new groovy.sql.Sql(dataSource);
    String query = 'SELECT att.id FROM Attendance att WHERE MONTH(created_date) = :month AND YEAR(created_date) = :year AND depart_id IN (:depart_id)';
    println "depart_ids ==>" + listDepartId.id.join(',');
    def attendance = Attendance.executeQuery(query, ['month': month, 'year': year, 'depart_id': listDepartId.id.join(',')]);
    if (attendance.size() == 0) {
      return [];
    }
    query = "SELECT attdt.user_info_id , COUNT(attdt.present) as work_days  FROM attendance_details attdt WHERE attdt.present = 1 AND attdt.attendance_id IN (${attendance.join(',')}) GROUP BY attdt.user_info_id";
    println "query =======>    " + query
    def userWorkDay = sql.rows(query);
    println "=============================================="
    return userWorkDay;
  }

  def collectNewTax() {
    println "============ Collect tax ================"
    def sql = new groovy.sql.Sql(dataSource);
    String query = "SELECT factor FOM tax WHERE effective_date <= NOW() ORDER BY effective_date DESC LIMIT 1";
    println "=============================================="
    return sql.firstRow(query);
  }

  def collectNewInsurance() {
    println "============ Collect insurance ================"
    def sql = new groovy.sql.Sql(dataSource);
    String query = "SELECT  * from insurances WHERE effective_date <= NOW() ORDER BY effective_date DESC LIMIT 1;";
    def insuaranceInstance = Insurances.findByEffectiveDateLessThanEquals(new Date(new java.util.Date().getTime()))
    println "=============================================="
//        return sql.rows(query);
    return insuaranceInstance;
  }

  def importRollPayDataToDb(mapData, rollPayHistoryId) {
    def sql = new groovy.sql.Sql(dataSource);
    sql.withBatch(mapData.size()) { stm ->
      mapData.each {
        String insertQuery = convertDataToInsertQuery(it, rollPayHistoryId);
        println "Query ====> " + insertQuery
        stm.addBatch(insertQuery);
      }
    }
  }

  def convertDataToInsertQuery(mapData, rollPayHistoryId) {
    String query = "INSERT INTO roll_pay (" +
      "version," +
      "allowance," +
      "bonus," +
      "tax," +
      "total," +
      "work_day," +
      "rank_salary_id," +
      "temp_salary," +
      "user_infor_id," +
      "effective_date," +
      "insurance," +
      "roll_pay_history_id) VALUES (0,${mapData['allowance']},${mapData['bonus']},${mapData['tax']},${mapData['total']},${mapData['work_days']},${mapData['rank_salary_id']},${mapData['temp_salary']},${mapData['user_id']},'${mapData['effective_date']}',${mapData['insurance']},${rollPayHistoryId})";
    return query
  }

  def exportRollPayToExcelFile(mapRollPay, rollPayHistoryId) {
    try {
      println "=============================== Export file=========================================="

      XSSFWorkbook workbook = new XSSFWorkbook();
      XSSFSheet sheet = workbook.createSheet("ROLLPAY");

      int rowCount = 0;
      def header = ['#', 'Name', 'Work Days', 'Allowance', 'Bonus', 'Rank Salary', 'Profession Name', 'Salary', 'Unauthorized Absence', 'Insurance Pay', 'Personal Income Tax', 'Net salary', 'Effective Date'];

      Row row = sheet.createRow(0);
      int columnHeaderCount = 0;
      println "set header ..."
      header.each {
        println "header ========> " + it
        Cell cell = row.createCell(columnHeaderCount++);
        cell.setCellValue((String) it);
      }
      rowCount = 1
      println "set content ..."
      mapRollPay.each { rollPay ->
        row = sheet.createRow(rowCount++);
        println "rollPay ========> " + rollPay
        Cell cell = row.createCell(0);
        cell.setCellValue((Integer) rowCount - 1);
        cell = row.createCell(1);
        cell.setCellValue((String) rollPay['user_name']);
        cell = row.createCell(2);
        cell.setCellValue((Integer) rollPay['work_days']);
        cell = row.createCell(3);
        cell.setCellValue((Integer) rollPay['allowance']);
        cell = row.createCell(4);
        cell.setCellValue((Integer) rollPay['bonus']);
        cell = row.createCell(5);
        cell.setCellValue((String) rollPay['rank_salary']);
        cell = row.createCell(6);
        cell.setCellValue((String) rollPay['profession_name']);
        cell = row.createCell(7);
        cell.setCellValue((String) rollPay['temp_salary']);
        cell = row.createCell(8);
        cell.setCellValue((Integer) rollPay['unauthorized_absence']);
        cell = row.createCell(9);
        cell.setCellValue((Integer) rollPay['insurance']);
        cell = row.createCell(10);
        cell.setCellValue((String) rollPay['tax']);
        cell = row.createCell(11);
        cell.setCellValue((String) rollPay['total']);
        cell = row.createCell(12);
        cell.setCellValue((String) rollPay['effective_date']);
      }
      SimpleDateFormat format = new SimpleDateFormat('yyyyMMddHHmmss')
      String fileName = EXPORT_FILE_NAME + format.format(new java.util.Date()) + CommonUtils.EXCEL_SUFFIX; ;
      println "file name ====> " + fileName

      String path = CommonUtils.EXPORT_PATH + '/' + rollPayHistoryId
      println "file path ====> " + path
      File folder = new File(path); ;
      if (!folder.exists()) {
        folder.mkdirs();
      }
      File file = new File(folder, fileName);
      FileOutputStream outputStream = new FileOutputStream(file)

      workbook.write(outputStream);
      println "Done...!! ";
      println "========================================================================="
      return fileName;
    } catch (Exception e) {
      e.printStackTrace();
      throw new Exception();
    }
  }
}
