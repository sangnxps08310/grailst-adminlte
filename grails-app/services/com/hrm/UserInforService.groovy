package com.hrm

import GUtils.FileUtils
import com.hrm.common.CommonUtils
import grails.transaction.Transactional
import org.springframework.web.multipart.commons.CommonsMultipartFile

import java.text.SimpleDateFormat

@Transactional
class UserInforService {
  FileUtils fileUtils

  def find(params) {
    println("params ================> " + params)
    def listUserInfor = UserInfor.createCriteria().list(params) {
        or {
          if (params.name) {
            like('name', '%' + params.name + '%')
          }
          if (params.name) {
            like('name', '%' + params.name + '%')
          }
          if (params.personalId) {
            like('personalId', '%' + params.personalId + '%')
          }
          if (params.level) {
            like('level', '%' + params.level + '%')
          }
          if (params.homeAddress) {
            like('homeAddress', '%' + params.homeAddress + '%')
          }
        }


      if (params.homeProvince.id != '0') {
        eq('homeProvince.id', Province.findByProvince_code(Integer.parseInt(params.homeProvince.id)).id)
      }
      if (params.homeDistrict.id != '0') {
        eq('homeDistrict.id', District.findByDistrict_code(Integer.parseInt(params.homeDistrict.id)).id)
      }
      if (params.homeWard.id != '0') {
        eq('homeWard.id', Ward.findByWard_code(Integer.parseInt(params.homeWard.id)).id)
      }
      if (params.laborContract.id != '0') {
        eq('laborContract.id', LaborContract.findById(Long.parseLong(params.laborContract.id)).id)
      }
    }
    return listUserInfor;
  }

  def saveUserImage(CommonsMultipartFile file, String path = null) {
    println("============== Upload file ==============")
    if (file) {
      CommonsMultipartFile srcFile = file
      String tempPath = "";
      if (path) {
        tempPath = "/" + path;
      }
      File folderUpload = new File(CommonUtils.IMAGES_PATH + tempPath)
      if (!folderUpload.exists()) {
        folderUpload.mkdirs();
      }
      if (srcFile.getSize() < 0) {
        return false;
      }
      if (!(srcFile.originalFilename.endsWith('png') || srcFile.originalFilename.endsWith('PNG')
        || srcFile.originalFilename.endsWith('jpg') || srcFile.originalFilename.endsWith('jpeg')
        || srcFile.originalFilename.endsWith('JPEG') || srcFile.originalFilename.endsWith('JPG'))) {
        return false;
      }
      File dstFile = new File(folderUpload.getAbsolutePath(), srcFile.originalFilename)
      srcFile.transferTo(dstFile);
      println("==============End Upload file ==============")
      return true;
    }
  }

  def loadUserInforInstance(params, UserInfor userInfor) {
    SimpleDateFormat format = new SimpleDateFormat('yyyy-MM-dd')
    userInfor.setName(params.name)
    userInfor.setPhoto(params.photo)
    userInfor.setGender(Boolean.parseBoolean(params.gender))
    userInfor.setDateOfBirth(format.parse(params.dateOfBirth))
    userInfor.setPhoneNumber(params.phoneNumber)
    userInfor.setEmail(params.email)
    userInfor.setCertificate(params.certificate)
    userInfor.setCertificatePhoto(params.certificatePhoto)
    userInfor.setEntranceSalary(Float.parseFloat(pareStringToNumber(params.entranceSalary + "")))
    userInfor.setHomeAddress(params.homeAddress)
    userInfor.setHomeDistrict(District.findByDistrict_code(Integer.parseInt(params.homeDistrict.id)))
    userInfor.setHomeProvince(Province.findByProvince_code(Integer.parseInt(params.homeProvince.id)))
    userInfor.setHomeWard(Ward.findByWard_code(Integer.parseInt(params.homeWard.id)))
    userInfor.setLaborContract(LaborContract.findById(Integer.parseInt(params.laborContract.id)))
    userInfor.setLevel(params.level)
    userInfor.setPermanentAddress(params.permanentAddress)
    userInfor.setPermanentDistrict(District.findByDistrict_code(Integer.parseInt(params.homeDistrict.id)))
    userInfor.setPermanentProvince(Province.findByProvince_code(Integer.parseInt(params.homeProvince.id)))
    userInfor.setPermanentWard(Ward.findByWard_code(Integer.parseInt(params.homeWard.id)))
    userInfor.setPersonalId(params.personalId)
    userInfor.setPersonalIdPhotoA(params.personalIdPhotoA)
    userInfor.setReleaseDate(format.parse(params.releaseDate))
    userInfor.setReleaseIn(params.releaseIn)
    userInfor.setStatus(params.status)

    return userInfor;
  }

  def pareStringToNumber(String string) {
    String number = string
    if (string.contains(',')) {
      number = string.replaceAll(",", "");
    }
    if (string.contains('.')) {
      number = string.replaceAll(",", "");
    }

    return number;
  }

  def getRestDaysRemain(userInfor) {
    println "======== RemainDate ==============="
    def requests = Requests.findAllByStaffAndStatus(userInfor, 'ACCEPTED');
    def listRestDays = []
    requests.each { request ->

      def atendance = Attendance.findByCreatedDateBetween(request.effectedAt, request.expiredAt);
      println "atendance ========> " + atendance

      if (atendance) {
        println "========> OK"
      }
      def attendanceDetails = AttendanceDetails.findByAttendanceAndUserInfo(atendance, userInfor);
      println "attendanceDetails ========> " + attendanceDetails
      if (attendanceDetails) {
        println "is rest Days ========>  OK"
        listRestDays << attendanceDetails.id
      }

    }
    println "Rest Days ========>  " + listRestDays.size()
    return listRestDays.size();
  }

  def addDays(Date date, second = 0, minute = 0, hour = 0, day = 0, month = 0, year = 0) {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    Calendar cal = Calendar.getInstance();
    cal.setTime(date)
    cal.add(Calendar.SECOND, second);
    cal.add(Calendar.MINUTE, minute);
    cal.add(Calendar.HOUR_OF_DAY, hour);
    cal.add(Calendar.DAY_OF_MONTH, day);
    cal.add(Calendar.MONTH, month);
    cal.add(Calendar.YEAR, year);
    String newDate = sdf.format(cal.getTime());
    System.out.println("Date after Addition: " + newDate);
  }

  def setDays(second = 0, minute = 0, hour = 0, day = 0, month = 0, year = 0) {
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.SECOND, second);
    cal.set(Calendar.MINUTE, minute);
    cal.set(Calendar.HOUR_OF_DAY, hour);
    cal.set(Calendar.DAY_OF_MONTH, day);
    cal.set(Calendar.MONTH, month);
    cal.set(Calendar.YEAR, year);
    return cal.getTime();
  }

}
