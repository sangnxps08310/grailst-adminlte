package com.hrm

import grails.transaction.Transactional

@Transactional
class RankSalaryService {

    def serviceMethod() {

    }

    def find(params) {
        def listRankSalaryInstance = RankSalary.createCriteria().list {
            or{
                if (params.nameRank) {
                    'like'('nameRank', '%'+params.nameRank+'%')
                }
                if (params.rankSalary) {
                    'like'('rankSalary', '%'+params.rankSalary +'%')
                }

            }
            if (params.profession){
                if (params.profession.id != '0') {
                    eq('profession.id', Profession.findById(Integer.parseInt(params.profession.id)).id)
                }
                println("=====> NGOC ANH ")
            }

            maxResults(10)
            firstResult(params.firstResult)

        }
        return listRankSalaryInstance;
    }
}
