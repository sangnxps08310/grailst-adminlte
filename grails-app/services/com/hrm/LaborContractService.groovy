package com.hrm

import GUtils.FileUtils
import com.hrm.common.CommonUtils
import grails.transaction.Transactional
import org.springframework.web.multipart.commons.CommonsMultipartFile

import java.text.SimpleDateFormat

// Ngoc Anh
@Transactional
class LaborContractService {
  FileUtils fileUtils

  def serviceMethod() {

  }

  def saveContractImage(CommonsMultipartFile file, String path = null) {
    println("============== Upload file ==============")
    if (file) {
      CommonsMultipartFile srcFile = file
      String tempPath = "";
      if (path) {
        tempPath = "/" + path;
      }
      File folderUpload = new File(CommonUtils.IMAGES_PATH + tempPath)
      if (!folderUpload.exists()) {
        folderUpload.mkdirs();
      }
      if (srcFile.getSize() < 0) {
        return false;
      }
      if (!(srcFile.originalFilename.endsWith('png') || srcFile.originalFilename.endsWith('PNG')
        || srcFile.originalFilename.endsWith('jpg') || srcFile.originalFilename.endsWith('jpeg')
        || srcFile.originalFilename.endsWith('JPEG') || srcFile.originalFilename.endsWith('JPG'))
      ) {
        return false;
      }
      File dstFile = new File(folderUpload.getAbsolutePath(), srcFile.originalFilename)
      srcFile.transferTo(dstFile);
      println("==============End Upload file ==============")
      return true;
    }
  }

  def saveContractFile(CommonsMultipartFile file, String path = null) {
    println("============== Upload file ==============")
    if (file) {
      CommonsMultipartFile srcFile = file
      String tempPath = "";
      if (path) {
        tempPath = "/" + path;
      }
      println("==============> " + path)
      File folderUpload = new File(CommonUtils.FILES_PATH + tempPath)
      if (!folderUpload.exists()) {
        folderUpload.mkdirs();
      }
      println("==============> " + folderUpload.absolutePath)
      if (srcFile.getSize() < 0) {
        return false;
      }
      println("src ==============> " + srcFile.name)
      File dstFile = new File(folderUpload.getAbsolutePath(), srcFile.originalFilename)
      if (dstFile.exists()) {
        return true;
      }
      srcFile.transferTo(dstFile);
      println("==============End Upload file ==============")
      return true;
    }
  }

  def find(params) {
    def mapExpiredDate = compareDate(params.expiredDateFrom + "", params.expiredDateTo + "")
    def listLaborContractInstance = LaborContract.createCriteria().list {
      or {
        if (params.contractNumber) {
          ilike('contractNumber', '%' + params.contractNumber + '%')
        }
        if (params.type) {
          if (params.type.id != '0') {
            eq('type.id', TypesOfContract.findById(Integer.parseInt(params.type.id)).id)
          }
        }

        if (params.profession) {
          if (params.profession.id != '0') {
            eq('profession.id', Profession.findById(Integer.parseInt(params.profession.id)).id)
          }
        }

        if (params.employeeName) {
          like('employeeName', '%' + params.employeeName + '%')
        }
        if (params.employerName) {
          like('employerName', '%' + params.employerName + '%')
        }
        between('expiredDate', mapExpiredDate['dateFrom'], mapExpiredDate['dateTo'])
      }
      maxResults(10)
      firstResult(params.firstResult)
      order('expiredDate', 'desc')
    }

    return listLaborContractInstance;
  }

  def compareDate(dateFrom, dateTo) {
    SimpleDateFormat format = new SimpleDateFormat('yyyy-MM-dd')
    long tempDateFrom = 0
    long tempDateTo = 0
    if (dateFrom) {
      tempDateFrom = format.parse(dateFrom).getTime();
    }
    if (dateTo) {
      tempDateTo = format.parse(dateTo).getTime();
    }
    def mapTime = [:]
    if (tempDateFrom <= tempDateTo) {
      mapTime['dateFrom'] = new Date(tempDateFrom)
      mapTime['dateTo'] = new Date(tempDateTo)
    }
    if (tempDateFrom > tempDateTo) {
      mapTime['dateFrom'] = new Date(tempDateTo)
      mapTime['dateTo'] = new Date(tempDateFrom)
    }
    if (!dateFrom) {
      mapTime['dateFrom'] = new Date(tempDateTo)
      mapTime['dateTo'] = new Date(tempDateTo)
    }
    if (!dateTo) {
      mapTime['dateFrom'] = new Date(tempDateFrom)
      mapTime['dateTo'] = new Date(tempDateFrom)
    }
    if (!dateTo && !dateFrom) {
      mapTime['dateFrom'] = new Date()
      mapTime['dateTo'] = new Date()
    }
    return mapTime;
  }
}
