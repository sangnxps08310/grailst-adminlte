package com.hrm

import grails.transaction.Transactional

import java.text.SimpleDateFormat

@Transactional
class TaxService {

    def serviceMethod() {

    }

    def find(params) {
        def mapEfftiveDate = compareDateTax(params.effectiveDateFrom + "", params.effectiveDateTo + "")
        println "params ===============> " +mapEfftiveDate

        def listTaxInstance = Tax.createCriteria().list {
            or{
                if (params.nameTax) {
                    'ilike'('nameTax', '%'+params.nameTax+'%')
                }
                if (params.percent) {
                    ilike('percent', '%'+params.percent +'%')
                }

                between('effectiveDate', mapEfftiveDate['dateFrom'], mapEfftiveDate['dateTo'])
            }


            maxResults(10)
            firstResult(params.firstResult)
            order('effectiveDate', 'desc')
        }
        return listTaxInstance;
    }
    def compareDateTax(dateFrom, dateTo) {
        SimpleDateFormat format = new SimpleDateFormat('yyyy-MM-dd')
        long tempDateFrom = 0
        long tempDateTo = 0
        if (dateFrom) {
            tempDateFrom = format.parse(dateFrom).getTime();
        }
        if (dateTo) {
            tempDateTo = format.parse(dateTo).getTime();
        }
        def mapTime = [:]
        if (tempDateFrom <= tempDateTo) {
            mapTime['dateFrom'] = new Date(tempDateFrom)
            mapTime['dateTo'] = new Date(tempDateTo)
        }
        if (tempDateFrom > tempDateTo) {
            mapTime['dateFrom'] = new Date(tempDateTo)
            mapTime['dateTo'] = new Date(tempDateFrom)
        }
        if (!dateFrom) {
            mapTime['dateFrom'] = new Date(tempDateTo)
            mapTime['dateTo'] = new Date(tempDateTo)
        }
        if (!dateTo) {
            mapTime['dateFrom'] = new Date(tempDateFrom)
            mapTime['dateTo'] = new Date(tempDateFrom)
        }
        if (!dateTo && !dateFrom) {
            mapTime['dateFrom'] = new Date()
            mapTime['dateTo'] = new Date()
        }
        return mapTime;
    }
}
