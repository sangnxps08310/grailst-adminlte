package com.hrm

import grails.transaction.Transactional

@Transactional
class ProfessionService {

    def serviceMethod() {

    }
    def find(params) {
        def listProfessionInstance = Profession.createCriteria().list {
            or{
                if (params.professionName) {
                    'like'('professionName', '%'+params.professionName+'%')
                }
                if (params.rank) {
                    'like'('rank', '%'+params.rank +'%')
                }
            }
            maxResults(10)
            firstResult(params.firstResult)

        }
        return listProfessionInstance;
    }
}
