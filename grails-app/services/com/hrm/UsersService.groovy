package com.hrm

import grails.transaction.Transactional
import org.springframework.security.crypto.bcrypt.BCrypt

@Transactional
class UsersService {
  private static int workload = 12;

  def hasPlaintext(String password_plaintext) {
    String salt = BCrypt.gensalt(workload);
    String hashed_password = BCrypt.hashpw(password_plaintext, salt);
    return (hashed_password);
  }

  def serviceMethod() {

  }
}
