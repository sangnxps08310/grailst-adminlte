package com.hrm


import grails.transaction.Transactional

import java.text.SimpleDateFormat

@Transactional
class ReviewSalaryService {

    def serviceMethod() {

    }

    def find(params) {
        def mapEfftiveDate = compareDate(params.effectiveDateFrom + "", params.effectiveDateTo + "")
//        def mapCreatedDate = compareDate(params.createdDateFrom + "", params.createdDateTo + "")
        println "params ===============> " + mapEfftiveDate

        def listReviewSalaryInstance = ReviewSalary.createCriteria().list {
            or {
                if (params.userId) {
                    if (params.userId.id != '0') {
                        eq('userId.id', UserInfor.findById(Integer.parseInt(params.userId.id)).id)
                    }
                }
                if (params.rankSalary) {
                    if (params.rankSalary.id != '0') {
                        eq('rankSalary.id', RankSalary.findById(Integer.parseInt(params.rankSalary.id)).id)
                    }
                }

                if (params.entranceSalary) {
                    'eq'("entranceSalary", Float.parseFloat(params.entranceSalary))
                    print("innnnnnnn=> " + params.entranceSalary)

                }
                if (params.newSalary) {
                    'eq'("newSalary", Float.parseFloat(params.newSalary))
                    print("annnnnnnn=> " + params.newSalary)
                }

                between('effectiveDate', mapEfftiveDate['dateFrom'], mapEfftiveDate['dateTo'])
            }
            maxResults(10)
            firstResult(params.firstResult)
            order('effectiveDate', 'desc')
        }
        return listReviewSalaryInstance;
    }

    def compareDate(dateFrom, dateTo) {
        SimpleDateFormat format = new SimpleDateFormat('yyyy-MM-dd')
        long tempDateFrom = 0
        long tempDateTo = 0
        if (dateFrom) {
            tempDateFrom = format.parse(dateFrom).getTime();
        }
        if (dateTo) {
            tempDateTo = format.parse(dateTo).getTime();
        }
        def mapTime = [:]
        if (tempDateFrom <= tempDateTo) {
            mapTime['dateFrom'] = new Date(tempDateFrom)
            mapTime['dateTo'] = new Date(tempDateTo)
        }
        if (tempDateFrom > tempDateTo) {
            mapTime['dateFrom'] = new Date(tempDateTo)
            mapTime['dateTo'] = new Date(tempDateFrom)
        }
        if (!dateFrom) {
            mapTime['dateFrom'] = new Date(tempDateTo)
            mapTime['dateTo'] = new Date(tempDateTo)
        }
        if (!dateTo) {
            mapTime['dateFrom'] = new Date(tempDateFrom)
            mapTime['dateTo'] = new Date(tempDateFrom)
        }
        if (!dateTo && !dateFrom) {
            mapTime['dateFrom'] = new Date()
            mapTime['dateTo'] = new Date()
        }
        return mapTime;
    }
}
