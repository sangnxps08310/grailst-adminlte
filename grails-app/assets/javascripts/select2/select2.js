//Select2
$.getScript('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js',function(){

  /* dropdown and filter select */
  var select1 = $('#select2').select2()
    .on("select2:select", function(e) {
      var selectValue = $(this).select2("data")[0];
      console.log(selectValue.id, selectValue.text);
    });

  /* Select2 plugin as tagpicker */
  var select2 = $("#tagPicker").select2({
    closeOnSelect:false
  })
    .on("select2:select", function(e) {
      var selectValues = $(this).select2("data");
      var values = selectValues.map(function(d) {
        return { ID: d.id, Name: d.text };
      });
      console.log(values);
    });

}); //script


$(document).ready(function() {});