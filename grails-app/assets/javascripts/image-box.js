$(window).load(function winLoad() {
  $('.clear-image').click(function clearImage() {
    $('.image-item').remove();
    $('#photo-path').val('');
    $('.box-image').hide();

  });
  $(document).load(function () {
    if ($('#photo-path').val() == "") {
      $('.box-image').hide();
      $('.box-image image').prop('src', '');
      $('.box-image image').hide();
    }
  });

  function readURL(input, id) {
    if (input.files) {
        var reader = new FileReader();
        reader.onload = function loadImage(e) {
          $('#box_'+id+' img').attr('src', e.target.result);
        }
        console.log(input.files[0].name)
        $('#' + id).val(input.files[0].name)
        reader.readAsDataURL(input.files[0]);
      }
  }

  $.each($("input:file"), function () {
    $(this).change(function () {
      if($(this).val().endsWith('.png')||$(this).val().endsWith('.jpg')||$(this).val().endsWith('.jpeg')){
        var name = $(this).attr('name');
        $('#box_' + name).show();
        $('#box_' + name).empty();
        $('#box_' + name).append(generateImageBox);
        readURL(this, name);
      }
    });
  });
  //
  // function loadImage(id) {
  //   var name = $(this).attr('name');
  //   $('#box-' + name).show();
  //   $('#box-' + name).empty();
  //   $('#box-' + name).append(generateImageBox);
  //   readURL(this, name);
  // }
  function generateImageBox() {
    return ' <div class="image-item">\n' +
      '      <img src="" width="150" height="150" id="image" alt="no-image">\n' +
      // '      <button type="button" class="image-button">&times;</button>\n' +
      '    </div>'
  }
});