$(document).ready(function () {
    var tag_i = $('.btn-search-toggle i');
    if ($('#search-box').is(':visible')) {
        tag_i.attr('class', 'fa fa-minus');
    } else {
        tag_i.attr('class', 'fa fa-plus');
    }
    $('.btn-search-toggle').click(function () {
        if (!$('#search-box').is(':visible')) {
            tag_i.attr('class', 'fa fa-minus');
        } else {
            tag_i.attr('class', 'fa fa-plus');
        }
    });
});