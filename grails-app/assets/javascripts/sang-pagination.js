$(document).ready(function () {
  var tempHtml =$('.sang-pagination').children() ;
  var html =$('.sang-pagination').html("<nav aria-label=\"Page navigation example\">\n" +
      "  <ul class=\"pagination justify-content-center\" id='sang-pg'>\n" +
      "    \n" +
      "  </ul>\n" +
      "</nav>");
  $.each(tempHtml,function () {
    var tagName =$(this).prop('tagName');
    var tagClass =$(this).prop('class')
    var tagHref =$(this).prop('href')
    var tagFull ="\n<li class=\"page-item\"><"+tagName.toLowerCase()+" href='"+tagHref+"' class='"+tagClass+"'>"+$(this).html()+"</"+tagName.toLowerCase()+"></li>"
    $('#sang-pg').append(tagFull);
  });
});