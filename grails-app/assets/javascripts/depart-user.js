$(document).ready(function () {

  $('#is-room-master').change(function () {
      if($(this).is(':checked')){
        $('#is-leader').attr('disabled','')
      }else {
        $('#is-leader').removeAttr('disabled')
      }
  });

  $('#is-leader').change(function () {
    if($(this).is(':checked')){
      $('#is-room-master').attr('disabled','')
    }else {
      $('#is-room-master').removeAttr('disabled')
    }
  });
});