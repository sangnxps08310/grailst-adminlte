package com.hrm

class Province {
  long version = 0
  int province_code
  String name

  static constraints = {
    version defaultValue: 0
  }
  static mapping = {
    id column: 'province_id'
    name sqlType: 'varchar(100)'

  }

}
