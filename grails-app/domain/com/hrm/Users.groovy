package com.hrm

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString


@EqualsAndHashCode(includes = 'username')
@ToString(includes = 'username', includeNames = true, includePackage = false)
class Users implements Serializable {

    private static final long serialVersionUID = 1

    transient springSecurityService

    String username
    String password
    UserInfor userInfor
    boolean enabled = true
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired

    Users(String username, String password) {
        this()
        this.username = username
        this.password = password
    }

    Set<Role> getAuthorities() {
        UsersRole.findAllByUsers(this)*.role
    }

    def beforeInsert() {
        encodePassword()
    }

    def beforeUpdate() {
        if (isDirty('password')) {
            encodePassword()
        }
    }

    protected void encodePassword() {
        password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
    }

    static transients = ['springSecurityService']

    static constraints = {
        username blank: false, unique: true
        password blank: false
        userInfor nullable: true, min: 0
    }

    static mapping = {
        password column: '`password`'
    }
}
