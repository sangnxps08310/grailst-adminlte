package com.hrm
import java.sql.Date

class LaborContract {

    String contractNumber
    String employeeName
    String employerName
    Date employerDateOfBirth = new Date(new java.util.Date().getTime())
    Date employeeDateOfBirth = new Date(new java.util.Date().getTime())
    String employerPhoneNumber
    String employeePhoneNumber
    String content
    String photo
    String fileFolder
    Date createdDate = new Date(new java.util.Date().getTime())
    Date signDate = new Date(new java.util.Date().getTime())
    Date effectiveDate = new Date(new java.util.Date().getTime())
    Date expiredDate = new Date(new java.util.Date().getTime())

    static belongsTo = [type:TypesOfContract, profession:Profession] // position
    static constraints = {
        contractNumber unique: true
    }
    static mapping = {
    }
}
