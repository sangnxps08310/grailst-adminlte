package com.hrm

class Requests {
    static belongsTo = [staff:UserInfor,depart:Depart, type:RequestType]
    String status
    Date createdDate = new Date()
    Date acceptedAt
    Date effectedAt
    Date expiredAt
    static constraints = {
        createdDate nullable: true, blank:true
        acceptedAt nullable: true, blank:true
        effectedAt nullable: true, blank:true
        expiredAt nullable: true, blank:true
    }
    static mapping = {
    }
}
