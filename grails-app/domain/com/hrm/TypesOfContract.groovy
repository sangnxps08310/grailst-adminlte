package com.hrm

class TypesOfContract {
  String name
  String notes
  static constraints = {
    name nullable: false, blank: false
    notes nullable: true, blank: true
  }
  static mapping = {
    name sqlType: 'varchar(100)'
    notes sqlType: 'varchar(100)'
  }
}
