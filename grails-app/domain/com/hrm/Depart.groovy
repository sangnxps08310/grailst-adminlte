package com.hrm

class Depart {

    String name
    Date createdDate = new Date()
    static constraints = {
        name nullable: false, blank: false
        version defaultValue: 0
    }
    static mapping = {
        table 'depart'
        name sqlType: 'varchar(50)', length: 50
        createdDate sqlType: 'datetime'
    }
}
