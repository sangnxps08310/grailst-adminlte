package com.hrm

class Ward {
  long version = 0
  int province_code
  int district_code
  int ward_code
  String name

  static constraints = {
  }
  static mapping = {
    id column: 'id_ward'
    name sqlType: 'varchar(100)'

  }
}
