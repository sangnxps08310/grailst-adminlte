package com.hrm

class RequestType {
    String name
    String description
    static constraints = {
        description blank: true, nullable: true
    }
    static mapping = {
        description sqlType: 'varchar(100)'
    }
}
