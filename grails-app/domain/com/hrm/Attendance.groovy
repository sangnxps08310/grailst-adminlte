package com.hrm

class Attendance {

    Date createdDate = new Date()
    static belongsTo = [depart: Depart]
    String status = 'WAITING'
    static constraints = {
        createdDate nullable: false, blank: false
    }
    static mapping = {
        table 'attendance'
        createdDate sqlType: 'datetime'
    }
}
