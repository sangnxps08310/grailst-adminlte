package com.hrm

import grails.gorm.DetachedCriteria
import groovy.transform.ToString

import org.apache.commons.lang.builder.HashCodeBuilder

@ToString(cache=true, includeNames=true, includePackage=false)
class UsersRole implements Serializable {

	private static final long serialVersionUID = 1

	Users users
	Role role

	UsersRole(Users u, Role r) {
		this()
		users = u
		role = r
	}

	@Override
	boolean equals(other) {
		if (!(other instanceof UsersRole)) {
			return false
		}

		other.users?.id == users?.id && other.role?.id == role?.id
	}

	@Override
	int hashCode() {
		def builder = new HashCodeBuilder()
		if (users) builder.append(users.id)
		if (role) builder.append(role.id)
		builder.toHashCode()
	}

	static UsersRole get(long usersId, long roleId) {
		criteriaFor(usersId, roleId).get()
	}

	static boolean exists(long usersId, long roleId) {
		criteriaFor(usersId, roleId).count()
	}

	private static DetachedCriteria criteriaFor(long usersId, long roleId) {
		UsersRole.where {
			users == Users.load(usersId) &&
			role == Role.load(roleId)
		}
	}

	static UsersRole create(Users users, Role role, boolean flush = false) {
		def instance = new UsersRole(users: users, role: role)
		instance.save(flush: flush, insert: true)
		instance
	}

	static boolean remove(Users u, Role r, boolean flush = false) {
		if (u == null || r == null) return false

		int rowCount = UsersRole.where { users == u && role == r }.deleteAll()

		if (flush) { UsersRole.withSession { it.flush() } }

		rowCount
	}

	static void removeAll(Users u, boolean flush = false) {
		if (u == null) return

		UsersRole.where { users == u }.deleteAll()

		if (flush) { UsersRole.withSession { it.flush() } }
	}

	static void removeAll(Role r, boolean flush = false) {
		if (r == null) return

		UsersRole.where { role == r }.deleteAll()

		if (flush) { UsersRole.withSession { it.flush() } }
	}

	static constraints = {
		role validator: { Role r, UsersRole ur ->
			if (ur.users == null || ur.users.id == null) return
			boolean existing = false
			UsersRole.withNewSession {
				existing = UsersRole.exists(ur.users.id, r.id)
			}
			if (existing) {
				return 'userRole.exists'
			}
		}
	}

	static mapping = {
		id composite: ['users', 'role']
		version false
	}
}
