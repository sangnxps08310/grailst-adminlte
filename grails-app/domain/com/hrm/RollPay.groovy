package com.hrm

class RollPay {
    String workDay
    String tempSalary
    String allowance
    String bonus
    String total
    String insurance
    String tax
    Date effectiveDate
    static belongsTo = [userInfor:UserInfor, rankSalary:RankSalary, rollPayHistory:RollPayHistory]
    static constraints = {
    }
}
