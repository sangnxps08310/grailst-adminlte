package com.hrm

class DepartUser {

    boolean is_leader
    boolean is_room_master
    static belongsTo = [depart: Depart, userInfo: UserInfor,group:DepartGroup]
    String description
    static constraints = {
        description nullable: true, blank: true
        group nullable: true, blank:true
    }

    static mapping = {
        table 'depart_user'
        description sqlType: 'varchar(100)'
    }

}
