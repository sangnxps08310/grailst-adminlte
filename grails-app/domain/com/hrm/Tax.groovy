package com.hrm

class Tax {
    String nameTax
    String percent
    Date createDate = new Date()
    Date effectiveDate
    static constraints = {
    }
    static mapping = {
        percent column: '[percent]'
    }
}
