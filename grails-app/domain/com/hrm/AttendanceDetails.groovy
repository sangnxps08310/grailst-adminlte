package com.hrm


class AttendanceDetails {

    String description
    boolean present = false
    static belongsTo = [attendance: Attendance,userInfo: UserInfor]
    static constraints = {
        description nullable: true, blank: true
    }
    static mapping = {
        table 'attendance_details'
    }
}
