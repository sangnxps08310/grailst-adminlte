package com.hrm

class DepartGroup {
    String name
    String description
    static belongsTo = [depart:Depart]
    static constraints = {
        name nullable: false, blank: false
        description nullable: true, blank: true
    }
    static mapping = {
        name sqlType: 'varchar(100)'
        description sqlType: 'varchar(100)'
    }
}
