package com.hrm

class RankSalary {
    static belongsTo = [profession:Profession] // rankposition
    String nameRank
    String rankSalary
    static constraints = {
        nameRank nullable: false, blank: false, matches: '[A-Z][0-9]'
        rankSalary nullable: false, blank: false

    }
}
