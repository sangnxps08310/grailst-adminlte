package com.hrm

import java.sql.Date

class ReviewSalary {
    static belongsTo = [userId:UserInfor, rankSalary:RankSalary]
    float entranceSalary
    float newSalary
    Date createDate = new Date(new java.util.Date().getTime())
    Date effectiveDate
    static constraints = {

    }
    static mapping = {
    }
}
