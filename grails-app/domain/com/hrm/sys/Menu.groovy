package com.hrm.sys

class Menu {
    String name
    String description
    String parent = 0
    String icon
    int position
    String url
    static constraints = {
        name(blank: false, nullable: false, size: 1..20)
        description(nullable: true, blank: true)
        position(max: 100)
        url(nullable: true, blank: true)
        icon(blank: true, nullable: true)
        parent(blank: true, nullable: true)
    }

}
