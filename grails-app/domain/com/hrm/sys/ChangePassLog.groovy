package com.hrm.sys

import java.sql.Date

class ChangePassLog {
    long createdBy
    Date createdAt = new Date(new java.util.Date().getTime())
    String status
    int submitCounter
    String token
    String auth_key
    static constraints = {
        auth_key nullable: true
    }
}
