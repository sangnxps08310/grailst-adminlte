package com.hrm

import grails.rest.Resource

class District {
    long version = 0
    int province_code
    int district_code
    String name
    static constraints = {
        version defaultValue: 0

    }
    static mapping = {
        id column: 'id_district'
        name sqlType: 'varchar(100)'
    }
}
