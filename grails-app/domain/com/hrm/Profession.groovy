package com.hrm

class Profession {
    String professionName  // tên chức vụ
    String rank           // hệ số chức vụ
    static constraints = {
        professionName  nullable: false, blank: false
        rank  nullable: false, blank: false
    }
    // positionName
}
