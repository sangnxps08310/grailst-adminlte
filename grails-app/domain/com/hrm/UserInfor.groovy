package com.hrm

class UserInfor {
  String name
  boolean gender
  Date dateOfBirth
  String email
  String phoneNumber
  String permanentAddress
  String homeAddress
  String photo
  String personalId
  String personalIdPhotoA
  Date releaseDate
  String releaseIn
  String certificate
  String certificatePhoto
  String status
  String level
  float entranceSalary
  int dependant = 0
  static belongsTo = [laborContract: LaborContract, permanentWard: Ward, permanentDistrict: District, permanentProvince: Province,
                      homeWard     : Ward, homeDistrict: District, homeProvince: Province]
  static constraints = {
    name nullable: false, blank: false, name:true , matches: '[a-zA-Z]{1,9}(\\s[a-zA-Z]{2,9}){0,4}\\s[a-zA-Z]{1,8}+'
    email nullable: false, blank: false, email: true, matches: '^([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+$'
    dateOfBirth nullable: false, blank: false
    phoneNumber nullable: false, blank: false, matches: '[0-9]{10,11}'
    dependant min: 0, max: 10
    photo nullable: true,blank: true
    personalIdPhotoA nullable: true,blank: true
    certificatePhoto nullable: true,blank: true

  }
  static mapping = {
    table 'user_info'
  }
}
