package com.hrm

class RollPayHistory {
    static belongsTo = [creater:UserInfor]
    int month
    Date createDate = new Date()
    String fileName
    String status
    static constraints = {
        fileName nullable: true, blank: true
    }
}

