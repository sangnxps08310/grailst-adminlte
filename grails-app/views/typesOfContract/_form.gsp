<%@ page import="com.hrm.TypesOfContract" %>



<g:if test="${typesOfContractInstance}">
  <div class="form-group ${hasErrors(bean: typesOfContractInstance, field: 'name', 'error')} required">
  <label class="control-label" for="name">
    <g:message code="typesOfContract.name.label" default="Name"/>
    <span class="text-red">*</span>
  </label>
  <br>
<g:textField class="form-control" name="name" required="" value="${typesOfContractInstance?.name}"/>
</g:if><g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="name">
  <g:message code="typesOfContract.name.label" default="Name"/>
  </label>
  <br>
  <input class="form-control" name="name" value="${params.name}">
</g:else>  <div class="help-block"></div>
</div>



<g:if test="${typesOfContractInstance}">
  <div class="form-group ${hasErrors(bean: typesOfContractInstance, field: 'notes', 'error')} ">
  <label class="control-label" for="notes">
    <g:message code="typesOfContract.notes.label" default="Notes"/>
  </label>
  <br>
<g:textField class="form-control" name="notes"  value="${typesOfContractInstance?.notes}"/>
  </div>
</g:if>



