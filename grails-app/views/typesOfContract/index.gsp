<%@ page import="com.hrm.TypesOfContract" %>
<!DOCTYPE html>

<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'typesOfContract.label', default: 'TypesOfContract')}"/>
  <title><g:message code="typesOfContract.label"
                    args="${[message( code: 'typesOfContract.label', default:'typesOfContract')]}">
  </g:message></title>

</head>

<body>
<div class="typesOfContract">

  <g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
  </g:if>
  <div id="list-typesOfContract" class="box box-primary" role="main">
    <div class="box-header">
      <h4 class="box-title collapsed" data-toggle="collapse" data-target="#search-box" aria-expanded="true"><i
        class="fa fa-search"></i><g:message code="default.search.label" args=" "/></h4>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool btn-search-toggle collapsed" data-toggle="collapse"
                data-target="#search-box" aria-expanded="true">
          <i class="fa fa-plus"></i>
        </button>
      </div>
      <div class="help-block"></div>

      <div class="search-typesOfContract" id="search-box">
          <g:form class="form rows"
                  url="[resource: typesOfContractInstance, action: 'index']" >
          <g:render template="form"/>
          <div class="buttons col-md-4">
            <label>&nbsp;</label>
            <br>
            <g:submitButton name="search" class="btn btn-success"
                            value="${message(code: 'default.button.search.label', default: 'search')}"/>
          </div>
          </g:form>
        </div>
    </div>
    <div class="box-body">
      <div class="clearfix">
      </div>
      <g:link action="create" class="btn btn-success pull-right"><i class="fa fa-plus"></i></g:link>
      <table class="table table-hover" style="border-top: 1px solid #f4f4f4; border-bottom: 1px solid #f4f4f4">
        <thead>
        <tr>
          <th>#</th>
          
          <g:sortableColumn property="name"
                            title="${message(code: 'typesOfContract.name.label', default: 'Name')}"/>
          
          <g:sortableColumn property="notes"
                            title="${message(code: 'typesOfContract.notes.label', default: 'Notes')}"/>
          
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${typesOfContractInstanceList}" status="i" var="typesOfContractInstance">
          <tr class="${(i % 2) == 0 ? 'even' : 'odd'}" scope="col">
            
            <td>${i+1}</td>
            <td><g:link action="show" id="${typesOfContractInstance.id}">${fieldValue(bean: typesOfContractInstance, field: "name")}</g:link></td>
            
            <td>${fieldValue(bean: typesOfContractInstance, field: "notes")}</td>
            
            <td><g:link action="edit" resource="${typesOfContractInstance}"><i
              class="glyphicon glyphicon-pencil"></i></g:link><g:link class="delete" action="delete"
                                                                      resource="${typesOfContractInstance}"><i
                class="glyphicon glyphicon-remove"></i></g:link></td>
          </tr>
        </g:each>
        </tbody>
      </table>
    </div>
    <div class="box-footer">
      <div class="pagination sang-pagination">
        <g:paginate total="${typesOfContractInstanceCount ?: 0}"/>
      </div>
    </div>
  </div>
</div>
</body>
</html>
