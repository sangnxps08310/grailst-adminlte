<%@ page import="com.hrm.TypesOfContract" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'typesOfContract.label', default: 'TypesOfContract')}"/>
  <title>
    <g:message code="default.edit.label"
               args="${[message( code: 'typesOfContract.label', default:'typesOfContract')]}">
    </g:message></title>

</head>

<body>
%{--		<a href="#edit-typesOfContract" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>--}%
%{--		<div class="nav" role="navigation">--}%
%{--			<ul>--}%
%{--				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
%{--				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>--}%
%{--				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>--}%
%{--			</ul>--}%
%{--		</div>--}%
<div class="typesOfContract">
  <div id="edit-typesOfContract" class="content scaffold-edit" role="main">
%{--    <h3><g:message code="default.edit.label" args="[entityName]"/></h3>--}%
    <g:if test="${flash.message}">
      <div class="message alert alert-success" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${typesOfContractInstance}">
      <div class="alert alert-warning">
      <ul class="errors" role="alert">
        <g:eachError bean="${typesOfContractInstance}" var="error">
          <li<g:if
               test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
            error="${error}"/></li>
        </g:eachError>
      </ul>
      </div>
    </g:hasErrors>
    <div class="box box-primary">
      <div class="box-body">
    <g:form class="form rows" url="[resource: typesOfContractInstance, action: 'update']"
            method="PUT" >
    <g:hiddenField class="form-control" name="version" value="${typesOfContractInstance?.version}"/>
    <g:render template="form"/>
    <div class="col-md-12"></div>
    <div class="buttons">
      <g:actionSubmit class="btn btn-success" action="update"
                      value="${message(code: 'default.button.update.label', default: 'Update')}"/>
    </div>
    </g:form>
      </div>
    </div>
  </div>
</div>
</body>
</html>
