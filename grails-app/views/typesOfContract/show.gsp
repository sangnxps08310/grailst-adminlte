
<%@ page import="com.hrm.TypesOfContract" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'typesOfContract.label', default: 'TypesOfContract')}"/>
  <title><g:message code="default.show.label"
                    args="${[message( code: 'typesOfContract.label', default:'typesOfContract')]}">
</g:message></title>
  <style>
    td {
      text-align: left!important;
    }
  </style>
</head>

<body>
%{--		<a href="#show-typesOfContract" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>--}%
%{--		<div class="nav" role="navigation">--}%
%{--			<ul>--}%
%{--				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
%{--				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>--}%
%{--				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>--}%
%{--			</ul>--}%
%{--		</div>--}%
<div class="typesOfContract">
  <div id="show-typesOfContract" class="content scaffold-show" role="main">
%{--    <h3><g:message code="default.show.label" args="[entityName]"/></h3>--}%
    <g:if test="${flash.message}">
      <div class="message alert alert-success" role="status">${flash.message}</div>
    </g:if>
    <div class="box box-primary">
      <div class="box-body">
        <h4><i class="fa fa-book"></i>Details: </h4>
        <table class="typesOfContract table no-margin"s>
          
          %{--          <g:if test="${typesOfContractInstance?.name}">--}%
          <tr class="fieldcontain">
            <th id="name-label" class="property-label"><g:message
              code="typesOfContract.name.label" default="Name"/></th>
            
            <td class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${typesOfContractInstance}"
                                                                                       field="name"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${typesOfContractInstance?.notes}">--}%
          <tr class="fieldcontain">
            <th id="notes-label" class="property-label"><g:message
              code="typesOfContract.notes.label" default="Notes"/></th>
            
            <td class="property-value" aria-labelledby="notes-label"><g:fieldValue bean="${typesOfContractInstance}"
                                                                                       field="notes"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
        </table>
        <g:form url="[resource: typesOfContractInstance, action: 'delete']" method="DELETE">
          <fieldset class="buttons">
            <g:link class="edit btn btn-info" action="edit" resource="${typesOfContractInstance}"><g:message
              code="default.button.edit.label"
              default="Edit"/></g:link>
            <g:actionSubmit class="delete btn btn-danger" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
          </fieldset>
        </g:form>
      </div>
    </div>
  </div>
</div>
</body>
</html>
