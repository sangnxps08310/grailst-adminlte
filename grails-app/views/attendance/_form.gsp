<%@ page import="com.hrm.Attendance" %>






<g:if test="${attendanceInstance}">
  <div class="form-group ${hasErrors(bean: attendanceInstance, field: 'createdDate', 'error')} required">
  <label class="control-label" for="createdDate">
    <g:message code="attendance.createdDate.label" default="Created Date"/>
    <span class="text-red">*</span>
  </label>
  <br>
  

  <div class='input-group' id='datetimepicker'>
  

<g:field id="date-picker-field" class="form-control" name="createdDate"  type="text"  value="${attendanceInstance?.createdDate}"/>

  <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
</div>
<script>
   $("#datetimepicker").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#date-picker-field").val()))
      });

</script>


</g:if><g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="createdDate">
  <g:message code="attendance.createdDate.label" default="Created Date"/>
  <span class="text-red">*</span>
  </label>
  <br>
  <input class="form-control" name="createdDate" value="${params.createdDate}">
</g:else>  <div class="help-block"></div>
</div>



<g:if test="${attendanceInstance}">
  <div class="form-group ${hasErrors(bean: attendanceInstance, field: 'users', 'error')} required">
  <label class="control-label" for="users">
    <g:message code="attendance.users.label" default="Users"/>
    <span class="text-red">*</span>
  </label>
  <br>
  

<g:select class="form-control many-to-one" id="users" name="users.id" from="${com.hrm.Users.list()}" optionKey="id" required="" value="${attendanceInstance?.users?.id}"/>



</g:if><g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="users">
  <g:message code="attendance.users.label" default="Users"/>
  <span class="text-red">*</span>
  </label>
  <br>
  <input class="form-control" name="users" value="${params.users}">
</g:else>  <div class="help-block"></div>
</div>


