<%@ page import="com.hrm.Attendance" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'attendance.label', default: 'Attendance')}"/>
  <title><g:message code="default.show.label"
                    args="${[message(code: 'attendance.label', default: 'attendance')]}">
  </g:message></title>
</head>

<body>
%{--		<a href="#show-attendance" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>--}%
%{--		<div class="nav" role="navigation">--}%
%{--			<ul>--}%
%{--				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
%{--				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>--}%
%{--				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>--}%
%{--			</ul>--}%
%{--		</div>--}%
<div class="attendance">
  <div id="show-attendance" class="content scaffold-show" role="main">
  %{--    <h3><g:message code="default.show.label" args="[entityName]"/></h3>--}%
    <g:if test="${flash.message}">
      <div class="message alert alert-success" role="status">${flash.message}</div>
    </g:if>
    <div id="list-attendance" class="box box-primary" role="main">
      <div class="box-header">
        <div class="col-md-3">
          <label class="control-label" for="date">
            Date
          </label>
          <br>

          <div class='input-group' id='datetimepicker-date'>

            <g:field id="date" class="form-control" name="date" type="text"
                     value="${date.getTime()}"/>
            <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
          </div>
          <script>

            $("#datetimepicker-date").datetimepicker({
              format: 'YYYY-MM-DD',
              date: new Date(Number.parseInt($("#date").val()))
            });
            $("#datetimepicker-date").on('dp.change', function () {
              $.ajax({
                url: 'show',
                method: 'GET',
                data: {date: Date.parse($("#date").val())},
                sync: false,
                success: function (html) {
                  $('body').html(html);
                },
              });
            });
          </script>
        </div>
      </div>

      <div class="box-body">
        <div class="clearfix">
        </div>
        <table class="table table-hover" style="border-top: 1px solid #f4f4f4; border-bottom: 1px solid #f4f4f4">
          <thead>
          <tr>
            <th>#</th>

            <th><g:message code="attendance.users.label" default="Name"/></th>
            <th><g:message code="attendance.users.label" default="Email"/></th>
            <th><g:message code="attendance.users.label" default="Phone Number"/></th>
            <th><g:message code="attendance.users.label" default="Photo"/></th>
            <th><g:message code="attendance.users.label" default="Present"/></th>
          </tr>
          </thead>
          <tbody>
          <g:each in="${listAttendanceInstance}" status="i" var="attendanceInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}" scope="col">

              <td style="text-align: left">${i+1}</td>
              <td style="text-align: left">${attendanceInstance.userInfo.name}</td>
              <td style="text-align: left">${attendanceInstance.userInfo.email}</td>
              <td style="text-align: left">${attendanceInstance.userInfo.phoneNumber}</td>
              <td style="text-align: left">
                <img
                  class="img-thumbnail"
                  src="${attendanceInstance.userInfo.photo ? createLink(controller: 'userInfor', action: 'loadImage',
                    params: [
                      'image'    : attendanceInstance.userInfo ? true : false,
                      'path'     : attendanceInstance.userInfo?.photo,
                      'ownFolder': attendanceInstance.userInfo?.id]) : resource(dir: 'asset/dist/img', file: 'no-image.png')}"
                  width="75" height="75" id="image" alt="no-image">
              <td style="text-align: left">
                <i class="fa ${attendanceInstance.present ? 'fa-check' : 'fa-remove'}"
                   style="color: ${attendanceInstance.present ? 'green' : 'red'}"></i>
              </td>
            </tr>
          </g:each>
          </tbody>
        </table>

      </div>

      %{--        <div class="box-footer">--}%
      %{--          <div class="col-md-12">--}%
      %{--            <div class="buttons pull-right">--}%
      %{--              <g:submitButton name="create" class="btn btn-success"--}%
      %{--                              value="${message(code: 'default.button.create.label', default: 'Create')}"/>--}%
      %{--            </div>--}%
      %{--          </div>--}%
      %{--        </div>--}%

      <div class="box-footer">
        <div class="pagination sang-pagination">
          <g:paginate total="${attendanceInstanceCount ?: 0}"/>
        </div>
      </div>
    </div>

  </div>
</div>
</body>
</html>
