<%@ page import="com.hrm.UserInfor; com.hrm.Requests; com.hrm.Attendance" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'attendance.label', default: 'Attendance')}"/>
  <title><g:message code="attendance.label"
                    args="${[message(code: 'attendance.label', default: 'attendance')]}">
  </g:message></title>
</head>

<body>
<div class="attendance">

  <g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
  </g:if>
  <div class="help-block"></div>

  <div id="list-attendance" class="box box-primary" role="main">
    <div class="box-header">
      <h4 class="box-title"><g:formatDate format="yyyy-MM-dd" date="${new java.util.Date()}"></g:formatDate> </h4>
    </div>
    <g:form class="form rows"
            url="[resource: usersInstance, action: 'save']">
      <div class="box-body">
        <div class="clearfix">
        </div>
        <table class="table table-hover" style="border-top: 1px solid #f4f4f4; border-bottom: 1px solid #f4f4f4">
          <thead>
          <tr>
            <th>#</th>

            <th style="text-align: left;"><g:message code="attendance.users.label" default="Name"/></th>
            <th style="text-align: left;"><g:message code="attendance.users.label" default="Email"/></th>
            <th><g:message code="attendance.users.label" default="Phone Number"/></th>
            <th><g:message code="attendance.users.label" default="Photo"/></th>
            <th><g:message code="attendance.users.label" default="Present"/></th>
          </tr>
          </thead>
          <tbody>
          <g:each in="${listAttendanceInstance}" status="i" var="attendanceInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}" scope="col">

              <td
              .>${i+1}</td>
              <td style="text-align: left">${fieldValue(bean: attendanceInstance, field: "name")}</td>
              <td style="text-align: left">${fieldValue(bean: attendanceInstance, field: "email")}</td>
              <td >${fieldValue(bean: attendanceInstance, field: "phoneNumber")}</td>
              <td >
                <img
                  class="img-thumbnail"
                  src="${attendanceInstance.photo ? createLink(controller: 'userInfor', action: 'loadImage',
                    params: [
                      'image'    : attendanceInstance ? true : false,
                      'path'     : attendanceInstance?.photo,
                      'ownFolder': attendanceInstance?.id]) : resource(dir: 'asset/dist/img', file: 'no-image.png')}"
                  width="75" height="75" id="image" alt="no-image">
              </td>
              <%
                def requestInstance = com.hrm.Requests.findByStaffAndEffectedAtLessThanEqualsAndExpiredAtGreaterThanEqualsAndStatus(attendanceInstance, new Date(), new Date(), 'ACCEPTED',[sort: 'effectedAt', order: 'desc'])
              %>

              <td >
              <g:if test="${requestInstance ? false : true}">
                <g:checkBox name="userPresent" value="${attendanceInstance.id}"/>
              </g:if><g:else>
                <g:checkBox name="userPresent" value="${attendanceInstance.id}"
                            checked="true"
                            disabled="true" />
                <input type="hidden" name="userPresent" value="${attendanceInstance.id}">
              </g:else>

              </td>
            </tr>
          </g:each>
          </tbody>
        </table>

      </div>

      <div class="box-footer">
        <div class="col-md-12">
          <div class="buttons pull-right">
            <g:submitButton name="create" class="btn btn-success"
                            value="${message(code: 'default.button.create.label', default: 'Create')}"/>
          </div>
        </div>
      </div>
    </g:form>

    <div class="box-footer">
      <div class="pagination sang-pagination">
        <g:paginate total="${attendanceInstanceCount ?: 0}"/>
      </div>
    </div>
  </div>
</div>
</body>

</html>
