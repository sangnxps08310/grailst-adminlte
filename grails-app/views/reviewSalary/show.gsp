
<%@ page import="com.hrm.ReviewSalary" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'reviewSalary.label', default: 'ReviewSalary')}"/>
  <title><g:message code="default.show.label"
                    args="${[message( code: 'reviewSalary.label', default:'reviewSalary')]}">
</g:message></title>
  <style>
  td {
    text-align: left!important;
  }
  </style>
</head>

<body>

<div class="reviewSalary">
  <div id="show-reviewSalary" class="content scaffold-show" role="main">
%{--    <h3><g:message code="default.show.label" args="[entityName]"/></h3>--}%
    <g:if test="${flash.message}">
      <div class="message alert alert-success" role="status">${flash.message}</div>
    </g:if>
    <div class="box box-primary">
      <div class="box-body">
        <h4><i class="fa fa-book"></i>Details: </h4>
        <table class="reviewSalary table no-margin"s>

          %{--          <g:if test="${reviewSalaryInstance?.userId}">--}%
          <tr class="fieldcontain">
            <th id="userId-label" class="property-label"><g:message
                    code="reviewSalary.userId.label" default="User Id"/></th>

            <td class="property-value" aria-labelledby="userId-label"><g:link
                    controller="userInfor" action="show"
                    id="${reviewSalaryInstance?.userId?.id}">
              ${com.hrm.UserInfor.findById(fieldValue(bean: reviewSalaryInstance, field: "userIdId")).name}</g:link></td>

          </tr>
          %{--          </g:if>--}%


          %{--          <g:if test="${reviewSalaryInstance?.rankSalary}">--}%
          <tr class="fieldcontain">
            <th id="rankSalary-label" class="property-label"><g:message
                    code="reviewSalary.rankSalary.label" default="Rank Salary"/></th>

            <td class="property-value" aria-labelledby="rankSalary-label">
              ${fieldValue(bean: reviewSalaryInstance, field: "rankSalary.nameRank")}
            </td>

          </tr>
          %{--          </g:if>--}%

          
          %{--          <g:if test="${reviewSalaryInstance?.entranceSalary}">--}%
          <tr class="fieldcontain">
            <th id="entranceSalary-label" class="property-label"><g:message
              code="reviewSalary.entranceSalary.label" default="Entrance Salary"/></th>
            
            <td class="property-value" aria-labelledby="entranceSalary-label"><g:fieldValue bean="${reviewSalaryInstance}"
                                                                                       field="entranceSalary"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${reviewSalaryInstance?.newSalary}">--}%
          <tr class="fieldcontain">
            <th id="newSalary-label" class="property-label"><g:message
              code="reviewSalary.newSalary.label" default="New Salary"/></th>
            
            <td class="property-value" aria-labelledby="newSalary-label"><g:fieldValue bean="${reviewSalaryInstance}"
                                                                                       field="newSalary"/></td>
            
          </tr>
          %{--          </g:if>--}%



          %{--          <g:if test="${reviewSalaryInstance?.createDate}">--}%
          <tr class="fieldcontain">
            <th id="createDate-label" class="property-label"><g:message
                    code="reviewSalary.createDate.label" default="Create Date"/></th>

            <td class="property-value" aria-labelledby="createDate-label"><g:formatDate format="yyyy-MM-dd"
                    date="${reviewSalaryInstance?.createDate}"/></td>

          </tr>
          %{--          </g:if>--}%

          %{--          <g:if test="${reviewSalaryInstance?.effectiveDate}">--}%
          <tr class="fieldcontain">
            <th id="effectiveDate-label" class="property-label"><g:message
                    code="reviewSalary.effectiveDate.label" default="Effective Date"/></th>

            <td class="property-value" aria-labelledby="effectiveDate-label"><g:formatDate format="yyyy-MM-dd"
                    date="${reviewSalaryInstance?.effectiveDate}"/></td>

          </tr>
          %{--          </g:if>--}%
          
        </table>
        <g:form url="[resource: reviewSalaryInstance, action: 'delete']" method="DELETE">
          <fieldset class="buttons">
            <g:link class="edit btn btn-info" action="edit" resource="${reviewSalaryInstance}"><g:message
              code="default.button.edit.label"
              default="Edit"/></g:link>
            <g:actionSubmit class="delete btn btn-danger" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
          </fieldset>
        </g:form>
      </div>
    </div>
  </div>
</div>
</body>
</html>
