<%@ page import="com.hrm.UserInfor; com.hrm.ReviewSalary; com.hrm.UserInfor; com.hrm.Profession; com.hrm.RankSalary; java.text.SimpleDateFormat; java.text.DateFormat" %>

<% java.text.SimpleDateFormat dateFormat = new SimpleDateFormat('yyyy-MM-dd') %>

<%-- User ID --%>
<g:if test="${reviewSalaryInstance}">
    <div class="form-group ${hasErrors(bean: reviewSalaryInstance, field: 'userId', 'error')} required">
        <label class="control-label" for="userId">
            <g:message code="reviewSalary.userId.label" default="User Id"/>
            <span class="text-red">*</span>
        </label>
        <br>
        <g:select class="form-control many-to-one" id="userId" name="userId.id" from="${com.hrm.UserInfor.list()}"
                  noSelection="[0: '']" optionKey="id" required="" optionValue="name" disabled="${params.action=='edit'}"
                  value="${reviewSalaryInstance?.userId?.id}"/>
        <div class="help-block"></div>
    </div>
</g:if><g:else>
    <div class="col-md-3">
        <div class="form-group">
            <label class="control-label" for="userId">
                <g:message code="reviewSalary.userId.label" default="User Id"/>
            </label>
            <br>

            <g:select class="form-control many-to-one" id="userId" name="userId.id" from="${com.hrm.UserInfor.list()}"
                      noSelection="[0: '-Select User-']" optionKey="id" required="" optionValue="name"
                      value="${reviewSalaryInstance?.userId?.id}"/>
            <div class="help-block"></div>
        </div>
    </div>
</g:else>

<%-- Rank Salary --%>
<g:if test="${reviewSalaryInstance}">
    <div class="form-group ${hasErrors(bean: reviewSalaryInstance, field: 'rankSalary', 'error')} required">
        <label class="control-label" for="rankSalary">
            <g:message code="reviewSalary.rankSalary.label" default="Rank Salary"/>
            <span class="text-red">*</span>
        </label>
        <br>
        <%

        %>
%{--            ${reviewSalaryInstance?.userId?.laborContract?.profession?.id}--}%
        <g:select class="form-control select2 many-to-one" id="rankSalary" name="rankSalary.id"
                  from="${com.hrm.RankSalary.findAllByProfession(reviewSalaryInstance?.userId?.laborContract?.profession)}"
                  optionKey="id" required="" optionValue="nameRank"
                   value="${reviewSalaryInstance?.rankSalary?.id}"/>
        <div class="help-block"></div>
        <script>
            $("#rankSalary").select2({
                ajax: {
                    url: '${createLink(controller: 'rankSalary',action: 'ajaxGetRankSalary')}',
                    dataType: 'json',
                    type: "POST",
                    quietMillis: 50,
                    data: function pushData(term) {
                        return {
                            userId: $('#userId').val(),
                            rankSalary: term.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true,
                },
            });
            $("#rankSalary").on('change', function (e) {
                //        detectRank($('#rankSalary option:selected').text())
                var newSalary = $('#newSalary').val();
                var entranceSalary = $('#entranceSalary').val();
                newSalary = Number.parseFloat(entranceSalary) + (Number.parseFloat(entranceSalary) * Number.parseFloat(detectRank($('#rankSalary option:selected').text())))
                $('#newSalary').val(newSalary);

            });
            function detectRank(rankValue) {
                var startValue = rankValue.toString().indexOf('(');
                var endValue = rankValue.toString().indexOf(')');
                var tempString = rankValue.toString().substring(startValue + 1, endValue);
                return tempString
            }
        </script>
    </div>
</g:if><g:else>
    <div class="col-md-3">
        <div class="form-group">
            <label class="control-label" for="rankSalary">
                <g:message code="reviewSalary.rankSalary.label" default="Rank Salary"/>
            </label>
            <br>
            <g:select class="form-control select2 many-to-one" id="rankSalary" name="rankSalary.id"
                      from="${com.hrm.RankSalary.list()}"
                      optionKey="id" required="" optionValue="nameRank"
                      noSelection="[0: '-Select Rank-']" value="${reviewSalaryInstance?.rankSalary?.id}"/>

            <div class="help-block"></div>
        </div>
    </div>
</g:else>

<%-- Entrance Salary --%>
<g:if test="${reviewSalaryInstance}">
    <div class="form-group ${hasErrors(bean: reviewSalaryInstance, field: 'entranceSalary', 'error')} required">
        <label class="control-label" for="entranceSalary">
            <g:message code="reviewSalary.entranceSalary.label" default="Entrance Salary"/>
            <span class="text-red">*</span>
        </label>
        <br>
        <g:field type="number" id="entranceSalary" class="form-control" name="entranceSalary"
                 value="${(int)reviewSalaryInstance.entranceSalary}"  required=""/>
        <div class="help-block"></div>
    </div>
</g:if><g:else>
    <div class="col-md-3">
        <div class="form-group">
            <label class="control-label" for="entranceSalary">
                <g:message code="reviewSalary.entranceSalary.label" default="Entrance Salary"/>
            </label>
            <br>
            <input class="form-control" name="entranceSalary" value="${params.entranceSalary}">

            <div class="help-block"></div>
        </div>
    </div>
</g:else>

<%-- New Salary --%>
<g:if test="${reviewSalaryInstance}">
    <div class="form-group ${hasErrors(bean: reviewSalaryInstance, field: 'newSalary', 'error')} required">
        <label class="control-label" for="newSalary">
            <g:message code="reviewSalary.newSalary.label" default="New Salary"/>
            <span class="text-red">*</span>
        </label>
        <br>
        <g:field id="newSalary" class="form-control" name="newSalary"
                 value="${fieldValue(bean: reviewSalaryInstance, field: 'newSalary')}" required=""/>
        <div class="help-block"></div>
    </div>
</g:if><g:else>
    <div class="col-md-3">
        <div class="form-group">
            <label class="control-label" for="newSalary">
                <g:message code="reviewSalary.newSalary.label" default="New Salary"/>
            </label>
            <br>
            <input class="form-control" name="newSalary" value="${params.newSalary}">

            <div class="help-block"></div>
        </div>
    </div>
</g:else>





<%-- Create Date --%>
<%--
<g:if test="${reviewSalaryInstance}">
    <div class="form-group ${hasErrors(bean: reviewSalaryInstance, field: 'createDate', 'error')} required">
        <label class="control-label" for="createDate">
            <g:message code="reviewSalary.createDate.label" default="Create Date"/>
            <span class="text-red">*</span>
        </label>
        <br>

        <div class='input-group' id='datetimepicker-createDate'>
            <g:field id="date-picker-field" class="form-control" name="createDate" type="text"
                     value="${reviewSalaryInstance?.createDate?.getTime()}"/>
            <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
        </div>
        <script>
            $("#datetimepicker-createDate").datetimepicker({
                format: 'YYYY-MM-DD',
                date: new Date(Number.parseInt($("#date-picker-field-createDate").val()))
            });
        </script>
        <div class="help-block"></div>
    </div>
</g:if>

<g:else>
    <div class="col-md-3">
        <div class="form-group">
            <label class="control-label" for="createDate">
                <g:message code="reviewSalary.createDate.label" default="Create Date"/>
                <span class="text-red">*</span>
            </label>
            <br>
            <input class="form-control" name="createDate" value="${params.createDate}">
            <div class="help-block"></div>
        </div>
    </div>
</g:else>
--%>

<%-- Effective Date --%>
<g:if test="${reviewSalaryInstance}">
    <div class="form-group ${hasErrors(bean: reviewSalaryInstance, field: 'effectiveDate', 'error')} required">
        <label class="control-label" for="effectiveDate">
            <g:message code="reviewSalary.effectiveDate.label" default="Effective Date"/>
            <span class="text-red">*</span>
        </label>
        <br>

        <div class='input-group' id='datetimepicker-effectiveDate'>
            <g:field id="date-picker-field-effectiveDate" class="form-control" name="effectiveDate" type="text"
                     value="${reviewSalaryInstance?.effectiveDate?.getTime()}"/>
            <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
        </div>
        <script>
            $("#datetimepicker-effectiveDate").datetimepicker({
                format: 'YYYY-MM-DD',
                date: new Date(Number.parseInt($("#date-picker-field-effectiveDate").val()))
            });
        </script>

        <div class="help-block"></div>
    </div>
</g:if>
<g:else>
    <div class="form-group col-md-3">
        <label class="control-label" for="effective-date-from-1">
            <g:message code="reviewSalary.effectiveDate1.label" default="Effective Date From"/>
        </label>
        <br>

        <div class='input-group' id='effective-date-from-1'>
            <g:field id="effective-date-field-from-1" class="form-control" type="text" name="effectiveDateFrom"
                     value="${dateFormat.parse(params.effectiveDateFrom ? params.effectiveDateFrom : dateFormat.format(new Date())).getTime()}"
                     required=""/>
            <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
        </div>
        <script type="text/javascript">
            $("#effective-date-from-1").datetimepicker({
                format: 'YYYY-MM-DD',
                date: new Date(Number.parseInt($("#effective-date-field-from-1").val()))
            });
        </script>

        <div class="help-block"></div>
    </div>

    <div class="form-group col-md-3">
        <label class="control-label" for="effective-date-to">
            <g:message code="reviewSalary.effectiveDate2.label" default="Effective Date To"/>
        </label>
        <br>

        <div class='input-group' id='effective-date-to'>
            <g:field id="effective-date-field-to" class="form-control" type="text" name="effectiveDateTo"
                     value="${dateFormat.parse(params.effectiveDateTo ? params.effectiveDateTo : dateFormat.format(new Date())).getTime()}"
                     required=""/>
            <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
        </div>
        <script type="text/javascript">
            $("#effective-date-to").datetimepicker({
                format: 'YYYY-MM-DD',
                date: new Date(Number.parseInt($("#effective-date-field-to").val()))
            });
        </script>

        <div class="help-block"></div>
    </div>
</g:else>


<script>
    $('#userId').change(function () {
        var json = {'userId': $(this).val()};
        $.ajax({
            url: '${createLink(controller: 'reviewSalary',action: 'fillEntranceByUserInfor')}',
            method: 'POST',
            data: json,
            sync: false,
            success: function (data) {
                console.log(data)
                $('#entranceSalary').val(data);
            },
        });
    });
</script>



