
<%@ page import="com.hrm.ReviewSalary" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'reviewSalary.label', default: 'ReviewSalary')}"/>
  <title><g:message code="reviewSalary.label"
                    args="${[message( code: 'reviewSalary.label', default:'reviewSalary')]}">
  </g:message></title>

</head>

<body>
<div class="reviewSalary">

  <g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
  </g:if>
  <div id="list-reviewSalary" class="box box-primary" role="main">
    <div class="box-header">
      <h4 class="box-title collapsed" data-toggle="collapse" data-target="#search-box" aria-expanded="true"><i
        class="fa fa-search"></i><g:message code="default.search.label" args=" "/></h4>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool btn-search-toggle collapsed" data-toggle="collapse"
                data-target="#search-box" aria-expanded="true">
          <i class="fa fa-plus"></i>
        </button>
      </div>
      <div class="help-block"></div>

      <div class="search-reviewSalary" id="search-box">
          <g:form class="form rows"
                  url="[resource: reviewSalaryInstance, action: 'index']" >
          <g:render template="form"/>
          <div class="buttons col-md-6">
            <label>&nbsp;</label>
            <br>
            <g:submitButton name="search" class="btn btn-success"
                            value="${message(code: 'default.button.search.label', default: 'search')}"/>
          </div>
          </g:form>
        </div>
    </div>
    <div class="box-body">
      <div class="clearfix">
      </div>
      <g:link action="create" class="btn btn-success pull-right"><i class="fa fa-plus"></i></g:link>
      <table class="table table-hover" style="border-top: 1px solid #f4f4f4; border-bottom: 1px solid #f4f4f4">
        <thead>
        <tr>
          <th>#</th>

          <g:sortableColumn property="rankSalary" style="text-align: left;"
                            title="${message(code: 'reviewSalary.userId.label', default: 'User')}"/>

          <g:sortableColumn property="rankSalary"
                            title="${message(code: 'reviewSalary.rankSalary.label', default: 'Rank Salary')}"/>

          <g:sortableColumn property="entranceSalary"
                            title="${message(code: 'reviewSalary.entranceSalary.label', default: 'Entrance Salary')}"/>

          <g:sortableColumn property="newSalary"
                            title="${message(code: 'reviewSalary.newSalary.label', default: 'New Salary')}"/>

          <g:sortableColumn property="effectiveDate"
                            title="${message(code: 'reviewSalary.effectiveDate.label', default: 'Effective Date')}"/>

          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${reviewSalaryInstanceList}" status="i" var="reviewSalaryInstance">
          <tr class="${(i % 2) == 0 ? 'even' : 'odd'}" scope="col">
            
            <td>${i+1}</td>
            <td style="text-align: left"><g:link action="show" id="${reviewSalaryInstance.id}">
              ${com.hrm.UserInfor.findById(fieldValue(bean: reviewSalaryInstance, field: "userIdId")).name}
            </g:link></td>

            <td>${com.hrm.RankSalary.findById(fieldValue(bean: reviewSalaryInstance, field: "rankSalaryId")).nameRank}</td>

            <td>${fieldValue(bean: reviewSalaryInstance, field: "entranceSalary")}</td>

            <td>${fieldValue(bean: reviewSalaryInstance, field: "newSalary")}</td>

            <td><g:formatDate format="yyyy-MM-dd" date="${reviewSalaryInstance.effectiveDate}"/></td>
            
            <td><g:link action="edit" resource="${reviewSalaryInstance}"><i
              class="glyphicon glyphicon-pencil"></i></g:link><g:link class="delete" action="delete"
                                                                      resource="${reviewSalaryInstance}"><i
                class="glyphicon glyphicon-remove"></i></g:link></td>
          </tr>
        </g:each>
        </tbody>
      </table>
    </div>
    <div class="box-footer">
      <div class="pagination sang-pagination">
        <g:paginate total="${reviewSalaryInstanceCount ?: 0}"/>
      </div>
    </div>
  </div>
</div>
</body>
</html>
