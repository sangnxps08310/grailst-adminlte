<div class="col-md-6">
  <div class="box box-primary ">
    <div class="box-body ">
      <div class="col-md-8">
        <h4 class="box-title"><i class="fa fa-group"></i> Group: ${departGroup.name}</h4>
      </div>

      <div class="col-md-4">
        <div class="box-tools pull-right">
          <div class="btn-group">
            <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
              <i class="fa fa-ellipsis-v"></i></button>
            <ul class="dropdown-menu in" role="menu">
              <li><g:link controller="departGroup" action="show" id="${departGroup.id}">Details</g:link></li>
              <li><g:link controller="departGroup" action="edit" id="${departGroup.id}">Edit</g:link></li>
            </ul>
          </div>
        </div>
      </div>

      <div class="col-md-12">
        <div class="box-tools pull-right">
          <g:link class="btn btn-success" controller="departUser" action="create"
                  params="[id: departGroup.depart.id, group: departGroup.id]">Add User</g:link>
        </div>
      </div>
      <table class="table table-hover">
        <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th>Position</th>
          <th>Photo</th>
          <th>Action</th>

        </tr>
        </thead>
        <tbody>
        <g:if test="${departUser.size() > 0}">
          <% int index = 0 %>
          <g:each in="${departUser}" var="user">
            <% index++ %>

            <tr>
              <td>${index}</td>
              <td><g:link id="${user.userInfo.id}" controller="userInfor"
                          action="show">${user.userInfo.name}</g:link></td>
              <td><span
                class="badge ${user.is_leader ? 'bg-blue' : 'bg-green'}">${user.is_leader ? 'Leader' : 'Member'}</span>
              </td>
              <td>${user.userInfo.photo}</td>
              <td><g:link controller="departUser" action="edit" id="${user.id}"><i
                class="fa fa-pencil"></i></g:link>
              &nbsp;
                <g:link style="color: red" controller="departUser" action="delete"
                        onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"
                        id="${user.id}"><i class="fa fa-close"></i></g:link></td>
            </tr>
          </g:each>
        </g:if><g:else>
          <tr>
            <td>-</td>
            <td>-</td>
          </tr>
        </g:else>
        </tbody>
      </table>
    </div>

    <!-- /.box-body -->
  </div>
</div>
