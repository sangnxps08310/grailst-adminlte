<%@ page import="com.hrm.Depart" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'depart.label', default: 'Depart')}"/>
  <title><g:message code="default.show.label"
                    args="${[message(code: 'depart.label', default: 'depart')]}">
  </g:message></title>
</head>

<body>
%{--		<a href="#show-depart" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>--}%
%{--		<div class="nav" role="navigation">--}%
%{--			<ul>--}%
%{--				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
%{--				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>--}%
%{--				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>--}%
%{--			</ul>--}%
%{--		</div>--}%
<div class="depart">
  <div id="show-depart" class="content scaffold-show" role="main">
  %{--    <h3><g:message code="default.show.label" args="[entityName]"/></h3>--}%
    <g:if test="${flash.message}">
      <div class="message alert alert-success" role="status">${flash.message}</div>
    </g:if>
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-body">
          <h4><i class="fa fa-book"></i>Details:</h4>
          <table class="depart table no-margin">
            %{--          <g:if test="${departInstance?.name}">--}%
            <tr class="fieldcontain">
              <th id="name-label" class="property-label" style="vertical-align:middle"><g:message
                code="depart.name.label" default="Name"/></th>
              <td class="property-value" style="text-align:left" aria-labelledby="name-label">
                <g:fieldValue bean="${departInstance}" class="property-value" field="name"/>
              </td>
            </tr>
            <tr class="fieldcontain">
              <th id="createdDate-label" class="property-label" style="vertical-align:middle">
                <g:message code="depart.createdDate.label" default="Created Date"/>
              </th>
              <td class="property-value" style="text-align:left" aria-labelledby="createdDate-label">
                <g:formatDate format="yyyy-MM-dd" date="${departInstance?.createdDate}"/>
              </td>
            </tr>
            <tr class="fieldcontain">
              <th id="room-master-name-label" class="property-label"
                  style="vertical-align:middle">Room Master:</th>
              <td class="property-value" style="text-align:left" aria-labelledby="createdDate-label">

                <g:link controller="userInfor" action="show"
                        id="${roomMasterInfo?.userInfo?.id}"><span class="label label-warning"
                                                                   style="font-size: larger">${roomMasterInfo?.userInfo?.name}</span></g:link>
              </td>
            </tr>
          </table>
          <g:form url="[resource: departInstance, action: 'delete']" method="DELETE">
            <fieldset class="buttons">
              <g:link class="edit btn btn-info" action="edit" resource="${departInstance}"><g:message
                code="default.button.edit.label"
                default="Edit"/></g:link>

              <g:actionSubmit class="delete btn btn-danger" action="delete"
                              value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                              onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>

              <g:link class="btn btn-success" controller="departGroup" action="create"
                      id="${departInstance.id}"><g:message
                code="default.button.group.create.label"
                default="Create Group"/></g:link>
            </fieldset>
          </g:form>

        </div>
      </div>
    </div>


    <g:each in="${departGroups}" var="departGroup">
      <g:render template="group-depart"
                model="[departGroup: departGroup, departUser: departUsers[departGroup.id]]"></g:render>
    </g:each>
  </div>
</div>
</body>
</html>
