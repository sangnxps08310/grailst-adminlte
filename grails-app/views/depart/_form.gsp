<%@ page import="com.hrm.Depart" %>






<g:if test="${departInstance}">
  <div class="form-group ${hasErrors(bean: departInstance, field: 'name', 'error')} required">
  <label class="control-label" for="name">
    <g:message code="depart.name.label" default="Name"/>
    <span class="text-red">*</span>
  </label>
  <br>


  <g:textField class="form-control" name="name" required="" value="${departInstance?.name}"/>

</g:if><g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="name">
  <g:message code="depart.name.label" default="Name"/>
  </label>
  <br>
  <input class="form-control" name="name" value="${params.name}">
</g:else>  <div class="help-block"></div>
</div>
