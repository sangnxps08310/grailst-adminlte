
<%@ page import="com.hrm.Profession" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'profession.label', default: 'Profession')}"/>
  <title><g:message code="default.show.label"
                    args="${[message( code: 'profession.label', default:'Position')]}">
</g:message></title>
  <style>
    td {
      text-align: left!important;
    }
  </style>
</head>

<body>
<div class="profession">
  <div id="show-profession" class="content scaffold-show" role="main">
%{--    <h3><g:message code="default.show.label" args="[entityName]"/></h3>--}%
    <g:if test="${flash.message}">
      <div class="message alert alert-success" role="status">${flash.message}</div>
    </g:if>
    <div class="box box-primary">
      <div class="box-body">
        <h4><i class="fa fa-book"></i>Details: </h4>
        <table class="profession table no-margin"s>
          
          %{--          <g:if test="${professionInstance?.positionName}">--}%
          <tr class="fieldcontain">
            <th id="professionName-label" class="property-label"><g:message
              code="profession.professionName.label" default="Position Name"/></th>
            
            <td class="property-value" aria-labelledby="professionName-label"><g:fieldValue bean="${professionInstance}"
                                                                                       field="professionName"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${professionInstance?.rank}">--}%
          <tr class="fieldcontain">
            <th id="rank-label" class="property-label"><g:message
              code="profession.rank.label" default="Rank"/></th>
            
            <td class="property-value" aria-labelledby="rank-label"><g:fieldValue bean="${professionInstance}"
                                                                                       field="rank"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
        </table>
        <g:form url="[resource: professionInstance, action: 'delete']" method="DELETE">
          <fieldset class="buttons">
            <g:link class="edit btn btn-info" action="edit" resource="${professionInstance}"><g:message
              code="default.button.edit.label"
              default="Edit"/></g:link>
            <g:actionSubmit class="delete btn btn-danger" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
          </fieldset>
        </g:form>
      </div>
    </div>
  </div>
</div>
</body>
</html>
