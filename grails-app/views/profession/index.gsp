
<%@ page import="com.hrm.Profession" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'profession.label', default: 'Profession')}"/>
  <title><g:message code="profession.label"
                    args="${[message( code: 'profession.label', default:'profession')]}">
  </g:message></title>

</head>

<body>
<div class="profession">

  <g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
  </g:if>
  <div id="list-profession" class="box box-primary" role="main">
    <div class="box-header">
      <h4 class="box-title collapsed" data-toggle="collapse" data-target="#search-box" aria-expanded="true"><i
        class="fa fa-search"></i><g:message code="default.search.label" args=" "/></h4>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool btn-search-toggle collapsed" data-toggle="collapse"
                data-target="#search-box" aria-expanded="true">
          <i class="fa fa-plus"></i>
        </button>
      </div>
      <div class="help-block"></div>

      <div class="search-profession" id="search-box">
          <g:form class="form rows"
                  url="[resource: professionInstance, action: 'index']" >
          <g:render template="form"/>
          <div class="buttons col-md-4">
            <label>&nbsp;</label>
            <br>
            <g:submitButton name="search" class="btn btn-success"
                            value="${message(code: 'default.button.search.label', default: 'search')}"/>
          </div>
          </g:form>
        </div>
    </div>
    <div class="box-body">
      <div class="clearfix">
      </div>
      <g:link action="create" class="btn btn-success pull-right"><i class="fa fa-plus"></i></g:link>
      <table class="table table-hover" style="border-top: 1px solid #f4f4f4; border-bottom: 1px solid #f4f4f4">
        <thead>
        <tr>
          <th>#</th>
          
          <g:sortableColumn property="professionName"
                            title="${message(code: 'profession.professionName.label', default: 'Profession Name')}"/>
          
          <g:sortableColumn property="rank"
                            title="${message(code: 'profession.rank.label', default: 'Rank')}"/>
          
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${professionInstanceList}" status="i" var="professionInstance">
          <tr class="${(i % 2) == 0 ? 'even' : 'odd'}" scope="col">
            
            <td>${i+1}</td>
            <td><g:link action="show" id="${professionInstance.id}">${fieldValue(bean: professionInstance, field: "professionName")}</g:link></td>
            
            <td>${fieldValue(bean: professionInstance, field: "rank")}</td>
            
            <td><g:link action="edit" resource="${professionInstance}"><i class="glyphicon glyphicon-pencil"></i></g:link>
                <g:link class="delete" action="delete" resource="${professionInstance}">
                  <i class="glyphicon glyphicon-remove"></i></g:link></td>
          </tr>
        </g:each>
        </tbody>
      </table>
    </div>
    <div class="box-footer">
      <div class="pagination sang-pagination">
        <g:paginate total="${professionInstanceCount ?: 0}"/>
      </div>
    </div>
  </div>
</div>
</body>
</html>
