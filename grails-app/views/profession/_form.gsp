<%@ page import="com.hrm.Profession" %>






<g:if test="${professionInstance}">
  <div class="form-group ${hasErrors(bean: professionInstance, field: 'professionName', 'error')} required">
  <label class="control-label" for="professionName">
    <g:message code="profession.professionName.label" default="Position Name"/>
    <span class="text-red">*</span>
  </label>
  <br>
  

<g:textField class="form-control" name="professionName" required="" value="${professionInstance?.professionName}"/>



</g:if><g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="professionName">
  <g:message code="profession.professionName.label" default="Position Name"/>
  </label>
  <br>
  <input class="form-control" name="professionName" value="${params.professionName}">
</g:else>  <div class="help-block"></div>
</div>



<g:if test="${professionInstance}">
  <div class="form-group ${hasErrors(bean: professionInstance, field: 'rank', 'error')} required">
  <label class="control-label" for="rank">
    <g:message code="profession.rank.label" default="Rank"/>
    <span class="text-red">*</span>
  </label>
  <br>
  

<g:textField class="form-control" name="rank" pattern="${professionInstance.constraints.rank.matches}" required="" value="${professionInstance?.rank}"/>



</g:if><g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="rank">
  <g:message code="profession.rank.label" default="Rank"/>
  </label>
  <br>
  <input class="form-control" name="rank" value="${params.rank}">
</g:else>  <div class="help-block"></div>
</div>


