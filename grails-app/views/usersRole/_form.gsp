<%@ page import="com.hrm.UsersRole" %>





<div class="form-group ${hasErrors(bean: usersRoleInstance, field: 'role', 'error')} required">
  <label class="control-label" for="role">
    <g:message code="usersRole.role.label" default="Role"/>
    <span class="text-red">*</span>
  </label>
  <br>
<g:if test="${usersRoleInstance}">
  <g:select class="form-control" id="role" name="role.id" from="${com.hrm.Role.list()}" optionKey="id" required="" value="${usersRoleInstance?.role?.id}" class="many-to-one"/>
</g:if><g:else>
  <input class="form-control" name="role">

</g:else>  <div class="help-block"></div>
</div>


<div class="form-group ${hasErrors(bean: usersRoleInstance, field: 'users', 'error')} required">
  <label class="control-label" for="users">
    <g:message code="usersRole.users.label" default="Users"/>
    <span class="text-red">*</span>
  </label>
  <br>
<g:if test="${usersRoleInstance}">
  <g:select class="form-control" id="users" name="users.id" from="${com.hrm.Users.list()}" optionKey="id" required="" value="${usersRoleInstance?.users?.id}" class="many-to-one"/>
</g:if><g:else>
  <input class="form-control" name="users">

</g:else>  <div class="help-block"></div>
</div>


