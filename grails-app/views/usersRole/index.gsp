
<%@ page import="com.hrm.UsersRole" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'usersRole.label', default: 'UsersRole')}"/>
  <title><g:message code="usersRole.label"
                    args="${[message( code: 'usersRole.label', default:'usersRole')]}">
  </g:message></title>

</head>

<body>
<div class="usersRole">

  %{--  <a href="#list-usersRole" class="skip" tabindex="-1"><g:message code="default.link.skip.label"--}%
  %{--                                                                                    default="Skip to content&hellip;"/></a>--}%

  %{--  <div class="nav" role="navigation">--}%
  %{--    <ul>--}%
  %{--      <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
  %{--      <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]"/></g:link></li>--}%
  %{--    </ul>--}%
  %{--  </div>--}%
  <div id="list-usersRole" class="box box-primary" role="main">
    <div class="box-header">
      <h4 class="box-title collapsed" data-toggle="collapse" data-target="#search-box" aria-expanded="false"><i
        class="fa fa-search"></i><g:message code="default.search.label" args=" "/></h4>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool btn-search-toggle collapsed" data-toggle="collapse"
                data-target="#search-box" aria-expanded="false">
          <i class="fa fa-plus"></i>
        </button>
      </div>
      <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
      </g:if>
        <div class="search-usersRole">

          <g:form class="form rows"
                  url="[resource: usersRoleInstance, action: 'index']" >
          <g:render template="form"/>
          <div class="col-md-12"></div>
          <div class="buttons">
            <g:submitButton name="search" class="btn btn-success"
                            value="${message(code: 'default.button.search.label', default: 'search')}"/>
          </div>
          </g:form>
        </div>
    </div>
    <div class="box-body">
      <div class="clearfix">
      </div>
      <g:link action="create" class="btn btn-success pull-right"><i class="fa fa-plus"></i></g:link>
      <table class="table table-hover" style="border-top: 1px solid #f4f4f4; border-bottom: 1px solid #f4f4f4">
        <thead>
        <tr>
          <th>#</th>
          
          <th><g:message code="usersRole.role.label" default="Role"/></th>
          
          <th><g:message code="usersRole.users.label" default="Users"/></th>
          
        </tr>
        </thead>
        <tbody>
        <g:each in="${usersRoleInstanceList}" status="i" var="usersRoleInstance">
          <tr class="${(i % 2) == 0 ? 'even' : 'odd'}" scope="col">
            
            <td>1</td>
            <td><g:link action="show" id="${usersRoleInstance.id}">${fieldValue(bean: usersRoleInstance, field: "role")}</g:link></td>
            
            <td>${fieldValue(bean: usersRoleInstance, field: "users")}</td>
            
          </tr>
        </g:each>
        </tbody>
      </table>
    </div>
    <div class="box-footer">
      <div class="pagination">
        <g:paginate total="${usersRoleInstanceCount ?: 0}"/>
      </div>
    </div>
  </div>
</div>
</body>
</html>
