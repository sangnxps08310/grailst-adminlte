
<%@ page import="com.hrm.StaffInfor" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'staffInfor.label', default: 'StaffInfor')}"/>
  <title><g:message code="default.show.label"
                    args="${[message( code: 'staffInfor.label', default:'staffInfor')]}">
</g:message></title>
</head>

<body>
%{--		<a href="#show-staffInfor" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>--}%
%{--		<div class="nav" role="navigation">--}%
%{--			<ul>--}%
%{--				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
%{--				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>--}%
%{--				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>--}%
%{--			</ul>--}%
%{--		</div>--}%
<div class="staffInfor">
  <div id="show-staffInfor" class="content scaffold-show" role="main">
%{--    <h3><g:message code="default.show.label" args="[entityName]"/></h3>--}%
    <g:if test="${flash.message}">
      <div class="message alert alert-success" role="status">${flash.message}</div>
    </g:if>
    <div class="box">
      <div class="box-body">
        <h4><i class="fa fa-book"></i>Details: </h4>
        <table class="staffInfor table no-margin"s>

          %{--          <g:if test="${staffInforInstance?.name}">--}%
          <tr class="fieldcontain">
            <th id="name-label" class="property-label"><g:message
                    code="staffInfor.name.label" default="Name"/></th>

            <td class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${staffInforInstance}"
                                                                                  field="name"/></td>

          </tr>
          %{--          </g:if>--}%

          %{--          <g:if test="${staffInforInstance?.gender}">--}%
          <tr class="fieldcontain">
            <th id="gender-label" class="property-label"><g:message
                    code="staffInfor.gender.label" default="Gender"/></th>

            <td class="property-value" aria-labelledby="gender-label"><g:formatBoolean
                    boolean="${staffInforInstance?.gender}" false="Male" true="Female"/></td>

          </tr>
          %{--          </g:if>--}%

          %{--          <g:if test="${staffInforInstance?.mail}">--}%
          <tr class="fieldcontain">
            <th id="mail-label" class="property-label"><g:message
                    code="staffInfor.mail.label" default="Mail"/></th>

            <td class="property-value" aria-labelledby="mail-label"><g:fieldValue bean="${staffInforInstance}"
                                                                                  field="mail"/></td>

          </tr>
          %{--          </g:if>--}%

          %{--          <g:if test="${staffInforInstance?.address}">--}%
          <tr class="fieldcontain">
            <th id="address-label" class="property-label"><g:message
              code="staffInfor.address.label" default="Address"/></th>
            
            <td class="property-value" aria-labelledby="address-label"><g:fieldValue bean="${staffInforInstance}"
                                                                                       field="address"/></td>
            
          </tr>
          %{--          </g:if>--}%

          %{--          <g:if test="${staffInforInstance?.phone}">--}%
          <tr class="fieldcontain">
            <th id="phone-label" class="property-label"><g:message
                    code="staffInfor.phone.label" default="Phone"/></th>

            <td class="property-value" aria-labelledby="phone-label"><g:fieldValue bean="${staffInforInstance}"
                                                                                   field="phone"/></td>

          </tr>
          %{--          </g:if>--}%

          %{--          <g:if test="${staffInforInstance?.entranceSalary}">--}%
          <tr class="fieldcontain">
            <th id="entranceSalary-label" class="property-label"><g:message
                    code="staffInfor.entranceSalary.label" default="Entrance Salary"/></th>

            <td class="property-value" aria-labelledby="entranceSalary-label"><g:fieldValue bean="${staffInforInstance}"
                                                                                            field="entranceSalary"/></td>

          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${staffInforInstance?.departID}">--}%
          <tr class="fieldcontain">
            <th id="departID-label" class="property-label"><g:message
              code="staffInfor.departID.label" default="Depart ID"/></th>
            
            <td class="property-value" aria-labelledby="departID-label"><g:fieldValue bean="${staffInforInstance}"
                                                                                       field="departID"/></td>
            
          </tr>
          %{--          </g:if>--}%

          
          %{--          <g:if test="${staffInforInstance?.photo}">--}%
          <tr class="fieldcontain">
            <th id="photo-label" class="property-label"><g:message
              code="staffInfor.photo.label" default="Photo"/></th>
            
            <td class="property-value" aria-labelledby="photo-label"><g:fieldValue bean="${staffInforInstance}"
                                                                                       field="photo"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
        </table>
        <g:form url="[resource: staffInforInstance, action: 'delete']" method="DELETE">
          <fieldset class="buttons">
            <g:link class="edit btn btn-info" action="edit" resource="${staffInforInstance}"><g:message
              code="default.button.edit.label"
              default="Edit"/></g:link>
            <g:actionSubmit class="delete btn btn-danger" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
          </fieldset>
        </g:form>
      </div>
    </div>
  </div>
</div>
</body>
</html>
