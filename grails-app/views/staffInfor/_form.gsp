<%@ page import="com.hrm.StaffInfor" %>


<g:if test="${staffInforInstance}">
<div class="form-group ${hasErrors(bean: staffInforInstance, field: 'name', 'error')} required">
  <label class="control-label" for="name">
    <g:message code="staffInfor.name.label" default="Name"/>
    <span class="text-red">*</span>
  </label>
  <br>
    <g:textField class="form-control" name="name" required="" value="${staffInforInstance?.name}"/>
  <div class="help-block"></div>
</div>
</g:if>
<g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="name">
  <g:message code="staffInfor.name.label" default="Name"/>
  </label>
  <br>
  <input class="form-control" name="name" value="${params.name}" >
  </div>
</g:else>


<g:if test="${staffInforInstance}">
<div class="form-group ${hasErrors(bean: staffInforInstance, field: 'gender', 'error')} ">
  <label class="control-label" for="gender">
    <g:message code="staffInfor.gender.label" default="Gender"/>

  </label>
  <br>
  <label><g:radio name="gender" value="true"/>&ensp;<g:message code="staffInfor.gender.female" default="Female"/></label>&emsp;
  <label><g:radio name="gender" value="false"/>&ensp;<g:message code="staffInfor.gender.male" default="Male"/></label>&emsp;
  <div class="help-block"></div>
</div>
</g:if>


<g:if test="${staffInforInstance}">
<div class="form-group ${hasErrors(bean: staffInforInstance, field: 'mail', 'error')} required">
  <label class="control-label" for="mail">
    <g:message code="staffInfor.mail.label" default="Mail"/>
    <span class="text-red">*</span>
  </label>
  <br>
    <g:textField class="form-control" name="mail" required="" value="${staffInforInstance?.mail}"/>
  <div class="help-block"></div>
</div>
</g:if>
<g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="mail">
  <g:message code="staffInfor.mail.label" default="Mail"/>
  </label>
  <br>
  <input class="form-control" name="mail" value="${params.mail}">
  </div>
</g:else>


<g:if test="${staffInforInstance}">
<div class="form-group ${hasErrors(bean: staffInforInstance, field: 'address', 'error')} required">
  <label class="control-label" for="address">
    <g:message code="staffInfor.address.label" default="Address"/>
    <span class="text-red">*</span>
  </label>
  <br>
  <g:textField class="form-control" name="address" required="" value="${staffInforInstance?.address}"/>
  <div class="help-block"></div>
</div>
</g:if>
<g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="address">
  <g:message code="staffInfor.address.label" default="Address"/>
  </label>
  <br>
  <input class="form-control" name="address" value="${params.address}">
  </div>
</g:else>


<g:if test="${staffInforInstance}">
<div class="form-group ${hasErrors(bean: staffInforInstance, field: 'phone', 'error')} required">
  <label class="control-label" for="phone">
    <g:message code="staffInfor.phone.label" default="Phone"/>
    <span class="text-red">*</span>
  </label>
  <br>
    <g:textField class="form-control" name="phone" required="" value="${staffInforInstance?.phone}"/>
  <div class="help-block"></div>
</div>
</g:if>
<g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="phone">
  <g:message code="staffInfor.phone.label" default="Phone"/>
  </label>
  <br>
  <input class="form-control" name="phone" value="${params.phone}">
  </div>
</g:else>


<g:if test="${staffInforInstance}">
<div class="form-group ${hasErrors(bean: staffInforInstance, field: 'entranceSalary', 'error')} required">
  <label class="control-label" for="entranceSalary">
    <g:message code="staffInfor.entranceSalary.label" default="Entrance Salary"/>
    <span class="text-red">*</span>
  </label>
  <br>
  <g:field class="form-control" name="entranceSalary" value="${fieldValue(bean: staffInforInstance, field: 'entranceSalary')}" required=""/>
  <div class="help-block"></div>
</div>
</g:if>

<g:if test="${staffInforInstance}">
<div class="form-group ${hasErrors(bean: staffInforInstance, field: 'departID', 'error')} required">
  <label class="control-label" for="departID">
    <g:message code="staffInfor.departID.label" default="Depart ID"/>
    <span class="text-red">*</span>
  </label>
  <br>
    <g:field class="form-control" name="departID" type="number" value="${staffInforInstance.departID}" required=""/>
  <div class="help-block"></div>
</div>
</g:if>


<g:if test="${staffInforInstance}">
<div class="form-group ${hasErrors(bean: staffInforInstance, field: 'photo', 'error')} required">
  <label class="control-label" for="photo">
    <g:message code="staffInfor.photo.label" default="Photo"/>
    <span class="text-red">*</span>
  </label>
  <br>
  <g:textField class="form-control" name="photo" required="" value="${staffInforInstance?.photo}"/>
  <div class="help-block"></div>
</div>
</g:if>


