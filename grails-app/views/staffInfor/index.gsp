<%@ page import="com.hrm.StaffInfor" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="adminlte">
    <g:set var="entityName" value="${message(code: 'staffInfor.label', default: 'StaffInfor')}"/>
    <title><g:message code="staffInfor.label"
                      args="${[message(code: 'staffInfor.label', default: 'staffInfor')]}">
    </g:message></title>
</head>

<body>
<div class="staffInfor">
    <g:if test="${flash.message}">
        <div class="alert alert-${flash.type}" role="alert">${flash.message}</div>
    </g:if>

    <div id="list-staffInfor" class="box box-primary" role="main">
        <div class="box-header">
            <h4 class="box-title collapsed" data-toggle="collapse" data-target="#search-box" aria-expanded="false"><i
                    class="fa fa-search"></i><g:message code="default.search.label" args=" "/></h4>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool btn-search-toggle collapsed" data-toggle="collapse"
                        data-target="#search-box-menu" aria-expanded="true">
                    <i class="fa fa-plus"></i>
                </button>
            </div>

            <div class="help-block"></div>

            <div class="search-staffInfor">
                <g:form class="form rows" url="[resource: staffInforInstance, action: 'index']">
                    <g:render template="form"/>
                    <div class="col-md-12"></div>
                    <div class="form-group col-md-2">
                        <g:submitButton name="search" class="btn btn-success">Search</g:submitButton>
                    </div>
                </g:form>
            </div>
        </div>

        <div class="box-body">
            <div class="clearfix">
            </div>
            <g:link action="create" class="btn btn-success pull-right"><i class="fa fa-plus"></i></g:link>
            <table class="table table-hover" style="border-top: 1px solid #f4f4f4; border-bottom: 1px solid #f4f4f4">
                <thead>
                <tr>
                    <th>#</th>
                    <g:sortableColumn property="name"
                                      title="${message(code: 'staffInfor.name.label', default: 'Name')}"/>

                    <g:sortableColumn property="gender"
                                      title="${message(code: 'staffInfor.gender.label', default: 'Gender')}"/>

                    <g:sortableColumn property="mail"
                                      title="${message(code: 'staffInfor.mail.label', default: 'Mail')}"/>

                    <g:sortableColumn property="address"
                                      title="${message(code: 'staffInfor.address.label', default: 'Address')}"/>

                    <g:sortableColumn property="address"
                                      title="${message(code: 'staffInfor.phone.label', default: 'Phone')}"/>

                    <g:sortableColumn property="entranceSalary"
                                      title="${message(code: 'staffInfor.entranceSalary.label', default: 'Entrance Salary')}"/>

                    <g:sortableColumn property="departID"
                                      title="${message(code: 'staffInfor.departID.label', default: 'Depart ID')}"/>

                </tr>
                </thead>
                <tbody>
                <g:each in="${staffInforInstanceList}" status="i" var="staffInforInstance">
                    <tr class="${(i % 2) == 0 ? 'even' : 'odd'}" scope="col">

                        <td style="text-align: left">${i + 1}</td>
                        <td style="text-align: left"><g:link action="show"
                                    id="${staffInforInstance.id}">${fieldValue(bean: staffInforInstance, field: "name")}</g:link></td>

                        <td style="text-align: left"><g:formatBoolean boolean="${staffInforInstance.gender}" true="Female" false="Male"/></td>

                        <td style="text-align: left">${fieldValue(bean: staffInforInstance, field: "mail")}</td>

                        <td style="text-align: left">${fieldValue(bean: staffInforInstance, field: "address")}</td>

                        <td style="text-align: left">${fieldValue(bean: staffInforInstance, field: "phone")}</td>

                        <td style="text-align: left"><g:link controller="reviewSalary" action="createReviewSalaryByStaffInfo"
                                    params="[staffInfoId: staffInforInstance.id]">
                            ${fieldValue(bean: staffInforInstance, field: "entranceSalary")}</g:link></td>
                        <td>${fieldValue(bean: staffInforInstance, field: "departID")}</td>
                        <td></td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>

        <div class="box-footer">
            <div class="pagination">
                <g:paginate total="${staffInforInstanceCount ?: 0}"/>
            </div>
        </div>
    </div>
</div>
</body>
</html>
