<%@ page import="com.hrm.UserInforService" %>
<!-- Profile Image -->
<div class="box box-primary">
  <div class="box-body box-profile">
    <img class="profile-user-img img-responsive img-circle"
         src="${userInforInstance.photo ? createLink(controller: 'userInfor', action: 'loadImage',
           params: [
             'image': userInforInstance ? true : false,
             'path' : userInforInstance?.photo]) : resource(dir: 'asset/dist/img', file: 'no-image.png')}"
         alt="User profile picture">

    <h3 class="profile-username text-center">${userInforInstance.name}</h3>

    <p class="text-muted text-center">${userInforInstance.laborContract.profession.professionName}</p>

    <ul class="list-group list-group-unbordered">
      <li class="list-group-item">
        <b>Date Of Birth</b> <a class="pull-right"><g:formatDate format="yyyy-MM-dd"
                                                                 date="${userInforInstance.dateOfBirth}"/></a>
      </li>
      <li class="list-group-item">
        <b>Phone Number</b> <a class="pull-right">${userInforInstance.phoneNumber}</a>
      </li>
      <%
        def departUser = com.hrm.DepartUser.findByUserInfo(userInforInstance)
        def departInstance
        if (departUser) {
          departInstance = com.hrm.Depart.findById(departUser.depart.id).name

        } else {
          departInstance = ''
        }
      %>
      <li class="list-group-item">
        <b>Depart</b> <a
        class="pull-right">${departInstance}</a>
      </li>
      <%    %>
      <li class="list-group-item">
        <b>Rest days remain</b> <a
        class="pull-right">${remainDays}/12</a>
      </li>
    </ul>
    %{--        <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>--}%
  </div>
  <!-- /.box-body -->
</div>

<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">About Me</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <strong><i class="fa fa-book margin-r-5"></i> Level</strong>

    <p class="text-muted">
      ${userInforInstance.level}
    </p>
    <hr>

    <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

    <p
      class="text-muted">${userInforInstance.permanentAddress} ${userInforInstance.permanentWard.name} ${userInforInstance.permanentDistrict.name} ${userInforInstance.permanentProvince.name}</p>
    %{--        <hr>--}%

    %{--        <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>--}%

    %{--        <p>--}%
    %{--          <span class="label label-danger">UI Design</span>--}%
    %{--          <span class="label label-success">Coding</span>--}%
    %{--          <span class="label label-info">Javascript</span>--}%
    %{--          <span class="label label-warning">PHP</span>--}%
    %{--          <span class="label label-primary">Node.js</span>--}%
    %{--        </p>--}%
    <hr>
    <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
  </div>
  <!-- /.box-body -->
</div>