<%@ page import="com.hrm.DepartUser; com.hrm.UserInfor" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'userInfor.label', default: 'UserInfor')}"/>
  <title><g:message code="userInfor.label"
                    args="${[message(code: 'userInfor.label', default: 'userInfor')]}">
  </g:message></title>
<style>
td {
  text-align: left;
}

</style>
</head>

<body>
<div class="userInfor">

  <g:if test="${flash.message}">
    <div class="alert alert-success" role="alert">${flash.message}</div>
  </g:if>
  <div id="list-userInfor" class="box box-primary" role="main">
    <div class="box-header">
      <h4 class="box-title collapsed" data-toggle="collapse" data-target="#search-box" aria-expanded="true"><i
        class="fa fa-search"></i><g:message code="default.search.label" args=" "/></h4>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool btn-search-toggle collapsed" data-toggle="collapse"
                data-target="#search-box" aria-expanded="true">
          <i class="fa fa-plus"></i>
        </button>
      </div>

      <div class="help-block"></div>

      <div class="search-userInfor" id="search-box">
        <g:form class="form rows"
                url="[resource: userInforInstance, action: 'index']">
          <g:render template="form"/>
          <div class="col-md-3">
            <g:submitButton name="search" class="btn btn-success"
                            value="${message(code: 'default.button.search.label', default: 'search')}"/>
          </div>
        </g:form>
      </div>
    </div>

    <div class="box-body">
      <div class="clearfix">
      </div>
      <g:link action="create" class="btn btn-success pull-right"><i class="fa fa-plus"></i></g:link>
      <table class="table table-hover" style="border-top: 1px solid #f4f4f4; border-bottom: 1px solid #f4f4f4">
        <thead>
        <tr>
          <th>#</th>

          <g:sortableColumn property="name" style="text-align: left;"
                            title="${message(code: 'userInfor.name.label', default: 'Name')}"  />

          <g:sortableColumn property="email" style="text-align: left;"
                            title="${message(code: 'userInfor.email.label', default: 'Email')}"/>

          <g:sortableColumn property="dateOfBirth"
                            title="${message(code: 'userInfor.dateOfBirth.label', default: 'Date Of Birth')}"/>

          <g:sortableColumn property="phoneNumber"
                            title="${message(code: 'userInfor.phoneNumber.label', default: 'Phone Number')}"/>

          <g:sortableColumn property="certificate"
                            title="${message(code: 'userInfor.certificate.label', default: 'Certificate')}"/>

          <th>Depart</th>
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${userInforInstanceList}" status="i" var="userInforInstance">
          <tr class="${(i % 2) == 0 ? 'even' : 'odd'}" scope="col">

            <td>${i+1}</td>
            <td style="text-align: left"><g:link action="show"
                        id="${userInforInstance.id}">${fieldValue(bean: userInforInstance, field: "name")}</g:link></td>

            <td style="text-align: left">${fieldValue(bean: userInforInstance, field: "email")}</td>

            <td ><g:formatDate format="yyyy-MM-dd" date="${userInforInstance.dateOfBirth}"/></td>

            <td >${fieldValue(bean: userInforInstance, field: "phoneNumber")}</td>

            <td >${fieldValue(bean: userInforInstance, field: "certificate")}</td>

            <td >${mapDepartInstance[userInforInstance.id]?.name}</td>
            <td ><g:link action="edit" resource="${userInforInstance}"><i
              class="glyphicon glyphicon-pencil"></i></g:link><g:link class="delete" action="delete"
                                                                      resource="${userInforInstance}"><i
                class="glyphicon glyphicon-remove"></i></g:link></td>
          </tr>
        </g:each>
        </tbody>
      </table>
    </div>

    <div class="box-footer">
      <div class="pagination sang-pagination">
        <g:paginate total="${userInforInstanceCount ?: 0}"/>
      </div>
    </div>
  </div>
</div>
</body>
</html>
