<div class="box-body">
  <g:form method="post" class="form rows" url="[resource: userInforInstance, action: 'changePass']">
    <div class="form-group">
      <label>Current Password</label>
      <input class="form-control" name="current_password" type="password" required>
    </div>

    <div class="form-group">
      <label>New Password</label>
      <input class="form-control" name="new_password" type="password" required>
    </div>

    <div class="form-group">
      <label>Confirm Password</label>
      <input class="form-control" name="confirm_password" type="password" required>
    </div>
    <input class="form-control" name="id" type="hidden" value="${userInforInstance.id}">
    <div class="form-group">
      <button class="btn btn-primary" type="submit">Submit</button>
    </div>
  </g:form>
</div>