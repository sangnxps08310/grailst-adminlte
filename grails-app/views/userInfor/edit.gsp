<%@ page import="com.hrm.UserInfor" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'userInfor.label', default: 'UserInfor')}"/>
  <title>
    <g:message code="default.edit.label"
               args="${[message(code: 'userInfor.label', default: 'userInfor')]}">
    </g:message></title>

</head>

<body>

<div class="userInfor">
  <div id="edit-userInfor" class="content scaffold-edit" role="main">
      <h3><g:message code="default.edit.label" args="[entityName]"/></h3>
    <g:if test="${flash.message}">
      <div class="message alert alert-success" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${userInforInstance}">
      <div class="alert alert-warning">
        <ul class="errors" role="alert">
          <g:eachError bean="${userInforInstance}" var="error">
            <li<g:if
            test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
          </g:eachError>
        </ul>
      </div>
    </g:hasErrors>
    <div class="box box-primary">
      <div class="box-body">
        <g:form class="form rows" url="[resource: userInforInstance, action: 'update']"
                enctype="multipart/form-data" method="POST">
          <g:hiddenField class="form-control" name="version" value="${userInforInstance?.version}"/>
          <g:render template="form"/>
          <div class="col-md-12"></div>

          <div class="col-md-3">
            <div class="col-md-3">
              <g:actionSubmit class="btn btn-success" action="update"
                              value="${message(code: 'default.button.update.label', default: 'Update')}"/>
            </div>
          </div>
        </g:form>
      </div>
    </div>
  </div>
</div>
</body>
</html>
