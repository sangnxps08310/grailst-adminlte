<%@ page import="com.hrm.UserInfor" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'userInfor.label', default: 'UserInfor')}"/>
  <title><g:message code="default.show.label"
                    args="${[message(code: 'userInfor.label', default: 'userInfor')]}">
  </g:message></title>
  <style>
  table td {
    text-align: left !important;
    width: 70%;
  }
  </style>
</head>

<body>

<div class="userInfor">
  <g:if test="${flash.message}">
    <div class="message alert alert-success" role="status">${flash.message}</div>
  </g:if>
  <div class="col-md-12"></div>

  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header">
        <h4 class="box-title"><i class="fa fa-book"></i>&nbsp;Details:</h4>
        <div class="pull-right">
          <g:form url="[resource: userInforInstance, action: 'delete']" method="DELETE">
            <fieldset class="buttons">
              <g:link class="edit btn btn-info" action="edit" resource="${userInforInstance}">
                <g:message code="default.button.edit.label" default="Edit"/></g:link>
              <g:actionSubmit class="delete btn btn-danger" action="delete"
                              value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                              onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
            </fieldset>
          </g:form>
        </div>
      </div>
      <div class="box-body">
        <table class="userInfor table no-margin">
          <tr class="fieldcontain">
            <th id="name-label" class="property-label">
              <g:message code="userInfor.name.label" default="Name"/>
            </th>
            <td class="property-value" aria-labelledby="name-label">
              <g:fieldValue bean="${userInforInstance}"  field="name"/>
            </td>
          </tr>
          <tr class="fieldcontain">
            <th id="email-label" class="property-label">
              <g:message code="userInfor.email.label" default="Email"/>
            </th>
            <td class="property-value" aria-labelledby="email-label">
              <g:fieldValue bean="${userInforInstance}" field="email"/>
            </td>
          </tr>
          <tr class="fieldcontain">
            <th id="dateOfBirth-label" class="property-label">
              <g:message code="userInfor.dateOfBirth.label" default="Date Of Birth"/>
            </th>
            <td class="property-value" aria-labelledby="dateOfBirth-label">
              <g:formatDate format="yyyy-MM-dd" date="${userInforInstance?.dateOfBirth}"/>
            </td>
          </tr>
          <tr class="fieldcontain">
            <th id="phoneNumber-label" class="property-label">
              <g:message code="userInfor.phoneNumber.label" default="Phone Number"/>
            </th>
            <td class="property-value" aria-labelledby="phoneNumber-label">
              <g:fieldValue bean="${userInforInstance}" field="phoneNumber"/>
            </td>
          </tr>
          <tr class="fieldcontain">
            <th id="gender-label" class="property-label">
              <g:message code="userInfor.gender.label" default="Gender"/>
            </th>
            <td class="property-value" aria-labelledby="gender-label">
              ${userInforInstance?.gender ? 'Male' : 'Female'}
            </td>
          </tr>
          <tr class="fieldcontain">
            <th id="photo-label" class="property-label">
              <g:message code="userInfor.photo.label" default="Photo"/>
            </th>
            <td class="property-value" aria-labelledby="photo-label">
              <img
                src="${createLink(controller: 'userInfor', action: 'loadImage', params: ['path': userInforInstance.photo])} "
                class="img-thumbnail" width="200px" height="200px"
                alt="User Image">
            </td>
          </tr>
        </table>
      </div>
    </div>
    <div class="box box-primary">
      <div class="box-header">
        <h4 class="box-title"><i class="fa fa-book"></i>&nbsp;Personal Information:</h4>
      </div>
      <div class="box-body">
        <table class="userInfor table no-margin">
          <tr class="fieldcontain">
            <th id="homeAddress-label" class="property-label"><g:message
              code="userInfor.homeAddress.label" default="Home Address"/></th>
            <td class="property-value" aria-labelledby="homeAddress-label">
              <g:fieldValue bean="${userInforInstance}" field="homeAddress"/>,
               ${userInforInstance?.homeWard?.name}, ${userInforInstance?.homeDistrict?.name}, ${userInforInstance?.homeProvince?.name}
            </td>
          </tr>
          <tr class="fieldcontain">
            <th id="permanentAddress-label" class="property-label"><g:message
              code="userInfor.permanentAddress.label" default="Permanent Address"/></th>

            <td class="property-value" aria-labelledby="permanentAddress-label">
              <g:fieldValue bean="${userInforInstance}" field="permanentAddress"/>
              ${userInforInstance?.permanentWard?.name}, ${userInforInstance?.permanentDistrict?.name}, ${userInforInstance?.permanentProvince?.name}
            </td>
          </tr>
          <tr class="fieldcontain">
            <th id="personalId-label" class="property-label"><g:message
              code="userInfor.personalId.label" default="ID card number"/></th>
            <td class="property-value" aria-labelledby="personalId-label">
              <g:fieldValue bean="${userInforInstance}" field="personalId"/>
            </td>
          </tr>
          <tr class="fieldcontain">
            <th id="personalIdPhotoA-label" class="property-label"><g:message
              code="userInfor.personalIdPhotoA.label" default="ID card Photo"/></th>
            <td class="property-value" aria-labelledby="personalIdPhotoA-label">
              <img
                src="${createLink(controller: 'userInfor', action: 'loadImage', params: ['path': userInforInstance.personalIdPhotoA])} "
                class="img-thumbnail" width="200px" height="200px"
                alt="User Image">
            </td>
          </tr>
          <tr class="fieldcontain">
            <th id="level-label" class="property-label"><g:message
              code="userInfor.level.label" default="Level"/></th>
            <td class="property-value" aria-labelledby="level-label"><g:fieldValue bean="${userInforInstance}"
                                                                                   field="level"/></td>
          </tr>
          <tr class="fieldcontain">
            <th id="releaseDate-label" class="property-label"><g:message
              code="userInfor.releaseDate.label" default="ID card release Date"/></th>

            <td class="property-value" aria-labelledby="releaseDate-label"><g:formatDate
              date="${userInforInstance?.releaseDate}"/></td>

          </tr>
          <tr class="fieldcontain">
            <th id="releaseIn-label" class="property-label"><g:message
              code="userInfor.releaseIn.label" default=" ID card release In"/></th>

            <td class="property-value" aria-labelledby="releaseIn-label"><g:fieldValue bean="${userInforInstance}"
                                                                                       field="releaseIn"/></td>

          </tr>
        </table>
      </div>
    </div>

    <div class="box box-primary">
      <div class="box-header">
        <h4 class="box-title"><i class="fa fa-book"></i>&nbsp;Educate:</h4>
      </div>
      <div class="box-body">
        <table class="userInfor table no-margin">
          <tr class="fieldcontain">
            <th id="certificate-label" class="property-label"><g:message
              code="userInfor.certificate.label" default="Certificate"/></th>
            <td class="property-value" aria-labelledby="certificate-label"><g:fieldValue bean="${userInforInstance}"
                                                                                         field="certificate"/></td>
          </tr>
          <tr class="fieldcontain">
            <th id="certificatePhoto-label" class="property-label"><g:message
              code="userInfor.certificatePhoto.label" default="Certificate Photo"/></th>
            <td class="property-value" aria-labelledby="certificatePhoto-label">
              <img
                src="${createLink(controller: 'userInfor', action: 'loadImage', params: ['path': userInforInstance.certificatePhoto])} "
                class="img-thumbnail" width="200px" height="200px"
                alt="User Image">
              </td>
          </tr>
        </table>
      </div>
    </div>

  </div>
</div>

</body>
</html>
