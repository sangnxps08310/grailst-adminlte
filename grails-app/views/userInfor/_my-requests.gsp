
<div class=" table-responsive no-padding">
  <div class="col-md-2 pull-right">
    <g:link class="btn btn-success" controller="requests" action="create" params="['userId':userInforInstance?.id,'departId':departInstance?.id]">New request</g:link>
  </div>
  <g:if test="${listRequestInstance}">
    <table class="table table-hover">
      <thead>
      <tr>
        %{--      <th>#</th>--}%
        %{--      <th>User</th>--}%
        %{--      <th>Date</th>--}%
        %{--      <th>Status</th>--}%
        %{--      <th>Reason</th>--}%
        <th></th>
        <th>Status</th>
        <th></th>
        <th>Content</th>
        <th>Accepted At</th>
        <th></th>
        <th></th>
        <th></th>
      </tr>
      </thead>
      <tbody>
      <g:each in="${listRequestInstance}" var="request">

        <tr >
          <td style="vertical-align: middle" width="100px"> <img width="80" height="80" src="${createLink(controller: 'userInfor', action: 'loadImage',
            params: [
              'image': true,
              'path' : request?.staff?.photo])}" class="img-circle"/></td>
          <td style="vertical-align: middle" width="100px"><i class="fa  fa-long-arrow-right" style="font-size: 80px; color: #b2b2b2"></i><br><span class="label label-success">Accepted</span></td>
          <td style="vertical-align: middle" width="100px"><img width="80" height="80" src="${createLink(controller: 'userInfor', action: 'loadImage',
            params: [
              'image': true,
              'path' : roomMaster?.photo])}" class="img-circle"/></td>
          <td style="vertical-align: middle" width="200px">${ request?.type?.description}</td>
          <td style="vertical-align: middle"><g:formatDate format="yyyy-MM-dd" date="${request?.acceptedAt}"></g:formatDate> </td>
          <td style="vertical-align: middle"></td>
          <td style="vertical-align: middle"></td>
          <td style="vertical-align: middle"></td>
        </tr>
      </g:each>
      </tbody>
    </table>
  </g:if><g:else>
  We have no avaiable request !!
</g:else>
</div>
