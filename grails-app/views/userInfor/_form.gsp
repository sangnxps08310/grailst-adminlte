<%@ page import="com.hrm.DepartUser; com.hrm.UserInfor" %>

<fieldset class="col-md-12">
  <legend>Personal Basic Information</legend>
  <g:if test="${userInforInstance}">
    <div class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'name', 'error')} required">
      <label class="control-label" for="name">
        <g:message code="userInfor.name.label" default="Name"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <g:textField class="form-control" name="name" required="" value="${userInforInstance?.name}"/>
      <div class="help-block"></div>
    </div>
  </g:if><g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="name">
        <g:message code="userInfor.name.label" default="Name"/>
      </label>
      <br>
      <input class="form-control" name="name" value="${params.name}">

      <div class="help-block"></div>
    </div>
  </div>
</g:else>
  <g:if test="${userInforInstance}">
    <div class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'email', 'error')} required">
      <label class="control-label" for="email">
        <g:message code="userInfor.email.label" default="Email"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <g:field class="form-control" type="email" name="email" required="" value="${userInforInstance?.email}"/>
      <div class="help-block"></div>
    </div>
  </g:if>

  <g:if test="${userInforInstance}">
    <div class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'dateOfBirth', 'error')} required">
      <label class="control-label" for="dateOfBirth">
        <g:message code="userInfor.dateOfBirth.label" default="Date Of Birth"/>
        <span class="text-red">*</span>
      </label>
      <br>

      <div class='input-group' id='datetimepicker-dateOfBirth'>
        <g:field id="date-picker-field-dateOfBirth" class="form-control" name="dateOfBirth" type="text"
                 value="${userInforInstance?.dateOfBirth?.getTime()}"/>
        <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
      </div>
      <script>
        $("#datetimepicker-dateOfBirth").datetimepicker({
          format: 'YYYY-MM-DD',
          date: new Date(Number.parseInt($("#date-picker-field-dateOfBirth").val()))
        });
      </script>

      <div class="help-block"></div>
    </div>
  </g:if>


  <g:if test="${userInforInstance}">
    <div class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'phoneNumber', 'error')} required">
      <label class="control-label" for="phoneNumber">
        <g:message code="userInfor.phoneNumber.label" default="Phone Number"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <g:textField class="form-control" name="phoneNumber"
                   required="" value="${userInforInstance?.phoneNumber}"/>
      <div class="help-block"></div>
    </div>
  </g:if>
  <g:if test="${userInforInstance}"></g:if><g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="personalId">
        <g:message code="userInfor.personalId.label" default="Personal Id"/>
      </label>
      <br>
      <input class="form-control" name="personalId" value="${params.personalId}">

      <div class="help-block"></div>
    </div>
  </div>
</g:else>
  <g:if test="${userInforInstance}"></g:if><g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="level">
        <g:message code="userInfor.level.label" default="Level"/>
      </label>
      <br>
      <input class="form-control" name="level" value="${params.level}">

      <div class="help-block"></div>
    </div>
  </div>
</g:else>
  <div class="col-md-12"></div>
  <g:if test="${userInforInstance}">
    <div class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'gender', 'error')} ">
      <label class="control-label" for="gender">
        <g:message code="userInfor.gender.label" default="Gender"/>

      </label>
      <br>


      <g:select from="${[['id': 'true', 'name': 'Male'], ['id': 'false', 'name': 'Female']]}"
                optionKey="id" class="form-control" optionValue="name" name="gender"
                value="${userInforInstance?.gender}"/>


      <div class="help-block"></div>
    </div>
  </g:if>
  <g:if test="${userInforInstance}">
    <div class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'photo', 'error')} required">
      <label class="control-label" for="photo">
        <g:message code="userInfor.photo.label" default="Photo"/>
        <span class="text-red">*</span>
      </label>
      <br>

      <div class="input-group">
        <span class="input-group-addon">
          <span class="input-group-text">Upload</span>
        </span>
        <g:textField id="image_photo" class="form-control" name="photo" onclick="\$('#file-photo').click()"
                     placeholder="No file choosen" required="" value="${userInforInstance?.photo}"/>
        <span class="input-group-addon">
          <span class="input-group-text clear-image">clear</span>
        </span>
      </div>

      <div class="help-block"></div>
    </div>

    <div class="form-group col-md-3">
      <div id="box_image_photo" class="box-image" style="width: 190px;">
        <div class="image-item">
          <img src="${userInforInstance.photo ? createLink(controller: 'userInfor', action: 'loadImage',
            params: [
              'image': userInforInstance ? true : false,
              'path' : userInforInstance?.photo]) : resource(dir: 'asset/dist/img', file: 'no-image.png')}"
               width="150" height="150" alt="no-image">
        </div>
      </div>
    </div>
  </g:if>

  <div class="col-md-12"></div>

</fieldset>

<fieldset class="col-md-12">
  <legend>Address</legend>
  <g:if test="${userInforInstance}">
    <div class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'homeAddress', 'error')} required">
      <label class="control-label" for="homeAddress">
        <g:message code="userInfor.homeAddress.label" default="Home Address"/>
        <span class="text-red">*</span>
      </label>
      <br>


      <g:textField class="form-control" name="homeAddress" required="" value="${userInforInstance?.homeAddress}"/>


      <div class="help-block"></div>
    </div>
  </g:if><g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="homeAddress">
        <g:message code="userInfor.homeAddress.label" default="Home Address"/>
      </label>
      <br>
      <input class="form-control" name="homeAddress" value="${params.homeAddress}">

      <div class="help-block"></div>
    </div>
  </div>
</g:else>
  <g:if test="${userInforInstance}">
    <div class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'homeProvince', 'error')} required">
      <label class="control-label" for="homeProvince">
        <g:message code="userInfor.homeProvince.label" default="Home Province"/>
        <span class="text-red">*</span>
      </label>
      <br>


      <g:select class="form-control many-to-one" id="homeProvince" name="homeProvince.id"
                from="${com.hrm.Province.list()}" optionKey="province_code" required="" optionValue="name"
                noSelection="['': ' ']" value="${userInforInstance?.homeProvince?.province_code}"/>
      <div class="help-block"></div>
    </div>
  </g:if><g:else>
  <div class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'homeProvince', 'error')} required">
    <label class="control-label" for="homeProvince">
      <g:message code="userInfor.homeProvince.label" default="Home Province"/>
    </label>
    <br>

    <%
      int provinceCode = (params?.homeProvince?.id) ? Integer.parseInt(params?.homeProvince?.id) : 0;
      int districtCode = (params?.homeDistrict?.id) ? Integer.parseInt(params?.homeDistrict?.id) : 0;
      int wardCode = (params?.homeWard?.id) ? Integer.parseInt(params?.homeWard?.id) : 0;
    %>
    <g:select class="form-control many-to-one" id="homeProvince" name="homeProvince.id"
              from="${com.hrm.Province.list()}" optionKey="province_code" optionValue="name"
              noSelection="['0': ' ']" value="${provinceCode}"/>
    <div class="help-block"></div>
  </div>
</g:else>
  <g:if test="${userInforInstance}">
    <div class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'homeDistrict', 'error')} required">
      <label class="control-label" for="homeDistrict">
        <g:message code="userInfor.homeDistrict.label" default="Home District"/>
        <span class="text-red">*</span>
      </label>
      <br>


      <g:select class="form-control select2 many-to-one" id="homeDistrict" name="homeDistrict.id"
                from="${com.hrm.District.findAllByProvince_code(userInforInstance?.homeProvince?.province_code)}"
                optionKey="district_code" required="" optionValue="name"
                noSelection="['': ' ']" value="${userInforInstance?.homeDistrict?.district_code}"/>
      <script>
        $("#homeDistrict").select2({
          ajax: {
            url: '${createLink(controller: 'district',action: 'getDistrict')}',
            dataType: 'json',
            type: "POST",
            quietMillis: 50,
            data: function pushData(term) {
              return {
                provinceCode: $('#homeProvince').val(),
                name: term.term
              };
            },
            processResults: function (data) {
              return {
                results: data
              };
            },
            cache: true,
          },
        });
      </script>

      <div class="help-block"></div>
    </div>
  </g:if><g:else>
  <div class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'homeDistrict', 'error')} required">
    <label class="control-label" for="homeDistrict">
      <g:message code="userInfor.homeDistrict.label" default="Home District"/>
    </label>
    <br>

    <g:select class="form-control select2 many-to-one" id="homeDistrict" name="homeDistrict.id"
              from="${com.hrm.District.findAllByProvince_code(provinceCode)}"
              optionKey="district_code" optionValue="name"
              noSelection="['0': ' ']" value="${districtCode}"/>
    <script>
      $("#homeDistrict").select2({
        ajax: {
          url: '${createLink(controller: 'district',action: 'getDistrict')}',
          dataType: 'json',
          type: "POST",
          quietMillis: 50,
          data: function pushData(term) {
            return {
              provinceCode: $('#homeProvince').val(),
              name: term.term
            };
          },
          processResults: function (data) {
            return {
              results: data
            };
          },
          cache: true,
        },
      });
    </script>

    <div class="help-block"></div>
  </div>
</g:else>
  <g:if test="${userInforInstance}">
    <div class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'homeWard', 'error')} required">
      <label class="control-label" for="homeWard">
        <g:message code="userInfor.homeWard.label" default="Home Ward"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <g:select class="form-control select2" id="homeWard" name="homeWard.id"
                from="${com.hrm.Ward.findAllByDistrict_codeAndProvince_code(userInforInstance?.homeDistrict?.district_code, userInforInstance?.homeProvince?.province_code)}"
                optionKey="ward_code" optionValue="name" noSelection="['': ' ']" required=""
                value="${userInforInstance?.homeWard?.ward_code}"/>
      <script>
        $("#homeWard").select2({
          ajax: {
            url: '${createLink(controller: 'ward',action: 'getWard')}',
            dataType: 'json',
            type: "POST",
            quietMillis: 50,
            data: function pushData(term) {
              return {
                provinceCode: $('#homeProvince').val(),
                districtCode: $('#homeDistrict').val(),
                name: term.term
              };
            },
            processResults: function (data) {
              return {
                results: data
              };
            },
            cache: true,
          },
        });
      </script>

      <div class="help-block"></div>

    </div>
  </g:if><g:else>
  <div class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'homeWard', 'error')} required">
    <label class="control-label" for="homeWard">
      <g:message code="userInfor.homeWard.label" default="Home Ward"/>
    </label>
    <br>
    <g:select class="form-control select2" id="homeWard" name="homeWard.id"
              from="${com.hrm.Ward.findAllByDistrict_codeAndProvince_code(districtCode, provinceCode)}"
              optionKey="ward_code" optionValue="name" noSelection="['0': ' ']"
              value="${wardCode}"/>
    <script>
      $("#homeWard").select2({
        ajax: {
          url: '${createLink(controller: 'ward',action: 'getWard')}',
          dataType: 'json',
          type: "POST",
          quietMillis: 50,
          data: function pushData(term) {
            return {
              provinceCode: $('#homeProvince').val(),
              districtCode: $('#homeDistrict').val(),
              name: term.term
            };
          },
          processResults: function (data) {
            return {
              results: data
            };
          },
          cache: true,
        },
      });
    </script>

    <div class="help-block"></div>

  </div>
</g:else>
  <div class="col-md-12"></div>
  <g:if test="${userInforInstance}">
    <div class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'permanentAddress', 'error')} required">
      <label class="control-label" for="permanentAddress">
        <g:message code="userInfor.permanentAddress.label" default="Permanent Address"/>
        <span class="text-red">*</span>
      </label>
      <br>


      <g:textField class="form-control" name="permanentAddress"
                   value="${userInforInstance?.permanentAddress}"/>


      <div class="help-block"></div>
    </div>
  </g:if>
  <g:if test="${userInforInstance}">
    <div
      class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'permanentProvince', 'error')} required">
      <label class="control-label" for="permanentProvince">
        <g:message code="userInfor.permanentProvince.label" default="Permanent Province"/>
        <span class="text-red">*</span>
      </label>
      <br>


      <g:select class="form-control many-to-one" id="permanentProvince" name="permanentProvince.id"
                from="${com.hrm.Province.list()}" optionKey="province_code" required=""
                optionValue="name" noSelection="['': ' ']"
                value="${userInforInstance?.permanentProvince?.province_code}"/>
      <div class="help-block"></div>
    </div>
  </g:if>
  <g:if test="${userInforInstance}">
    <div
      class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'permanentDistrict', 'error')} required">
      <label class="control-label" for="permanentDistrict">
        <g:message code="userInfor.permanentDistrict.label" default="Permanent District"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <g:select class="form-control many-to-one select2" id="permanentDistrict" name="permanentDistrict.id"
                from="${com.hrm.District.findAllByProvince_code(userInforInstance?.permanentProvince?.province_code)}"
                optionKey="district_code" required=""
                optionValue="name" noSelection="['': ' ']"
                value="${userInforInstance?.permanentDistrict?.district_code}"/>
      <script>
        $("#permanentDistrict").select2({
          ajax: {
            url: '${createLink(controller: 'district',action: 'getDistrict')}',
            dataType: 'json',
            type: "POST",
            quietMillis: 50,
            data: function pushData(term) {
              return {
                provinceCode: $('#permanentProvince').val(),
                name: term.term
              };
            },
            processResults: function (data) {
              return {
                results: data
              };
            },
            cache: true,
          },
        });

      </script>

      <div class="help-block"></div>
    </div>
  </g:if>
  <g:if test="${userInforInstance}">
    <div class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'permanentWard', 'error')} required">
      <label class="control-label" for="permanentWard">
        <g:message code="userInfor.permanentWard.label" default="Permanent Ward"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <g:select class="form-control many-to-one select2" id="permanentWard" name="permanentWard.id"
                from="${com.hrm.Ward.findAllByProvince_codeAndDistrict_code(userInforInstance?.permanentProvince?.province_code, userInforInstance?.permanentDistrict?.district_code)}"
                optionValue="name" noSelection="['': ' ']" optionKey="ward_code" required=""
                value="${userInforInstance?.permanentWard?.ward_code}"/>
      <script>
        $("#permanentWard").select2({
          ajax: {
            url: '${createLink(controller: 'ward',action: 'getWard')}',
            dataType: 'json',
            type: "POST",
            quietMillis: 50,
            data: function pushData(term) {
              return {
                provinceCode: $('#permanentProvince').val(),
                districtCode: $('#permanentDistrict').val(),
                name: term.term
              };
            },
            processResults: function (data) {
              return {
                results: data
              };
            },
            cache: true,
          },
        });
      </script>

      <div class="help-block"></div>
    </div>
  </g:if>

</fieldset>
<fieldset class="col-md-12">
  <legend>Labor Information</legend>
  <g:if test="${userInforInstance}">
    <div class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'personalId', 'error')} required">
      <label class="control-label" for="personalId">
        <g:message code="userInfor.personalId.label" default="Personal Id"/>
        <span class="text-red">*</span>
      </label>
      <br>


      <g:textField class="form-control" name="personalId" required="" value="${userInforInstance?.personalId}"/>


      <div class="help-block"></div>
    </div>
  </g:if>
  <g:if test="${userInforInstance}">
    <div class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'personalIdPhotoA', 'error')} required">
      <label class="control-label" for="personalIdPhotoA">
        <g:message code="userInfor.personalIdPhotoA.label" default="Personal Id Photo A"/>
        <span class="text-red">*</span>
      </label>
      <br>

      <div class="input-group">
        <span class="input-group-addon">
          <span class="input-group-text">Upload</span>
        </span>
        <g:textField id="image_personal_id" class="form-control" name="personalIdPhotoA"
                     onclick="\$('#file-personal-id').click()"
                     placeholder="No file choosen" required="" value="${userInforInstance?.personalIdPhotoA}"/>
        <span class="input-group-addon">
          <span class="input-group-text clear-image">clear</span>
        </span>
      </div>

      <div class="help-block"></div>
    </div>

    <div class="form-group col-md-3">
      <div id="box_image_personal_id" class="box-image" style="width: 190px;">
        <div class="image-item">
          <img src="${userInforInstance.photo ? createLink(controller: 'userInfor', action: 'loadImage',
            params: [
              'image': userInforInstance ? true : false,
              'path' : userInforInstance?.personalIdPhotoA]) : resource(dir: 'asset/dist/img', file: 'no-image.png')}"
               width="150" height="150" alt="no-image">
        </div>
      </div>
    </div>
  </g:if>
  <g:if test="${userInforInstance}">
    <div class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'releaseDate', 'error')} required">
      <label class="control-label" for="releaseDate">
        <g:message code="userInfor.releaseDate.label" default="Release Date"/>
        <span class="text-red">*</span>
      </label>
      <br>


      <div class='input-group' id='datetimepicker-releaseDate'>

        <g:field id="date-picker-field-releaseDate" class="form-control" name="releaseDate" type="text"
                 value="${userInforInstance?.releaseDate?.getTime()}"/>

        <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
      </div>
      <script>
        $("#datetimepicker-releaseDate").datetimepicker({
          format: 'YYYY-MM-DD',
          date: new Date(Number.parseInt($("#date-picker-field-releaseDate").val()))
        });
      </script>

      <div class="help-block"></div>
    </div>
  </g:if>
  <g:if test="${userInforInstance}">
    <div class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'releaseIn', 'error')} required">
      <label class="control-label" for="releaseIn">
        <g:message code="userInfor.releaseIn.label" default="Release In"/>
        <span class="text-red">*</span>
      </label>
      <br>


      <g:textField class="form-control" name="releaseIn" required="" value="${userInforInstance?.releaseIn}"/>


      <div class="help-block"></div>
    </div>
  </g:if>
  <div class="col-md-12"></div>
  <g:if test="${userInforInstance}">
    <div class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'level', 'error')} required">
      <label class="control-label" for="level">
        <g:message code="userInfor.level.label" default="Level"/>
        <span class="text-red">*</span>
      </label>
      <br>


      <g:textField class="form-control" name="level" required="" value="${userInforInstance?.level}"/>


      <div class="help-block"></div>
    </div>
  </g:if>
  <g:if test="${userInforInstance}">
    <div class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'certificate', 'error')} required">
      <label class="control-label" for="certificate">
        <g:message code="userInfor.certificate.label" default="Certificate"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <g:textField class="form-control" name="certificate" required="" value="${userInforInstance?.certificate}"/>
      <div class="help-block"></div>
    </div>
  </g:if>
  <g:if test="${userInforInstance}">
    <div class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'certificatePhoto', 'error')} required">
      <label class="control-label" for="certificatePhoto">
        <g:message code="userInfor.certificatePhoto.label" default="Certificate Photo"/>
        <span class="text-red">*</span>
      </label>
      <br>


      <div class="input-group">
        <span class="input-group-addon">
          <span class="input-group-text">Upload</span>
        </span>
        <g:textField id="image_certificate" class="form-control" name="certificatePhoto"
                     onclick="\$('#file-certificate').click()"
                     placeholder="No file choosen" required="" value="${userInforInstance?.photo}"/>
        <span class="input-group-addon">
          <span class="input-group-text clear-image">clear</span>
        </span>
      </div>

      <div class="help-block"></div>
    </div>

    <div class="form-group col-md-3">
      <div id="box_image_certificate" class="box-image" style="width: 190px;">
        <div class="image-item">
          <img src="${userInforInstance.photo ? createLink(controller: 'userInfor', action: 'loadImage',
            params: [
              'image': userInforInstance ? true : false,
              'path' : userInforInstance?.certificatePhoto]) : resource(dir: 'asset/dist/img', file: 'no-image.png')}"
               width="150" height="150" alt="no-image">
        </div>
      </div>
    </div>
  </g:if>
  <g:if test="${userInforInstance}">
    <div class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'laborContract', 'error')} required">
      <label class="control-label" for="laborContract">
        <g:message code="userInfor.laborContract.label" default="Labor Contract"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <g:select class="form-control many-to-one" id="laborContract" name="laborContract.id"
                from="${com.hrm.LaborContract.findAllByIdNotInList(UserInfor.findAll().laborContract.id)}"
                optionKey="id" required=""
                optionValue="contractNumber" value="${userInforInstance?.laborContract?.id}"/>
      <div class="help-block"></div>
    </div>
  </g:if><g:else>
  <div class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'laborContract', 'error')} required">
    <label class="control-label" for="laborContract">
      <g:message code="userInfor.laborContract.label" default="Labor Contract"/>
    </label>
    <br>
    <g:select class="form-control many-to-one" id="laborContract" name="laborContract.id"
              from="${com.hrm.LaborContract.list()}" optionKey="id"
              noSelection="[0: ' ']" optionValue="contractNumber" value="${params?.laborContract?.id}"/>
    <div class="help-block"></div>
  </div>
</g:else>
  <g:if test="${userInforInstance}">
    <div class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'entranceSalary', 'error')} required">
      <label class="control-label" for="entranceSalary">
        <g:message code="userInfor.entranceSalary.label" default="Entrance Salary"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <g:field id="entrance-salary" class="form-control" name="entranceSalary"
               value="${fieldValue(bean: userInforInstance, field: 'entranceSalary')}" required=""/>
      <div class="help-block"></div>
    </div>
  </g:if>
  <script>
    $('#entrance-salary').change(function () {
      if($(this).val() >= 9000000){
        $('.dependant-container').show();
      }else {
        $('.dependant-container').hide();
      }
    });
  </script>
  <g:if test="${userInforInstance}">
    <div class="dependant-container form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'dependant', 'error')} required">
      <label class="control-label" for="dependant">
        <g:message code="userInfor.dependant.label" default="Dependant"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <g:field class="form-control" name="dependant"
               value="${fieldValue(bean: userInforInstance, field: 'dependant')}" required=""/>
      <div class="help-block"></div>
    </div>
<script>
  $('.dependant-container').hide();
</script>
  </g:if>
  <g:if test="${userInforInstance}">
    <div class="form-group col-md-3 ${hasErrors(bean: userInforInstance, field: 'status', 'error')} required">
      <label class="control-label" for="status">
        <g:message code="userInfor.status.label" default="Status"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <g:textField class="form-control" name="status" required="" value="${userInforInstance?.status}"/>
      <div class="help-block"></div>
    </div>
  </g:if>
  <g:if test="${userInforInstance}">
    <div class="form-group col-md-3 required">
      <label class="control-label" for="user-depart">
        <g:message code="depart.name.label" default="Depart"/>
      </label>
      <br>
      <g:select class="form-control" name="depart" from="${com.hrm.Depart.list()}" optionKey="id"
                optionValue="name"
                value="${userDepartInstance?.id}"
                noSelection="[0: ' ']">
      </g:select>
      <div class="help-block"></div>
    </div>
  </g:if>
</fieldset>
<g:if test="${params.action != 'index' && params.action != ''}">
  <div style="opacity: 0" class="col-md-3 pull-right">
    <input type="file" class="form-control" id="file-photo" name="image_photo"/>
    <input type="file" class="form-control" id="file-certificate" name="image_certificate"/>
    <input type="file" class="form-control" id="file-personal-id" name="image_personal_id"/>
  </div>
</g:if>

