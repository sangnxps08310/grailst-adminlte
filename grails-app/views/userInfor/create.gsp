<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'userInfor.label', default: 'UserInfor')}"/>
  <title><g:message code="default.create.label"
                    args="${[message(code: 'userInfor.label', default: 'userInfor')]}">
  </g:message></title>

</head>
<body>

<div class="userInfor">
  <div id="create-userInfor" class="content scaffold-create" role="main">
  %{--    <h1><g:message code="default.create.label" args="[entityName]"/></h1>--}%
    <g:if test="${flash.message}">
      <div class="message alert alert-success" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${userInforInstance}">
      <div class="alert alert-warning">
        <ul class="errors" role="alert">
          <g:eachError bean="${userInforInstance}" var="error">
            <li<g:if
            test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
            error="${error}"/></li>
          </g:eachError>
        </ul>
      </div>
    </g:hasErrors>
    <div class="box box-primary">
      <div class="box-body">
        <g:form class="form"
                url="[resource: userInforInstance, action: 'save']"
                enctype="multipart/form-data">
          <div class="row">
            <g:render template="form"/>

            <div class="col-md-12"></div>
            <div class="col-md-12">
              <div class="col-md-3">
                <g:submitButton name="create" class="btn btn-success"
                                value="${message(code: 'default.button.create.label', default: 'Create')}"/>
              </div>
            </div>
          </div>
        </g:form>
      </div>

    </div>
  </div>

</div>
</body>
</html>
