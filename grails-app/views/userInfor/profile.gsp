<%--
  Created by IntelliJ IDEA.
  User: nhm95
  Date: 3/23/2020
  Time: 3:26 PM
--%>
<%@ page import="com.hrm.UserInforService; com.hrm.DepartUser; com.hrm.UserInfor" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'userInfor.label', default: 'UserInfor')}"/>
  <title><g:message code="userInfor.label"
                    args="${[message(code: 'userInfor.label.profile', default: 'User Profile')]}">
  </g:message></title>

</head>

<body>
<g:if test="${flash.message}">
  <div class="message alert alert-${flash.message_type}" role="status">${flash.message}</div>
</g:if>
<div class="row">

  <div class="col-md-3">
    <g:render template="profile-aside"></g:render>
    <!-- /.box -->
  </div>
  <!-- /.col -->
  <div class="col-md-9">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        %{--        <li ><a href="#general" data-toggle="tab">General</a></li>--}%
        %{--        <li><a href="#contract" data-toggle="tab">Contract</a></li>--}%
        %{--        <li><a href="#timeline" data-toggle="tab">Work</a></li>--}%
        <li class="active"><a href="#request" data-toggle="tab">Request</a></li>
        <li><a href="#changePass" data-toggle="tab">Change Password</a></li>
      </ul>

      <div class="tab-content">
        %{--        <div class="tab-pane" id="general">--}%
        %{--          <!-- The timeline -->--}%
        %{--          <g:render template="general"></g:render>--}%
        %{--        </div>--}%
        %{--        <div class="active tab-pane" id="contract">--}%
        %{--          <!-- Post -->--}%
        %{--          <!-- /.post -->--}%
        %{--        </div>--}%
        %{--        <!-- /.tab-pane -->--}%
        %{--        <div class="tab-pane" id="timeline">--}%
        %{--          <!-- The timeline -->--}%
        %{--        </div>--}%
        <!-- /.tab-pane -->

        <div class="tab-pane active" id="request">
          <g:render template="my-requests"></g:render>
        </div>

        <div class="tab-pane" id="changePass">
          <g:render template="change-pass"></g:render>
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- /.nav-tabs-custom -->
  </div>
  <!-- /.col -->
</div>
</body>
</html>