<%@ page import="com.hrm.LaborContract" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'laborContract.label', default: 'LaborContract')}"/>
  <title>
    <g:message code="default.edit.label"
               args="${[message( code: 'laborContract.label', default:'laborContract')]}">
    </g:message></title>

</head>

<body>
<div class="laborContract">
  <div id="edit-laborContract" class="content scaffold-edit" role="main">
%{--    <h3><g:message code="default.edit.label" args="[entityName]"/></h3>--}%
    <g:if test="${flash.message}">
      <div class="message alert alert-success" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${laborContractInstance}">
      <div class="alert alert-warning">
      <ul>
        <g:eachError bean="${laborContractInstance}" var="error">
          <li><g:message
            error="${error}"/>
          </li>
        </g:eachError>
      </ul>
      </div>
    </g:hasErrors>
    <div class="box box-primary">
      <div class="box-body">
    <g:form class="form rows" url="[resource: laborContractInstance, action: 'update']"
            method="PUT"   enctype="multipart/form-data" >
    <g:hiddenField class="form-control" name="version" value="${laborContractInstance?.version}"/>
    <g:render template="form"/>
    <div class="col-md-12"></div>
    <div class="buttons">
      <g:actionSubmit class="btn btn-success" action="update"
                      value="${message(code: 'default.button.update.label', default: 'Update')}"/>
    </div>
    </g:form>
      </div>
    </div>
  </div>
</div>
</body>
</html>
