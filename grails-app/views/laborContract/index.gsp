
<%@ page import="com.hrm.LaborContract" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'laborContract.label', default: 'LaborContract')}"/>
  <title><g:message code="laborContract.label"
                    args="${[message( code: 'laborContract.label', default:'laborContract')]}">
  </g:message></title>

</head>

<body>
<div class="laborContract">

  <g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
  </g:if>
  <div id="list-laborContract" class="box box-primary" role="main">
    <div class="box-header">
      <h4 class="box-title collapsed" data-toggle="collapse" data-target="#search-box" aria-expanded="true"><i
        class="fa fa-search"></i><g:message code="default.search.label" args=" "/></h4>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool btn-search-toggle collapsed" data-toggle="collapse"
                data-target="#search-box" aria-expanded="true">
          <i class="fa fa-plus"></i>
        </button>
      </div>
      <div class="help-block"></div>

      <div class="search-laborContract" id="search-box">
          <g:form class="form rows"
                  url="[resource: laborContractInstance, action: 'index']" >
          <g:render template="form"/>
          <div class="buttons col-md-3">
            <label>&nbsp;</label>
            <br>
            <g:submitButton name="search" class="btn btn-success"
                            value="${message(code: 'default.button.search.label', default: 'search')}"/>
          </div>
          </g:form>
        </div>
    </div>
    <div class="box-body">
      <div class="clearfix">
      </div>
      <g:link action="create" class="btn btn-success pull-right"><i class="fa fa-plus"></i></g:link>
      <table class="table table-hover" style="border-top: 1px solid #f4f4f4; border-bottom: 1px solid #f4f4f4">
        <thead>
        <tr>
          <th>#</th>
          
          <g:sortableColumn property="contractNumber"
                            title="${message(code: 'laborContract.contractNumber.label', default: 'Contract Number')}"/>
          
          <g:sortableColumn property="employeeName" style="text-align: left;"
                            title="${message(code: 'laborContract.employeeName.label', default: 'Employee Name')}"/>

          <g:sortableColumn property="employerName" style="text-align: left;"
                            title="${message(code: 'laborContract.employerName.label', default: 'Employer Name')}"/>

          <g:sortableColumn property="professionId"
                            title="${message(code: 'laborContract.professionId.label', default: 'Profession Id')}"/>

          <g:sortableColumn property="typeId"
                            title="${message(code: 'laborContract.typeId.label', default: 'Type Id')}"/>

          <g:sortableColumn property="effectiveDate"
                            title="${message(code: 'laborContract.effectiveDate.label', default: 'Effective Date')}"/>

          <g:sortableColumn property="expiredDate"
                            title="${message(code: 'laborContract.expiredDate.label', default: 'Expired Date')}"/>

          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${laborContractInstanceList}" status="i" var="laborContractInstance">
          <tr class="${(i % 2) == 0 ? 'even' : 'odd'}" scope="col">
            
            <td>${i+1}</td>

            <td><g:link action="show" id="${laborContractInstance.id}">${fieldValue(bean: laborContractInstance, field: "contractNumber")}</g:link></td>

            <td style="text-align: left" >${fieldValue(bean: laborContractInstance, field: "employeeName")}</td>

            <td style="text-align: left">${fieldValue(bean: laborContractInstance, field: "employerName")}</td>

            <td> ${com.hrm.Profession.findById(fieldValue(bean: laborContractInstance, field: "professionId")).professionName} </td>

            <td>
            ${com.hrm.TypesOfContract.findById(fieldValue(bean: laborContractInstance, field: "typeId")).name}
            </td>

            <td>
              <g:formatDate format="yyyy-MM-dd" date="${laborContractInstance.effectiveDate}"/>
            </td>

            <td>
              <g:formatDate format="yyyy-MM-dd" date="${laborContractInstance.expiredDate}"/>
            </td>

            <td><g:link action="edit" resource="${laborContractInstance}"><i
              class="glyphicon glyphicon-pencil"></i></g:link><g:link class="delete" action="delete"
                                                                      resource="${laborContractInstance}"><i
                class="glyphicon glyphicon-remove"></i></g:link></td>
          </tr>
        </g:each>
        </tbody>
      </table>
    </div>
    <div class="box-footer">
      <div class="pagination sang-pagination">
        <g:paginate total="${laborContractInstanceCount ?: 0}"/>
      </div>
    </div>
  </div>
</div>
</body>
</html>
