<%@ page import="com.hrm.LaborContract; java.text.SimpleDateFormat; java.text.DateFormat; com.hrm.Profession; com.hrm.TypesOfContract" %>

<% java.text.SimpleDateFormat dateFormat = new SimpleDateFormat('yyyy-MM-dd')%>


<%-- CONTENT--%>
<g:if test="${laborContractInstance}">
  <div class="form-group ${hasErrors(bean: laborContractInstance, field: 'content', 'error')} required">
    <label class="control-label" for="content">
      <g:message code="laborContract.content.label" default="Content"/>
      <span class="text-red">*</span>
    </label>
    <br>
      <g:textField class="form-control" name="content" required="" value="${laborContractInstance?.content}"/>
    <div class="help-block"></div>
  </div>
</g:if>

<%-- CONTRACT NUMBER--%>
<g:if test="${laborContractInstance}">
  <div class="form-group ${hasErrors(bean: laborContractInstance, field: 'contractNumber', 'error')} required">
    <label class="control-label" for="contractNumber">
      <g:message code="laborContract.contractNumber.label" default="Contract Number"/>
      <span class="text-red">*</span>
    </label>
    <br>
      <g:textField class="form-control" name="contractNumber" required="" value="${laborContractInstance?.contractNumber}"/>
    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="contractNumber">
        <g:message code="laborContract.contractNumber.label" default="Contract Number"/>
      </label>
      <br>
      <input class="form-control" name="contractNumber" value="${params.contractNumber}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>

<%-- TYPE CONTRACT--%>
<g:if test="${laborContractInstance}">
  <div class="form-group ${hasErrors(bean: laborContractInstance, field: 'type', 'error')} required">
    <label class="control-label" for="type">
      <g:message code="laborContract.type.label" default="Type"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <g:select class="form-control many-to-one" id="type" name="type.id" from="${TypesOfContract.list()}" optionValue="name" optionKey="id" required="" value="${laborContractInstance?.type?.id}"/>
    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="type">
        <g:message code="laborContract.type.label" default="Type"/>
      </label>
      <br>
      <g:select class="form-control many-to-one" id="type" name="type.id" from="${TypesOfContract.list()}" noSelection="[0:'-Select Type-']" optionValue="name" optionKey="id" required="" value="${laborContractInstance?.type?.id}"/>
      <div class="help-block"></div>
    </div>
  </div>
</g:else>

<%-- CREATE DATE --%>
<%--
<g:if test="${laborContractInstance}">
  <div class="form-group ${hasErrors(bean: laborContractInstance, field: 'createdDate', 'error')} required">
    <label class="control-label" for="createdDate">
      <g:message code="laborContract.createdDate.label" default="Created Date"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <div class='input-group' id='datetimepicker-createdDate'>
      <g:field id="date-picker-field" class="form-control" name="createdDate"  type="text"  value="${laborContractInstance?.createdDate?.getTime()}"/>
        <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
    </div>
    <script>
       $("#datetimepicker-createdDate").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#date-picker-field").val()))
      });
    </script>
    <div class="help-block"></div>
  </div>
</g:if>
<g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="createdDate">
        <g:message code="laborContract.createdDate.label" default="Created Date"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <input class="form-control" name="createdDate" value="${params.createdDate}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>
--%>

<%-- EFFECTIVE DATE--%>
<g:if test="${laborContractInstance}">
  <div class="form-group ${hasErrors(bean: laborContractInstance, field: 'effectiveDate', 'error')} required">
    <label class="control-label" for="effectiveDate">
      <g:message code="laborContract.effectiveDate.label" default="Effective Date"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <div class='input-group' id='datetimepicker-effectiveDate'>
      <g:field id="effective-date-field-from" class="form-control" type="text" name="effectiveDate"  value="${laborContractInstance?.effectiveDate?.getTime()}" required=""/>
        <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
    </div>
    <script>
       $("#datetimepicker-effectiveDate").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#effective-date-field-from").val()))
      });
    </script>
    <div class="help-block"></div>
  </div>
</g:if>
<%--
<g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="effectiveDate">
        <g:message code="laborContract.effectiveDate.label" default="Effective Date"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <input class="form-control" name="effectiveDate" value="${params.effectiveDate}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>
--%>

<%-- PROFESSION--%>
<g:if test="${laborContractInstance}">
  <div class="form-group ${hasErrors(bean: laborContractInstance, field: 'profession', 'error')} required">
    <label class="control-label" for="profession">
      <g:message code="laborContract.profession.label" default="Position"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <g:select class="form-control many-to-one" id="profession" name="profession.id" from="${Profession.list()}" optionKey="id" required="" optionValue="professionName" value="${laborContractInstance?.profession?.id}"/>
    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="profession">
        <g:message code="laborContract.profession.label" default="Position"/>
      </label>
      <br>
      <g:select class="form-control many-to-one" id="profession" name="profession.id" from="${Profession.list()}" noSelection="[0:'-Select Position-']" optionKey="id" required="" optionValue="professionName" value="${laborContractInstance?.profession?.id}"/>
      <div class="help-block"></div>
    </div>
  </div>
</g:else>

<%-- EMPLOYEE NAME --%>
<g:if test="${laborContractInstance}">
  <div class="form-group ${hasErrors(bean: laborContractInstance, field: 'employeeName', 'error')} required">
    <label class="control-label" for="employeeName">
      <g:message code="laborContract.employeeName.label" default="Employee Name"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <g:textField class="form-control" name="employeeName" required="" value="${laborContractInstance?.employeeName}"/>
    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="employeeName">
        <g:message code="laborContract.employeeName.label" default="Employee Name"/>
      </label>
      <br>
      <input class="form-control" name="employeeName" value="${params.employeeName}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>


<%-- EMPLOYEE DATE OF BIRTH --%>
<g:if test="${laborContractInstance}">
  <div class="form-group ${hasErrors(bean: laborContractInstance, field: 'employeeDateOfBirth', 'error')} required">
    <label class="control-label" for="employeeDateOfBirth">
      <g:message code="laborContract.employeeDateOfBirth.label" default="Employee Date Of Birth"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <div class='input-group' id='datetimepicker-employeeDateOfBirth'>
      <g:field id="date-picker-field" class="form-control" name="employeeDateOfBirth"  type="text"  value="${laborContractInstance?.employeeDateOfBirth?.getTime()}"/>
        <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
    </div>
    <script>
       $("#datetimepicker-employeeDateOfBirth").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#date-picker-field").val()))
      });
    </script>
    <div class="help-block"></div>
  </div>
</g:if>
<%--
<g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="employeeDateOfBirth">
        <g:message code="laborContract.employeeDateOfBirth.label" default="Employee Date Of Birth"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <input class="form-control" name="employeeDateOfBirth" value="${params.employeeDateOfBirth}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>
--%>


<%-- EMPLOYEE PHONE NUMBER--%>
<g:if test="${laborContractInstance}">
  <div class="form-group ${hasErrors(bean: laborContractInstance, field: 'employeePhoneNumber', 'error')} required">
    <label class="control-label" for="employeePhoneNumber">
      <g:message code="laborContract.employeePhoneNumber.label" default="Employee Phone Number"/>
      <span class="text-red">*</span>
    </label>
    <br>
      <g:textField class="form-control" name="employeePhoneNumber" required="" value="${laborContractInstance?.employeePhoneNumber}"/>
    <div class="help-block"></div>
  </div>
</g:if>


<%-- EMPLOYER NAME--%>
<g:if test="${laborContractInstance}">
  <div class="form-group ${hasErrors(bean: laborContractInstance, field: 'employerName', 'error')} required">
    <label class="control-label" for="employerName">
      <g:message code="laborContract.employerName.label" default="Employer Name"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <g:textField class="form-control" name="employerName" required="" value="${laborContractInstance?.employerName}"/>
    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="employerName">
        <g:message code="laborContract.employerName.label" default="Employer Name"/>
      </label>
      <br>
      <input class="form-control" name="employerName" value="${params.employerName}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>

<%-- EMPLOYER DATE OF BIRTH--%>
<g:if test="${laborContractInstance}">
  <div class="form-group ${hasErrors(bean: laborContractInstance, field: 'employerDateOfBirth', 'error')} required">
    <label class="control-label" for="employerDateOfBirth">
      <g:message code="laborContract.employerDateOfBirth.label" default="Employer Date Of Birth"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <div class='input-group' id='datetimepicker-employerDateOfBirth'>
      <g:field id="date-picker-field" class="form-control" name="employerDateOfBirth"  type="text"  value="${laborContractInstance?.employerDateOfBirth?.getTime()}"/>
        <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
    </div>
    <script>
       $("#datetimepicker-employerDateOfBirth").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#date-picker-field").val()))
      });
    </script>
    <div class="help-block"></div>
  </div>
</g:if>
<%--
<g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="employerDateOfBirth">
        <g:message code="laborContract.employerDateOfBirth.label" default="Employer Date Of Birth"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <input class="form-control" name="employerDateOfBirth" value="${params.employerDateOfBirth}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>
--%>

<%-- EMPLOYER PHONE NUMBER --%>
<g:if test="${laborContractInstance}">
  <div class="form-group ${hasErrors(bean: laborContractInstance, field: 'employerPhoneNumber', 'error')} required">
    <label class="control-label" for="employerPhoneNumber">
      <g:message code="laborContract.employerPhoneNumber.label" default="Employer Phone Number"/>
      <span class="text-red">*</span>
    </label>
    <br>
      <g:textField class="form-control" name="employerPhoneNumber" required="" value="${laborContractInstance?.employerPhoneNumber}"/>
    <div class="help-block"></div>
  </div>
</g:if>


<%-- EXPIRED DATE--%>
<g:if test="${laborContractInstance}">
  <div class="form-group ${hasErrors(bean: laborContractInstance, field: 'expiredDate', 'error')} required">
    <label class="control-label" for="expiredDate">
      <g:message code="laborContract.expiredDate.label" default="Expired Date"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <div class='input-group' id='datetimepicker-expiredDate'>
      <g:field id="date-picker-field" class="form-control" name="expiredDate"  type="text"  value="${laborContractInstance?.expiredDate?.getTime()}"/>
        <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
    </div>
    <script>
       $("#datetimepicker-expiredDate").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#date-picker-field").val()))
      });
    </script>
    <div class="help-block"></div>
  </div>
</g:if>
<g:else>
  <div class="form-group col-md-3">
    <label class="control-label" for="expired-date-field-from-1">
      <g:message code="laborContract.expiredDate1.label" default="Expired Date From"/>
    </label>
    <br>

    <div class='input-group' id='expired-date-from-1'>
      <g:field id="expired-date-field-from-1" class="form-control" type="text" name="expiredDateFrom"
               value="${dateFormat.parse(params.expiredDateFrom ? params.expiredDateFrom : dateFormat.format(new Date())).getTime()}"
               required=""/>
      <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
    </div>
    <script type="text/javascript">
      $("#expired-date-from-1").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#expired-date-field-from-1").val()))
      });
    </script>

    <div class="help-block"></div>
  </div>

  <div class="form-group col-md-3">
    <label class="control-label" for="expired-date-to">
      <g:message code="laborContract.expiredDate2.label" default="Expired Date To"/>
    </label>
    <br>

    <div class='input-group' id='expired-date-to'>
      <g:field id="expired-date-field-to" class="form-control" type="text" name="expiredDateTo"
               value="${dateFormat.parse(params.expiredDateTo ? params.expiredDateTo : dateFormat.format(new Date())).getTime()}"
               required=""/>
      <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
    </div>
    <script type="text/javascript">
      $("#expired-date-to").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#expired-date-field-to").val()))
      });
    </script>

    <div class="help-block"></div>
  </div>
</g:else>


<%-- PHOTO --%>
<g:if test="${laborContractInstance}">
  <div class="form-group  ${hasErrors(bean: laborContractInstance, field: 'photo', 'error')} required">
    <label class="control-label" for="photo">
      <g:message code="laborContract.photo.label" default="Photo"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <div class="input-group">
      <span class="input-group-addon">
        <span class="input-group-text">Upload</span>
      </span>
      <g:textField id="image_photo" class="form-control" name="photo" onclick="\$('#file-photo').click()"
                   placeholder="No file choosen" required="" value="${laborContractInstance?.photo}"/>
      <span class="input-group-addon">
        <span class="input-group-text clear-image">clear</span>
      </span>
    </div>
    <div class="help-block"></div>
  </div>
  <div class="form-group col-md-12">
    <div id="box_image_photo" class="box-image" style="width: 190px;">
      <div class="image-item">
        <img src="${laborContractInstance.photo ? createLink(controller: 'laborContract', action: 'loadImage',
                params: [
                        'image': laborContractInstance ? true : false,
                        'path' : laborContractInstance?.photo]) : resource(dir: 'asset/dist/img', file: 'no-image.png')}"
             width="150" height="150" alt="no-image">
      </div>
    </div>
  </div>
</g:if>


<%-- FILEFOLDER--%>
  <g:if test="${laborContractInstance}">
    <div class="form-group ${hasErrors(bean: laborContractInstance, field: 'fileFolder', 'error')}">
      <label class="control-label" for="fileFolder">
        <g:message code="laborContract.fileFolder.label" default="File Foler"/>
      </label>
      <br>
      <div class="input-group">
        <span class="input-group-addon">
          <span class="input-group-text">Upload</span>
        </span>
        <g:textField id="fileFolder" class="form-control" name="fileFolder" onclick="\$('#attachFile').click()"
                     placeholder="No file choosen" required="" value="${laborContractInstance?.fileFolder}"/>
      </div>
      <div class="help-block"></div>
    </div>
  </g:if>

<%-- SIGN DATE --%>
<g:if test="${laborContractInstance}">
  <div class="form-group ${hasErrors(bean: laborContractInstance, field: 'signDate', 'error')} required">
    <label class="control-label" for="signDate">
      <g:message code="laborContract.signDate.label" default="Sign Date"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <div class='input-group' id='datetimepicker-signDate'>
      <g:field id="date-picker-field" class="form-control" name="signDate"  type="text"  value="${laborContractInstance?.signDate?.getTime()}"/>
        <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
    </div>
    <script>
       $("#datetimepicker-signDate").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#date-picker-field").val()))
      });
    </script>
    <div class="help-block"></div>
  </div>
</g:if>
<%--
<g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="signDate">
        <g:message code="laborContract.signDate.label" default="Sign Date"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <input class="form-control" name="signDate" value="${params.signDate}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>
--%>

<script>
  $(document).ready(function () {
    $('#attachFile').change(function () {
      $('#fileFolder').val($(this)[0].files[0].name);
    });
  });
</script>
<g:if test="${params.action != 'index' && params.action != ''}">
  <div style="opacity: 0" class="col-md-3 pull-right">
    <input type="file" class="image form-control" id="file-photo" name="image_photo"/>
    <input type="file" class="form-control" id="attachFile" name="attachFile"/>
  </div>
</g:if>

