
<%@ page import="com.hrm.LaborContract; com.hrm.TypesOfContract " %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'laborContract.label', default: 'LaborContract')}"/>
  <title><g:message code="default.show.label"
                    args="${[message( code: 'laborContract.label', default:'laborContract')]}">
</g:message></title>
  <style>
  td {
    text-align: left!important;
  }
  </style>
</head>

<body>
<div class="laborContract">
  <div id="show-laborContract" class="content scaffold-show" role="main">
%{--    <h3><g:message code="default.show.label" args="[entityName]"/></h3>--}%
    <g:if test="${flash.message}">
      <div class="message alert alert-success" role="status">${flash.message}</div>
    </g:if>
    <div class="box box-primary">
      <div class="box-body">
        <h4><i class="fa fa-book"></i>Details: </h4>
        <table class="laborContract table no-margin"s>
          
          %{--          <g:if test="${laborContractInstance?.content}">--}%
          <tr class="fieldcontain">
            <th id="content-label" class="property-label"><g:message
              code="laborContract.content.label" default="Content"/></th>
            
            <td class="property-value" aria-labelledby="content-label"><g:fieldValue bean="${laborContractInstance}"
                                                                                       field="content"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${laborContractInstance?.contractNumber}">--}%
          <tr class="fieldcontain">
            <th id="contractNumber-label" class="property-label"><g:message
              code="laborContract.contractNumber.label" default="Contract Number"/></th>
            
            <td class="property-value" aria-labelledby="contractNumber-label"><g:fieldValue bean="${laborContractInstance}"
                                                                                       field="contractNumber"/></td>
            
          </tr>
          %{--          </g:if>--}%


          %{--          <g:if test="${laborContractInstance?.type}">--}%
          <tr class="fieldcontain">
            <th id="type-label" class="property-label"><g:message
                    code="laborContract.type.label" default="Type"/></th>

            <td class="property-value" aria-labelledby="type-label">
              <g:link controller="typesOfContract" action="show" id="${laborContractInstance?.type?.id}">
                ${com.hrm.TypesOfContract.findById(fieldValue(bean: laborContractInstance, field: "typeId")).name}
              </g:link>

            </td>

          </tr>
          %{--          </g:if>--}%



          %{--          <g:if test="${laborContractInstance?.createdDate}">--}%
          <tr class="fieldcontain">
            <th id="createdDate-label" class="property-label"><g:message
              code="laborContract.createdDate.label" default="Created Date"/></th>
            
            <td class="property-value" aria-labelledby="createdDate-label"><g:formatDate format="yyyy-MM-dd"
              date="${laborContractInstance?.createdDate}"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${laborContractInstance?.effectiveDate}">--}%
          <tr class="fieldcontain">
            <th id="effectiveDate-label" class="property-label"><g:message
              code="laborContract.effectiveDate.label" default="Effective Date"/></th>
            
            <td class="property-value" aria-labelledby="effectiveDate-label"><g:formatDate format="yyyy-MM-dd"
              date="${laborContractInstance?.effectiveDate}"/></td>
            
          </tr>
          %{--          </g:if>--}%


          %{--          <g:if test="${laborContractInstance?.profession}">--}%
          <tr class="fieldcontain">
            <th id="profession-label" class="property-label"><g:message
                    code="laborContract.profession.label" default="Profession"/></th>

            <td class="property-value" aria-labelledby="profession-label">
              <g:link controller="profession" action="show" id="${laborContractInstance?.profession?.id}">
                ${com.hrm.Profession.findById(fieldValue(bean: laborContractInstance, field: "professionId")).professionName}
              </g:link></td>

          </tr>
          %{--          </g:if>--}%

          %{--          <g:if test="${laborContractInstance?.employeeName}">--}%
          <tr class="fieldcontain">
            <th id="employeeName-label" class="property-label"><g:message
                    code="laborContract.employeeName.label" default="Employee Name"/></th>

            <td class="property-value" aria-labelledby="employeeName-label"><g:fieldValue bean="${laborContractInstance}"
                                                                                          field="employeeName"/></td>

          </tr>
          %{--          </g:if>--}%


          %{--          <g:if test="${laborContractInstance?.employeeDateOfBirth}">--}%
          <tr class="fieldcontain">
            <th id="employeeDateOfBirth-label" class="property-label"><g:message
              code="laborContract.employeeDateOfBirth.label" default="Employee Date Of Birth"/></th>
            
            <td class="property-value" aria-labelledby="employeeDateOfBirth-label"><g:formatDate format="yyyy-MM-dd"
              date="${laborContractInstance?.employeeDateOfBirth}"/></td>
            
          </tr>
          %{--          </g:if>--}%
          

          
          %{--          <g:if test="${laborContractInstance?.employeePhoneNumber}">--}%
          <tr class="fieldcontain">
            <th id="employeePhoneNumber-label" class="property-label"><g:message
              code="laborContract.employeePhoneNumber.label" default="Employee Phone Number"/></th>
            
            <td class="property-value" aria-labelledby="employeePhoneNumber-label"><g:fieldValue bean="${laborContractInstance}"
                                                                                       field="employeePhoneNumber"/></td>
            
          </tr>
          %{--          </g:if>--}%



          %{--          <g:if test="${laborContractInstance?.employerName}">--}%
          <tr class="fieldcontain">
            <th id="employerName-label" class="property-label"><g:message
                    code="laborContract.employerName.label" default="Employer Name"/></th>

            <td class="property-value" aria-labelledby="employerName-label"><g:fieldValue bean="${laborContractInstance}"
                                                                                          field="employerName"/></td>

          </tr>
          %{--          </g:if>--}%

          %{--          <g:if test="${laborContractInstance?.employerDateOfBirth}">--}%
          <tr class="fieldcontain">
            <th id="employerDateOfBirth-label" class="property-label"><g:message
              code="laborContract.employerDateOfBirth.label" default="Employer Date Of Birth"/></th>
            
            <td class="property-value" aria-labelledby="employerDateOfBirth-label"><g:formatDate format="yyyy-MM-dd"
              date="${laborContractInstance?.employerDateOfBirth}"/></td>
            
          </tr>
          %{--          </g:if>--}%
          

          
          %{--          <g:if test="${laborContractInstance?.employerPhoneNumber}">--}%
          <tr class="fieldcontain">
            <th id="employerPhoneNumber-label" class="property-label"><g:message
              code="laborContract.employerPhoneNumber.label" default="Employer Phone Number"/></th>
            
            <td class="property-value" aria-labelledby="employerPhoneNumber-label"><g:fieldValue bean="${laborContractInstance}"
                                                                                       field="employerPhoneNumber"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${laborContractInstance?.expiredDate}">--}%
          <tr class="fieldcontain">
            <th id="expiredDate-label" class="property-label"><g:message
              code="laborContract.expiredDate.label" default="Expired Date"/></th>
            
            <td class="property-value" aria-labelledby="expiredDate-label"><g:formatDate format="yyyy-MM-dd"
              date="${laborContractInstance?.expiredDate}"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${laborContractInstance?.photo}">--}%
          <tr class="fieldcontain">
            <th id="photo-label" class="property-label"><g:message
              code="laborContract.photo.label" default="Photo"/></th>
            
            <td class="property-value" aria-labelledby="photo-label">
              %{--    <g:fieldValue bean="${laborContractInstance}" field="photo"/> --}%
              <div class="modal-body">
                <div class="container-fluid">
                  <div class="row">
                    <div class="col-md-12">
                      <img src="${laborContractInstance.photo ? createLink(controller: 'laborContract', action: 'loadImage',
                        params: [
                          'image': laborContractInstance ? true : false,
                          'path' : laborContractInstance?.photo]) : resource(dir: 'asset/dist/img', file: 'no-image.png')}"
                           width="150" height="150" alt="no-image">
                    </div>
                  </div>
                </div>
              </div>
          </td>
        </tr>



        %{--          </g:if>--}%
          
          %{--          <g:if test="${laborContractInstance?.fileFolder}">--}%
          <tr class="fieldcontain">
            <th id="fileFolder-label" class="property-label"><g:message
              code="laborContract.fileFolder.label" default="File Folder"/></th>
            
            <td class="property-value" aria-labelledby="fileFolder-label">
              <g:link action="download" id="${laborContractInstance.id}"><g:fieldValue bean="${laborContractInstance}" field="fileFolder"/>&nbsp;<i class="fa fa-download"></i></g:link>
            </td>
          </tr>
          %{--          </g:if>--}%
          

          
          %{--          <g:if test="${laborContractInstance?.signDate}">--}%
          <tr class="fieldcontain">
            <th id="signDate-label" class="property-label"><g:message
              code="laborContract.signDate.label" default="Sign Date"/></th>
            
            <td class="property-value" aria-labelledby="signDate-label"><g:formatDate format="yyyy-MM-dd"
              date="${laborContractInstance?.signDate}"/></td>
            
          </tr>
          %{--          </g:if>--}%
          

          
        </table>
        <g:form url="[resource: laborContractInstance, action: 'delete']" method="DELETE">
          <fieldset class="buttons">
            <g:link class="edit btn btn-info" action="edit" resource="${laborContractInstance}"><g:message
              code="default.button.edit.label"
              default="Edit"/></g:link>
            <g:actionSubmit class="delete btn btn-danger" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
            <g:link class="generate btn btn-warning" controller="userInfor"  action="generateUserInfo" id="${laborContractInstance.id}"><g:message
              code="default.button.generate.label"
              default="Generate User Info"/></g:link>
          </fieldset>
        </g:form>
      </div>
    </div>
  </div>
</div>
</body>
</html>
