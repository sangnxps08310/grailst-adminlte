
<%@ page import="com.hrm.RollPay" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'rollPay.label', default: 'RollPay')}"/>
  <title><g:message code="default.show.label"
                    args="${[message( code: 'rollPay.label', default:'rollPay')]}">
</g:message></title>
</head>

<body>
%{--		<a href="#show-rollPay" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>--}%
%{--		<div class="nav" role="navigation">--}%
%{--			<ul>--}%
%{--				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
%{--				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>--}%
%{--				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>--}%
%{--			</ul>--}%
%{--		</div>--}%
<div class="rollPay">
  <div id="show-rollPay" class="content scaffold-show" role="main">
%{--    <h3><g:message code="default.show.label" args="[entityName]"/></h3>--}%
    <g:if test="${flash.message}">
      <div class="message alert alert-success" role="status">${flash.message}</div>
    </g:if>
    <div class="box box-primary">
      <div class="box-body">
        <h4><i class="fa fa-book"></i>Details: </h4>
        <table class="rollPay table no-margin"s>
          
          %{--          <g:if test="${rollPayInstance?.allowance}">--}%
          <tr class="fieldcontain">
            <th id="allowance-label" class="property-label"><g:message
              code="rollPay.allowance.label" default="Allowance"/></th>
            
            <td class="property-value" aria-labelledby="allowance-label"><g:fieldValue bean="${rollPayInstance}"
                                                                                       field="allowance"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${rollPayInstance?.bonus}">--}%
          <tr class="fieldcontain">
            <th id="bonus-label" class="property-label"><g:message
              code="rollPay.bonus.label" default="Bonus"/></th>
            
            <td class="property-value" aria-labelledby="bonus-label"><g:fieldValue bean="${rollPayInstance}"
                                                                                       field="bonus"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${rollPayInstance?.effectiveDate}">--}%
          <tr class="fieldcontain">
            <th id="effectiveDate-label" class="property-label"><g:message
              code="rollPay.effectiveDate.label" default="Effective Date"/></th>
            
            <td class="property-value" aria-labelledby="effectiveDate-label"><g:formatDate
              date="${rollPayInstance?.effectiveDate}"/></td>
            
          </tr>
          %{--          </g:if>--}%

          
          %{--          <g:if test="${rollPayInstance?.insurance}">--}%
          <tr class="fieldcontain">
            <th id="insurance-label" class="property-label"><g:message
              code="rollPay.insurance.label" default="Insurance"/></th>
            
            <td class="property-value" aria-labelledby="insurance-label"><g:link
              controller="insurances" action="show"
              id="${rollPayInstance?.insurance}">${rollPayInstance?.insurance?.encodeAsHTML()}</g:link></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${rollPayInstance?.rankSalary}">--}%
          <tr class="fieldcontain">
            <th id="rankSalary-label" class="property-label"><g:message
              code="rollPay.rankSalary.label" default="Rank Salary"/></th>
            
            <td class="property-value" aria-labelledby="rankSalary-label"><g:link
              controller="rankSalary" action="show"
              id="${rollPayInstance?.rankSalary?.id}">${rollPayInstance?.rankSalary?.encodeAsHTML()}</g:link></td>
            
          </tr>
          %{--          </g:if>--}%
          

          

          %{--          </g:if>--}%
          
          %{--          <g:if test="${rollPayInstance?.tax}">--}%
          <tr class="fieldcontain">
            <th id="tax-label" class="property-label"><g:message
              code="rollPay.tax.label" default="Tax"/></th>
            
            <td class="property-value" aria-labelledby="tax-label"><g:link
              controller="tax" action="show"
              id="${rollPayInstance?.tax}">${rollPayInstance?.tax?.encodeAsHTML()}</g:link></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${rollPayInstance?.tempSalary}">--}%
          <tr class="fieldcontain">
            <th id="tempSalary-label" class="property-label"><g:message
              code="rollPay.tempSalary.label" default="Temp Salary"/></th>
            
            <td class="property-value" aria-labelledby="tempSalary-label"><g:fieldValue bean="${rollPayInstance}"
                                                                                       field="tempSalary"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${rollPayInstance?.total}">--}%
          <tr class="fieldcontain">
            <th id="total-label" class="property-label"><g:message
              code="rollPay.total.label" default="Total"/></th>
            
            <td class="property-value" aria-labelledby="total-label"><g:fieldValue bean="${rollPayInstance}"
                                                                                       field="total"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${rollPayInstance?.userInfor}">--}%
          <tr class="fieldcontain">
            <th id="userInfor-label" class="property-label"><g:message
              code="rollPay.userInfor.label" default="User Infor"/></th>
            
            <td class="property-value" aria-labelledby="userInfor-label"><g:link
              controller="userInfor" action="show"
              id="${rollPayInstance?.userInfor?.id}">${rollPayInstance?.userInfor?.encodeAsHTML()}</g:link></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${rollPayInstance?.workDay}">--}%
          <tr class="fieldcontain">
            <th id="workDay-label" class="property-label"><g:message
              code="rollPay.workDay.label" default="Work Day"/></th>
            
            <td class="property-value" aria-labelledby="workDay-label"><g:fieldValue bean="${rollPayInstance}"
                                                                                       field="workDay"/></td>
            
          </tr>
          %{--          </g:if>--}%

          %{--          <g:if test="${rollPayInstance?.rollPayHistory}">--}%
          <tr class="fieldcontain">
            <th id="rollPayHistory-label" class="property-label"><g:message
                    code="rollPay.rollPayHistory.label" default="Roll Pay History"/></th>

            <td class="property-value" aria-labelledby="rollPayHistory-label"><g:link
                    controller="rollPayHistory" action="show"
                    id="${rollPayInstance?.rollPayHistory?.id}">${rollPayInstance?.rollPayHistory?.encodeAsHTML()}</g:link></td>

          </tr>
          
        </table>
        <g:form url="[resource: rollPayInstance, action: 'delete']" method="DELETE">
          <fieldset class="buttons">
            <g:link class="edit btn btn-info" action="edit" resource="${rollPayInstance}"><g:message
              code="default.button.edit.label"
              default="Edit"/></g:link>
            <g:actionSubmit class="delete btn btn-danger" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
          </fieldset>
        </g:form>
      </div>
    </div>
  </div>
</div>
</body>
</html>
