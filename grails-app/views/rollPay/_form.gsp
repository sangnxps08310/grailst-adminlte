<%@ page import="com.hrm.RollPay" %>


<%-- User Information --%>
<g:if test="${rollPayInstance}">
  <div class="form-group ${hasErrors(bean: rollPayInstance, field: 'userInfor', 'error')} required">
    <label class="control-label" for="userInfor">
      <g:message code="rollPay.userInfor.label" default="User Infor"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <g:select class="form-control many-to-one" id="userInfor" name="userInfor.id" from="${com.hrm.UserInfor.list()}" optionValue="name" optionKey="id" required="" value="${rollPayInstance?.userInfor?.id}"/>
    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="userInfor">
        <g:message code="rollPay.userInfor.label" default="User Infor"/>
      </label>
      <br>
      <input class="form-control" name="userInfor" value="${params.userInfor}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>


<%-- Rank Salary --%>
<g:if test="${rollPayInstance}">
  <div class="form-group ${hasErrors(bean: rollPayInstance, field: 'rankSalary', 'error')} required">
    <label class="control-label" for="rankSalary">
      <g:message code="rollPay.rankSalary.label" default="Rank Salary"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <g:select class="form-control many-to-one" id="rankSalary" name="rankSalary.id" from="${com.hrm.RankSalary.list()}" optionValue="rankSalary" optionKey="id" required="" value="${rollPayInstance?.rankSalary?.id}"/>
    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="rankSalary">
        <g:message code="rollPay.rankSalary.label" default="Rank Salary"/>
      </label>
      <br>
      <input class="form-control" name="rankSalary" value="${params.rankSalary}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>

<%-- Workday --%>
<g:if test="${rollPayInstance}">
  <div class="form-group ${hasErrors(bean: rollPayInstance, field: 'workDay', 'error')} required">
    <label class="control-label" for="workDay">
      <g:message code="rollPay.workDay.label" default="Work Day"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <g:textField class="form-control" name="workDay" required="" value="${rollPayInstance?.workDay}"/>
    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="workDay">
        <g:message code="rollPay.workDay.label" default="Work Day"/>
      </label>
      <br>
      <input class="form-control" name="workDay" value="${params.workDay}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>

<%-- Temp Salary --%>
<g:if test="${rollPayInstance}">
  <div class="form-group ${hasErrors(bean: rollPayInstance, field: 'tempSalary', 'error')} required">
    <label class="control-label" for="tempSalary">
      <g:message code="rollPay.tempSalary.label" default="Temp Salary"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <g:textField class="form-control" name="tempSalary" required="" value="${rollPayInstance?.tempSalary}"/>
    <div class="help-block"></div>
  </div>
</g:if>
<%--
<g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="tempSalary">
        <g:message code="rollPay.tempSalary.label" default="Temp Salary"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <input class="form-control" name="tempSalary" value="${params.tempSalary}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>--%>

<g:if test="${rollPayInstance}">
  <div class="form-group ${hasErrors(bean: rollPayInstance, field: 'allowance', 'error')} required">
    <label class="control-label" for="allowance">
      <g:message code="rollPay.allowance.label" default="Allowance"/>
      <span class="text-red">*</span>
    </label>
    <br>
      <g:textField class="form-control" name="allowance" required="" value="${rollPayInstance?.allowance}"/>
    <div class="help-block"></div>
  </div>
</g:if>
<%--
<g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="allowance">
        <g:message code="rollPay.allowance.label" default="Allowance"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <input class="form-control" name="allowance" value="${params.allowance}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>--%>


<g:if test="${rollPayInstance}">
  <div class="form-group ${hasErrors(bean: rollPayInstance, field: 'bonus', 'error')} required">
    <label class="control-label" for="bonus">
      <g:message code="rollPay.bonus.label" default="Bonus"/>
      <span class="text-red">*</span>
    </label>
    <br>
      <g:textField class="form-control" name="bonus" required="" value="${rollPayInstance?.bonus}"/>
    <div class="help-block"></div>
  </div>
</g:if>
<%--
<g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="bonus">
        <g:message code="rollPay.bonus.label" default="Bonus"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <input class="form-control" name="bonus" value="${params.bonus}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>--%>

<%-- Tax --%>
<g:if test="${rollPayInstance}">
  <div class="form-group ${hasErrors(bean: rollPayInstance, field: 'tax', 'error')} required">
    <label class="control-label" for="tax">
      <g:message code="rollPay.tax.label" default="Tax"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <g:textField class="form-control" name="tax" required="" value="${rollPayInstance?.tax}"/>
    <div class="help-block"></div>
  </div>
</g:if>
<%--
<g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="tax">
        <g:message code="rollPay.tax.label" default="Tax"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <input class="form-control" name="tax" value="${params.tax}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>--%>

<%-- Insurance --%>
<g:if test="${rollPayInstance}">
  <div class="form-group ${hasErrors(bean: rollPayInstance, field: 'insurance', 'error')} required">
    <label class="control-label" for="insurance">
      <g:message code="rollPay.insurance.label" default="Insurance"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <%--
    <g:select class="form-control many-to-one" id="insurance" name="insurance.id" from="${com.hrm.Insurances.list()}" optionKey="id" required="" value="${rollPayInstance?.insurance?.id}"/>
    --%>
    <g:each in="${com.hrm.Insurances.list()}" var="r">
      <td>
        <g:checkBox name="insurance" value="${r.factor}" checked="" id="${r.id}"/>
        <label>
          ${r.name}
        </label>
        &nbsp; &nbsp; &nbsp;
      </td>
      </tr>
    </g:each>

  <g:each in="${insuranceName}" var="r">
    <td>
      <label>
        <g:checkBox name="factor" value="${insurancesInstance?.factor}"/>
        ${r.name}
      </label>
    </td>
    </tr>
  </g:each>
    <div class="help-block"></div>
  </div>
</g:if>
<%---
<g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="insurance">
        <g:message code="rollPay.insurance.label" default="Insurance"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <input class="form-control" name="insurance" value="${params.insurance}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>--%>

<%-- Total --%>
<g:if test="${rollPayInstance}">
  <div class="form-group ${hasErrors(bean: rollPayInstance, field: 'total', 'error')} required">
    <label class="control-label" for="total">
      <g:message code="rollPay.total.label" default="Total"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <g:textField class="form-control" name="total" required="" value="${rollPayInstance?.total}"/>
    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="total">
        <g:message code="rollPay.total.label" default="Total"/>
      </label>
      <br>
      <input class="form-control" name="total" value="${params.total}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>

<g:if test="${rollPayInstance}">
  <div class="form-group ${hasErrors(bean: rollPayInstance, field: 'effectiveDate', 'error')} required">
    <label class="control-label" for="effectiveDate">
      <g:message code="rollPay.effectiveDate.label" default="Effective Date"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <div class='input-group' id='datetimepicker-effectiveDate'>
      <g:field id="date-picker-field" class="form-control" name="effectiveDate"  type="text"  value="${rollPayInstance?.effectiveDate}"/>
        <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
    </div>
    <script>
       $("#datetimepicker-effectiveDate").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#date-picker-field-effectiveDate").val()))
      });
    </script>
    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="effectiveDate">
        <g:message code="rollPay.effectiveDate.label" default="Effective Date"/>
      </label>
      <br>
      <input class="form-control" name="effectiveDate" value="${params.effectiveDate}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>



<%-- Roll Pay History --%>
<g:if test="${rollPayInstance}">
  <div class="form-group ${hasErrors(bean: rollPayInstance, field: 'rollPayHistory', 'error')} required">
    <label class="control-label" for="rollPayHistory">
      <g:message code="rollPay.rollPayHistory.label" default="Roll Pay History"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <g:select class="form-control many-to-one" id="rollPayHistory" name="rollPayHistory.id" from="${com.hrm.RollPayHistory.list()}" optionValue="id" optionKey="id" required="" value="${rollPayInstance?.rollPayHistory?.id}"/>
    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="rollPayHistory">
        <g:message code="rollPay.rollPayHistory.label" default="Roll Pay History"/>
      </label>
      <br>
      <input class="form-control" name="rollPayHistory" value="${params.rollPayHistory}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>














