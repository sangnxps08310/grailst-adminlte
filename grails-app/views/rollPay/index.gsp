
<%@ page import="com.hrm.RollPay" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'rollPay.label', default: 'RollPay')}"/>
  <title><g:message code="rollPay.label"
                    args="${[message( code: 'rollPay.label', default:'rollPay')]}">
  </g:message></title>

</head>

<body>
<div class="rollPay">

  <g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
  </g:if>
  <div id="list-rollPay" class="box box-primary" role="main">
    <div class="box-header">
      <h4 class="box-title collapsed" data-toggle="collapse" data-target="#search-box" aria-expanded="true"><i
        class="fa fa-search"></i><g:message code="default.search.label" args=" "/></h4>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool btn-search-toggle collapsed" data-toggle="collapse"
                data-target="#search-box" aria-expanded="true">
          <i class="fa fa-plus"></i>
        </button>
      </div>
      <div class="help-block"></div>

      <div class="search-rollPay" id="search-box">
          <g:form class="form rows"
                  url="[resource: rollPayInstance, action: 'index']" >
          <g:render template="form"/>
          <div class="buttons col-md-4">
            <label>&nbsp;</label>
            <br>
            <g:submitButton name="search" class="btn btn-success"
                            value="${message(code: 'default.button.search.label', default: 'search')}"/>
          </div>
          </g:form>
        </div>
    </div>
    <div class="box-body">
      <div class="clearfix">
      </div>
      <g:link action="create" class="btn btn-success pull-right"><i class="fa fa-plus"></i></g:link>
      <table class="table table-hover" style="border-top: 1px solid #f4f4f4; border-bottom: 1px solid #f4f4f4">
        <thead>
        <tr>
          <th>#</th>
          
          <g:sortableColumn property="allowance"
                            title="${message(code: 'rollPay.allowance.label', default: 'Allowance')}"/>
          
          <g:sortableColumn property="bonus"
                            title="${message(code: 'rollPay.bonus.label', default: 'Bonus')}"/>
          
          <g:sortableColumn property="effectiveDate"
                            title="${message(code: 'rollPay.effectiveDate.label', default: 'Effective Date')}"/>

          
          <th><g:message code="rollPay.insurance.label" default="Insurance"/></th>
          
          <th><g:message code="rollPay.rankSalary.label" default="Rank Salary"/></th>
          
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${rollPayInstanceList}" status="i" var="rollPayInstance">
          <tr class="${(i % 2) == 0 ? 'even' : 'odd'}" scope="col">
            
            <td>${i+1}</td>
            <td><g:link action="show" id="${rollPayInstance.id}">${fieldValue(bean: rollPayInstance, field: "allowance")}</g:link></td>
            
            <td>${fieldValue(bean: rollPayInstance, field: "bonus")}</td>
            
            <td><g:formatDate date="${rollPayInstance.effectiveDate}"/></td>
            
            <td>${fieldValue(bean: rollPayInstance, field: "insurance")}</td>
            
            <td>${fieldValue(bean: rollPayInstance, field: "rankSalary")}</td>
            
            <td><g:link action="edit" resource="${rollPayInstance}"><i
              class="glyphicon glyphicon-pencil"></i></g:link><g:link class="delete" action="delete"
                                                                      resource="${rollPayInstance}"><i
                class="glyphicon glyphicon-remove"></i></g:link></td>
          </tr>
        </g:each>
        </tbody>
      </table>
    </div>
    <div class="box-footer">
      <div class="pagination sang-pagination">
        <g:paginate total="${rollPayInstanceCount ?: 0}"/>
      </div>
    </div>
  </div>
</div>
</body>
</html>
