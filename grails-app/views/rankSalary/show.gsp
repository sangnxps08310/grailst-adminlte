
<%@ page import="com.hrm.RankSalary" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'rankSalary.label', default: 'RankSalary')}"/>
  <title><g:message code="default.show.label"
                    args="${[message( code: 'rankSalary.label', default:'rankSalary')]}">
</g:message></title>
  <style>
  td {
    text-align: left!important;
  }
  </style>
</head>

<body>
%{--		<a href="#show-rankSalary" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>--}%
%{--		<div class="nav" role="navigation">--}%
%{--			<ul>--}%
%{--				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
%{--				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>--}%
%{--				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>--}%
%{--			</ul>--}%
%{--		</div>--}%
<div class="rankSalary">
  <div id="show-rankSalary" class="content scaffold-show" role="main">
%{--    <h3><g:message code="default.show.label" args="[entityName]"/></h3>--}%
    <g:if test="${flash.message}">
      <div class="message alert alert-success" role="status">${flash.message}</div>
    </g:if>
    <div class="box box-primary">
      <div class="box-body">
        <h4><i class="fa fa-book"></i>Details: </h4>
        <table class="rankSalary table no-margin"s>
          
          %{--          <g:if test="${rankSalaryInstance?.nameRank}">--}%
          <tr class="fieldcontain">
            <th id="nameRank-label" class="property-label"><g:message
              code="rankSalary.nameRank.label" default="Name Rank"/></th>
            
            <td class="property-value" aria-labelledby="nameRank-label"><g:fieldValue bean="${rankSalaryInstance}"
                                                                                       field="nameRank"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${rankSalaryInstance?.rankSalary}">--}%
          <tr class="fieldcontain">
            <th id="rankSalary-label" class="property-label"><g:message
              code="rankSalary.rankSalary.label" default="Rank Salary"/></th>
            
            <td class="property-value" aria-labelledby="rankSalary-label"><g:fieldValue bean="${rankSalaryInstance}"
                                                                                       field="rankSalary"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${rankSalaryInstance?.profession}">--}%
          <tr class="fieldcontain">
            <th id="profession-label" class="property-label"><g:message
              code="rankSalary.profession.label" default="Rank Position"/></th>
            
            <td class="property-value" aria-labelledby="profession-label"><g:link
              controller="profession" action="show"
              id="${rankSalaryInstance?.profession?.id}">${com.hrm.Profession.findById(fieldValue(bean: rankSalaryInstance, field: "professionId")).professionName}</g:link></td>
            
          </tr>
          %{--          </g:if>--}%
          
        </table>
        <g:form url="[resource: rankSalaryInstance, action: 'delete']" method="DELETE">
          <fieldset class="buttons">
            <g:link class="edit btn btn-info" action="edit" resource="${rankSalaryInstance}"><g:message
              code="default.button.edit.label"
              default="Edit"/></g:link>
            <g:actionSubmit class="delete btn btn-danger" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
          </fieldset>
        </g:form>
      </div>
    </div>
  </div>
</div>
</body>
</html>
