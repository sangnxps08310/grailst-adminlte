
<%@ page import="com.hrm.RankSalary" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'rankSalary.label', default: 'RankSalary')}"/>
  <title><g:message code="rankSalary.label"
                    args="${[message( code: 'rankSalary.label', default:'rankSalary')]}">
  </g:message></title>

</head>

<body>
<div class="rankSalary">

  <g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
  </g:if>
  <div id="list-rankSalary" class="box box-primary" role="main">
    <div class="box-header">
      <h4 class="box-title collapsed" data-toggle="collapse" data-target="#search-box" aria-expanded="true"><i
        class="fa fa-search"></i><g:message code="default.search.label" args=" "/></h4>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool btn-search-toggle collapsed" data-toggle="collapse"
                data-target="#search-box" aria-expanded="true">
          <i class="fa fa-plus"></i>
        </button>
      </div>
      <div class="help-block"></div>

      <div class="search-rankSalary" id="search-box">
          <g:form class="form rows"
                  url="[resource: rankSalaryInstance, action: 'index']" >
          <g:render template="form"/>
          <div class="buttons col-md-3">
            <label>&nbsp;</label>
            <br>
            <g:submitButton name="search" class="btn btn-success"
                            value="${message(code: 'default.button.search.label', default: 'search')}"/>
          </div>
          </g:form>
        </div>
    </div>
    <div class="box-body">
      <div class="clearfix">
      </div>
      <g:link action="create" class="btn btn-success pull-right"><i class="fa fa-plus"></i></g:link>
      <table class="table table-hover" style="border-top: 1px solid #f4f4f4; border-bottom: 1px solid #f4f4f4">
        <thead>
        <tr>
          <th>#</th>
          
          <g:sortableColumn property="nameRank"
                            title="${message(code: 'rankSalary.nameRank.label', default: 'Name Rank')}"/>
          
          <g:sortableColumn property="rankSalary"
                            title="${message(code: 'rankSalary.rankSalary.label', default: 'Rank Salary')}"/>
          
          <th><g:message code="rankSalary.profession.label" default="Rank Position"/></th>
          
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${rankSalaryInstanceList}" status="i" var="rankSalaryInstance">
          <tr class="${(i % 2) == 0 ? 'even' : 'odd'}" scope="col">
            
            <td>${i+1}</td>
            <td><g:link action="show" id="${rankSalaryInstance.id}">${fieldValue(bean: rankSalaryInstance, field: "nameRank")}</g:link></td>
            
            <td>${fieldValue(bean: rankSalaryInstance, field: "rankSalary")}</td>
            
            <td><g:link action="show" controller="profession" id="${fieldValue(bean: rankSalaryInstance, field: "professionId")}">${com.hrm.Profession.findById(fieldValue(bean: rankSalaryInstance, field: "professionId")).professionName}</g:link></td>

            <td><g:link action="edit" resource="${rankSalaryInstance}"><i
              class="glyphicon glyphicon-pencil"></i></g:link><g:link class="delete" action="delete"
                                                                      resource="${rankSalaryInstance}"><i
                class="glyphicon glyphicon-remove"></i></g:link></td>
          </tr>
        </g:each>
        </tbody>
      </table>
    </div>
    <div class="box-footer">
      <div class="pagination sang-pagination">
        <g:paginate total="${rankSalaryInstanceCount ?: 0}"/>
      </div>
    </div>
  </div>
</div>
</body>
</html>
