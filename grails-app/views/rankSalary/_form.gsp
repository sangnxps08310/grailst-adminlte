<%@ page import="com.hrm.RankSalary" %>
<%@ page import="com.hrm.Profession" %>






<g:if test="${rankSalaryInstance}">
  <div class="form-group ${hasErrors(bean: rankSalaryInstance, field: 'nameRank', 'error')} required">
  <label class="control-label" for="nameRank">
    <g:message code="rankSalary.nameRank.label" default="Name Rank"/>
    <span class="text-red">*</span>
  </label>
  <br>
<g:textField class="form-control" name="nameRank" pattern="${rankSalaryInstance.constraints.nameRank.matches}" required="" value="${rankSalaryInstance?.nameRank}"/>

</g:if><g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="nameRank">
  <g:message code="rankSalary.nameRank.label" default="Name Rank"/>
  </label>
  <br>
  <input class="form-control" name="nameRank" value="${params.nameRank}">
</g:else>  <div class="help-block"></div>
</div>



<g:if test="${rankSalaryInstance}">
  <div class="form-group ${hasErrors(bean: rankSalaryInstance, field: 'rankSalary', 'error')} required">
  <label class="control-label" for="rankSalary">
    <g:message code="rankSalary.rankSalary.label" default="Rank Salary"/>
    <span class="text-red">*</span>
  </label>
  <br>
<g:textField class="form-control" name="rankSalary" required="" value="${rankSalaryInstance?.rankSalary}"/>
</g:if><g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="rankSalary">
  <g:message code="rankSalary.rankSalary.label" default="Rank Salary"/>
  </label>
  <br>
  <input class="form-control" name="rankSalary" value="${params.rankSalary}">
</g:else>  <div class="help-block"></div>
</div>



<g:if test="${rankSalaryInstance}">
  <div class="form-group ${hasErrors(bean: rankSalaryInstance, field: 'profession', 'error')} required">
  <label class="control-label" for="profession">
    <g:message code="rankSalary.profession.label" default="Rank Position"/>
    <span class="text-red">*</span>
  </label>
  <br>
<g:select class="form-control many-to-one" id="profession" name="profession.id" from="${Profession.list()}" optionValue="professionName" optionKey="id" required="" value="${rankSalaryInstance?.profession?.id}"/>
</g:if><g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="profession">
  <g:message code="rankSalary.profession.label" default="Rank Position"/>
  </label>
  <br>
  <g:select class="form-control many-to-one" id="profession" name="profession.id" from="${Profession.list()}" noSelection="[0:'-Choose Profession-']" required="" optionValue="professionName" optionKey="id"  value="${rankSalaryInstance?.profession?.id}"/>

</g:else>  <div class="help-block"></div>
</div>


