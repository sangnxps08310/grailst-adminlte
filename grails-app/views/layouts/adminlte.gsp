<%--
  Created by IntelliJ IDEA.
  User: nhm95
  Date: 1/14/2020
  Time: 12:03 PM
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><g:layoutTitle default="H-R-M"/></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="shortcut icon" href="${createLinkTo(dir:'images',file:'favicon.ico')}" type="image/x-icon" />
  <asset:javascript src="bower_components/jquery/dist/jquery.min.js"></asset:javascript>
  <asset:javascript src="moment.js"></asset:javascript>
  <asset:javascript src="bower_components/bootstrap/dist/js/bootstrap.min.js"></asset:javascript>
  <asset:javascript src="bootstrap-datetimepicker.js"></asset:javascript>

  <asset:link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css"></asset:link>

  %{--    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">--}%
  <!-- Font Awesome -->
  <asset:link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css"></asset:link>

  %{--    <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">--}%
  <!-- Ionicons -->
%{--  <asset:link rel="stylesheet" href="bower_components/ic/css/ionicons.min.css"></asset:link>--}%
  %{--    <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">--}%
  <!-- Theme style -->
  <asset:link rel="stylesheet" href="dist/css/AdminLTE.min.css"></asset:link>
  %{--    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">--}%
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <asset:link rel="stylesheet" href="dist/css/skins/skin-blue.css"></asset:link>
  <asset:link rel="stylesheet" href="bootstrap-datetimepicker.min.css"></asset:link>
  %{--    <link rel="stylesheet" href="dist/css/skins/skin-blue.min.css">--}%

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <link rel="stylesheet"
        href="${resource(dir: 'adminlte', file: 'bower_components/bootstrap/dist/css/bootstrap.min.css')}
        ">

  <asset:link href="customize.css" rel="stylesheet"></asset:link>

  <asset:javascript src="application.js"></asset:javascript>

  <asset:javascript src="bower_components/select2/dist/js/select2.min.js"></asset:javascript>
  <asset:link rel="stylesheet" href="bower_components/select2/dist/css/select2.min.css"></asset:link>
  <g:layoutHead/>
  <r:layoutResources/>

</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">
    <!-- Logo -->
    <g:render template="/layouts/header-content"></g:render>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <g:render template="/layouts/menu_left"></g:render>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content-header">
      <g:render template="/layouts/content-header"></g:render>
    </section>
    <!-- Main content -->
    <section class="content container-fluid">
      <g:layoutBody/>

      <!--------------------------
| Your Page Content Here |
-------------------------->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <g:render template="/layouts/aside-tab-control"></g:render>
    <!-- Tab panes -->
    <g:render template="/layouts/aside-tab-content"></g:render>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->

%{--<script src="bower_components/jquery/dist/jquery.min.js"></script>--}%
<!-- Bootstrap 3.3.7 -->

%{--<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>--}%
<!-- AdminLTE App -->
<asset:javascript src="dist/js/adminlte.min.js"></asset:javascript>

%{--<script src="dist/js/adminlte.min.js"></script>--}%

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->

<r:layoutResources/>

</body>
</html>