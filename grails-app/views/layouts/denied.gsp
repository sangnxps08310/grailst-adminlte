<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><g:if env="development">Access Denied</g:if><g:else>Denied</g:else></title>
  %{--  <meta name="layout" content="adminlte">--}%
  %{--  <g:if env="development"><asset:stylesheet src="errors.css"/></g:if>--}%
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Font Awesome -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <asset:link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css"></asset:link>
  %{--    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">--}%
  <!-- Font Awesome -->
  <asset:link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css"></asset:link>

  %{--    <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">--}%
  <!-- Ionicons -->
  <asset:link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css"></asset:link>

  %{--    <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">--}%
  <!-- Theme style -->
  <asset:link rel="stylesheet" href="dist/css/AdminLTE.min.css"></asset:link>

  %{--    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">--}%
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <asset:link rel="stylesheet" href="dist/css/skins/skin-blue.min.css"></asset:link>

  %{--    <link rel="stylesheet" href="dist/css/skins/skin-blue.min.css">--}%

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <link rel="stylesheet"
        href="${resource(dir: 'adminlte', file: 'bower_components/bootstrap/dist/css/bootstrap.min.css')}
        ">
  <asset:javascript src="jquery-3.4.1.js"></asset:javascript>

  <asset:link href="customize.css" rel="stylesheet"></asset:link>

  <asset:javascript src="application.js"></asset:javascript>

  <r:layoutResources/>

</head>
<body>
    %{--  <!-- Content Header (Page header) -->--}%
    %{--  <section class="content-header">--}%
    %{--    <h1>--}%
    %{--      500 Error Page--}%
    %{--    </h1>--}%
    %{--    <ol class="breadcrumb">--}%
    %{--      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}%
    %{--      <li><a href="#">Examples</a></li>--}%
    %{--      <li class="active">500 error</li>--}%
    %{--    </ol>--}%
    %{--  </section>--}%
    <!-- Main content -->


     <g:layoutBody/>
      <!-- /.error-page -->


  <!-- Control Sidebar -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<asset:javascript src="jquery.min.js"></asset:javascript>

%{--<script src="bower_components/jquery/dist/jquery.min.js"></script>--}%
<!-- Bootstrap 3.3.7 -->
<asset:javascript src="bootstrap.min.js"></asset:javascript>

%{--<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>--}%
<!-- AdminLTE App -->
<asset:javascript src="adminlte.min.js"></asset:javascript>

%{--<script src="dist/js/adminlte.min.js"></script>--}%

<!-- Optionally, you can add Slimscroll and FastClick plugins.
       Both of these plugins are recommended to enhance the
       user experience. -->
<r:layoutResources/>
</body>
</html>
