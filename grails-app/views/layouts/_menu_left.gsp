
<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">

  <!-- Sidebar user panel (optional) -->
<g:render template="/layouts/user-header"></g:render>
  <ul class="sidebar-menu" data-widget="tree">
    <li class="header">Main Menu</li>
    <!-- Optionally, you can add icons to the links -->
    <g:render template="/layouts/menu_content"></g:render>
  </ul>
  <!-- /.sidebar-menu -->
</section>
<!-- /.sidebar -->