<!-- Content Header (Page header) -->
%{--    <section class="content-header">--}%
%{--      <h1>--}%
%{--        Helloword Test--}%
%{--        <small>AdminLTE Template</small>--}%
%{--      </h1>--}%
%{--      <ol class="breadcrumb">--}%
%{--        <li><a href="# "><i class="fa fa-dashboard"></i> Level</a></li>--}%
%{--        <li class="active">Here</li>--}%
%{--      </ol>--}%
%{--    </section>--}%
<h1>
<g:if test="${params.controller != null}">
  ${message(code: params.controller + '.label')}
</g:if>
  <small>${params.action}</small>
</h1>
<ol class="breadcrumb">
  <g:if test="${params.controller != null}">
    <li><a href="${createLink(controller: 'index',action: 'index')}"><i class="fa fa-gears"></i> Home</a></li>
    <g:if test="${params.action == 'index' || params.action == null}">
      <li class="active">${params.controller}</li>
    </g:if><g:else>
    <g:if test="${params.action == 'show'}">
      <li><a href="${createLink(controller: params.controller)}">${params.controller}</a></li>
      <li class="active">${params.id}</li>
    </g:if><g:else>
      <li><a href="${createLink(controller: params.controller)}">${params.controller}</a></li>
      <li class="active">${params.action}</li>
    </g:else>
  </g:else>
  </g:if>
</ol>