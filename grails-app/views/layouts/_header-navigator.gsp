<nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <g:render template="/layouts/header-toggle-btn"></g:render>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->
%{--            <li class="dropdown messages-menu">--}%

%{--                <g:render template="/layouts/header-message"></g:render>--}%
%{--                <!-- /.messages-menu -->--}%
%{--            </li>--}%
%{--            <!-- Notifications Menu -->--}%
%{--            <li class="dropdown notifications-menu">--}%
%{--                <!-- Menu toggle button -->--}%
%{--               <g:render template="/layouts/header-notification"></g:render>--}%
%{--            </li>--}%
%{--            <!-- Tasks Menu -->--}%
%{--            <li class="dropdown tasks-menu">--}%
%{--                <!-- Menu Toggle Button -->--}%
%{--               <g:render template="/layouts/header-task"></g:render>--}%
%{--            </li>--}%
%{--            <!-- User Account Menu -->--}%
            <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
               <g:render template="/layouts/header-account-info"></g:render>
            </li>
            <!-- Control Sidebar Toggle Button -->
%{--            <li>--}%
%{--                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>--}%
%{--            </li>--}%
        </ul>
    </div>
</nav>