<%@ page import="grails.plugin.springsecurity.SpringSecurityService" %>
<div class="user-panel">

    <div class="pull-left image">
        <img src="${createLink(controller: 'userInfor',action: 'loadImage',params: ['path':''])}<sec:loggedInUserInfo field="photo"></sec:loggedInUserInfo>" class="img-circle" alt="User Image">
    </div>
    <div class="pull-left info">
        <p><a href="${createLink(controller: 'userInfor',action: 'profile')}/<sec:loggedInUserInfo field="userInfor.id"></sec:loggedInUserInfo>"><sec:loggedInUserInfo field="userRealName"></sec:loggedInUserInfo></a></p>
        <!-- Status -->
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
</div>