<%@ page import="org.springframework.util.StringUtils; com.hrm.sys.Menu" %>

<li><a
  href="${createLink(controller: 'index', action: 'index')}">
  <i class="fa fa-home"></i><span>Home</span></a>
</li>
<%
  def menuInstance = Menu.list(sort: 'position', order: 'asc')
  String urlInstance = params.controller
  def menuActice = Menu.findByUrl(urlInstance)
  def mapAciveId = [:]
  if (menuActice) {
    mapAciveId[menuActice.parent] = "active"
  }
  menuInstance.each { menu ->
    if (menu.parent == '0') {
      def menuItemInstance = Menu.findAllByParent(menu.id)
      if (menuItemInstance.size() > 0) {
%>
<li class=" ${mapAciveId.containsKey(menu.id + "") ? 'active' : ''} treeview
${mapAciveId.containsKey(menu.id) ? 'menu-poen' : ''}">
  <a href="${menu.url}"><i class="fa fa-${menu.icon}"></i> <span>${menu.name}</span>
    <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
    </span>
  </a>
  <ul class="treeview-menu">
    <% def menuItemsInstance = Menu.findAllByParent(menu.id, [sort: 'position', order: 'asc'])
      menuItemsInstance.each { menuItem ->
        if (menuItem.url.trim() != '' && menuItem.url.trim() != null) {
          String controllerUrl = menuItem.url
          String actionUrl = ''
          if (menuItem.url.contains('/')) {
            String[] url = menuItem.url.split("/");
            controllerUrl = url[0]
            actionUrl = url[1]
          }
    %>

    <li class="${urlInstance == controllerUrl ? 'active' : ''}"><a
      href="${createLink(controller: controllerUrl, action: actionUrl)}">
      <i class="fa fa-${menuItem.icon}"></i><span>${menuItem.name}</span></a>
    </li>
    <% }
    } %>
  </ul>
</li>
<%
  } else {
    if (menu.url.trim() != '') {
      String controllerUrl = menu.url
      String actionUrl = ''
      if (menu.url.contains('/')) {
        String[] url = menu.url.split("/");
        controllerUrl = url[0]
        actionUrl = url[1]
      }
%>
<li><a href="${createLink(controller: controllerUrl, action: actionUrl)}"><i
  class="fa fa-${menu.icon}"></i><span>${menu.name}</span></a></li>
<% }
}

}
} %>