<a href="#" class="dropdown-toggle" data-toggle="dropdown">
  <!-- The user image in the navbar-->

  <img src="${createLink(controller: 'userInfor', action: 'loadImage', params: ['path': ''])}<sec:loggedInUserInfo
    field="photo"></sec:loggedInUserInfo>" class="user-image"
       alt="User Image">
  <!-- hidden-xs hides the username on small devices so only the image appears. -->
  <span class="hidden-xs"><sec:loggedInUserInfo field="userRealName"></sec:loggedInUserInfo></span>
</a>
<ul class="dropdown-menu">
  <!-- The user image in the menu -->
  <li class="user-header">
    <img src="${createLink(controller: 'userInfor', action: 'loadImage', params: ['path': ''])}<sec:loggedInUserInfo
      field="photo"></sec:loggedInUserInfo>" class="img-circle"
         alt="User Image">
    %{--                                <asset:image src="dist/img/user2-160x160.jpg"></asset:image>--}%
    <p>
      <sec:loggedInUserInfo field="userRealName"></sec:loggedInUserInfo>
      <small>
        <sec:loggedInUserInfo field="phoneNumber"></sec:loggedInUserInfo>
      </small>
    </p>
  </li>
  <!-- Menu Body -->
  %{--    <li class="user-body">--}%
  %{--        <div class="row">--}%
  %{--            <div class="col-xs-4 text-center">--}%
  %{--                <a href="#">Followers</a>--}%
  %{--            </div>--}%

  %{--            <div class="col-xs-4 text-center">--}%
  %{--                <a href="#">Sales</a>--}%
  %{--            </div>--}%

  %{--            <div class="col-xs-4 text-center">--}%
  %{--                <a href="#">Friends</a>--}%
  %{--            </div>--}%
  %{--        </div>--}%
  %{--        <!-- /.row -->--}%
  %{--    </li>--}%
  <!-- Menu Footer-->
  <li class="user-footer">
    <div class="pull-left">
      <a class="btn btn-default btn-flat" href="${createLink(controller: 'userInfor', action: 'profile')}/<sec:loggedInUserInfo field="userInfor.id"></sec:loggedInUserInfo>">Profile</a>
    </div>

    <div class="pull-right">
      <g:link controller="logout" action="index" class="btn btn-default btn-flat">Sign out</g:link>
    </div>
  </li>
</ul>