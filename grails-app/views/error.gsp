<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><g:if env="development">Grails Runtime Exception</g:if><g:else>Error</g:else></title>
%{--  <meta name="layout" content="adminlte">--}%
%{--  <g:if env="development"><asset:stylesheet src="errors.css"/></g:if>--}%
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Font Awesome -->
<asset:link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css"></asset:link>

<!-- Ionicons -->
<asset:link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css"></asset:link>

<!-- Theme style -->
<asset:link rel="stylesheet" href="dist/css/AdminLTE.min.css"></asset:link>

<asset:link rel="stylesheet" href="dist/css/skins/skin-blue.min.css"></asset:link>

<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Google Font -->
<link rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<link rel="stylesheet"
      href="${resource(dir: 'adminlte', file: 'bower_components/bootstrap/dist/css/bootstrap.min.css')}
      ">
<asset:javascript src="jquery-3.4.1.js"></asset:javascript>


<asset:javascript src="application.js"></asset:javascript>
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
<g:render template="/layouts/header-content"></g:render>
</header>

<div class="content-wrapper">
%{--  <!-- Content Header (Page header) -->--}%
%{--  <section class="content-header">--}%
%{--    <h1>--}%
%{--      500 Error Page--}%
%{--    </h1>--}%
%{--    <ol class="breadcrumb">--}%
%{--      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}%
%{--      <li><a href="#">Examples</a></li>--}%
%{--      <li class="active">500 error</li>--}%
%{--    </ol>--}%
%{--  </section>--}%
<!-- Main content -->
<section class="content-header">
<h1>
  500 Error
</h1>
<ol class="breadcrumb">
  <li><g:link controller="index"><i class="fa fa-gears"></i> Home</g:link></li>
  <li class="active">500 Error</li>
  </ol>
  </section>
  <section class="content">

    <div class="error-page">

      <div class="error-content">
        <h3><i class="fa fa-warning text-red"></i> Oops! Something went wrong.</h3>
        <p>
          We will work on fixing that right away.
          Meanwhile,<br> you may <g:link elementId="return"  controller="menu"  >return to index <i id="countdown"></i></g:link>.
        </p>

      </div>
      <div class="error-details">
        <strong>${exception.className} Line ${exception.lineNumber}:</strong><br>${exception.message}

      </div>
    </div>
    %{--        <script>--}%
    %{--          $(document).ready(function(){--}%
    %{--            var i = 3;--}%
    %{--            setInterval(function(){--}%
    %{--              if(i==-1){--}%
    %{--               window.location.href= $('#return').prop('href');--}%
    %{--               return;--}%
    %{--              }--}%
    %{--              $('#countdown').text("in: "+ i);--}%
    %{--              i--;--}%
    %{--            },1000)--}%
    %{--          });--}%
    %{--        </script>--}%
    <div class="panel">
      <div class="panel-heading">
      </div>
      <div class="panel-body">
        <g:if env="development">
          <g:renderException exception="${exception}"/>
        </g:if>
      </div>
    </div>
    <!-- /.error-page -->
  </section>
  </div>
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <g:render template="/layouts/aside-tab-control"></g:render>
    <!-- Tab panes -->
    <g:render template="/layouts/aside-tab-content"></g:render>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  </div>
  <!-- REQUIRED JS SCRIPTS -->

  <!-- jQuery 3 -->
  <asset:javascript src="bower_components/jquery/dist/jquery.min.js"></asset:javascript>

%{--<script src="bower_components/jquery/dist/jquery.min.js"></script>--}%
  <!-- Bootstrap 3.3.7 -->
  <asset:javascript src="bower_components/bootstrap/dist/js/bootstrap.min.js"></asset:javascript>

%{--<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>--}%
  <!-- AdminLTE App -->
  <asset:javascript src="dist/js/adminlte.min.js"></asset:javascript>

%{--<script src="dist/js/adminlte.min.js"></script>--}%

  <!-- Optionally, you can add Slimscroll and FastClick plugins.
       Both of these plugins are recommended to enhance the
       user experience. -->
  </body>
  </html>
