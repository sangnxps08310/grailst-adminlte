<div class=" table-responsive no-padding">
 <g:if test="${listWaitingRequest}">

  <table class="table table-hover">
    <thead>
    <tr>
%{--      <th>#</th>--}%
%{--      <th>User</th>--}%
%{--      <th>Date</th>--}%
%{--      <th>Status</th>--}%
%{--      <th>Reason</th>--}%
      <th></th>
      <th>Status</th>
      <th></th>
      <th>Content</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
    </thead>
    <tbody>

    <g:each in="${listWaitingRequest}" var="waitingRequest">

      <tr >
        <td style="vertical-align: middle" width="100px"> <img width="80" height="80" src="${createLink(controller: 'userInfor', action: 'loadImage',
          params: [
            'image': true,
            'path' : waitingRequest?.staff?.photo])}" class="img-circle"/></td>
        <td style="vertical-align: middle" width="100px"><i class="fa  fa-long-arrow-right" style="font-size: 80px; color: #b2b2b2"></i><br><span class="label label-warning">Waiting</span></td>
        <td style="vertical-align: middle" width="100px"><img width="80" height="80" src="${createLink(controller: 'userInfor', action: 'loadImage',
          params: [
            'image': true,
            'path' : roomMaster?.photo])}" class="img-circle"/></td>
        <td style="vertical-align: middle" width="200px">${ waitingRequest?.type?.description}</td>
        <td style="vertical-align: middle" width="100px"><g:link class="btn btn-success" controller="requests" action="accept" id="${waitingRequest.id}">Accept</g:link></td>
        <td style="vertical-align: middle"></td>
        <td style="vertical-align: middle"></td>
        <td style="vertical-align: middle"></td>
      </tr>
    </g:each>
    </tbody>
  </table>
 </g:if><g:else>
  We have no waiting request
</g:else>
</div>
