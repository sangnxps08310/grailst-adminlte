<%--
  Created by IntelliJ IDEA.
  User: nhm95
  Date: 3/24/2020
  Time: 10:07 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'index.label', default: 'Dashboard')}"/>
  <title><g:message code="index.label"
                    args="${[message(code: 'insurances.label', default: 'General')]}">
  </g:message></title>
</head>

<body>
<div class="general-information">
  %{--  <div class="box box-primary">--}%

  <div class=" box box-primary">
    <ul class="nav nav-tabs">

      <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_MANAGER">
        <li class="active"><a href="#request" data-toggle="tab">Request</a></li>
%{--        <li><a href="#recruitment" data-toggle="tab">Recruitment list</a></li>--}%
      </sec:ifAnyGranted>
%{--      <li><a href="#work" data-toggle="tab">Work</a></li>--}%
      <sec:ifAnyGranted roles="ROLE_SUPERVISOR">
%{--        <li><a href="#list-point" data-toggle="tab">List Point</a></li>--}%
      </sec:ifAnyGranted>
    </ul>
  <div class="tab-content box-body">
    <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_MANAGER">
      <div class="tab-pane active " id="request">
        <g:render template="requests"></g:render>
      </div>

%{--      <div class="tab-pane" id="recruitment">--}%

%{--      </div>--}%
    </sec:ifAnyGranted>
%{--    <div class="tab-pane" id="work">--}%

%{--    </div>--}%

%{--    <div class="tab-pane" id="list-point">--}%

%{--    </div>--}%
  </div>
  </div>


  %{--  </div>--}%
</div>
</body>
</html>