<%@ page import="com.hrm.Role; com.hrm.UsersRole; com.hrm.Users" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="adminlte">
    <g:set var="entityName" value="${message(code: 'users.label', default: 'Users')}"/>
    <title><g:message code="users.label"
                      args="${[message(code: 'users.label', default: 'users')]}">
    </g:message></title>

</head>

<body>
<div class="users">
    <g:if test="${flash.message}">
        <div class="alert alert-${flash.type}" role="alert">${flash.message}</div>
    </g:if>

    <div id="list-users" class="box box-primary" role="main">
        <div class="box-header">
            <h4 class="box-title collapsed" data-toggle="collapse" data-target="#search-box" aria-expanded="true"><i
                    class="fa fa-search"></i><g:message code="default.search.label" args=" "/></h4>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool btn-search-toggle collapsed" data-toggle="collapse"
                        data-target="#search-box" aria-expanded="true">
                    <i class="fa fa-plus"></i>
                </button>
            </div>

            <div class="help-block"></div>

            <div class="search-users" id="search-box">
                <g:form class="form rows"
                        url="[resource: usersInstance, action: 'index']">
                    <g:render template="form"/>
                    <div class="col-md-3">
                        <label>&nbsp;</label>
                        <br>
                        <g:submitButton name="search" class="btn btn-success"
                                        value="${message(code: 'default.button.search.label', default: 'search')}"/>
                    </div>
                </g:form>
            </div>
        </div>

        <div class="box-body">
            <div class="clearfix">
            </div>
            <g:link action="create" class="btn btn-success pull-right"><i class="fa fa-plus"></i></g:link>
            <table class="table table-hover"
                   style="border-top: 1px solid #f4f4f4; border-bottom: 1px solid #f4f4f4">
                <thead>
                <tr>
                    <th>#</th>
                    <g:sortableColumn property="username"
                                      title="${message(code: 'users.username.label', default: 'Username')}"/>
                    <th><g:message code="users.userInfor.label" default="User Infor"/></th>
                    <g:sortableColumn property="role"
                                      title="Role"/>

                    <g:sortableColumn property="accountExpired"
                                      title="${message(code: 'users.accountExpired.label', default: 'Account Expired')}"/>

                    <g:sortableColumn property="accountLocked"
                                      title="${message(code: 'users.accountLocked.label', default: 'Account Locked')}"/>

                    <g:sortableColumn property="enabled"
                                      title="${message(code: 'users.enabled.label', default: 'Enabled')}"/>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${usersInstanceList}" status="i" var="usersInstance">
                    <tr class="${(i % 2) == 0 ? 'even' : 'odd'}" scope="col">
                        <td>${i + 1}</td>
                        <td style="text-align: left"><g:link action="show"
                                    id="${usersInstance.id}">${fieldValue(bean: usersInstance, field: "username")}</g:link></td>

                        <td style="text-align: left">${fieldValue(bean: usersInstance, field: "userInfor.name")}</td>
                        <%
                            def roleId = UsersRole.findAllByUsers(usersInstance).roleId
                            def roles = com.hrm.Role.findAllByIdInList(roleId).authority
                        %>
                        <td ><g:each in="${roles}" var="name"><span class="badge bg-aqua">${name}</span></g:each>
                        </td>
                        <td><i class="fa ${usersInstance.accountExpired ? 'fa-check' : 'fa-close'}"
                               style="color: ${usersInstance.accountExpired ? 'green' : 'red'}"></i></td>
                        <td><i class="fa ${usersInstance.accountLocked ? 'fa-check' : 'fa-close'}"
                               style="color: ${usersInstance.accountLocked ? 'green' : 'red'}"></i></td>
                        <td><i class="fa ${usersInstance.enabled ? 'fa-check' : 'fa-close'}"
                               style="color: ${usersInstance.enabled ? 'green' : 'red'}"></i></td>
                        <td>
                            <g:link controller="users" action="edit" resource="${usersInstance}">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </g:link>
                            <g:link controller="users" action="delete" resource="${usersInstance}"
                                    onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
                                <i class="glyphicon glyphicon-remove"></i>
                            </g:link>


                        %{--              <g:form url="[resource: menuInstance, action: 'delete']" method="DELETE">--}%
                        %{--                <fieldset class="buttons">--}%
                        %{--                    <g:link controller="users" action="edit" resource="${usersInstance}">--}%
                        %{--                        <i class="glyphicon glyphicon-pencil"></i>--}%
                        %{--              </g:link>--}%
                        %{--              <g:link controller="users" action="edit" resource="${usersInstance}">--}%
                        %{--                <i class="glyphicon glyphicon-remove"></i>--}%
                        %{--              </g:link>--}%
                        %{--              <g:actionSubmit class="delete btn btn-danger" action="delete"--}%
                        %{--                              value="${message(code: 'default.button.delete.label', default: 'Delete')}"--}%
                        %{--                              onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>--}%
                        %{--            </fieldset>--}%
                        %{--              </g:form>--}%
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>

        <div class="box-footer sang-pagination">
            <g:paginate total="${usersInstanceCount ?: 0}"/>
        </div>
    </div>
</div>
</div>
</body>
</html>
