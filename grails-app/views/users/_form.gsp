<%@ page import="com.hrm.Role; com.hrm.UsersRole; com.hrm.UserInfor; com.hrm.Users" %>


<g:if test="${usersInstance}">
  <div class="form-group ${hasErrors(bean: usersInstance, field: 'username', 'error')} required">
  <label class="control-label" for="username">
    <g:message code="users.username.label" default="Username"/>
    <span class="text-red">*</span>
  </label>
  <br>
  <g:textField class="form-control" name="username" required="" value="${usersInstance?.username}"/>
</g:if>
<g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="username">
        <g:message code="users.username.label" default="Username"/>
      </label>
      <input class="form-control" name="username" value="${params.username}">

      <div class="help-block"></div>
    </div>
  </div>
</g:else>

  <g:if test="${usersInstance}">
    <div class="form-group ${hasErrors(bean: usersInstance, field: 'password', 'error')} required">

      <label class="control-label" for="password">
        <g:message code="users.password.label" default="Password"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <g:passwordField class="form-control" name="password" required="" value="${usersInstance?.password}"/>
      <br>
      <label class="control-label" for="password">
        <g:message code="users.password.label" default="Confirm Password"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <input type="password" name="confirmPassword" class="form-control" value="${usersInstance?.password}" required="">

      <div class="help-block"></div>
    </div>
  </g:if>


<g:if test="${usersInstance}">
  <div class="form-group ${hasErrors(bean: usersInstance, field: 'userInfor', 'error')} ">

    <%
      //
      def listUserInfoIdContainsInUser = Users.findAllByUserInforNotEqual(usersInstance?.userInfor)
      def listUserInfoInstance = UserInfor.findAllByIdNotInList(listUserInfoIdContainsInUser.userInfor.id);
    %>
    <label class="control-label" for="userInfor">
      <g:message code="users.userInfor.label" default="User Infor"/>
    </label>
    <br>
    <g:select id="userInfor" name="userInfor.id" from="${listUserInfoInstance}" disabled="${params.action == 'edit'}"
              optionKey="id" optionValue="name"
              value="${usersInstance?.userInfor?.id}" class="many-to-one form-control"/>
    <div class="help-block"></div>
  </div>
</g:if>


%{---------------------------------------------------------------------------------------------------}%



<g:if test="${usersInstance}">
  <div class="form-group">
    <label class="control-label" for="role">
      <g:message code="usersRole.role.label" default="Role"/>
    </label>

    <g:select class="many-to-one form-control js-states select2" id="role" name="role" from="${com.hrm.Role.list()}"
              optionKey="id" optionValue="authority"/>
    <script>
      $('#role').select2({
        multiple: true,
        placeholder: 'Select role',
        // templateSelection: formatState
      });
      <g:if test="${usersInstance.id}">
      $('#role').val(${com.hrm.UsersRole.findAllByUsers(usersInstance).roleId});
      var role = "${com.hrm.UsersRole.findAllByUsers(usersInstance).role.authority.join(",")}";
      console.log(role.split(','));

      $('.select2-selection__rendered').html('');
      var roles = role.split(',');
      roles.forEach(function (item) {
        var html = '<li class="select2-selection__choice" title="' + item + '" data-select2-id="' + Math.round(Math.random() * 100) + '">' + item + '</li>'
        $('.select2-selection__rendered').append(html);
      });
      </g:if>

      %{--$('#role').select2('val',${com.hrm.UsersRole.findAllByUsers(usersInstance).role.id});--}%
      %{--<li class="select2-selection__choice" title="ROLE_ADMIN" data-select2-id="39"><span class="select2-selection__choice__remove" role="presentation">×</span>ROLE_ADMIN</li>--}%
    </script>
    <script>
      $('#role').select2({
        multiple: true,
        placeholder: 'Select role',
        // templateSelection: formatState
      });
    </script>

    <div class="help-block"></div>
  </div>
</g:if>

<g:if test="${usersInstance}">
  <div class="col-md-2 form-group ${hasErrors(bean: usersInstance, field: 'accountExpired', 'error')} ">

    <label class="control-label" for="accountExpired">
      <g:message code="users.accountExpired.label" default="Account Expired"/>

    </label>
    <br>
    <g:checkBox name="accountExpired" value="${usersInstance?.accountExpired}"/>
    <div class="help-block"></div>
  </div>
</g:if>



<g:if test="${usersInstance}">
  <div class="col-md-2 form-group ${hasErrors(bean: usersInstance, field: 'accountLocked', 'error')} ">

    <label class="control-label" for="accountLocked">
      <g:message code="users.accountLocked.label" default="Account Locked"/>
    </label>
    <br>
    <g:checkBox name="accountLocked" value="${usersInstance?.accountLocked}"/>
    <div class="help-block"></div>
  </div>
</g:if>




<g:if test="${usersInstance}">
  <div class="col-md-2 form-group ${hasErrors(bean: usersInstance, field: 'enabled', 'error')} ">

    <label class="control-label" for="enabled">
      <g:message code="users.enabled.label" default="Enabled"/>

    </label>
    <br>
    <g:checkBox name="enabled" value="${usersInstance?.enabled}"/>
    <div class="help-block"></div>
  </div>
</g:if>


<g:if test="${usersInstance}">
  <div class="col-md-3 form-group ${hasErrors(bean: usersInstance, field: 'passwordExpired', 'error')} ">

    <label class="control-label" for="passwordExpired">
      <g:message code="users.passwordExpired.label" default="Password Expired"/>
    </label>
    <br>
    <g:checkBox name="passwordExpired" value="${usersInstance?.passwordExpired}"/>
    <div class="help-block"></div>
  </div>
</g:if>


