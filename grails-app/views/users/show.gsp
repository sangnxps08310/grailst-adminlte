
<%@ page import="com.hrm.UsersRole; com.hrm.Users" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'users.label', default: 'Users')}"/>
  <title><g:message code="default.show.label"
                    args="${[message( code: 'users.label', default:'users')]}">
</g:message></title>
</head>

<body>
%{--		<a href="#show-users" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>--}%
%{--		<div class="nav" role="navigation">--}%
%{--			<ul>--}%
%{--				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
%{--				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>--}%
%{--				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>--}%
%{--			</ul>--}%
%{--		</div>--}%
<div class="users">
  <div id="show-users" class="content scaffold-show" role="main">
%{--    <h3><g:message code="default.show.label" args="[entityName]"/></h3>--}%
    <g:if test="${flash.message}">
      <div class="message alert alert-success" role="status">${flash.message}</div>
    </g:if>
    <div class="box box-primary">
      <div class="box-body">
        <h4><i class="fa fa-book"></i>Details:</h4>
        <table class="users table no-margin">
          
          %{--          <g:if test="${usersInstance?.username}">--}%
          <tr class="fieldcontain">
            <th id="username-label" class="property-label"><g:message
              code="users.username.label" default="Username"/></th>
            
            <td class="property-value" aria-labelledby="username-label"><g:fieldValue bean="${usersInstance}"
                                                                                       field="username"/></td>
          </tr>
          <tr class="fieldcontain">
            <th id="userInfor-label" class="property-label"><g:message
              code="users.userInfor.label" default="User Infor"/></th>
            <td class="property-value" aria-labelledby="userInfor-label"><g:link
              controller="userInfor" action="show"
              id="${usersInstance?.userInfor?.id}">${usersInstance?.userInfor?.name}</g:link></td>

          </tr>


          <tr class="fieldcontain">
            <%
              def roleId = UsersRole.findAllByUsers(usersInstance).role.id
              def roles = com.hrm.Role.findAllByIdInList(roleId).authority
            %>
            <th id="usersRole-label" class="property-label"><g:message
                    code="usersRole.role.label" default="Role"/></th>
            <td class="property-value" aria-labelledby="usersRole-label"><g:each in="${roles}" var="name"><span class="badge bg-aqua">${name}</span></g:each></td>
          </tr>

          %{--          </g:if>--}%
          
          %{--          <g:if test="${usersInstance?.accountExpired}">--}%
          <tr class="fieldcontain">
            <th id="accountExpired-label" class="property-label"><g:message
              code="users.accountExpired.label" default="Account Expired"/></th>
            
            <td class="property-value" aria-labelledby="accountExpired-label"><g:formatBoolean
              boolean="${usersInstance?.accountExpired}"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${usersInstance?.accountLocked}">--}%
          <tr class="fieldcontain">
            <th id="accountLocked-label" class="property-label"><g:message
              code="users.accountLocked.label" default="Account Locked"/></th>
            
            <td class="property-value" aria-labelledby="accountLocked-label"><g:formatBoolean
              boolean="${usersInstance?.accountLocked}"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${usersInstance?.enabled}">--}%
          <tr class="fieldcontain">
            <th id="enabled-label" class="property-label"><g:message
              code="users.enabled.label" default="Enabled"/></th>
            
            <td class="property-value" aria-labelledby="enabled-label"><g:formatBoolean
              boolean="${usersInstance?.enabled}"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${usersInstance?.passwordExpired}">--}%
          <tr class="fieldcontain">
            <th id="passwordExpired-label" class="property-label"><g:message
              code="users.passwordExpired.label" default="Password Expired"/></th>
            
            <td class="property-value" aria-labelledby="passwordExpired-label"><g:formatBoolean
              boolean="${usersInstance?.passwordExpired}"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
        </table>
        <g:form url="[resource: usersInstance, action: 'delete']" method="DELETE">
          <fieldset class="buttons">
            <g:link class="edit btn btn-info" action="edit" resource="${usersInstance}"><g:message
              code="default.button.edit.label"
              default="Edit"/></g:link>
            <g:actionSubmit class="delete btn btn-danger" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
          </fieldset>
        </g:form>
      </div>
    </div>
  </div>
</div>
</body>
</html>
