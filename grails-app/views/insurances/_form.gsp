<%@ page import="com.hrm.Insurances" %>





%{--<g:if test="${insurancesInstance}">--}%
%{--  <div class="form-group ${hasErrors(bean: insurancesInstance, field: 'createDate', 'error')} required">--}%
%{--    <label class="control-label" for="createDate">--}%
%{--      <g:message code="insurances.createDate.label" default="Create Date"/>--}%
%{--      <span class="text-red">*</span>--}%
%{--    </label>--}%
%{--    <br>--}%
%{--    --}%
%{--    <div class='input-group' id='datetimepicker-createDate'>--}%
%{--      --}%
%{--      <g:field id="date-picker-field" class="form-control" name="createDate"  type="text"  value="${insurancesInstance?.createDate}"/>--}%
%{--        <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>--}%
%{--    </div>--}%
%{--    <script>--}%
%{--       $("#datetimepicker-createDate").datetimepicker({--}%
%{--        format: 'YYYY-MM-DD',--}%
%{--        date: new Date(Number.parseInt($("#date-picker-field-createDate").val()))--}%
%{--      });--}%
%{--    </script>--}%
%{--    --}%
%{--    <div class="help-block"></div>--}%
%{--  </div>--}%
%{--</g:if><g:else>--}%
%{--  <div class="col-md-3">--}%
%{--    <div class="form-group">--}%
%{--      <label class="control-label" for="createDate">--}%
%{--        <g:message code="insurances.createDate.label" default="Create Date"/>--}%
%{--        <span class="text-red">*</span>--}%
%{--      </label>--}%
%{--      <br>--}%
%{--      <input class="form-control" name="createDate" value="${params.createDate}">--}%
%{--      <div class="help-block"></div>--}%
%{--    </div>--}%
%{--  </div>--}%
%{--</g:else>--}%

<g:if test="${insurancesInstance}">
  <div class="form-group ${hasErrors(bean: insurancesInstance, field: 'effectiveDate', 'error')} required">
    <label class="control-label" for="effectiveDate">
      <g:message code="insurances.effectiveDate.label" default="Effective Date"/>
      <span class="text-red">*</span>
    </label>
    <br>
    
    <div class='input-group' id='datetimepicker-effectiveDate'>
      
      <g:field id="date-picker-field-effectiveDate" class="form-control" name="effectiveDate"  type="text"  value="${insurancesInstance?.effectiveDate?.getTime()}"/>
        <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
    </div>
    <script>
       $("#datetimepicker-effectiveDate").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#date-picker-field-effectiveDate").val()))
      });
    </script>
    
    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="effectiveDate">
        <g:message code="insurances.effectiveDate.label" default="Effective Date"/>
      </label>
      <br>
      <input class="form-control" name="effectiveDate" value="${params.effectiveDate}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>

<g:if test="${insurancesInstance}">
  <div class="form-group ${hasErrors(bean: insurancesInstance, field: 'healthInsurance', 'error')} required">
    <label class="control-label" for="healthInsurance">
      <g:message code="insurances.healthInsurance.label" default="Health Insurance"/>
      <span class="text-red">*</span>
    </label>
    <br>
    
      <g:field class="form-control" name="healthInsurance" value="${fieldValue(bean: insurancesInstance, field: 'healthInsurance')}" required=""/>
      
    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="healthInsurance">
        <g:message code="insurances.healthInsurance.label" default="Health Insurance"/>
      </label>
      <br>
      <input class="form-control" name="healthInsurance" value="${params.healthInsurance}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>

<g:if test="${insurancesInstance}">
  <div class="form-group ${hasErrors(bean: insurancesInstance, field: 'socialInsurance', 'error')} required">
    <label class="control-label" for="socialInsurance">
      <g:message code="insurances.socialInsurance.label" default="Social Insurance"/>
      <span class="text-red">*</span>
    </label>
    <br>
    
      <g:field class="form-control" name="socialInsurance" value="${fieldValue(bean: insurancesInstance, field: 'socialInsurance')}" required=""/>
      
    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="socialInsurance">
        <g:message code="insurances.socialInsurance.label" default="Social Insurance"/>
      </label>
      <br>
      <input class="form-control" name="socialInsurance" value="${params.socialInsurance}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>

<g:if test="${insurancesInstance}">
  <div class="form-group ${hasErrors(bean: insurancesInstance, field: 'unEmploymentInsurance', 'error')} required">
    <label class="control-label" for="unEmploymentInsurance">
      <g:message code="insurances.unEmploymentInsurance.label" default="Un Employment Insurance"/>
      <span class="text-red">*</span>
    </label>
    <br>
    
      <g:field class="form-control" name="unEmploymentInsurance" value="${fieldValue(bean: insurancesInstance, field: 'unEmploymentInsurance')}" required=""/>
      
    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="unEmploymentInsurance">
        <g:message code="insurances.unEmploymentInsurance.label" default="Un Employment Insurance"/>
      </label>
      <br>
      <input class="form-control" name="unEmploymentInsurance" value="${params.unEmploymentInsurance}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>

