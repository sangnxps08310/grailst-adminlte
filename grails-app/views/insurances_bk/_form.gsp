<%@ page import="java.text.DateFormat; com.hrm.Insurances; java.text.SimpleDateFormat" %>



<%--  Name --%>

<g:if test="${insurancesInstance}">
  <div class="form-group ${hasErrors(bean: insurancesInstance, field: 'name', 'error')} required">
  <label class="control-label" for="name">
    <g:message code="insurances.name.label" default="Name"/>
    <span class="text-red">*</span>
  </label>
  <br>
  <g:textField class="form-control" name="name" required="" value="${insurancesInstance?.name}"/>
    <div class="help-block"></div>
  </div>
</g:if>

<%--  Factor --%>
<g:if test="${insurancesInstance}">
  <div class="form-group ${hasErrors(bean: insurancesInstance, field: 'factor', 'error')} required">
  <label class="control-label" for="factor">
    <g:message code="insurances.factor.label" default="Factor"/>
    <span class="text-red">*</span>
  </label>
  <br>
  <div class="input-group">
    <g:select class="form-control" id="factor" name="factor" from="${(0..100).step(5)}" value="${Double.parseDouble(insurancesInstance?.factor)*100}"/>
    <span class="input-group-addon"><b><span class="input-group-text">%</span></b></span>
  </div>
    <div class="help-block"></div>
  </div>
</g:if>
<% java.text.SimpleDateFormat dateFormat = new SimpleDateFormat('yyyy-MM-dd')%>


<%--  Effective Date --%>
<g:if test="${insurancesInstance}">
  <div class="form-group">
    <label class="control-label" for="effective-date-from">
      <g:message code="insurances.effectiveDate2.label" default="Effective Date"/>
    </label>
    <br>
    <div class='input-group' id='effective-date-from'>
      <g:field id="effective-date-field-from" class="form-control" type="text" name="effectiveDate"
               value="${insurancesInstance?.effectiveDate?.getTime()}" required=""/>
      <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
    </div>
    <script type="text/javascript">
      $("#effective-date-from").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#effective-date-field-from").val()))
      });
    </script>
    <div class="help-block"></div>
  </div>
</g:if>
<g:else>
  <div class="form-group col-md-3">
    <label class="control-label" for="effective-date-from-1">
      <g:message code="insurances.effectiveDate1.label" default="Effective Date From"/>
    </label>
    <br>
    <div class='input-group' id='effective-date-from-1'>
      <g:field id="effective-date-field-from-1" class="form-control" type="text" name="effectiveDateFrom"
               value="${dateFormat.parse(params.effectiveDateFrom?params.effectiveDateFrom:dateFormat.format(new Date())).getTime()}" required=""/>
      <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
    </div>
    <script type="text/javascript">
      $("#effective-date-from-1").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#effective-date-field-from-1").val()))
      });
    </script>
    <div class="help-block"></div>
  </div>
  <div class="form-group col-md-3">
    <label class="control-label" for="effective-date-to">
      <g:message code="insurances.effectiveDate2.label" default="Effective Date To"/>
    </label>
    <br>
    <div class='input-group' id='effective-date-to'>
      <g:field id="effective-date-field-to" class="form-control" type="text" name="effectiveDateTo"
               value="${dateFormat.parse(params.effectiveDateTo?params.effectiveDateTo:dateFormat.format(new Date())).getTime()}" required=""/>
      <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
    </div>
    <script type="text/javascript">
      $("#effective-date-to").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#effective-date-field-to").val()))
      });
    </script>
    <div class="help-block"></div>
  </div>
</g:else>



<%--  Create Date --%>
<%--
<g:if test="${insurancesInstance}">
  <div class="form-group ${hasErrors(bean: insurancesInstance, field: 'createDate', 'error')} required">
  <label class="control-label" for="createDate">
    <g:message code="insurances.createDate.label" default="Create Date"/>
    <span class="text-red">*</span>
  </label>
  <br>
  <div class='input-group' id='datetimepicker'>
<g:field id="date-picker-field" class="form-control" name="createDate"  type="text"  value="${insurancesInstance?.createDate?.getTime()}"/>
  <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
</div>
<script>
   $("#datetimepicker").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#date-picker-field").val()))
      });
</script>
</g:if>

<g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="createDate">
  <g:message code="insurances.createDate.label" default="Create Date"/>
  <span class="text-red">*</span>
  </label>
  <br>
  <input class="form-control" name="createDate" value="${params.createDate}">
</g:else>  <div class="help-block"></div>
</div>--%>




















