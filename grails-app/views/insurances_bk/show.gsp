
<%@ page import="com.hrm.Insurances" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'insurances.label', default: 'Insurances')}"/>
  <title><g:message code="default.show.label"
                    args="${[message( code: 'insurances.label', default:'insurances')]}">
</g:message></title>
  <style>
    td {
      text-align: left!important;
    }
  </style>
</head>

<body>

<div class="insurances">
  <div id="show-insurances" class="content scaffold-show" role="main">
%{--    <h3><g:message code="default.show.label" args="[entityName]"/></h3>--}%
    <g:if test="${flash.message}">
      <div class="message alert alert-success" role="status">${flash.message}</div>
    </g:if>
    <div class="box box-primary">
      <div class="box-body">
        <h4><i class="fa fa-book"></i>Details: </h4>
        <table class="insurances table no-margin"s>


          %{--          <g:if test="${insurancesInstance?.name}">--}%
          <tr class="fieldcontain">
            <th id="name-label" class="property-label"><g:message
                    code="insurances.name.label" default="Name"/></th>

            <td class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${insurancesInstance}"
                                                                                  field="name"/></td>

          </tr>
          %{--          </g:if>--}%

          %{--          <g:if test="${insurancesInstance?.factor}">--}%
          <tr class="fieldcontain">
            <th id="factor-label" class="property-label"><g:message
                    code="insurances.factor.label" default="Factor"/></th>
          <%
            String tempFactor = (Double.parseDouble(insurancesInstance.factor)*100)+"%"
          %>
          <td class="property-value" aria-labelledby="percent-label">${tempFactor}</td>

          </tr>
          %{--          </g:if>--}%

          %{--          <g:if test="${insurancesInstance?.effectiveDate}">--}%
          <tr class="fieldcontain">
            <th id="effectiveDate-label" class="property-label"><g:message
                    code="insurances.effectiveDate3.label" default="Effective Date"/></th>

            <td class="property-value" aria-labelledby="effectiveDate-label"><g:formatDate format="yyyy-MM-dd"
                    date="${insurancesInstance?.effectiveDate}"/></td>

          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${insurancesInstance?.createDate}">--}%
          <tr class="fieldcontain">
            <th id="createDate-label" class="property-label"><g:message
              code="insurances.createDate.label" default="Create Date"/></th>
            
            <td class="property-value" aria-labelledby="createDate-label"><g:formatDate format="yyyy-MM-dd"
              date="${insurancesInstance?.createDate}"/></td>
            
          </tr>
          %{--          </g:if>--}%
          

          

          
        </table>
        <g:form url="[resource: insurancesInstance, action: 'delete']" method="DELETE">
          <fieldset class="buttons">
            <g:link class="edit btn btn-info" action="edit" resource="${insurancesInstance}"><g:message
              code="default.button.edit.label"
              default="Edit"/></g:link>
            <g:actionSubmit class="delete btn btn-danger" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
          </fieldset>
        </g:form>
      </div>
    </div>
  </div>
</div>
</body>
</html>
