<%@ page import="com.hrm.Insurances" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'insurances.label', default: 'Insurances')}"/>
  <title><g:message code="insurances.label"
                    args="${[message(code: 'insurances.label', default: 'insurances')]}">
  </g:message></title>

</head>

<body>
<div class="insurances">

  <g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
  </g:if>
  <div id="list-insurances" class="box box-primary" role="main">
    <div class="box-header">
      <h4 class="box-title collapsed" data-toggle="collapse" data-target="#search-box" aria-expanded="true"><i
        class="fa fa-search"></i><g:message code="default.search.label" args=" "/></h4>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool btn-search-toggle collapsed" data-toggle="collapse"
                data-target="#search-box" aria-expanded="true">
          <i class="fa fa-plus"></i>
        </button>
      </div>

      <div class="help-block"></div>

      <div class="search-insurances" id="search-box">
        <g:form class="form rows"
                url="[resource: insurancesInstance, action: 'index']">
          <g:render template="form"/>
          <div class="col-md-12"></div>

          <div class="buttons">
            <br>
            <g:submitButton name="search" class="btn btn-success"
                            value="${message(code: 'default.button.search.label', default: 'search')}"/>
          </div>
        </g:form>
      </div>
    </div>

    <div class="box-body">
      <div class="clearfix">
      </div>
      <g:link action="create" class="btn btn-success pull-right"><i class="fa fa-plus"></i></g:link>
      <table class="table table-hover" style="border-top: 1px solid #f4f4f4; border-bottom: 1px solid #f4f4f4">
        <thead>
        <tr>
          <th>#</th>
          <th>${'Social insurance'}</th>
          <th>${'Health Insurance'}</th>
          <th>${'UnEmployment Insurance'}</th>

          %{--          <g:sortableColumn property="name"--}%
          %{--                            title="${message(code: 'insurances.name.label', default: 'Social insurance')}"/>--}%

          %{--          <g:sortableColumn property="factor"--}%
          %{--                            title="${message(code: 'insurances.factor.label', default: 'Health Insurance')}"/>--}%
          %{--          <g:sortableColumn property="factor"--}%
          %{--                            title="${message(code: 'insurances.factor.label', default: 'Health Insurance')}"/>--}%
          <g:sortableColumn property="effectiveDate"
                            title="${message(code: 'insurances.effectiveDate.label', default: 'Effective Date')}"/>

          <g:sortableColumn property="createDate"
                            title="${message(code: 'insurances.createDate.label', default: 'Create Date')}"/>

          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${insurancesInstanceList}" status="i" var="insurancesInstance">
          <tr class="${(i % 2) == 0 ? 'even' : 'odd'}" scope="col">

            <td>${i + 1}</td>
            <td><g:link action="show"
                        id="${insurancesInstance.id}">${fieldValue(bean: insurancesInstance, field: "name")}</g:link></td>


            <%
              String tempFactor = (Double.parseDouble(insurancesInstance.factor) * 100) + "%"
            %>
            <td>${tempFactor}</td>

            <td><g:formatDate format="yyyy-MM-dd" date="${insurancesInstance.effectiveDate}"/></td>

            <td><g:formatDate format="yyyy-MM-dd" date="${insurancesInstance.createDate}"/></td>

            <td><g:link action="edit" resource="${insurancesInstance}"><i
              class="glyphicon glyphicon-pencil"></i></g:link><g:link class="delete" action="delete"
                                                                      resource="${insurancesInstance}"><i
                class="glyphicon glyphicon-remove"></i></g:link></td>
          </tr>
        </g:each>
        </tbody>
      </table>
    </div>

    <div class="box-footer">
      <div class="pagination sang-pagination">
        <g:paginate total="${insurancesInstanceCount ?: 0}"/>
      </div>
    </div>
  </div>
</div>
</body>
</html>
