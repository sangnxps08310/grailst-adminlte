
<%@ page import="com.hrm.Tax" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'tax.label', default: 'Tax')}"/>
  <title><g:message code="default.show.label"
                    args="${[message( code: 'tax.label', default:'tax')]}">
</g:message></title>
  <style>
    td {
      text-align: left!important;
    }
  </style>
</head>

<body>

<div class="tax">
  <div id="show-tax" class="content scaffold-show" role="main">
%{--    <h3><g:message code="default.show.label" args="[entityName]"/></h3>--}%
    <g:if test="${flash.message}">
      <div class="message alert alert-success" role="status">${flash.message}</div>
    </g:if>
    <div class="box box-primary">
      <div class="box-body">
        <h4><i class="fa fa-book"></i>Details: </h4>
        <table class="tax table no-margin"s>

          
          %{--          <g:if test="${taxInstance?.nameTax}">--}%
          <tr class="fieldcontain">
            <th id="nameTax-label" class="property-label"><g:message
              code="tax.nameTax.label" default="Name Tax"/></th>
            
            <td class="property-value" aria-labelledby="nameTax-label"><g:fieldValue bean="${taxInstance}" field="nameTax"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${taxInstance?.percent}">--}%
          <tr class="fieldcontain">
            <th id="percent-label" class="property-label"><g:message
              code="tax.percent.label" default="Percent"/></th>
            <%
                String tempPercent = (Double.parseDouble(taxInstance.percent)*100)+"%"
        %>
            <td class="property-value" aria-labelledby="percent-label">${tempPercent}</td>
            
          </tr>
          %{--          </g:if>--}%



          %{--          <g:if test="${taxInstance?.effectiveDate}">--}%
          <tr class="fieldcontain">
            <th id="effectiveDate-label" class="property-label"><g:message
                    code="tax.effectiveDate.label" default="Effective Date"/></th>

            <td class="property-value" aria-labelledby="effectiveDate-label"><g:formatDate format="yyyy-MM-dd" date="${taxInstance?.effectiveDate}"/></td>

          </tr>
          %{--          </g:if>--}%

          %{--          <g:if test="${taxInstance?.createDate}">--}%
          <tr class="fieldcontain">
            <th id="createDate-label" class="property-label"><g:message
                    code="tax.createDate.label" default="Create Date"/></th>

            <td class="property-value" aria-labelledby="createDate-label"><g:formatDate format="yyyy-MM-dd" date="${taxInstance?.createDate}"/></td>

          </tr>
          %{--          </g:if>--}%


          
        </table>
        <g:form url="[resource: taxInstance, action: 'delete']" method="DELETE">
          <fieldset class="buttons">
            <g:link class="edit btn btn-info" action="edit" resource="${taxInstance}"><g:message
              code="default.button.edit.label"
              default="Edit"/></g:link>
            <g:actionSubmit class="delete btn btn-danger" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
          </fieldset>
        </g:form>
      </div>
    </div>
  </div>
</div>
</body>
</html>
