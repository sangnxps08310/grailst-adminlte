
<%@ page import="com.hrm.Tax" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'tax.label', default: 'Tax')}"/>
  <title><g:message code="tax.label"
                    args="${[message( code: 'tax.label', default:'tax')]}">
  </g:message></title>

</head>

<body>
<div class="tax">


  <div id="list-tax" class="box box-primary" role="main">
    <div class="box-header">
      <h4 class="box-title collapsed" data-toggle="collapse" data-target="#search-box" aria-expanded="false"><i
        class="fa fa-search"></i><g:message code="default.search.label" args=" "/></h4>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool btn-search-toggle collapsed" data-toggle="collapse"
                data-target="#search-box" aria-expanded="false">
          <i class="fa fa-plus"></i>
        </button>
      </div>
    <g:if test="${flash.message}">
      <div class="message" role="status">${flash.message}</div>
    </g:if>
      <div class="search-tax" id="search-box">
        <g:form class="form rows"
                url="[resource: taxInstance, action: 'index']" >
          <g:render template="form"/>
          <div class="col-md-12"></div>
          <div class="buttons col-md-2">
            <g:submitButton name="search" class="btn btn-success"
                            value="${message(code: 'default.button.search.label', default: 'search')}"/>
          </div>
        </g:form>
      </div>
    </div>
    <div class="box-body">
      <div class="clearfix">
      </div>
      <g:link action="create" class="btn btn-success pull-right"><i class="fa fa-plus"></i></g:link>
      <table class="table table-hover" style="border-top: 1px solid #f4f4f4; border-bottom: 1px solid #f4f4f4">
        <thead>
        <tr>
          <th>#</th>

          <g:sortableColumn property="nameTax"
                            title="${message(code: 'tax.nameTax.label', default: 'Name Tax')}"/>

          <g:sortableColumn property="percent"
                            title="${message(code: 'tax.percent.label', default: 'Percent')}"/>



          <g:sortableColumn property="effectiveDate"
                            title="${message(code: 'tax.effectiveDate.label', default: 'Effective Date')}"/>

          <g:sortableColumn property="createDate"
                            title="${message(code: 'tax.createDate.label', default: 'Create Date')}"/>

          
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${taxInstanceList}" status="i" var="taxInstance">
          <tr class="${(i % 2) == 0 ? 'even' : 'odd'}" scope="col">
            
            <td>${i+1}</td>
            <td><g:link action="show" id="${taxInstance.id}">${fieldValue(bean: taxInstance, field: "nameTax")}</g:link></td>

            <%
              String tempPercent = (Double.parseDouble(taxInstance.percent)*100)+"%"
            %>

            <td>${tempPercent}</td>



            <td><g:formatDate format="yyyy-MM-dd" date="${taxInstance.effectiveDate}"/></td>

            <td><g:formatDate format="yyyy-MM-dd" date="${taxInstance.createDate}"/></td>

            <td><g:link action="edit" resource="${taxInstance}"><i
              class="glyphicon glyphicon-pencil"></i></g:link><g:link class="delete" action="delete"
                                                                      resource="${taxInstance}"><i
                class="glyphicon glyphicon-remove"></i></g:link></td>
          </tr>
        </g:each>

        </tbody>
      </table>
    </div>
    <div class="box-footer">
      <div class="pagination sang-pagination">
        <g:paginate total="${taxInstanceCount ?: 0}"/>
      </div>
    </div>
  </div>
</div>
</body>
</html>
