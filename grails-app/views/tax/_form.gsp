<%@ page import="com.hrm.Tax; java.text.DateFormat; java.text.SimpleDateFormat" %>

<%-- NAME --%>
<g:if test="${taxInstance}">
  <div class="form-group ${hasErrors(bean: taxInstance, field: 'nameTax', 'error')} required">
    <label class="control-label" for="nameTax">
      <g:message code="tax.nameTax.label" default="Name Tax"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <g:textField class="form-control" name="nameTax" required="" value="${taxInstance?.nameTax}"/>
    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-4">
    <div class="form-group">
      <label class="control-label" for="nameTax">
        <g:message code="tax.nameTax.label" default="Name Tax"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <input class="form-control" name="nameTax" value="${params.nameTax}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>

<%-- PERCENT --%>
<g:if test="${taxInstance}">
  <div class="form-group ${hasErrors(bean: taxInstance, field: 'percent', 'error')} required">
    <label class="control-label" for="percent">
      <g:message code="tax.percent.label" default="Percent"/>
      <span class="text-red">*</span>
    </label>
    <br>
  <%--  <g:textField class="form-control" name="percent" required="" value="${taxInstance?.percent}"/> --%>
    <%
  def listTax = [
    ['key':5,'value':5],
    ['key':10,'value':10],
    ['key':15,'value':15],
    ['key':20,'value':20],
    ['key':25,'value':25],
    ['key':30,'value':30],
    ['key':35,'value':35],
    ['key':40,'value':40],
    ['key':45,'value':45],
  ]
%>
  <div class="input-group">
    <g:select class="form-control" id="taxPercent" name="percent" optionKey="key" optionValue="value" from="${listTax}" value="${(Double.parseDouble(taxInstance?.percent)*100)}"/>
    <span class="input-group-addon"><b><span class="input-group-text">%</span></b></span>
  </div>

    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-4">
    <div class="form-group">
      <label class="control-label" for="percent">
        <g:message code="tax.percent.label" default="Percent"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <input class="form-control" name="percent" value="${params.percent}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>

<%-- REDUCE --%>
<g:if test="${taxInstance}">
  <div class="form-group ${hasErrors(bean: taxInstance, field: 'reduce', 'error')} required">
    <label class="control-label" for="reduce">
      <g:message code="tax.reduce.label" default="Reduce"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <g:textField class="form-control" name="reduce" required="" value="${taxInstance?.reduce}"/>
    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-4">
    <div class="form-group">
      <label class="control-label" for="reduce">
        <g:message code="tax.reduce.label" default="Reduce"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <input class="form-control" name="reduce" value="${params.reduce}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>



<% java.text.SimpleDateFormat dateFormat = new SimpleDateFormat('yyyy-MM-dd')%>
<%-- EFFECTIVE DATE --%>
<g:if test="${taxInstance}">
  <div class="form-group ${hasErrors(bean: taxInstance, field: 'effectiveDate', 'error')} required">
    <label class="control-label" for="effectiveDate">
      <g:message code="tax.effectiveDate.label" default="Effective Date"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <div class='input-group' id='datetimepicker-effectiveDate'>
      <g:field id="date-picker-field" class="form-control" name="effectiveDate"  type="text"  value="${taxInstance?.effectiveDate?.getTime()}"/>
      <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
    </div>
    <script>
      $("#datetimepicker-effectiveDate").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#date-picker-field").val()))
      });
    </script>
    <div class="help-block"></div>
  </div>
</g:if>
<g:else>
  <div class="form-group col-md-4">
    <label class="control-label" for="effective-date-from-1">
      <g:message code="tax.effectiveDate1.label" default="Effective Date From"/>
    </label>
    <br>

    <div class='input-group' id='effective-date-from-1'>
      <g:field id="effective-date-field-from-1" class="form-control" type="text" name="effectiveDateFrom"
               value="${dateFormat.parse(params.effectiveDateFrom?params.effectiveDateFrom:dateFormat.format(new Date())).getTime()}" required=""/>
      <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
    </div>
    <script type="text/javascript">

      $("#effective-date-from-1").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#effective-date-field-from-1").val()))
      });
    </script>
    <div class="help-block"></div>
  </div>

  <div class="form-group col-md-4">
    <label class="control-label" for="effective-date-to">
      <g:message code="tax.effectiveDate2.label" default="Effective Date To"/>
    </label>
    <br>

    <div class='input-group' id='effective-date-to'>
      <g:field id="effective-date-field-to" class="form-control" type="text" name="effectiveDateTo"
               value="${dateFormat.parse(params.effectiveDateTo?params.effectiveDateTo:dateFormat.format(new Date())).getTime()}" required=""/>
      <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
    </div>
    <script type="text/javascript">

      $("#effective-date-to").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#effective-date-field-to").val()))
      });
    </script>
    <div class="help-block"></div>
  </div>
</g:else>

<%-- CREATE DATE
<g:if test="${taxInstance}">
  <div class="form-group ${hasErrors(bean: taxInstance, field: 'createDate', 'error')} required">
    <label class="control-label" for="createDate">
      <g:message code="tax.createDate.label" default="Create Date"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <div class='input-group' id='datetimepicker-createDate'>
      <g:field id="date-picker-field" class="form-control" name="createDate"  type="text"  value="${taxInstance?.createDate}"/>
        <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
    </div>
    <script>
       $("#datetimepicker-createDate").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#date-picker-field-createDate").val()))
      });
    </script>
    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="createDate">
        <g:message code="tax.createDate.label" default="Create Date"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <input class="form-control" name="createDate" value="${params.createDate}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>

--%>






