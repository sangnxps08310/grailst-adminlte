<%@ page import="com.hrm.Role" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'role.label', default: 'Role')}"/>
  <title><g:message code="role.label"
                    args="${[message(code: 'role.label', default: 'role')]}">
  </g:message></title>

</head>

<body>
<g:if test="${flash.message}">
  <div class="message" role="status">${flash.message}</div>
</g:if>
<div class="role">
  <div id="list-role" class="box box-primary" role="main">
    <div class="box-header">
      <h4 class="box-title collapsed" data-toggle="collapse" data-target="#search-box" aria-expanded="true"><i
        class="fa fa-search"></i><g:message code="default.search.label" args=" "/></h4>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool btn-search-toggle collapsed" data-toggle="collapse"
                data-target="#search-box" aria-expanded="true">
          <i class="fa fa-plus"></i>
        </button>
      </div>

      <div class="help-block"></div>

      <div class="search-role">
        <g:form class="form rows"
                url="[resource: roleInstance, action: 'index']">
          <g:render template="form"/>
          <div class="col-md-12"></div>

          <div class="buttons">
            <g:submitButton name="search" class="btn btn-success"
                            value="${message(code: 'default.button.search.label', default: 'search')}"/>
          </div>
        </g:form>
      </div>
    </div>
    <div class="box-body" id="search-box">
    <g:link action="create" class="btn btn-success pull-right"><i class="fa fa-plus"></i></g:link>
    <table class="table table-hover" style="border-top: 1px solid #f4f4f4; border-bottom: 1px solid #f4f4f4">
      <thead>
      <tr>
        <th>#</th>
        <g:sortableColumn property="authority"
                          title="${message(code: 'role.authority.label', default: 'Authority')}"/>
        <th>Action</th>
      </tr>
      </thead>
      <tbody>
      <g:each in="${roleInstanceList}" status="i" var="roleInstance">
        <tr class="${(i % 2) == 0 ? 'even' : 'odd'}" scope="col">
          <td>${i + 1}</td>
          <td><g:link action="show"
                      id="${roleInstance.id}">${fieldValue(bean: roleInstance, field: "authority")}</g:link></td>
          <td><g:link action="edit" resource="${roleInstance}"><i
            class="glyphicon glyphicon-pencil"></i></g:link><g:link class="delete" action="delete"
                                                                    resource="${roleInstance}"><i
              class="glyphicon glyphicon-remove"></i></g:link></td>
        </tr>
      </g:each>
      </tbody>
    </table>
  </div>
  <div class="box-footer">
    <div class="pagination sang-pagination">
      <g:paginate total="${roleInstanceCount ?: 0}"/>
    </div>
  </div>
</div>

</body>
</html>
