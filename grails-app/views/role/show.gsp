
<%@ page import="com.hrm.Role" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'role.label', default: 'Role')}"/>
  <title><g:message code="default.show.label"
                    args="${[message( code: 'role.label', default:'role')]}">
</g:message></title>
</head>

<body>
%{--		<a href="#show-role" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>--}%
%{--		<div class="nav" role="navigation">--}%
%{--			<ul>--}%
%{--				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
%{--				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>--}%
%{--				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>--}%
%{--			</ul>--}%
%{--		</div>--}%
<div class="role">
  <div id="show-role" class="content scaffold-show" role="main">
%{--    <h3><g:message code="default.show.label" args="[entityName]"/></h3>--}%
    <g:if test="${flash.message}">
      <div class="message alert alert-success" role="status">${flash.message}</div>
    </g:if>
    <div class="box box-primary">
      <div class="box-body">
        <h4><i class="fa fa-book"></i>Details: </h4>
        <table class="role table no-margin"s>
          
          %{--          <g:if test="${roleInstance?.authority}">--}%
          <tr class="fieldcontain">
            <th id="authority-label" class="property-label"><g:message
              code="role.authority.label" default="Authority"/></th>
            
            <td class="property-value" aria-labelledby="authority-label"><g:fieldValue bean="${roleInstance}"
                                                                                       field="authority"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
        </table>
        <g:form url="[resource: roleInstance, action: 'delete']" method="DELETE">
          <fieldset class="buttons">
            <g:link class="edit btn btn-info" action="edit" resource="${roleInstance}"><g:message
              code="default.button.edit.label"
              default="Edit"/></g:link>
            <g:actionSubmit class="delete btn btn-danger" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
          </fieldset>
        </g:form>
      </div>
    </div>
  </div>
</div>
</body>
</html>
