<%@ page import="com.hrm.Role" %>


<g:if test="${roleInstance}">
  <div class="form-group ${hasErrors(bean: roleInstance, field: 'authority', 'error')} required">
  <label class="control-label" for="authority">
    <g:message code="role.authority.label" default="Authority"/>
    <span class="text-red">*</span>
  </label>
  <br>
  <g:textField class="form-control" name="authority" required="" value="${roleInstance?.authority}"/>
</g:if><g:else>
  <div class="form-group col-md-3">
  <input class="form-control" name="authority" value="${params.authority}">
</g:else>  <div class="help-block"></div>
</div>


