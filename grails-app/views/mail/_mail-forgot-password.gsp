<%--
  Created by IntelliJ IDEA.
  User: nhm95
  Date: 5/8/2020
  Time: 12:38 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body onload="">
<div class="container">
  <div class="row">
    <div class="col-md-5">
      <p>Hi, <strong>${name}</strong>!</p>
      <h4 style="color: red">Click submit to change your Hrm account password !</h4>
      <form action="https://45.122.222.162:8443/webservice/checkChangePass" method="post">
%{--      <form action="http://localhost:8080/webservice/checkChangePass" method="post">--}%
        <input type="hidden" name="token" value="${token}">
        <button style="box-shadow: 0px 10px 14px -7px #276873;
        background:linear-gradient(to bottom, #599bb3 5%, #408c99 100%);
        background-color:#599bb3;
        border-radius:8px;
        display:inline-block;
        cursor:pointer;
        color:#ffffff;
        font-family:Arial;
        font-size:20px;
        font-weight:bold;
        padding:6px 24px;
        text-decoration:none;
        text-shadow:0px 1px 0px #3d768a;" onmouseover="this.style.background='#599bb3'"
                onmouseout="this.style.background='#408c99'" href="#" class="myButton">Submit</button>
      </form>
    </div>
  </div>
</div>
</body>
</html>