      <%@ page import="com.hrm.sys.Menu" %>

    <g:if test="${menuInstance}">
<div class="form-group ${hasErrors(bean: menuInstance, field: 'name', 'error')} required">
<label class="control-label" for="name">
    <g:message code="menu.name.label" default="Name"/>
<span class="text-red">*</span>
</label>
<br>
      <g:textField class="form-control" name="name" maxlength="20" required="" value="${menuInstance?.name}"/>
<div class="help-block"></div>
</div>
    </g:if>

    <g:else>
    <div class="form-group col-md-3">
<label class="control-label" for="name">
    <g:message code="menu.name.label" default="Name"/>
</label>
    <input class="form-control" name="name">
    </div>
    </g:else>

<div class="form-group ${hasErrors(bean: menuInstance, field: 'description', 'error')}">

    <g:if test="${menuInstance}">
    <label class="control-label" for="description">
    <g:message code="menu.description.label" default="Description"/>
</label>
<br>
      <g:textField class="form-control" name="description" value="${menuInstance?.description}"/>
    </g:if>

<div class="help-block"></div>
</div>
<div class="form-group ${hasErrors(bean: menuInstance, field: 'parent', 'error')}" id="parent">
<g:if test="${menuInstance}">
<label class="control-label" for="parent">
    <g:message code="menu.parent.label" default="Parent"/>
</label>
<br>
<%
def menuParent =   com.hrm.sys.Menu.findAllByParent(0);
def nullMenu = new Menu();
nullMenu.setId(0);
nullMenu.setIcon(' ')
nullMenu.setName(' ')
nullMenu.setUrl(' ')
menuParent.add(nullMenu);
menuParent.sort{it.name}
%>
 <g:select class="select2 form-control" name="parent" from="${menuParent}" optionValue="name" optionKey="id" value="${ menuInstance?.parent}"></g:select>

</g:if>
<div class="help-block"></div>
</div>
<div class="form-group ${hasErrors(bean: menuInstance, field: 'position', 'error')}
required">

    <g:if test="${menuInstance}">
    <label class="control-label" for="position">
    <g:message code="menu.position.label" default="Position"/>
<span class="text-red">*</span>
</label>
<br>
      <g:field class="form-control" name="position" type="number" max="100" value="${menuInstance.position}" required=""/>
    </g:if>
<div class="help-block"></div>
</div>

<div class="form-group ${hasErrors(bean: menuInstance, field: 'url', 'error')}">


    <g:if test="${menuInstance}">
    <label class="control-label" for="url">
    <g:message code="menu.url.label" default="Url"/>
</label>
<br>
    <g:field class="form-control" name="url" type="text" value="${menuInstance?.url}" />
    </g:if>
    <g:else>

    </g:else>

<div class="help-block"></div>
</div>
<div class="form-group ${hasErrors(bean: menuInstance, field: 'icon', 'error')}">
<g:if test="${menuInstance}">
<label class="control-label" for="icon">
    <g:message code="menu.icon.label" default="Icon"/>
</label>
<br>
%{--<select class="select2 fa-select form-control" name="icon" id="icon" >--}%
%{--    <g:each in="${FASData}" var="data">--}%
%{--        <option value="${data.code}" ${menuInstance?.icon == data?'selected':''}> ${data.name}</option>--}%
%{--    </g:each>--}%
%{--</select>--}%
 <g:select class="select2 fa-select form-control" name="icon" from="${FASData}" optionValue="name" optionKey="code" itemid="icon" value="${ menuInstance?.icon}"></g:select>

<script>

$("#icon").select2({
templateResult: formatState,
templateSelection: formatState,
});

function formatState (state) {
if (!state.id) { return state.text; }
var $state = $('<span>&nbsp;<i class="fa fa-'+state.id+'"></i>&nbsp;'+ state.text+'</span>');
return $state;me
}
</script>
</g:if>

<div class="help-block"></div>
</div>  
