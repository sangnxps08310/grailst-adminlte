  <!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
    <g:set var="entityName" value="${message(code: 'menu.label', default: 'Menu')}"/>
  <title>
    <g:message code="default.create.label"
      args="${[message( code: 'menu.label', default:'menu')]}">
  </g:message>
    </title>
  <asset:javascript src="select2-4.0.13/dist/js/select2.min.js"></asset:javascript>
  <asset:link rel="stylesheet" href="select2-4.0.13/dist/css/select2.min.css"></asset:link>
</head>
<body>
<div class="menu">
  <div id="create-menu" class="content scaffold-create" role="main">
    <g:if test="${flash.message}">
      <div class="message alert alert-success" role="status">${flash.message}
      </div>
    </g:if>
    <g:hasErrors bean="${menuInstance}">
      <div class="alert alert-warning">
        <ul class="errors" role="alert">
      <g:eachError bean="${menuInstance}" var="error">
            <li
        <g:if
          test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"
        </g:if>
        >
        <g:message
          error="${error}"/>
        </li>
      </g:eachError>
        </ul>
      </div>
    </g:hasErrors>
    <div class="box box-primary">
      <div class="box-body">
    <g:form class="form rows"
      url="[resource: menuInstance, action: 'save']" >
      <g:render template="form"/>
    <div class="col-md-12"></div>
    <div class="buttons">
      <g:submitButton name="create" class="btn btn-success"
        value="${message(code: 'default.button.create.label', default: 'Create')}"/>
    </div>
    </g:form>
      </div>
    </div>
</body>
</html>
