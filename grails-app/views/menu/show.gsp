
<%@ page import="com.hrm.sys.Menu" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'menu.label', default: 'Menu')}"/>
  <title><g:message code="default.show.label"
                    args="${[message( code: 'menu.label', default:'menu')]}">
</g:message></title>
<style>
 td {
 text-align: left!important;
 }
</style>
</head>

<body>
%{--		<a href="#show-menu" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>--}%
%{--		<div class="nav" role="navigation">--}%
%{--			<ul>--}%
%{--				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
%{--				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>--}%
%{--				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>--}%
%{--			</ul>--}%
%{--		</div>--}%
<div class="menu">
  <div id="show-menu" class="content scaffold-show" role="main">
%{--    <h3><g:message code="default.show.label" args="[entityName]"/></h3>--}%
    <g:if test="${flash.message}">
      <div class="message alert alert-success" role="status">${flash.message}</div>
    </g:if>
    <div class="box box-primary">
      <div class="box-body">
        <h4><i class="fa fa-book"></i>Details: </h4>
        <table class="menu table no-margin">
          
          %{--          <g:if test="${menuInstance?.name}">--}%
          <tr class="fieldcontain">
            <th id="name-label" class="property-label"><g:message
              code="menu.name.label" default="Name"/></th>
            
            <td class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${menuInstance}"
                                                                                       field="name"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${menuInstance?.description}">--}%
          <tr class="fieldcontain">
            <th id="description-label" class="property-label"><g:message
              code="menu.description.label" default="Description"/></th>
            
            <td class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${menuInstance}"
                                                                                       field="description"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${menuInstance?.parent}">--}%
          <tr class="fieldcontain">
            <th id="parent-label" class="property-label"><g:message
              code="menu.parent.label" default="Parent"/></th>
            
            <td class="property-value" aria-labelledby="parent-label"><g:fieldValue bean="${menuInstance}"
                                                                                       field="parent"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${menuInstance?.position}">--}%
          <tr class="fieldcontain">
            <th id="position-label" class="property-label"><g:message
              code="menu.position.label" default="Position"/></th>
            
            <td class="property-value" aria-labelledby="position-label"><g:fieldValue bean="${menuInstance}"
                                                                                       field="position"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${menuInstance?.url}">--}%
          <tr class="fieldcontain">
            <th id="url-label" class="property-label"><g:message
              code="menu.url.label" default="Url"/></th>
            
            <td class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${menuInstance}"
                                                                                       field="url"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${menuInstance?.icon}">--}%
          <tr class="fieldcontain">
            <th id="icon-label" class="property-label"><g:message
              code="menu.icon.label" default="Icon"/></th>
            
            <td class="property-value" aria-labelledby="icon-label"><g:fieldValue bean="${menuInstance}"
                                                                                       field="icon"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
        </table>
        <g:form url="[resource: menuInstance, action: 'delete']" method="DELETE">
          <fieldset class="buttons">
            <g:link class="edit btn btn-info" action="edit" resource="${menuInstance}"><g:message
              code="default.button.edit.label"
              default="Edit"/></g:link>
            <g:actionSubmit class="delete btn btn-danger" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
          </fieldset>
        </g:form>
      </div>
    </div>
  </div>
</div>
</body>
</html>
