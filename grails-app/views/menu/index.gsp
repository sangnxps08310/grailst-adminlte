
<%@ page import="com.hrm.sys.Menu" %>

<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">

  <g:set var="entityName" value="${message(code: 'menu.label', default: 'Menu')}"/>

  <title>
  <g:message code="menu.label"
    args="${[message( code: 'menu.label', default:'menu')]}">
  </g:message>

  </title>
</head>
<body>
<div class="menu">


  <g:if test="${flash.message}">
        <div class="alert alert-${flash.type}

    " role="alert">${flash.message}

    </div>


  </g:if>


  <div id="list-menu" class="box box-primary" role="main">
    <div class="box-header">
      <h4 class="box-title"  ><i
        class="fa fa-search"></i>

  <g:message code="default.search.label" args=" "/>


  </h4>
      <div class="box-tools pull-right">
        <button id="btn-collapse" class="btn btn-box-tool btn-search-toggle"
                  data-toggle="collapse"
                data-target="#search-box-menu">
          <i class="fa fa-plus"></i>
        </button>
      </div>
      <div id="search-box-menu" class="collapse">
           <g:form class="form rows" url="[resource: menuInstance, action: 'index']" >
              <g:render template="form"></g:render>
              <div class="form-group col-md-2">
              <label>&nbsp;</label>
              <br>
              <g:submitButton name="search" class="btn btn-success">Search</g:submitButton>
              </div>
          </g:form>

      </div>
    </div>
    <div class="box-body">

      <div class="clearfix">
      </div>
      <g:link action="create" class="btn btn-success pull-right"><i class="fa fa-plus"></i></g:link>
      <table class="table table-hover" style="border-top: 1px solid #f4f4f4; border-bottom: 1px solid #f4f4f4">
        <thead>
        <tr>
          <th>#</th>
          
          <g:sortableColumn property="name"
                            title="${message(code: 'menu.name.label', default: 'Name')}" style="text-align: left" />
          
          <g:sortableColumn property="description"
                            title="${message(code: 'menu.description.label', default: 'Description')}" style="text-align: left"/>
          
          <g:sortableColumn property="parent"
                            title="${message(code: 'menu.parent.label', default: 'Parent')}" style="text-align: left"/>
          
          <g:sortableColumn property="position"
                            title="${message(code: 'menu.position.label', default: 'Position')}"/>
          
          <g:sortableColumn property="url"
                            title="${message(code: 'menu.url.label', default: 'Url')}" style="text-align: left"/>
          
          <g:sortableColumn property="icon"
                            title="${message(code: 'menu.icon.label', default: 'Icon')}"/>
          <th> Action</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${menuInstanceList}" status="i" var="menuInstance">
          <tr class="${(i % 2) == 0 ? 'even' : 'odd'}" scope="col">
            <td>${i+1}</td>
            <td style="text-align: left"><g:link action="show" id="${menuInstance.id}">${fieldValue(bean: menuInstance, field: "name")}</g:link></td>
            
            <td style="text-align: left">${fieldValue(bean: menuInstance, field: "description")}</td>
            <%
               def parent = new Menu()
               if(menuInstance){
                parent = Menu.findById(Long.parseLong(menuInstance.parent+""));

              }
            %>
            <td style="text-align: left">${parent?.name}</td>
            
            <td>${fieldValue(bean: menuInstance, field: "position")}</td>
            
            <td style="text-align: left">${fieldValue(bean: menuInstance, field: "url")}</td>
            
            <td><i class="fa fa-${fieldValue(bean: menuInstance, field: "icon")}"></i> </td>
            <td><g:link controller="menu" action="edit" resource="${menuInstance}"><i class="glyphicon glyphicon-pencil"></i></g:link><g:link class="delete" controller="menu" action="delete" resource="${menuInstance}"><i class="glyphicon glyphicon-remove"></i></g:link></td>
          </tr>
        </g:each>
        </tbody>
      </table>
    </div>
    <div class="box-footer">
      <div class="pagination sang-pagination">
        <g:paginate total="${menuInstanceCount ?: 0}"/>
      </div>
    </div>
  </div>
</div>
</body>
</html>
