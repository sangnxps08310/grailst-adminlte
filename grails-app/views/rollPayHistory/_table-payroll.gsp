<table class="table table-striped table-hover" id="table-rollpay">
  <thead>
  <tr>
    <th style="width: 10px">#</th>
    <th>Name</th>
    <th class="number">Work Days</th>
    %{--4--}%
    <th class="number">Allowance</th>
    %{--5--}%
    <th class="number">Bonus</th>
    %{--6--}%
    <th class="number">Rank Salary</th>
    <th>Profession Name</th>
    %{--8--}%
    <th class="number">Salary</th>


    <th class="number">Unauthorized Absence</th>
    %{--10--}%
    <th class="number">Insurance Pay</th>
    %{--11--}%
    <th class="number">Personal Income Tax</th>
    %{--12--}%
    <th class="number">Net salary</th>
    <th>Effective Date</th>
  </tr>
  </thead>
  <tbody>
  <% int i = 0 %>
  <g:each in="${mapUserRollPay}" var="rollPay">
    <% i++ %>
    <tr>
      <td style="widtd: 10px">${i}</td>
      <td>${rollPay['user_name']}</td>
      <td class="number">${rollPay['work_days']}</td>
      <g:if test="${params.action == 'show'}">
        <td><input name="${rollPay['user_id']}_allowance" value="${rollPay['allowance']}"
                   onclick=" if ($(this).val() == 0) $(this).val('')" onkeypress="javascript:return isNumber(event)"
                   style="width: 120px; text-align: right"/></td>
        <td><input name="${rollPay['user_id']}_bonus" value="${rollPay['bonus']}"
                   onclick="if ($(this).val() == 0) $(this).val('')" onkeypress="javascript:return isNumber(event)"
                   style="width: 120px; text-align: right"/></td>
      </g:if><g:else>
      <td class="number">${rollPay['allowance']}</td>
      <td class="number">${rollPay['bonus']}</td>

    </g:else>
      <td class="number">${rollPay['rank_salary']}</td>
      <td>${rollPay['profession_name']}</td>
      <td class="number">${rollPay['temp_salary']}</td>
      <td class="number">${rollPay['unauthorized_absence']}</td>
      <td class="number">${rollPay['insurance']}</td>
      <td class="number">${rollPay['tax']}</td>
      <td class="number">${rollPay['total']}</td>
      <td class="number">${rollPay['effective_date']}</td>
    </tr>
  </g:each>
  %{--  <tr>--}%
  %{--    <td style="widtd: 10px">1</td>--}%
  %{--    <td>Sang</td>--}%
  %{--    <td>30</td>--}%
  %{--    4--}%
  %{--    <td>500000</td>--}%
  %{--    5--}%
  %{--    <td>500000</td>--}%
  %{--    <td>0.5</td>--}%
  %{--    <td>Nhân Viên</td>--}%
  %{--    8--}%
  %{--    <td>15000000</td>--}%
  %{--    <td>0</td>--}%
  %{--    10--}%
  %{--    <td>1000000</td>--}%
  %{--    11--}%
  %{--    <td>100000</td>--}%
  %{--    12--}%
  %{--    <td>14000000</td>--}%
  %{--    <td>2020-04-20</td>--}%
  %{--  </tr>--}%
  </tbody>
</table>

<script>
  $(document).ready(function () {
    $.each($('#table-rollpay tr'), function () {
      var col8 = $(this).find('td').eq(7);
      var col10 = $(this).find('td').eq(9);
      var col11 = $(this).find('td').eq(10);
      var col12 = $(this).find('td').eq(11);
      <g:if test="${params.action != 'show'}">
      var col4 = $(this).find('td').eq(3);
      var col5 = $(this).find('td').eq(4);
      col4.text(convertNumberToMoney(col4.text()) + "đ");
      col5.text(convertNumberToMoney(col5.text()) + "đ");
      </g:if>
      col8.text(convertNumberToMoney(col8.text()) + "đ");
      col10.text(convertNumberToMoney(col10.text()) + "đ");
      col11.text(convertNumberToMoney(col11.text()) + "đ");
      col12.text(convertNumberToMoney(col12.text()) + "đ");
    });
  });

  function validateNuber(input) {
    if (input.val().matches('[0-9]{0,99999}')) {
      input.val(input.val());
    }
  }

  function isNumber(evt) {
    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
      return false;

    return true;
  }

  function convertNumberToMoney(number) {
    var temp_number = number.split("").reverse();
    var money = "";
    var index = 0;
    for (let i = 0; i < temp_number.length; i++) {
      money += temp_number[i];
      index++;
      if (index == 3 && i != (temp_number.length - 1)) {
        money += ".";
        index = 0;
      }
    }
    return money.split('').reverse().join('');
  }
</script>
<style>
  .number {
    text-align: right!important;
  }
</style>