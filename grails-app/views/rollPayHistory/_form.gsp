<%@ page import="com.hrm.RollPayHistory" %>




<%--
<g:if test="${rollPayHistoryInstance}">
  <div class="form-group ${hasErrors(bean: rollPayHistoryInstance, field: 'createDate', 'error')} required">
    <label class="control-label" for="createDate">
      <g:message code="rollPayHistory.createDate.label" default="Create Date"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <div class='input-group' id='datetimepicker-createDate'>
      <g:field id="date-picker-field" class="form-control" name="createDate"  type="text"  value="${rollPayHistoryInstance?.createDate?.getTime()}"/>
        <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
    </div>
    <script>
       $("#datetimepicker-createDate").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#date-picker-field").val()))
      });
    </script>
    
    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="createDate">
        <g:message code="rollPayHistory.createDate.label" default="Create Date"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <input class="form-control" name="createDate" value="${params.createDate}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>
--%>

<%--
<g:if test="${rollPayHistoryInstance}">
  <div class="form-group ${hasErrors(bean: rollPayHistoryInstance, field: 'creater', 'error')} required">
    <label class="control-label" for="creater">
      <g:message code="rollPayHistory.creater.label" default="Creater"/>
      <span class="text-red">*</span>
    </label>
    <br>
      <g:select class="form-control many-to-one" id="creater" name="creater.id" from="${com.hrm.UserInfor.list()}" optionKey="id" required="" value="${rollPayHistoryInstance?.creater?.id}"/>
    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="creater">
        <g:message code="rollPayHistory.creater.label" default="Creater"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <input class="form-control" name="creater" value="${params.creater}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>
--%>

<%--
<g:if test="${rollPayHistoryInstance}">
  <div class="form-group ${hasErrors(bean: rollPayHistoryInstance, field: 'fileName', 'error')} required">
    <label class="control-label" for="fileName">
      <g:message code="rollPayHistory.fileName.label" default="File Name"/>
      <span class="text-red">*</span>
    </label>
    <br>
      <g:textField class="form-control" name="fileName" required="" value="${rollPayHistoryInstance?.fileName}"/>
    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="fileName">
        <g:message code="rollPayHistory.fileName.label" default="File Name"/>
        <span class="text-red">*</span>
      </label>
      <br>
      <input class="form-control" name="fileName" value="${params.fileName}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>
--%>

<g:if test="${rollPayHistoryInstance}">
  <div class="form-group col-md-3 ${hasErrors(bean: rollPayHistoryInstance, field: 'month', 'error')} required">
    <label class="control-label" for="month">
      <g:message code="rollPayHistory.month.label" default="Month"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <g:select class="form-control" name="month" from="${1..12}" value="${rollPayHistoryInstance?.month}"/>
    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="month">
        <g:message code="rollPayHistory.month.label" default="Month"/>
      </label>
      <br>
      <g:select class="form-control" name="month" from="${1..12}" value="${params.month}"/>
      <div class="help-block"></div>
    </div>
  </div>
</g:else>

