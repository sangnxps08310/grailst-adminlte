<%@ page import="com.hrm.RollPayHistory" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'rollPayHistory.label', default: 'RollPayHistory')}"/>
  <title><g:message code="default.show.label"
                    args="${[message(code: 'rollPayHistory.label', default: 'rollPayHistory')]}">
  </g:message></title>
  <style>
  td {
    text-align: left !important;
  }
  </style>
</head>

<body>
%{--		<a href="#show-rollPayHistory" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>--}%
%{--		<div class="nav" role="navigation">--}%
%{--			<ul>--}%
%{--				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
%{--				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>--}%
%{--				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>--}%
%{--			</ul>--}%
%{--		</div>--}%
<div class="rollPayHistory">
  <div id="show-rollPayHistory" class="content scaffold-show" role="main">
  %{--    <h3><g:message code="default.show.label" args="[entityName]"/></h3>--}%
    <g:if test="${flash.message}">
      <div class="message alert alert-success" role="status">${flash.message}</div>
    </g:if>
    <div class="box box-primary">
      <div class="box-body">
        <h4><i class="fa fa-book"></i>Details:</h4>
        <table class="rollPayHistory table no-margin" s>

          %{--          <g:if test="${rollPayHistoryInstance?.createDate}">--}%
          <tr class="fieldcontain">
            <th id="createDate-label" class="property-label"><g:message
              code="rollPayHistory.createDate.label" default="Create Date"/></th>

            <td class="property-value" aria-labelledby="createDate-label"><g:formatDate format="yyyy-MM-dd"
                                                                                        date="${rollPayHistoryInstance?.createDate}"/></td>

          </tr>
          %{--          </g:if>--}%

          %{--          <g:if test="${rollPayHistoryInstance?.creater}">--}%
          <tr class="fieldcontain">
            <th id="creater-label" class="property-label"><g:message
              code="rollPayHistory.creater.label" default="Creater"/></th>

            <td class="property-value" aria-labelledby="creater-label"><g:link
              controller="userInfor" action="show"
              id="${rollPayHistoryInstance?.creater?.id}">${rollPayHistoryInstance?.creater?.name}</g:link></td>

          </tr>
          %{--          </g:if>--}%

          %{--          <g:if test="${rollPayHistoryInstance?.fileName}">--}%
          <tr class="fieldcontain">
            <th id="fileName-label" class="property-label"><g:message
              code="rollPayHistory.fileName.label" default="File Name"/></th>
            <td class="property-value" aria-labelledby="fileName-label">
              <g:link action="download" id="${rollPayHistoryInstance.id}">
                <g:fieldValue bean="${rollPayHistoryInstance}" field="fileName"/>&nbsp;
                <g:if test="${rollPayHistoryInstance.fileName}">
                  <i class="fa fa-download" style="color: green"></i>
                </g:if>
              </g:link>
            </td>
          </tr>
          %{--          </g:if>--}%

          %{--          <g:if test="${rollPayHistoryInstance?.month}">--}%
          <tr class="fieldcontain">
            <th id="month-label" class="property-label"><g:message
              code="rollPayHistory.month.label" default="Month"/></th>

            <td class="property-value" aria-labelledby="month-label"><g:fieldValue bean="${rollPayHistoryInstance}"
                                                                                   field="month"/></td>

          </tr>
          %{--          </g:if>--}%
          <tr class="fieldcontain">
            <th id="status-label" class="property-label"><g:message
              code="rollPayHistory.status.label" default="Status"/></th>
            <td class="property-value" aria-labelledby="status-label"><g:fieldValue bean="${rollPayHistoryInstance}"
                                                                                    field="status"/></td>
          </tr>
        </table>

        <fieldset class="buttons">
          <g:link id="download" style="display: none" class="download btn btn-success" action="download"
                  resource="${rollPayHistoryInstance}"><g:message
            code="default.button.download.label"
            default="Download"/></g:link>
        </fieldset>
      </div>
      <div class="overlay">
        <i class="fa fa-refresh fa-spin"></i>
      </div>
    </div>

    <div class="box box-primary">
      <g:form action="confirmRollPay" id="${rollPayHistoryInstance.id}">
        <div class="box-header">
          <h3 class="box-title">Table Payroll</h3>
          <g:if test="${params.action == 'show'}">
            <div class="pull-right">
              <button class="btn btn-success" onclick="return confirm('Are you sure?')">Confirm</button>
            </div>
          </g:if><g:else>
          <g:if test="${!rollPayHistoryInstance.fileName}">

            <div class="pull-right">
              <button class="btn btn-success" id="export" type="button"
                      onclick="return confirm('Are you sure?')">Export</button>
            </div>
          </g:if>
        </g:else>

        </div>

        <div class="box-body">
          <g:render template="table-payroll"></g:render>
        </div>
      </g:form>
      <div class="overlay">
        <i class="fa fa-refresh fa-spin"></i>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function () {
    $('.overlay').hide();
    $('#export').click(function () {
      $('.overlay').show();

      $.ajax({
        url: '${createLink(controller: 'rollPayHistory',action: 'exportExcelFile')}',
        method: 'GET',
        data: {id:${rollPayHistoryInstance.id}},
        success: function (data) {
          if (data != 500 && data != 501) {
            $('body').html(data);
            $('.overlay').hide();
          }
        }
      })
    });
  });
</script>
%{--<script>--}%
%{--  $(document).ready(function () {--}%
%{--    $.ajax({--}%
%{--      url: "checkExportStatus",--}%
%{--      method: "POST",--}%
%{--      data: {--}%
%{--        id: ${rollPayHistoryInstance.id}--}%
%{--      },--}%
%{--      success: function (data) {--}%
%{--        $('body').html(data);--}%
%{--      }--}%
%{--    })--}%
%{--  })--}%

%{--</script>--}%
</body>
</html>
