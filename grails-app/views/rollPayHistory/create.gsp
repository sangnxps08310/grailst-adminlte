<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'rollPayHistory.label', default: 'RollPayHistory')}"/>
  <title><g:message code="default.create.label"
                    args="${[message( code: 'rollPayHistory.label', default:'rollPayHistory')]}">
  </g:message></title>

</head>

<body>
%{--<a href="#create-rollPayHistory" class="skip" tabindex="-1"><g:message code="default.link.skip.label"--}%
%{--                                                                                    default="Skip to content&hellip;"/></a>--}%

%{--<div class="nav" role="navigation">--}%
%{--  <ul>--}%
%{--    <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
%{--    <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>--}%
%{--  </ul>--}%
%{--</div>--}%
<div class="rollPayHistory">
  <div id="create-rollPayHistory" class="content scaffold-create" role="main">
%{--    <h1><g:message code="default.create.label" args="[entityName]"/></h1>--}%
    <g:if test="${flash.message}">
      <div class="message alert alert-success" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${rollPayHistoryInstance}">
      <div class="alert alert-warning">
        <ul class="errors" role="alert">
          <g:eachError bean="${rollPayHistoryInstance}" var="error">
            <li<g:if
            test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
            error="${error}"/></li>
          </g:eachError>
        </ul>
      </div>
    </g:hasErrors>
    <div class="box box-primary">
      <div class="box-body">

        <g:form class="form rows"
            url="[resource: rollPayHistoryInstance, action: 'save']" >
    <g:render template="form"/>
    <div class="col-md-12"></div>

    <div class="buttons">
      <br>
     <%-- <g:submitButton name="create" class="btn btn-success"
                      value="${message(code: 'default.button.create.label', default: 'Create')}"/> --%>
      <div class="buttons">
        <g:submitButton name="generate" class="btn btn-warning"
                        value="${message(code: 'default.button.generate.label', default: 'Generate')}"/>
      </div>
    </div>

    </g:form>
      </div>

    </div>
</body>
</html>
