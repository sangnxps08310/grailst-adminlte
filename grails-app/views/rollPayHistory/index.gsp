<%@ page import="com.hrm.RollPayHistory" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="adminlte">
    <g:set var="entityName" value="${message(code: 'rollPayHistory.label', default: 'RollPayHistory')}"/>
    <title><g:message code="rollPayHistory.label"
                      args="${[message(code: 'rollPayHistory.label', default: 'rollPayHistory')]}">
    </g:message></title>

</head>

<body>
<div class="rollPayHistory">

    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <div id="list-rollPayHistory" class="box box-primary" role="main">
        <div class="box-header">
            <h4 class="box-title collapsed" data-toggle="collapse" data-target="#search-box" aria-expanded="true"><i
                    class="fa fa-search"></i><g:message code="default.search.label" args=" "/></h4>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool btn-search-toggle collapsed" data-toggle="collapse"
                        data-target="#search-box" aria-expanded="true">
                    <i class="fa fa-plus"></i>
                </button>
            </div>

            <div class="help-block"></div>

            <div class="search-rollPayHistory" id="search-box">
                <g:form class="form rows"
                        url="[resource: rollPayHistoryInstance, action: 'index']">
                    <g:render template="form"/>
                    <div class="buttons col-md-3">
                        <label>&nbsp;</label>
                        <br>
                        <g:submitButton name="search" class="btn btn-success"
                                        value="${message(code: 'default.button.search.label', default: 'search')}"/>
                    </div>
                </g:form>
            </div>
        </div>

        <div class="box-body">
            <div class="clearfix">
            </div>
            <g:link action="create" class="btn btn-success pull-right"><i class="fa fa-plus"></i></g:link>
            <table class="table table-hover" style="border-top: 1px solid #f4f4f4; border-bottom: 1px solid #f4f4f4">
                <thead>
                <tr>
                    <th>#</th>

                    <g:sortableColumn property="createDate" style="text-align: left;"
                                      title="${message(code: 'rollPayHistory.createDate.label', default: 'Create Date')}"/>

                    <th  style="text-align: left;"><g:message code="rollPayHistory.creater.label" default="Creater"/></th>

                    <g:sortableColumn property="fileName"
                                      title="${message(code: 'rollPayHistory.fileName.label', default: 'File Name')}"/>

                    <g:sortableColumn property="month"
                                      title="${message(code: 'rollPayHistory.month.label', default: 'Month')}"/>

                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${rollPayHistoryInstanceList}" status="i" var="rollPayHistoryInstance">
                    <tr class="${(i % 2) == 0 ? 'even' : 'odd'}" scope="col">

                        <td>${i + 1}</td>
                        <td style="text-align: left">
                            <g:formatDate date="${rollPayHistoryInstance.createDate}"
                                          format="yyyy-MM-dd"></g:formatDate>
                        </td>

                        <td style="text-align: left">${fieldValue(bean: rollPayHistoryInstance, field: "creater.name")}</td>

                        <td >${fieldValue(bean: rollPayHistoryInstance, field: "fileName")}</td>

                        <td>${fieldValue(bean: rollPayHistoryInstance, field: "month")}</td>

                        <td >
                            <g:link action="show" resource="${rollPayHistoryInstance}"><i
                                    class="fa fa-eye"></i></g:link>
                            <g:link action="download" id="${rollPayHistoryInstance.id}"><i class="fa fa-download"
                                                                                           style="color: green"></i></g:link>
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>

        <div class="box-footer">
            <div class="pagination sang-pagination">
                <g:paginate total="${rollPayHistoryInstanceCount ?: 0}"/>
            </div>
        </div>
    </div>
</div>
</body>
</html>
