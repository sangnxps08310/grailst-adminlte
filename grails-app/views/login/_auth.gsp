<g:if test='${flash.message}'>
  <div class='login_message'>${flash.message}</div>
</g:if>
<form action='${postUrl}' method='POST' id='loginForm' class='cssform' autocomplete='off'>
  <p>
    <label for='username'><g:message code="springSecurity.login.username.label"/>:</label>
    <input type='text' class='text_ form-control' name='j_username' id='username'/>
  </p>

  <p>
    <label for='password'><g:message code="springSecurity.login.password.label"/>:</label>
    <input type='password' class='text_ form-control' name='j_password' id='password'/>
  </p>

  <p id="remember_me_holder" class="checkbox">
    <label for='remember_me'><input type='checkbox' name='${rememberMeParameter}' id='remember_me'
                                    <g:if test='${hasCookie}'>checked='checked'</g:if>/>

      <g:message code="springSecurity.login.remember.me.label"/></label>
    <a class="btn btn-link pull-right" id="btn-check-forgot"><i>Forgot password?</i></a>

  </p>

  <p>
    <button class="btn btn-info" id="submit" value=''>${message(code: "springSecurity.login.button")}</button>
  </p>
</form>
<script>
  $(document).ready(function () {
    $('#btn-check-forgot').click(function () {
      $('#login-container').slideUp();
      $('#forgot-container').slideDown();
    });
  })
</script>
<script type='text/javascript'>
  (function () {
    document.forms['loginForm'].elements['j_username'].focus();
  })();

</script>
%{--</body>--}%
%{--</html>--}%
