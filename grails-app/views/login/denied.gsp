<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="denied">
  <title>Denied</title>
  <link rel="shortcut icon" href="${createLinkTo(dir:'images',file:'favicon.ico')}" type="image/x-icon" />

  <asset:javascript src="select2-4.0.13/dist/js/select2.min.js"></asset:javascript>
  <asset:link rel="stylesheet" href="select2-4.0.13/dist/css/select2.min.css"></asset:link>
</head>

<body> <div class="error-page">
  <div class="error-content">
    <h1><i class="fa fa-frown-o" style="font-size:300px; color: #b2b2b2"></i></h1>
    <h3><i class="fa fa-warning text-red"></i> Oops! <g:message code="springSecurity.denied.message" />.</h3>
  </div>
</div>
</body>
</html>
