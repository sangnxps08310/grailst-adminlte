<a class="btn btn-transparent" style=" font-weight: bold;" id="btn-back"><i class="fa fa-arrow-left"></i></a>

<p class="login-box-msg">Enter username to check password</p>

<p id="message" style="color: rgba(145,7,17,0.85); font-weight: bolder"></p>

<p>
  <label for='username'><g:message code="springSecurity.login.username.label"/>:</label>
  <input type='text' class='text_ form-control' name='username-forgot' id='username-forgot'/>
</p>

<p>
  <button class="btn btn-info" id="submit-forgot">Summit</button>
</p>

<div class="overlay">
  <i class="fa fa-refresh fa-spin"></i>
</div>
<script>
  var autoLoginLoad = false;
  var loginDataTemp = {};

  var interv = setInterval(checkAutoLogin, 1000);
  $(document).ready(function () {
    $('.overlay').hide();
    $('#btn-back').click(function () {
      $('#forgot-container').slideUp();
      $('#login-container').slideDown();
    });

    $('#submit-forgot').click(function () {
      $('#message').html('')
      $('.overlay').show();
      $.ajax({
        url: '/webservice/sendForgotMail',
        method: 'POST',
        data: {
          "usn": $('#username-forgot').val(),
        },
        success: function (data) {
          if (data.status == 501) {
            $('#message').html('Username is invalid !')
            $('.overlay').hide();
            clearInterval(interv)
          } else if (data.status == 500) {
            $('#message').html('This username have changed the password more than 3 times today, please try again tomorrow !')
            $('.overlay').hide();
            clearInterval(interv)
          } else {
            var loginData = {
              id: data.access_id,
            };
            loginDataTemp = loginData;
            autoLoginLoad = true;
            $('.overlay').show();
          }
        },
        error: function () {
          $('.overlay').hide();
          $('#message').html('Username is invalid !')
          clearInterval(interv);
        }
      })
    });
  });

  function checkAutoLogin() {
    console.log(loginDataTemp);
    console.log(autoLoginLoad);
    if (autoLoginLoad) {
      $.ajax({
        url: '/webservice/checkAutoLogin',
        method: "POST",
        data: loginDataTemp,
        success: function (data) {
          if (data.status == 200) {
            var username = $('#username-forgot').val();
            console.log('username ====>' + username);
            var authkey = data.auth_key;
            $('#password').val(authkey);
            $('#username').val(username);
            $('#submit').click();
            console.log('form submited');
            autoLoginLoad = false;
            clearInterval(interv)
          } else if (data.status == 0 || data.status == 1) {
            console.log('Loading...');
            autoLoginLoad = true;
          } else if (data.status == 501) {
            autoLoginLoad = false;
            clearInterval(interv)
          }
        }
      })
    }
  }
</script>