<%--
  Created by IntelliJ IDEA.
  User: nhm95
  Date: 2/21/2020
  Time: 8:24 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Change Pass</title>
  <link rel="shortcut icon" href="${createLinkTo(dir:'images',file:'favicon.ico')}" type="image/x-icon" />

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <asset:link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css"></asset:link>
  %{--    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">--}%
  <!-- Font Awesome -->
  <asset:link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css"></asset:link>

  %{--    <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">--}%
  <!-- Ionicons -->
  <asset:link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css"></asset:link>

  %{--    <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">--}%
  <!-- Theme style -->
  <asset:link rel="stylesheet" href="dist/css/AdminLTE.min.css"></asset:link>

  %{--    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">--}%
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <asset:link rel="stylesheet" href="dist/css/skins/skin-blue.min.css"></asset:link>

  %{--    <link rel="stylesheet" href="dist/css/skins/skin-blue.min.css">--}%

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <link rel="stylesheet"
        href="${resource(dir: 'adminlte', file: 'bower_components/bootstrap/dist/css/bootstrap.min.css')}
        ">
  <asset:javascript src="jquery-3.4.1.js"></asset:javascript>

  <asset:link href="customize.css" rel="stylesheet"></asset:link>

  <asset:javascript src="application.js"></asset:javascript>
</head>

<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    %{--        <a href="../../index2.html"><b>Admin</b>LTE</a>--}%
    <asset:image src="logo-hrm.png" style="width: 150px;height: 70px"></asset:image>
  </div>
  <!-- /.login-logo -->

  <div id="forgot-container" class="login-box-body box" style="display: none;">
    <g:render template="change-pass-form"></g:render>
  </div>

  <asset:javascript src="bower_components/jquery/dist/jquery.min.js"></asset:javascript>

  <asset:javascript src="bower_components/bootstrap/dist/js/bootstrap.min.js"></asset:javascript>

  <asset:javascript src="dist/js/adminlte.min.js"></asset:javascript>
  <r:layoutResources/>
  <script>
    $(function () {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' /* optional */
      });
    });
  </script>
</body>
</html>
