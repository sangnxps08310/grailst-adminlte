<p class="login-box-msg">New pass password</p>

<p id="message" style="color: rgba(145,7,17,0.85); font-weight: bolder">${flash.message}</p>

<p>
  <label for='new-pass'>New password:</label>
  <input type='password' class='text_ form-control' name='new_pass' id='new-pass'/>
</p>

<p>
  <label for='confirm-pass'>Confirm password:</label>
  <input type='password' class='text_ form-control' name='confirm_pass' id='confirm-pass'/>
</p>
<input type='hidden' class='text_ form-control' name='token' id='token' value="${token}"/>

<p>
  <button class="btn btn-info" id="submit-forgot">Summit</button>
</p>

<div class="overlay">
  <i class="fa fa-refresh fa-spin"></i>
</div>
<script>
  $(document).ready(function () {
    $('.overlay').hide();
    $('#submit-forgot').click(function () {
      $('#message').html('')
      $('.overlay').show();
      $.ajax({
        url: '/webservice/changePass',
        method: 'POST',
        data: {
          "password": $('#new-pass').val(),
          "confirm_password": $('#confirm-pass').val(),
          "token": $('#token').val(),
        },
        success: function (data) {
          $('#change-pass-container').html(data);
          $('.overlay').hide();
        },
        error: function () {
          $('.overlay').hide();
        }
      })
    });
  });
</script>