<%@ page import="com.hrm.DepartUser" %>

<g:if test="${departUserInstance}">
  <div class="form-group ${hasErrors(bean: departUserInstance, field: 'userInfo', 'error')} required">
  <label class="control-label" for="userInfo">
    <g:message code="departUser.userInfo.label" default="User Info"/>
    <span class="text-red">*</span>
  </label>
  <br>
  <g:select disabled="${params.action == 'edit' ? true : false}" class="form-control many-to-one" id="userInfo"
            name="userInfo.id" optionValue="name" from="${listNoDepartUser}"
            optionKey="id" required="" value="${departUserInstance?.userInfo?.id}"/>

</g:if><g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="userInfo">
  <g:message code="departUser.userInfo.label" default="User Info"/>
  <span class="text-red">*</span>
  </label>
  <br>
  <input class="form-control" name="userInfo" value="${params.userInfo}">
</g:else>  <div class="help-block"></div>
</div>

<g:if test="${departUserInstance}">
  <div class="form-group ${hasErrors(bean: departUserInstance, field: 'description', 'error')} ">
  <label class="control-label" for="description">
    <g:message code="departUser.description.label" default="Description"/>
  </label>
  <br>

  <g:textField class="form-control" name="description" value="${departUserInstance?.description}"/>
</g:if><g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="description">
  <g:message code="departUser.description.label" default="Description"/>
  </label>
  <br>
  <input class="form-control" name="description" value="${params.description}">
</g:else>  <div class="help-block"></div>
</div>

<g:if test="${departUserInstance}">
  <div class="form-group ${hasErrors(bean: departUserInstance, field: 'depart', 'error')} required">
  <label class="control-label" for="depart">
    <g:message code="departUser.depart.label" default="Depart"/>
    <span class="text-red">*</span>
  </label>
  <br>

  <g:select disabled="true" class="form-control many-to-one" id="depart" optionValue="name" name="depart.id"
            from="${com.hrm.Depart.list()}" optionKey="id"
            required="" value="${departUserInstance?.depart?.id}"/>
  <g:hiddenField name="departId" value="${departUserInstance?.depart?.id}"></g:hiddenField>
</g:if><g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="depart">
  <g:message code="departUser.depart.label" default="Depart"/>
  <span class="text-red">*</span>
  </label>
  <br>
  <input class="form-control" name="depart" value="${params.depart}">
</g:else>  <div class="help-block"></div>
</div>

<g:if test="${departUserInstance}">
  <div class="form-group ${hasErrors(bean: departUserInstance, field: 'group', 'error')} required">
  <label class="control-label" for="group">
    <g:message code="departUser.group.label" default="Group"/>
    <span class="text-red">*</span>
  </label>
  <br>

  <g:select class="form-control many-to-one" id="group" name="group.id" isabled="${params.action == 'edit' ? false : true}" optionValue="name"
            from="${com.hrm.DepartGroup.list()}"
            optionKey="id" required="" value="${departUserInstance?.group?.id}"/>
  <g:hiddenField name="group" value="${departUserInstance?.group?.id}"></g:hiddenField>
</g:if><g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="group">
  <g:message code="departUser.group.label" default="Group"/>
  <span class="text-red">*</span>
  </label>
  <br>
  <input class="form-control" name="group" value="${params.group}">
</g:else>  <div class="help-block"></div>
</div>

<g:if test="${departUserInstance}">
  <div id="is-leader-box" class="form-group ${hasErrors(bean: departUserInstance, field: 'is_leader', 'error')} ">
  <label class="control-label" for="is_leader">
    <g:message code="departUser.is_leader.label" default="Is Leader"/>
  </label>
  <br>

  <g:checkBox id="is-leader" name="is_leader" value="${departUserInstance?.is_leader}"/>
</g:if><g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="is_leader">
  <g:message code="departUser.is_leader.label" default="Is Leader"/>
  </label>
  <br>
  <input class="form-control" name="is_leader" value="${params.is_leader}">
</g:else>  <div class="help-block"></div>
</div>

<g:if test="${departUserInstance}">
  <div id="is-room-master-box" class="form-group ${hasErrors(bean: departUserInstance, field: 'is_room_master', 'error')} ">
  <label class="control-label" for="is_room_master">
    <g:message code="departUser.is_room_master.label" default="Is Room Master"/>
  </label>
  <br>
  <g:checkBox id="is-room-master" name="is_room_master" value="${departUserInstance?.is_room_master}"/>
</g:if><g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="is_room_master">
  <g:message code="departUser.is_room_master.label" default="Isroommaster"/>
  </label>
  <br>
  <input class="form-control" name="is_room_master" value="${params.is_room_master}">
</g:else>  <div class="help-block"></div>
</div>
<script>
  if ($('#is-leader').is(':checked')) {
    $('#is-room-master').attr('disabled', '')
  }
  if ($('#is-room-master').is(':checked')) {
    $('#is-leader').attr('disabled', '')
  }
</script>

