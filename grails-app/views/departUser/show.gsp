
<%@ page import="com.hrm.DepartUser" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'departUser.label', default: 'DepartUser')}"/>
  <title><g:message code="default.show.label"
                    args="${[message( code: 'departUser.label', default:'departUser')]}">
</g:message></title>
</head>

<body>
%{--		<a href="#show-departUser" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>--}%
%{--		<div class="nav" role="navigation">--}%
%{--			<ul>--}%
%{--				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
%{--				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>--}%
%{--				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>--}%
%{--			</ul>--}%
%{--		</div>--}%
<div class="departUser">
  <div id="show-departUser" class="content scaffold-show" role="main">
%{--    <h3><g:message code="default.show.label" args="[entityName]"/></h3>--}%
    <g:if test="${flash.message}">
      <div class="message alert alert-success" role="status">${flash.message}</div>
    </g:if>
    <div class="box box-primary">
      <div class="box-body">
        <h4><i class="fa fa-book"></i>Details: </h4>
        <table class="departUser table no-margin"s>
          
          %{--          <g:if test="${departUserInstance?.description}">--}%
          <tr class="fieldcontain">
            <th id="description-label" class="property-label"><g:message
              code="departUser.description.label" default="Description"/></th>
            
            <td class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${departUserInstance}"
                                                                                       field="description"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${departUserInstance?.depart}">--}%
          <tr class="fieldcontain">
            <th id="depart-label" class="property-label"><g:message
              code="departUser.depart.label" default="Depart"/></th>
            
            <td class="property-value" aria-labelledby="depart-label"><g:link
              controller="depart" action="show"
              id="${departUserInstance?.depart?.id}">${departUserInstance?.depart?.encodeAsHTML()}</g:link></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${departUserInstance?.group}">--}%
          <tr class="fieldcontain">
            <th id="group-label" class="property-label"><g:message
              code="departUser.group.label" default="Group"/></th>
            
            <td class="property-value" aria-labelledby="group-label"><g:link
              controller="departGroup" action="show"
              id="${departUserInstance?.group?.id}">${departUserInstance?.group?.encodeAsHTML()}</g:link></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${departUserInstance?.is_leader}">--}%
          <tr class="fieldcontain">
            <th id="is_leader-label" class="property-label"><g:message
              code="departUser.is_leader.label" default="Isleader"/></th>
            
            <td class="property-value" aria-labelledby="is_leader-label"><g:formatBoolean
              boolean="${departUserInstance?.is_leader}"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${departUserInstance?.is_room_master}">--}%
          <tr class="fieldcontain">
            <th id="is_room_master-label" class="property-label"><g:message
              code="departUser.is_room_master.label" default="Isroommaster"/></th>
            
            <td class="property-value" aria-labelledby="is_room_master-label"><g:formatBoolean
              boolean="${departUserInstance?.is_room_master}"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${departUserInstance?.userInfo}">--}%
          <tr class="fieldcontain">
            <th id="userInfo-label" class="property-label"><g:message
              code="departUser.userInfo.label" default="User Info"/></th>
            
            <td class="property-value" aria-labelledby="userInfo-label"><g:link
              controller="userInfor" action="show"
              id="${departUserInstance?.userInfo?.id}">${departUserInstance?.userInfo?.encodeAsHTML()}</g:link></td>
            
          </tr>
          %{--          </g:if>--}%
          
        </table>
        <g:form url="[resource: departUserInstance, action: 'delete']" method="DELETE">
          <fieldset class="buttons">
            <g:link class="edit btn btn-info" action="edit" resource="${departUserInstance}"><g:message
              code="default.button.edit.label"
              default="Edit"/></g:link>
            <g:actionSubmit class="delete btn btn-danger" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
          </fieldset>
        </g:form>
      </div>
    </div>
  </div>
</div>
</body>
</html>
