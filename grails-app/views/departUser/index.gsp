
<%@ page import="com.hrm.DepartUser" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'departUser.label', default: 'DepartUser')}"/>
  <title><g:message code="departUser.label"
                    args="${[message( code: 'departUser.label', default:'departUser')]}">
  </g:message></title>

</head>

<body>
<div class="departUser">

  <g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
  </g:if>
  <div id="list-departUser" class="box box-primary" role="main">
    <div class="box-header">
      <h4 class="box-title collapsed" data-toggle="collapse" data-target="#search-box" aria-expanded="true"><i
        class="fa fa-search"></i><g:message code="default.search.label" args=" "/></h4>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool btn-search-toggle collapsed" data-toggle="collapse"
                data-target="#search-box" aria-expanded="true">
          <i class="fa fa-plus"></i>
        </button>
      </div>
      <div class="help-block"></div>

      <div class="search-departUser" id="search-box">
          <g:form class="form rows"
                  url="[resource: departUserInstance, action: 'index']" >
          <g:render template="form"/>
          <div class="col-md-12"></div>
          <div class="buttons">
            <br>
            <g:submitButton name="search" class="btn btn-success"
                            value="${message(code: 'default.button.search.label', default: 'search')}"/>
          </div>
          </g:form>
        </div>
    </div>
    <div class="box-body">
      <div class="clearfix">
      </div>
      <g:link action="create" class="btn btn-success pull-right"><i class="fa fa-plus"></i></g:link>
      <table class="table table-hover" style="border-top: 1px solid #f4f4f4; border-bottom: 1px solid #f4f4f4">
        <thead>
        <tr>
          <th>#</th>
          
          <g:sortableColumn property="description"
                            title="${message(code: 'departUser.description.label', default: 'Description')}"/>
          
          <th><g:message code="departUser.depart.label" default="Depart"/></th>
          
          <th><g:message code="departUser.group.label" default="Group"/></th>
          
          <g:sortableColumn property="is_leader"
                            title="${message(code: 'departUser.is_leader.label', default: 'Isleader')}"/>
          
          <g:sortableColumn property="is_room_master"
                            title="${message(code: 'departUser.is_room_master.label', default: 'Isroommaster')}"/>
          
          <th><g:message code="departUser.userInfo.label" default="User Info"/></th>
          
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${departUserInstanceList}" status="i" var="departUserInstance">
          <tr class="${(i % 2) == 0 ? 'even' : 'odd'}" scope="col">
            
            <td>1</td>
            <td><g:link action="show" id="${departUserInstance.id}">${fieldValue(bean: departUserInstance, field: "description")}</g:link></td>
            
            <td>${fieldValue(bean: departUserInstance, field: "depart")}</td>
            
            <td>${fieldValue(bean: departUserInstance, field: "group")}</td>
            
            <td><g:formatBoolean boolean="${departUserInstance.is_leader}"/></td>
            
            <td><g:formatBoolean boolean="${departUserInstance.is_room_master}"/></td>
            
            <td>${fieldValue(bean: departUserInstance, field: "userInfo")}</td>
            
            <td><g:link action="edit" resource="${departUserInstance}"><i
              class="glyphicon glyphicon-pencil"></i></g:link><g:link class="delete" action="delete"
                                                                      resource="${departUserInstance}"><i
                class="glyphicon glyphicon-remove"></i></g:link></td>
          </tr>
        </g:each>
        </tbody>
      </table>
    </div>
    <div class="box-footer">
      <div class="pagination sang-pagination">
        <g:paginate total="${departUserInstanceCount ?: 0}"/>
      </div>
    </div>
  </div>
</div>
</body>
</html>
