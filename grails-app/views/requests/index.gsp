<%@ page import="com.hrm.Requests" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'requests.label', default: 'Requests')}"/>
  <title><g:message code="requests.label"
                    args="${[message(code: 'requests.label', default: 'requests')]}">
  </g:message></title>

</head>

<body>
<div class="requests">

  <g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
  </g:if>
  <div id="list-requests" class="box box-primary" role="main">
    <div class="box-header">
      <h4 class="box-title collapsed" data-toggle="collapse" data-target="#search-box" aria-expanded="true"><i
        class="fa fa-search"></i><g:message code="default.search.label" args=" "/></h4>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool btn-search-toggle collapsed" data-toggle="collapse"
                data-target="#search-box" aria-expanded="true">
          <i class="fa fa-plus"></i>
        </button>
      </div>

      <div class="help-block"></div>

      <div class="search-requests " id="search-box">
        <g:form class="form rows"
                url="[resource: requestsInstance, action: 'index']">
          <g:render template="form"/>
          <div class="buttons col-md-4">
            <label>&nbsp;</label>
            <br>

            <g:submitButton name="search" class="btn btn-success"
                            value="${message(code: 'default.button.search.label', default: 'search')}"/>
          </div>
        </g:form>
      </div>
    </div>

    <div class="box-body">
      <div class="clearfix">
      </div>
      <g:link action="create" class="btn btn-success pull-right"><i class="fa fa-plus"></i></g:link>
      <table class="table table-hover" style="border-top: 1px solid #f4f4f4; border-bottom: 1px solid #f4f4f4">
        <thead>
        <tr>
          <th>#</th>
          <th><g:message code="requests.staff.label" default="Staff"/></th>

          <g:sortableColumn property="createdDate"
                            title="${message(code: 'requests.createdDate.label', default: 'Created Date')}"/>

          <g:sortableColumn property="acceptedAt"
                            title="${message(code: 'requests.acceptedAt.label', default: 'Accepted At')}"/>

          <g:sortableColumn property="effectedAt"
                            title="${message(code: 'requests.effectedAt.label', default: 'Effected At')}"/>

          <g:sortableColumn property="expiredAt"
                            title="${message(code: 'requests.expiredAt.label', default: 'Expired At')}"/>


          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${requestsInstanceList}" status="i" var="requestsInstance">
          <tr class="${(i % 2) == 0 ? 'even' : 'odd'}" scope="col">

            <td>${i+1}</td>

            <td style="text-align: left"><g:link controller="userInfor" action="show"
                        id="${requestsInstance?.staff?.id}">${requestsInstance?.staff?.name}</g:link></td>

            <td><g:formatDate format="yyyy-MM-dd"
                              date="${requestsInstance.createdDate}"/></td>

            <td><g:formatDate format="yyyy-MM-dd"
                              date="${requestsInstance.acceptedAt}"/></td>

            <td><g:formatDate format="yyyy-MM-dd" date="${requestsInstance.effectedAt}"/></td>

            <td><g:formatDate format="yyyy-MM-dd" date="${requestsInstance.expiredAt}"/></td>



            <td><g:link action="edit" resource="${requestsInstance}"><i
              class="glyphicon glyphicon-pencil"></i></g:link><g:link class="delete" action="delete"
                                                                      resource="${requestsInstance}"><i
                class="glyphicon glyphicon-remove"></i></g:link></td>
          </tr>
        </g:each>
        </tbody>
      </table>
    </div>

    <div class="box-footer">
      <div class="pagination sang-pagination">
        <g:paginate total="${requestsInstanceCount ?: 0}"/>
      </div>
    </div>
  </div>
</div>
</body>
</html>
