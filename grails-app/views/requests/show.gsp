
<%@ page import="com.hrm.Requests" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'requests.label', default: 'Requests')}"/>
  <title><g:message code="default.show.label"
                    args="${[message( code: 'requests.label', default:'requests')]}">
</g:message></title>
</head>

<body>
%{--		<a href="#show-requests" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>--}%
%{--		<div class="nav" role="navigation">--}%
%{--			<ul>--}%
%{--				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
%{--				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>--}%
%{--				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>--}%
%{--			</ul>--}%
%{--		</div>--}%
<div class="requests">
  <div id="show-requests" class="content scaffold-show" role="main">
%{--    <h3><g:message code="default.show.label" args="[entityName]"/></h3>--}%
    <g:if test="${flash.message}">
      <div class="message alert alert-success" role="status">${flash.message}</div>
    </g:if>
    <div class="box box-primary">
      <div class="box-body">
        <h4><i class="fa fa-book"></i>Details: </h4>
        <table class="requests table no-margin"s>
          
          %{--          <g:if test="${requestsInstance?.createdDate}">--}%
          <tr class="fieldcontain">
            <th id="createdDate-label" class="property-label"><g:message
              code="requests.createdDate.label" default="Created Date"/></th>
            
            <td class="property-value" aria-labelledby="createdDate-label"><g:formatDate
              format="yyyy-MM-dd"
              date="${requestsInstance?.createdDate}"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${requestsInstance?.acceptedAt}">--}%
          <tr class="fieldcontain">
            <th id="acceptedAt-label" class="property-label"><g:message
              code="requests.acceptedAt.label" default="Accepted At"/></th>
            
            <td class="property-value" aria-labelledby="acceptedAt-label"><g:formatDate
              format="yyyy-MM-dd"
              date="${requestsInstance?.acceptedAt}"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${requestsInstance?.effectedAt}">--}%
          <tr class="fieldcontain">
            <th id="effectedAt-label" class="property-label"><g:message
              code="requests.effectedAt.label" default="Effected At"/></th>
            
            <td class="property-value" aria-labelledby="effectedAt-label"><g:formatDate
              format="yyyy-MM-dd"
              date="${requestsInstance?.effectedAt}"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${requestsInstance?.expiredAt}">--}%
          <tr class="fieldcontain">
            <th id="expiredAt-label" class="property-label"><g:message
              code="requests.expiredAt.label" default="Expired At"/></th>
            
            <td class="property-value" aria-labelledby="expiredAt-label"><g:formatDate
              format="yyyy-MM-dd"
              date="${requestsInstance?.expiredAt}"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${requestsInstance?.depart}">--}%
          <tr class="fieldcontain">
            <th id="depart-label" class="property-label"><g:message
              code="requests.depart.label" default="Depart"/></th>
            
            <td class="property-value" aria-labelledby="depart-label"><g:link
              controller="depart" action="show"
              id="${requestsInstance?.depart?.id}">${requestsInstance?.depart?.name?.encodeAsHTML()}</g:link></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${requestsInstance?.staff}">--}%
          <tr class="fieldcontain">
            <th id="staff-label" class="property-label"><g:message
              code="requests.staff.label" default="Staff"/></th>
            
            <td class="property-value" aria-labelledby="staff-label"><g:link
              controller="userInfor" action="show"
              id="${requestsInstance?.staff?.id}">${requestsInstance?.staff?.name?.encodeAsHTML()}</g:link></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${requestsInstance?.status}">--}%
          <tr class="fieldcontain">
            <th id="status-label" class="property-label"><g:message
              code="requests.status.label" default="Status"/></th>
            
            <td class="property-value" aria-labelledby="status-label"><g:fieldValue bean="${requestsInstance}"
                                                                                       field="status"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${requestsInstance?.type}">--}%
          <tr class="fieldcontain">
            <th id="type-label" class="property-label"><g:message
              code="requests.type.label" default="Type"/></th>
            
            <td class="property-value" aria-labelledby="type-label"><g:link
              controller="requestType" action="show"
              id="${requestsInstance?.type?.id}">${requestsInstance?.type?.description?.encodeAsHTML()}</g:link></td>
            
          </tr>
          %{--          </g:if>--}%
          
        </table>
        <g:form url="[resource: requestsInstance, action: 'delete']" method="DELETE">
          <fieldset class="buttons">
            <g:link class="edit btn btn-info" action="edit" resource="${requestsInstance}"><g:message
              code="default.button.edit.label"
              default="Edit"/></g:link>
            <g:actionSubmit class="delete btn btn-danger" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
          </fieldset>
        </g:form>
      </div>
    </div>
  </div>
</div>
</body>
</html>
