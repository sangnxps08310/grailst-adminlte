<%@ page import="java.text.SimpleDateFormat; com.hrm.Requests" %>

<script>

  <sec:ifAnyGranted roles="ROLE_USER">
  $('#box-staff').attr('disabled','');
  $('#box-depart').attr('disabled','');
  $('#box-acceptedAt').hide();
  </sec:ifAnyGranted>
  <sec:ifAnyGranted roles="ROLE_SUPERVISOR">
  $('#box-depart').hide();
  $('#box-acceptedAt').hide();
  </sec:ifAnyGranted>
</script>
<%
  java.text.SimpleDateFormat format = new SimpleDateFormat('yyyy-MM-dd')
%>
<g:if test="${requestsInstance}">
  <div class="form-group ${hasErrors(bean: requestsInstance, field: 'depart', 'error')} required" id="box-depart">
    <label class="control-label" for="depart">
      <g:message code="requests.depart.label" default="Depart"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <sec:ifAnyGranted roles="ROLE_USER,ROLE_SUPERVISOR">
      <g:select class="form-control many-to-one" id="depart" disabled="true" name="depart.id" from="${com.hrm.Depart.list()}"
                optionKey="id" optionValue="name" required="" value="${requestsInstance?.depart?.id}"/>
    </sec:ifAnyGranted>
    <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_MANAGER">
      <g:select class="form-control many-to-one" id="depart" name="depart.id" from="${com.hrm.Depart.list()}"
                optionKey="id" optionValue="name" required="" value="${requestsInstance?.depart?.id}"/>
    </sec:ifAnyGranted>

    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_MANAGER">
    <div class="col-md-2">
      <div class="form-group">
        <label class="control-label" for="depart">
          <g:message code="requests.depart.label" default="Depart"/>
        </label>
        <br>

      <g:select class="form-control many-to-one" id="depart" name="depart.id" from="${com.hrm.Depart.list()}"
                  optionKey="id" optionValue="name" required="" value="${params?.depart?.id}" noSelection="[0: '']"/>
        <div class="help-block"></div>
      </div>
    </div>
  </sec:ifAnyGranted>
</g:else>
<g:if test="${requestsInstance}">
  <div class="form-group ${hasErrors(bean: requestsInstance, field: 'staff', 'error')} required" id="box-staff">
    <label class="control-label" for="staff">
      <g:message code="requests.staff.label" default="Staff"/>
      <span class="text-red">*</span>
    </label>
    <br>
    <sec:ifAnyGranted roles="ROLE_USER,ROLE_SUPERVISOR">
      <g:select class="form-control many-to-one" id="staff" disabled="" name="staff.id" from="${com.hrm.UserInfor.list()}"
                optionKey="id" optionValue="name" required="" value="${requestsInstance?.staff?.id}"/>
    </sec:ifAnyGranted>
    <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_MANAGER">
      <g:select class="form-control many-to-one" id="staff" name="staff.id" from="${com.hrm.UserInfor.list()}"
                optionKey="id" optionValue="name" required="" value="${requestsInstance?.staff?.id}"/>
    </sec:ifAnyGranted>


    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-2">
    <div class="form-group">
      <label class="control-label" for="staff">
        <g:message code="requests.staff.label" default="Staff"/>
      </label>
      <br>
      <input class="form-control" name="staff" value="${params.staff}">

      <div class="help-block"></div>
    </div>
  </div>
</g:else>
<g:if test="${requestsInstance}">
  <div class="form-group ${hasErrors(bean: requestsInstance, field: 'type', 'error')} required">
    <label class="control-label" for="type">
      <g:message code="requests.type.label" default="Type"/>
      <span class="text-red">*</span>
    </label>
    <br>

    <g:select class="form-control many-to-one" id="type" name="type.id" from="${com.hrm.RequestType.list()}"
              optionKey="id" optionValue="description" required="" value="${requestsInstance?.type?.id}"/>

    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-2">
    <div class="form-group">
      <label class="control-label" for="type">
        <g:message code="requests.type.label" default="Type"/>
      </label>
      <br>

      <g:select class="form-control many-to-one" id="type" name="type.id" from="${com.hrm.RequestType.list()}"
                optionKey="id" noSelection="[0: '']" optionValue="description" required="" value="${params?.type?.id}"/>

      <div class="help-block"></div>
    </div>
  </div>

  <div class="col-md-4">
    <label class="control-label col-md-12 no-padding " for="createdAt">
      <g:message code="requests.createdAt.label" default="created At"/>
    </label>
    <div class="form-group no-padding">


      <div class="col-md-6 no-margin no-padding">
        <div class='input-group ' id="datetimepicker-createdFrom">
          <span class="input-group-addon">From</span>

          <g:field id="date-picker-field-createdFrom" class="form-control" name="createdFrom" type="text"
                   value="${params?.createdFrom ? format.parse(params?.createdFrom).getTime() : ''}"/>
          <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
        </div>
      </div>

      <div class="col-md-6 no-margin no-padding">
        <div class='input-group ' id="datetimepicker-createdTo">
          <span class="input-group-addon">To</span>
          <g:field id="date-picker-field-createdTo" class="form-control" name="createdTo" type="text"
                   value="${params?.createdTo ? format.parse(params?.createdTo).getTime() : ''}"/>
          <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
        </div>
      </div>
    </div>
    <script>
      $("#datetimepicker-createdFrom").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#date-picker-field-createdFrom").val()))
      });
      $("#datetimepicker-createdTo").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#date-picker-field-createdTo").val()))
      });
    </script>

    <div class="help-block"></div>
  </div>

  <div class="col-md-12"></div>
</g:else>
%{--<g:if test="${requestsInstance}">--}%
%{--  <div class="form-group ${hasErrors(bean: requestsInstance, field: 'acceptedAt', 'error')} " id="box-acceptedAt">--}%
%{--    <label class="control-label" for="acceptedAt">--}%
%{--      <g:message code="requests.acceptedAt.label" default="Accepted At"/>--}%

%{--    </label>--}%
%{--    <br>--}%

%{--    <div class='input-group' id='datetimepicker-acceptedAt'>--}%

%{--      <g:field id="date-picker-field-acceptedAt" class="form-control" name="acceptedAt" type="text"--}%
%{--               value="${requestsInstance?.acceptedAt}"/>--}%
%{--      <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>--}%
%{--    </div>--}%
%{--    <script>--}%
%{--      $("#datetimepicker-acceptedAt").datetimepicker({--}%
%{--        format: 'YYYY-MM-DD',--}%
%{--        date: new Date(Number.parseInt($("#date-picker-field-acceptedAt").val()))--}%
%{--      });--}%
%{--    </script>--}%

%{--    <div class="help-block"></div>--}%
%{--  </div>--}%
%{--</g:if><g:else>--}%
%{--  <div class="col-md-4">--}%
%{--    <div class="form-group">--}%
%{--      <label class="control-label col-md-12 no-padding no-margin" for="acceptedAt">--}%
%{--        <g:message code="requests.acceptedAt.label" default="accepted At"/>--}%

%{--      </label>--}%

%{--      <div class="col-md-6 no-margin no-padding">--}%
%{--        <div class='input-group ' id="datetimepicker-acceptedFrom">--}%
%{--          <span class="input-group-addon">From</span>--}%

%{--          <g:field id="date-picker-field-acceptedFrom" class="form-control" name="acceptedFrom" type="text"--}%
%{--                   value="${params?.acceptedFrom ? format.parse(params?.acceptedFrom).getTime() : ''}"/>--}%
%{--          <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>--}%
%{--        </div>--}%
%{--      </div>--}%

%{--      <div class="col-md-6 no-margin no-padding">--}%
%{--        <div class='input-group ' id="datetimepicker-acceptedTo">--}%
%{--          <span class="input-group-addon">To</span>--}%
%{--          <g:field id="date-picker-field-acceptedTo" class="form-control" name="acceptedTo" type="text"--}%
%{--                   value="${params?.acceptedTo ? format.parse(params?.acceptedTo).getTime() : ''}"/>--}%
%{--          <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>--}%
%{--        </div>--}%
%{--      </div>--}%
%{--    </div>--}%
%{--    <script>--}%
%{--      $("#datetimepicker-acceptedFrom").datetimepicker({--}%
%{--        format: 'YYYY-MM-DD',--}%
%{--        date: new Date(Number.parseInt($("#date-picker-field-acceptedFrom").val()))--}%
%{--      });--}%
%{--      $("#datetimepicker-acceptedTo").datetimepicker({--}%
%{--        format: 'YYYY-MM-DD',--}%
%{--        date: new Date(Number.parseInt($("#date-picker-field-acceptedTo").val()))--}%
%{--      });--}%
%{--    </script>--}%

%{--    <div class="help-block"></div>--}%
%{--  </div>--}%
%{--</g:else>--}%

<g:if test="${requestsInstance}">
  <div class="form-group ${hasErrors(bean: requestsInstance, field: 'effectedAt', 'error')} " id="box-effectedAt">
    <label class="control-label " for="effectedAt">
      <g:message code="requests.effectedAt.label" default="Effected At"/>
    </label>
    <br>
    <div class='input-group' id='datetimepicker-effectedAt'>
      <g:field id="date-picker-field-effectedAt" class="form-control" name="effectedAt" type="text"
               value="${requestsInstance?.effectedAt}"/>
      <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
    </div>
    <script>
      $("#datetimepicker-effectedAt").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#date-picker-field-effectedAt").val()))
      });
    </script>

    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-4">
    <label class="control-label col-md-12 no-padding" for="effectedAt">
      <g:message code="requests.effectedAt.label" default="Effected At"/>
    </label>

    <div class="form-group">

      <div class="col-md-6 no-margin no-padding">
        <div class='input-group ' id="datetimepicker-effectedFrom">
          <span class="input-group-addon">From</span>

          <g:field id="date-picker-field-effectedFrom" class="form-control" name="effectedFrom" type="text"
                   value="${params?.effectedFrom ? format.parse(params?.effectedFrom).getTime() : ''}"/>
          <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
        </div>
      </div>

      <div class="col-md-6 no-margin no-padding">
        <div class='input-group ' id="datetimepicker-effectedTo">
          <span class="input-group-addon">To</span>
          <g:field id="date-picker-field-effectedTo" class="form-control" name="effectedTo" type="text"
                   value="${params?.effectedTo ? format.parse(params?.effectedTo).getTime() : ''}"/>
          <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
        </div>
      </div>
    </div>
    <script>
      $("#datetimepicker-effectedFrom").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#date-picker-field-effectedFrom").val()))
      });
      $("#datetimepicker-effectedTo").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#date-picker-field-effectedTo").val()))
      });
    </script>

    <div class="help-block"></div>
  </div>
</g:else>

<g:if test="${requestsInstance}">
  <div class="form-group ${hasErrors(bean: requestsInstance, field: 'expiredAt', 'error')} " id="box-expiredAt">
    <label class="control-label" for="expiredAt">
      <g:message code="requests.expiredAt.label" default="Expired At"/>

    </label>
    <br>

    <div class='input-group' id='datetimepicker-expiredAt'>

      <g:field id="date-picker-field-expiredAt" class="form-control" name="expiredAt" type="text"
               value="${requestsInstance?.expiredAt}"/>
      <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
    </div>
    <script>
      $("#datetimepicker-expiredAt").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#date-picker-field-expiredAt").val()))
      });
    </script>

    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-4">
    <label class="control-label col-md-12 no-padding " for="expiredAt">
      <g:message code="requests.expiredAt.label" default="expired At"/>

    </label>
    <div class="form-group">


      <div class="col-md-6 no-margin no-padding">
        <div class='input-group ' id="datetimepicker-expiredFrom">
          <span class="input-group-addon">From</span>

          <g:field id="date-picker-field-expiredFrom" class="form-control" name="expiredFrom" type="text"
                   value="${params?.expiredFrom ? format.parse(params?.expiredFrom).getTime() : ''}"/>
          <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
        </div>
      </div>

      <div class="col-md-6 no-margin no-padding">
        <div class='input-group ' id="datetimepicker-expiredTo">
          <span class="input-group-addon">To</span>
          <g:field id="date-picker-field-expiredTo" class="form-control" name="expiredTo" type="text"
                   value="${params?.expiredTo ? format.parse(params?.expiredTo).getTime() : ''}"/>
          <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
        </div>
      </div>
    </div>
    <script>
      $("#datetimepicker-expiredFrom").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#date-picker-field-expiredFrom").val()))
      });
      $("#datetimepicker-expiredTo").datetimepicker({
        format: 'YYYY-MM-DD',
        date: new Date(Number.parseInt($("#date-picker-field-expiredTo").val()))
      });
    </script>

    <div class="help-block"></div>
  </div>
</g:else>

