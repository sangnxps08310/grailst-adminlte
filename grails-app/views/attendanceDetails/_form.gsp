<%@ page import="com.hrm.AttendanceDetails" %>






<g:if test="${attendanceDetailsInstance}">
  <div class="form-group ${hasErrors(bean: attendanceDetailsInstance, field: 'description', 'error')} ">
  <label class="control-label" for="description">
    <g:message code="attendanceDetails.description.label" default="Description"/>
    
  </label>
  <br>
  

<g:textField class="form-control" name="description" value="${attendanceDetailsInstance?.description}"/>



</g:if><g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="description">
  <g:message code="attendanceDetails.description.label" default="Description"/>
  
  </label>
  <br>
  <input class="form-control" name="description" value="${params.description}">
</g:else>  <div class="help-block"></div>
</div>



<g:if test="${attendanceDetailsInstance}">
  <div class="form-group ${hasErrors(bean: attendanceDetailsInstance, field: 'attendance', 'error')} required">
  <label class="control-label" for="attendance">
    <g:message code="attendanceDetails.attendance.label" default="Attendance"/>
    <span class="text-red">*</span>
  </label>
  <br>
  

<g:select class="form-control many-to-one" id="attendance" name="attendance.id" from="${com.hrm.Attendance.list()}" optionKey="id" required="" value="${attendanceDetailsInstance?.attendance?.id}"/>



</g:if><g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="attendance">
  <g:message code="attendanceDetails.attendance.label" default="Attendance"/>
  <span class="text-red">*</span>
  </label>
  <br>
  <input class="form-control" name="attendance" value="${params.attendance}">
</g:else>  <div class="help-block"></div>
</div>



<g:if test="${attendanceDetailsInstance}">
  <div class="form-group ${hasErrors(bean: attendanceDetailsInstance, field: 'present', 'error')} ">
  <label class="control-label" for="present">
    <g:message code="attendanceDetails.present.label" default="Present"/>
    
  </label>
  <br>
  

<g:checkBox name="present" value="${attendanceDetailsInstance?.present}" />



</g:if><g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="present">
  <g:message code="attendanceDetails.present.label" default="Present"/>
  
  </label>
  <br>
  <input class="form-control" name="present" value="${params.present}">
</g:else>  <div class="help-block"></div>
</div>



<g:if test="${attendanceDetailsInstance}">
  <div class="form-group ${hasErrors(bean: attendanceDetailsInstance, field: 'user', 'error')} required">
  <label class="control-label" for="user">
    <g:message code="attendanceDetails.user.label" default="User"/>
    <span class="text-red">*</span>
  </label>
  <br>
  

<g:select class="form-control many-to-one" id="user" name="user.id" from="${com.hrm.Users.list()}" optionKey="id" required="" value="${attendanceDetailsInstance?.user?.id}"/>



</g:if><g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="user">
  <g:message code="attendanceDetails.user.label" default="User"/>
  <span class="text-red">*</span>
  </label>
  <br>
  <input class="form-control" name="user" value="${params.user}">
</g:else>  <div class="help-block"></div>
</div>


