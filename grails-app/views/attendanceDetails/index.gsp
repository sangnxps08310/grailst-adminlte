
<%@ page import="com.hrm.AttendanceDetails" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'attendanceDetails.label', default: 'AttendanceDetails')}"/>
  <title><g:message code="attendanceDetails.label"
                    args="${[message( code: 'attendanceDetails.label', default:'attendanceDetails')]}">
  </g:message></title>

</head>

<body>
<div class="attendanceDetails">

  <g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
  </g:if>
  <div id="list-attendanceDetails" class="box box-primary" role="main">
    <div class="box-header">
      <h4 class="box-title collapsed" data-toggle="collapse" data-target="#search-box" aria-expanded="true"><i
        class="fa fa-search"></i><g:message code="default.search.label" args=" "/></h4>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool btn-search-toggle collapsed" data-toggle="collapse"
                data-target="#search-box" aria-expanded="true">
          <i class="fa fa-plus"></i>
        </button>
      </div>
      <div class="help-block"></div>

      <div class="search-attendanceDetails" id="search-box">
          <g:form class="form rows"
                  url="[resource: attendanceDetailsInstance, action: 'index']" >
          <g:render template="form"/>
          <div class="col-md-12"></div>
          <div class="buttons">
            <br>
            <g:submitButton name="search" class="btn btn-success"
                            value="${message(code: 'default.button.search.label', default: 'search')}"/>
          </div>
          </g:form>
        </div>
    </div>
    <div class="box-body">
      <div class="clearfix">
      </div>
      <g:link action="create" class="btn btn-success pull-right"><i class="fa fa-plus"></i></g:link>
      <table class="table table-hover" style="border-top: 1px solid #f4f4f4; border-bottom: 1px solid #f4f4f4">
        <thead>
        <tr>
          <th>#</th>
          
          <g:sortableColumn property="description"
                            title="${message(code: 'attendanceDetails.description.label', default: 'Description')}"/>
          
          <th><g:message code="attendanceDetails.attendance.label" default="Attendance"/></th>
          
          <g:sortableColumn property="present"
                            title="${message(code: 'attendanceDetails.present.label', default: 'Present')}"/>
          
          <th><g:message code="attendanceDetails.user.label" default="User"/></th>
          
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${attendanceDetailsInstanceList}" status="i" var="attendanceDetailsInstance">
          <tr class="${(i % 2) == 0 ? 'even' : 'odd'}" scope="col">
            
            <td>1</td>
            <td><g:link action="show" id="${attendanceDetailsInstance.id}">${fieldValue(bean: attendanceDetailsInstance, field: "description")}</g:link></td>
            
            <td>${fieldValue(bean: attendanceDetailsInstance, field: "attendance")}</td>
            
            <td><g:formatBoolean boolean="${attendanceDetailsInstance.present}"/></td>
            
            <td>${fieldValue(bean: attendanceDetailsInstance, field: "user")}</td>
            
            <td><g:link action="edit" resource="${attendanceDetailsInstance}"><i
              class="glyphicon glyphicon-pencil"></i></g:link><g:link class="delete" action="delete"
                                                                      resource="${attendanceDetailsInstance}"><i
                class="glyphicon glyphicon-remove"></i></g:link></td>
          </tr>
        </g:each>
        </tbody>
      </table>
    </div>
    <div class="box-footer">
      <div class="pagination sang-pagination">
        <g:paginate total="${attendanceDetailsInstanceCount ?: 0}"/>
      </div>
    </div>
  </div>
</div>
</body>
</html>
