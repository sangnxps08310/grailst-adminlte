
<%@ page import="com.hrm.AttendanceDetails" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'attendanceDetails.label', default: 'AttendanceDetails')}"/>
  <title><g:message code="default.show.label"
                    args="${[message( code: 'attendanceDetails.label', default:'attendanceDetails')]}">
</g:message></title>
  <style>

  </style>
</head>

<body>
%{--		<a href="#show-attendanceDetails" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>--}%
%{--		<div class="nav" role="navigation">--}%
%{--			<ul>--}%
%{--				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
%{--				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>--}%
%{--				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>--}%
%{--			</ul>--}%
%{--		</div>--}%
<div class="attendanceDetails">
  <div id="show-attendanceDetails" class="content scaffold-show" role="main">
%{--    <h3><g:message code="default.show.label" args="[entityName]"/></h3>--}%
    <g:if test="${flash.message}">
      <div class="message alert alert-success" role="status">${flash.message}</div>
    </g:if>
    <div class="box box-primary">
      <div class="box-body">
        <h4><i class="fa fa-book"></i>Details: </h4>
        <table class="attendanceDetails table no-margin"s>
          
          %{--          <g:if test="${attendanceDetailsInstance?.description}">--}%
          <tr class="fieldcontain">
            <th id="description-label" class="property-label"><g:message
              code="attendanceDetails.description.label" default="Description"/></th>
            
            <td class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${attendanceDetailsInstance}"
                                                                                       field="description"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${attendanceDetailsInstance?.attendance}">--}%
          <tr class="fieldcontain">
            <th id="attendance-label" class="property-label"><g:message
              code="attendanceDetails.attendance.label" default="Attendance"/></th>
            
            <td class="property-value" aria-labelledby="attendance-label"><g:link
              controller="attendance" action="show"
              id="${attendanceDetailsInstance?.attendance?.id}">${attendanceDetailsInstance?.attendance?.encodeAsHTML()}</g:link></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${attendanceDetailsInstance?.present}">--}%
          <tr class="fieldcontain">
            <th id="present-label" class="property-label"><g:message
              code="attendanceDetails.present.label" default="Present"/></th>
            
            <td class="property-value" aria-labelledby="present-label"><g:formatBoolean
              boolean="${attendanceDetailsInstance?.present}"/></td>
            
          </tr>
          %{--          </g:if>--}%
          
          %{--          <g:if test="${attendanceDetailsInstance?.user}">--}%
          <tr class="fieldcontain">
            <th id="user-label" class="property-label"><g:message
              code="attendanceDetails.user.label" default="User"/></th>
            
            <td class="property-value" aria-labelledby="user-label"><g:link
              controller="users" action="show"
              id="${attendanceDetailsInstance?.user?.id}">${attendanceDetailsInstance?.user?.encodeAsHTML()}</g:link></td>
            
          </tr>
          %{--          </g:if>--}%
          
        </table>
        <g:form url="[resource: attendanceDetailsInstance, action: 'delete']" method="DELETE">
          <fieldset class="buttons">
            <g:link class="edit btn btn-info" action="edit" resource="${attendanceDetailsInstance}"><g:message
              code="default.button.edit.label"
              default="Edit"/></g:link>
            <g:actionSubmit class="delete btn btn-danger" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
          </fieldset>
        </g:form>
      </div>
    </div>
  </div>
</div>
</body>
</html>
