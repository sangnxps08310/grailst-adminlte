<div class="box box-primary ">
  <div class="box-header">
    <h4 class="box-title"><i class="fa fa-group"></i> Members:</h4>

    <div class="col-md-1 pull-right">
      %{--      <g:link class="btn btn-success" controller="" ><i class="fa fa-plus"></i></g:link>--}%
    </div>
  </div>

  <div class="box-body ">
    <table class="table table-hover">
      <thead>
      <tr>
        <th>#</th>
        <th>Name</th>
        <th>Position</th>
        <th>Action</th>
      </tr>
      </thead>
      <tbody>
      <g:if test="${listUserInfor.size() > 0}">
        <% int index = 0 %>
        <g:each in="${listUserInfor}" var="user">
          <% index++ %>

          <tr>
            <td>${index}</td>
            <td><g:link id="${user.userInfo.id}" controller="userInfor"
                        action="show">${user.userInfo.name}</g:link></td>
            <td><span
              class="badge bg-green">Member</span>
            </td>
            <td>
              <g:link controller="departUser" action="edit" id="${user.id}"><i
                class="fa fa-pencil"></i></g:link>
              <g:link style="color: red" controller="departUser" action="delete"
                      onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"
                      id="${user.id}"><i class="fa fa-close"></i></g:link></td>
          </tr>
        </g:each>
      </g:if><g:else>
        <tr>
          <td>-</td>
          <td>-</td>
        </tr>
      </g:else>
      </tbody>
    </table>

  </div>
</div>