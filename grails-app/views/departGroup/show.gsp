<%@ page import="com.hrm.DepartGroup" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="${message(code: 'departGroup.label', default: 'DepartGroup')}"/>
  <title><g:message code="default.show.label"
                    args="${[message(code: 'departGroup.label', default: 'departGroup')]}">
  </g:message></title>
</head>

<body>
%{--		<a href="#show-departGroup" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>--}%
%{--		<div class="nav" role="navigation">--}%
%{--			<ul>--}%
%{--				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
%{--				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>--}%
%{--				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>--}%
%{--			</ul>--}%
%{--		</div>--}%
<div class="departGroup">
  <div id="show-departGroup" class="content scaffold-show" role="main">
  %{--    <h3><g:message code="default.show.label" args="[entityName]"/></h3>--}%
    <g:if test="${flash.message}">
      <div class="message alert alert-success" role="status">${flash.message}</div>
    </g:if>
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-body">
          <h4><i class="fa fa-book"></i>Details:</h4>
          <table class="departGroup table no-margin" s>

            %{--          <g:if test="${departGroupInstance?.name}">--}%
            <tr class="fieldcontain">
              <th id="name-label" class="property-label"><g:message
                code="departGroup.name.label" default="Group Name"/></th>

              <td class="property-value" style="text-align:left" aria-labelledby="name-label"><g:fieldValue
                bean="${departGroupInstance}"
                field="name"/></td>

            </tr>
            %{--          </g:if>--}%

            %{--          <g:if test="${departGroupInstance?.description}">--}%
            <tr class="fieldcontain">
              <th id="description-label" class="property-label"><g:message
                code="departGroup.description.label" default="Description"/></th>
              <td class="property-value" style="text-align:left" aria-labelledby="description-label"><g:fieldValue
                bean="${departGroupInstance}"
                field="description"/></td>
            </tr>
            <tr class="fieldcontain">
              <th id="depart-label" class="property-label"><g:message
                code="departGroup.depat.label" default="Depart"/></th>
              <td class="property-value" style="text-align:left" aria-labelledby="depart-label"><g:link
                controller="depart" action="show"
                id="${departGroupInstance?.depart?.id}">${departGroupInstance?.depart?.name.encodeAsHTML()}</g:link></td>
            </tr>
            <tr class="fieldcontain">
              <th id="leader-label" class="property-label"><g:message
                code="departGroup.depat.label" default="Leader"/></th>
              <td class="property-value" style="text-align:left" aria-labelledby="leader-label"><g:link
                controller="userInfor" action="show"
                id="${leaderInfo?.userInfo?.id}"><span
                  class="label label-primary">${leaderInfo?.userInfo?.name.encodeAsHTML()}</span></g:link></td>
            </tr>
          </table>
          <g:form url="[resource: departGroupInstance, action: 'delete']" method="DELETE">
            <fieldset class="buttons">
              <g:link class="edit btn btn-info" action="edit" resource="${departGroupInstance}"><g:message
                code="default.button.edit.label"
                default="Edit"/></g:link>
              <g:actionSubmit class="delete btn btn-danger" action="delete"
                              value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                              onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
            </fieldset>
          </g:form>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <g:render template="group-user" model="[listUserInfor:listUserInfor]"></g:render>
    </div>
  </div>
</div>
</body>
</html>
