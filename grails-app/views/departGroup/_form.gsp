<%@ page import="com.hrm.DepartGroup" %>






<g:if test="${departGroupInstance}">
  <div class="form-group ${hasErrors(bean: departGroupInstance, field: 'name', 'error')} required">
  <label class="control-label" for="name">
    <g:message code="departGroup.name.label" default="Name"/>
    <span class="text-red">*</span>
  </label>
  <br>
  

<g:textField class="form-control" name="name" required="" value="${departGroupInstance?.name}"/>
</g:if><g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="name">
  <g:message code="departGroup.name.label" default="Name"/>
  <span class="text-red">*</span>
  </label>
  <br>
  <input class="form-control" name="name" value="${params.name}">
</g:else>  <div class="help-block"></div>
</div>



<g:if test="${departGroupInstance}">
  <div class="form-group ${hasErrors(bean: departGroupInstance, field: 'description', 'error')} ">
  <label class="control-label" for="description">
    <g:message code="departGroup.description.label" default="Description"/>
    
  </label>
  <br>
  

<g:textField class="form-control" name="description" value="${departGroupInstance?.description}"/>



</g:if><g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="description">
  <g:message code="departGroup.description.label" default="Description"/>
  
  </label>
  <br>
  <input class="form-control" name="description" value="${params.description}">
</g:else>  <div class="help-block"></div>
</div>



<g:if test="${departGroupInstance}">

%{--<g:select id="depart" name="departId" from="${com.hrm.Depart.list()}" optionKey="id" optionValue="name" required="" disabled="true" value="${departGroupInstance?.depart?.id}"/>--}%

<g:hiddenField name="departId" value="${departGroupInstance?.depart?.id}"></g:hiddenField>

</g:if><g:else>
  <div class="form-group col-md-3">
   <label class="control-label" for="depart">
  <g:message code="departGroup.depart.label" default="Depart"/>
  <span class="text-red">*</span>
  </label>
  <br>
  <input class="form-control" name="depat" value="${params.depart}">
    <div class="help-block"></div>
  </div>
</g:else>


