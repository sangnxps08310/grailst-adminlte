package grails.template;

import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.*;
import groovy.lang.*;
import groovy.util.*;

public class Storage
  extends java.lang.Object  implements
    groovy.lang.GroovyObject {
;
public  groovy.lang.MetaClass getMetaClass() { return (groovy.lang.MetaClass)null;}
public  void setMetaClass(groovy.lang.MetaClass mc) { }
public  java.lang.Object invokeMethod(java.lang.String method, java.lang.Object arguments) { return null;}
public  java.lang.Object getProperty(java.lang.String property) { return null;}
public  void setProperty(java.lang.String property, java.lang.Object value) { }
public  java.lang.String getDirectory() { return (java.lang.String)null;}
public  void setDirectory(java.lang.String value) { }
public  java.lang.String getScope() { return (java.lang.String)null;}
public  void setScope(java.lang.String value) { }
public  int getCurrentSize() { return (int)0;}
public  void setCurrentSize(int value) { }
public  int getMaxSize() { return (int)0;}
public  void setMaxSize(int value) { }
public static  java.lang.Object getConstraints() { return null;}
public static  void setConstraints(java.lang.Object value) { }
}
