<%=packageName%>
<% import grails.persistence.Event %>
<% import org.codehaus.groovy.grails.scaffolding.DefaultGrailsTemplateGenerator %>

<% excludedProps = Event.allEvents.toList() << 'version' << 'dateCreated' << 'lastUpdated'
persistentPropNames = domainClass.persistentProperties*.name
boolean hasHibernate = pluginManager?.hasGrailsPlugin('hibernate') || pluginManager?.hasGrailsPlugin('hibernate4')
if (hasHibernate) {
  def GrailsDomainBinder = getClass().classLoader.loadClass('org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsDomainBinder')
  if (GrailsDomainBinder.newInstance().getMapping(domainClass)?.identity?.generator == 'assigned') {
    persistentPropNames << domainClass.identifier.name
  }
}
props = domainClass.properties.findAll { persistentPropNames.contains(it.name) && !excludedProps.contains(it.name) && (domainClass.constrainedProperties[it.name] ? domainClass.constrainedProperties[it.name].display : true) }
Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
%>
<%
  for (p in props) {
    if (p.embedded) {
      def embeddedPropNames = p.component.persistentProperties*.name
      def embeddedProps = p.component.properties.findAll { embeddedPropNames.contains(it.name) && !excludedProps.contains(it.name) }
      Collections.sort(embeddedProps, comparator.constructors[0].newInstance([p.component] as Object[])) %>

<% for (ep in p.component.properties) {
%>
<% renderFieldForProperty(ep, p.component, "${p.name}.")
} %>
<% } else {
  renderFieldForProperty(p, domainClass)

}
}

private renderFieldForProperty(p, owningClass, prefix = "") {
  DefaultGrailsTemplateGenerator defaultGrailsTemplateGenerator = new DefaultGrailsTemplateGenerator()
  boolean hasHibernate = pluginManager?.hasGrailsPlugin('hibernate') || pluginManager?.hasGrailsPlugin('hibernate4')
  boolean required = false
  if (hasHibernate) {
    cp = owningClass.constrainedProperties[p.name]
    required = (cp ? !(cp.propertyType in [boolean, Boolean]) && !cp.nullable : false)
  }

%>
<g:if test="\${${defaultGrailsTemplateGenerator.getPropertyName(domainClass)}Instance}">
  <div class="form-group \${hasErrors(bean: ${propertyName}, field: '${prefix}${p.name}', 'error')} ${
      required ? 'required': ''}">
    <label class="control-label" for="${prefix}${p.name}">
      <g:message code="${domainClass.propertyName}.${prefix}${p.name}.label" default="${p.naturalName}"/>
      <% if (required) { %><span class="text-red">*</span><% } %>
    </label>
    <br>
    <% if (p.type == Date || p.type == java.sql.Date || p.type == java.sql.Time || p.type == Calendar) {
    %>
    <div class='input-group' id='datetimepicker-${p.name}'>
      <% } %>
      ${renderEditor(p)}
      <% if (p.type == Date || p.type == java.sql.Date || p.type == java.sql.Time || p.type == Calendar) {
      %>  <span class="input-group-addon"><spsan class="glyphicon glyphicon-calendar"></spsan></span>
    </div>
    <script>
      <%
     print(" \$(\"#datetimepicker-"+p.name+"\").datetimepicker({\n" +
"        format: 'YYYY-MM-DD',\n" +
"        date: new Date(Number.parseInt(\$(\"#date-picker-field-"+p.name+"\").val()))\n" +
"      });")
        %>
    </script>
    <% } %>
    <div class="help-block"></div>
  </div>
</g:if><g:else>
  <div class="col-md-3">
    <div class="form-group">
      <label class="control-label" for="${prefix}${p.name}">
        <g:message code="${domainClass.propertyName}.${prefix}${p.name}.label" default="${p.naturalName}"/>
        <% if (required) { %><span class="text-red">*</span><% } %>
      </label>
      <br>
      <input class="form-control" name="${p.name}" value="\${params.${p.name}}">
      <div class="help-block"></div>
    </div>
  </div>
</g:else>
<%
  } %>
