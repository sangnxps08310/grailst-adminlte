<% import grails.persistence.Event %>
<%=packageName%>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}"/>
  <title><g:message code="${domainClass.propertyName}.label"
                    args="\${[message( code: '${domainClass.propertyName}.label', default:'${domainClass.propertyName}')]}">
  </g:message></title>

</head>

<body>
<div class="${domainClass.propertyName}">

  <g:if test="\${flash.message}">
    <div class="message" role="status">\${flash.message}</div>
  </g:if>
  <div id="list-${domainClass.propertyName}" class="box box-primary" role="main">
    <div class="box-header">
      <h4 class="box-title collapsed" data-toggle="collapse" data-target="#search-box" aria-expanded="true"><i
        class="fa fa-search"></i><g:message code="default.search.label" args=" "/></h4>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool btn-search-toggle collapsed" data-toggle="collapse"
                data-target="#search-box" aria-expanded="true">
          <i class="fa fa-plus"></i>
        </button>
      </div>
      <div class="help-block"></div>

      <div class="search-${domainClass.propertyName}" id="search-box">
          <g:form class="form rows"
                  url="[resource: ${propertyName}, action: 'index']" <%=multiPart ? ' enctype="multipart/form-data"' : '' %>>
          <g:render template="form"/>
          <div class="col-md-12"></div>
          <div class="buttons">
            <br>
            <g:submitButton name="search" class="btn btn-success"
                            value="\${message(code: 'default.button.search.label', default: 'search')}"/>
          </div>
          </g:form>
        </div>
    </div>
    <div class="box-body">
      <div class="clearfix">
      </div>
      <g:link action="create" class="btn btn-success pull-right"><i class="fa fa-plus"></i></g:link>
      <table class="table table-hover" style="border-top: 1px solid #f4f4f4; border-bottom: 1px solid #f4f4f4">
        <thead>
        <tr>
          <th>#</th>
          <% excludedProps = Event.allEvents.toList() << 'id' << 'version'
          allowedNames = domainClass.persistentProperties*.name << 'dateCreated' << 'lastUpdated'
          props = domainClass.properties.findAll { allowedNames.contains(it.name) && !excludedProps.contains(it.name) && it.type != null && !Collection.isAssignableFrom(it.type) && (domainClass.constrainedProperties[it.name] ? domainClass.constrainedProperties[it.name].display : true) }
          Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
          props.eachWithIndex { p, i ->
            if (i < 6) {
              if (p.isAssociation()) { %>
          <th><g:message code="${domainClass.propertyName}.${p.name}.label" default="${p.naturalName}"/></th>
          <% } else { %>
          <g:sortableColumn property="${p.name}"
                            title="\${message(code: '${domainClass.propertyName}.${p.name}.label', default: '${
                                p.naturalName}')}"/>
          <% }
          }
          } %>
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="\${${propertyName}List}" status="i" var="${propertyName}">
          <tr class="\${(i % 2) == 0 ? 'even' : 'odd'}" scope="col">
            <% props.eachWithIndex { p, i ->
              if (i == 0) { %>
            <td>${i + 1}</td>
            <td><g:link action="show" id="\${${propertyName}.id}">\${fieldValue(bean: ${propertyName}, field: "${
                p.name}")}</g:link></td>
            <% } else if (i < 6) {
              if (p.type == Boolean || p.type == boolean) { %>
            <td><g:formatBoolean boolean="\${${propertyName}.${p.name}}"/></td>
            <%
              } else if (p.type == Date || p.type == java.sql.Date || p.type == java.sql.Time || p.type == Calendar) { %>
            <td><g:formatDate date="\${${propertyName}.${p.name}}"/></td>
            <% } else { %>
            <td>\${fieldValue(bean: ${propertyName}, field: "${p.name}")}</td>
            <% }
            }
            } %>
            <td><g:link action="edit" resource="\${${propertyName}}"><i
              class="glyphicon glyphicon-pencil"></i></g:link><g:link class="delete" action="delete"
                                                                      resource="\${${propertyName}}"><i
                class="glyphicon glyphicon-remove"></i></g:link></td>
          </tr>
        </g:each>
        </tbody>
      </table>
    </div>
    <div class="box-footer">
      <div class="pagination sang-pagination">
        <g:paginate total="\${${propertyName}Count ?: 0}"/>
      </div>
    </div>
  </div>
</div>
</body>
</html>
