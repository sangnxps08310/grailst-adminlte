<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}"/>
  <title><g:message code="default.create.label"
                    args="\${[message( code: '${domainClass.propertyName}.label', default:'${domainClass.propertyName}')]}">
  </g:message></title>

</head>

<body>
%{--<a href="#create-${domainClass.propertyName}" class="skip" tabindex="-1"><g:message code="default.link.skip.label"--}%
%{--                                                                                    default="Skip to content&hellip;"/></a>--}%

%{--<div class="nav" role="navigation">--}%
%{--  <ul>--}%
%{--    <li><a class="home" href="\${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
%{--    <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>--}%
%{--  </ul>--}%
%{--</div>--}%
<div class="${domainClass.propertyName}">
  <div id="create-${domainClass.propertyName}" class="content scaffold-create" role="main">
%{--    <h1><g:message code="default.create.label" args="[entityName]"/></h1>--}%
    <g:if test="\${flash.message}">
      <div class="message alert alert-success" role="status">\${flash.message}</div>
    </g:if>
    <g:hasErrors bean="\${${propertyName}}">
      <div class="alert alert-warning">
        <ul class="errors" role="alert">
          <g:eachError bean="\${${propertyName}}" var="error">
            <li<g:if
            test="\${error in org.springframework.validation.FieldError}">data-field-id="\${error.field}"</g:if>><g:message
            error="\${error}"/></li>
          </g:eachError>
        </ul>
      </div>
    </g:hasErrors>
    <div class="box box-primary">
      <div class="box-body">

        <g:form class="form rows"
            url="[resource: ${propertyName}, action: 'save']" <%=multiPart ? ' enctype="multipart/form-data"' : '' %>>
    <g:render template="form"/>
    <div class="col-md-12"></div>

    <div class="buttons">
      <g:submitButton name="create" class="btn btn-success"
                      value="\${message(code: 'default.button.create.label', default: 'Create')}"/>
    </div>
    </g:form>
      </div>

    </div>
</body>
</html>
