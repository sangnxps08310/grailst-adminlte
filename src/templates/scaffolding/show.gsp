<% import grails.persistence.Event %>
<%=packageName%>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="adminlte">
  <g:set var="entityName" value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}"/>
  <title><g:message code="default.show.label"
                    args="\${[message( code: '${domainClass.propertyName}.label', default:'${domainClass.propertyName}')]}">
</g:message></title>
</head>

<body>
%{--		<a href="#show-${domainClass.propertyName}" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>--}%
%{--		<div class="nav" role="navigation">--}%
%{--			<ul>--}%
%{--				<li><a class="home" href="\${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
%{--				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>--}%
%{--				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>--}%
%{--			</ul>--}%
%{--		</div>--}%
<div class="${domainClass.propertyName}">
  <div id="show-${domainClass.propertyName}" class="content scaffold-show" role="main">
%{--    <h3><g:message code="default.show.label" args="[entityName]"/></h3>--}%
    <g:if test="\${flash.message}">
      <div class="message alert alert-success" role="status">\${flash.message}</div>
    </g:if>
    <div class="box box-primary">
      <div class="box-body">
        <h4><i class="fa fa-book"></i>Details: </h4>
        <table class="${domainClass.propertyName} table no-margin"s>
          <% excludedProps = Event.allEvents.toList() << 'id' << 'version'
          allowedNames = domainClass.persistentProperties*.name << 'dateCreated' << 'lastUpdated'
          props = domainClass.properties.findAll { allowedNames.contains(it.name) && !excludedProps.contains(it.name) && (domainClass.constrainedProperties[it.name] ? domainClass.constrainedProperties[it.name].display : true) }
          Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
          props.each { p -> %>
          %{--          <g:if test="\${${propertyName}?.${p.name}}">--}%
          <tr class="fieldcontain">
            <th id="${p.name}-label" class="property-label"><g:message
              code="${domainClass.propertyName}.${p.name}.label" default="${p.naturalName}"/></th>
            <% if (p.isEnum()) { %>
            <td class="property-value" aria-labelledby="${p.name}-label"><g:fieldValue bean="\${${propertyName}}"
                                                                                       field="${p.name}"/></td>
            <% } else if (p.oneToMany || p.manyToMany) { %>
            %{--              <select id="select2s">--}%
            %{--                <g:each in="\${${propertyName}.${p.name}}" var="${p.name[0]}">--}%
            %{--                  <option class="property-value" aria-labelledby="${p.name}-label"><g:link--}%
            %{--                    controller="${p.referencedDomainClass?.propertyName}" action="show"--}%
            %{--                    id="\${${p.name[0]}.id}">\${${p.name[0]}?.encodeAsHTML()}</g:link></option>--}%
            %{--                </g:each>--}%
            %{--              </select>--}%
            <% } else if (p.manyToOne || p.oneToOne) { %>
            <td class="property-value" aria-labelledby="${p.name}-label"><g:link
              controller="${p.referencedDomainClass?.propertyName}" action="show"
              id="\${${propertyName}?.${p.name}?.id}">\${${propertyName}?.${p.name}?.encodeAsHTML()}</g:link></td>
            <% } else if (p.type == Boolean || p.type == boolean) { %>
            <td class="property-value" aria-labelledby="${p.name}-label"><g:formatBoolean
              boolean="\${${propertyName}?.${p.name}}"/></td>
            <%
              } else if (p.type == Date || p.type == java.sql.Date || p.type == java.sql.Time || p.type == Calendar) { %>
            <td class="property-value" aria-labelledby="${p.name}-label"><g:formatDate
              date="\${${propertyName}?.${p.name}}"/></td>
            <% } else if (!p.type.isArray()) { %>
            <td class="property-value" aria-labelledby="${p.name}-label"><g:fieldValue bean="\${${propertyName}}"
                                                                                       field="${p.name}"/></td>
            <% } %>
          </tr>
          %{--          </g:if>--}%
          <% } %>
        </table>
        <g:form url="[resource: ${propertyName}, action: 'delete']" method="DELETE">
          <fieldset class="buttons">
            <g:link class="edit btn btn-info" action="edit" resource="\${${propertyName}}"><g:message
              code="default.button.edit.label"
              default="Edit"/></g:link>
            <g:actionSubmit class="delete btn btn-danger" action="delete"
                            value="\${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('\${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
          </fieldset>
        </g:form>
      </div>
    </div>
  </div>
</div>
</body>
</html>
