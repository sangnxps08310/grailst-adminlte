package Utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
  public static String DATE_PATTERN_DD_MM_YYYY= "dd-MM-yyyy";
  public static String DATE_PATTERN_DD_MM_YYYY_HH_MM_SS= "yyyy-MM-dd hh:mm:ss";

  public static String getDateConverted(String pattern, Date date){
    SimpleDateFormat format = new SimpleDateFormat(pattern);
    return format.format(date);
  }
}
