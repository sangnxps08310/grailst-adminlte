package GUtils

import org.codehaus.groovy.grails.commons.GrailsClass

import java.nio.channels.FileChannel


class FileUtils {
  def getListUrlInstance(GrailsClass[] controllers) {
    def route = [];
    def defaultRoute = ['login', 'logout', 'menu', 'dbdoc']
    route << ['name': ' ', 'id': ' ']
    controllers.each { controller ->
      if (!defaultRoute.contains(controller.logicalPropertyName)) {
        route << ['name': controller.logicalPropertyName, 'id': controller.logicalPropertyName]
        File files = new File('./grails-app/views/' + controller.logicalPropertyName);
        if (files.exists()) {
          if (files.listFiles().length > 0) {
            for (int i = 0; i < files.listFiles().length; i++) {
              String name = files.listFiles()[i].name;
              if (name.contains('_')) {
                continue;
              }
              String fileName = name.substring(0, name.lastIndexOf('.'))
              route << ['name': controller.logicalPropertyName + "/" + fileName, 'id': controller.logicalPropertyName + "/" + fileName]
            }
          }
        }
      }
    }
    return route;
  }

  def transferToByFileChannel(File source, File dest) {
    FileChannel sourceChannel = null;
    FileChannel destChannel = null;
    try {
      sourceChannel = new FileInputStream(source).getChannel();
      destChannel = new FileOutputStream(dest).getChannel();
      destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
    } finally {
      sourceChannel.close();
      destChannel.close();
    }
  }
}
