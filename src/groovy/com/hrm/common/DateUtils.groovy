package com.hrm.common

import java.text.SimpleDateFormat

class DateUtils {
  def addDays(Date date, int second = 0, int minute = 0, int hour = 0, int day = 0, int month = 0, int year = 0) {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
    Calendar cal = Calendar.getInstance();
    cal.setTime(date)
    cal.add(Calendar.SECOND, second);
    cal.add(Calendar.MINUTE, minute);
    cal.add(Calendar.HOUR_OF_DAY, hour);
    cal.add(Calendar.DAY_OF_MONTH, day);
    cal.add(Calendar.MONTH, month);
    cal.add(Calendar.YEAR, year);
    String newDate = sdf.format(cal.getTime());
    System.out.println("Date after Addition: " + newDate);
  }

  public static void main(String[] args) {
    addDays(new Date(),1,1,1);
  }
}
