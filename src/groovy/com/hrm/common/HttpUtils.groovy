package com.hrm.common

class HttpUtils {
  public final static String HTTP_OK = '200'
  public final static String HTTP_NOT_FOUND = '404'
  public final static String HTTP_INTERNAL_ERROR = '501'
}
